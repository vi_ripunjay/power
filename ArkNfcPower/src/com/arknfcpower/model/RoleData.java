package com.arknfcpower.model;

public class RoleData {

	
	int id;
	String iRoleId;
	String strRole;
    String dtCreated;    
    String dtUpdated;   
    String flgIsDirty;    
    String strType;
    String flgDeleted;    
    String txtDescription;
    String iRankPriority;
    String iTenantId;
    String strRoleType;
    
    
    public String getStrRoleType() {
		return strRoleType;
	}

	public void setStrRoleType(String strRoleType) {
		this.strRoleType = strRoleType;
	}

	public RoleData(int id,String iRoleId,String strRole,String dtCreated, String dtUpdated,String flgIsDirty,    
    String strType,String flgDeleted,String txtDescription,String iRankPriority,String iTenantId,  String strRoleType)
    {
    	this.id = id;
    	this.iRoleId = iRoleId;
    	this.strRole = strRole;
        this.dtCreated = dtCreated;    
        this.dtUpdated = dtUpdated;   
        this.flgIsDirty = flgIsDirty;    
        this.strType = strType;
        this.flgDeleted = flgDeleted;    
        this.txtDescription = txtDescription;
        this.iRankPriority = iRankPriority;
        this.iTenantId = iTenantId;
        this.strRoleType = strRoleType;
    	
    }

    public RoleData(String iRoleId,String strRole,String dtCreated, String dtUpdated,String flgIsDirty,    
    	    String strType,String flgDeleted,String txtDescription,String iRankPriority,String iTenantId,String strRoleType)
    {
    	this.iRoleId = iRoleId;
    	this.strRole = strRole;
        this.dtCreated = dtCreated;    
        this.dtUpdated = dtUpdated;   
        this.flgIsDirty = flgIsDirty;    
        this.strType = strType;
        this.flgDeleted = flgDeleted;    
        this.txtDescription = txtDescription;
        this.iRankPriority = iRankPriority;
        this.iTenantId = iTenantId;
        this.strRoleType = strRoleType;

        
    }
    public RoleData()
    {
    	
    }

    
    
    public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getiRoleId() {
		return iRoleId;
	}
	public void setiRoleId(String iRoleId) {
		this.iRoleId = iRoleId;
	}
	public String getStrRole() {
		return strRole;
	}
	public void setStrRole(String strRole) {
		this.strRole = strRole;
	}
	public String getDtCreated() {
		return dtCreated;
	}
	public void setDtCreated(String dtCreated) {
		this.dtCreated = dtCreated;
	}
	public String getDtUpdated() {
		return dtUpdated;
	}
	public void setDtUpdated(String dtUpdated) {
		this.dtUpdated = dtUpdated;
	}
	public String getFlgIsDirty() {
		return flgIsDirty;
	}
	public void setFlgIsDirty(String flgIsDirty) {
		this.flgIsDirty = flgIsDirty;
	}
	public String getStrType() {
		return strType;
	}
	public void setStrType(String strType) {
		this.strType = strType;
	}
	public String getFlgDeleted() {
		return flgDeleted;
	}
	public void setFlgDeleted(String flgDeleted) {
		this.flgDeleted = flgDeleted;
	}
	public String getTxtDescription() {
		return txtDescription;
	}
	public void setTxtDescription(String txtDescription) {
		this.txtDescription = txtDescription;
	}
	public String getiRankPriority() {
		return iRankPriority;
	}
	public void setiRankPriority(String iRankPriority) {
		this.iRankPriority = iRankPriority;
	}
	public String getiTenantId() {
		return iTenantId;
	}
	public void setiTenantId(String iTenantId) {
		this.iTenantId = iTenantId;
	}      


}
