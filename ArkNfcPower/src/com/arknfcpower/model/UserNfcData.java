package com.arknfcpower.model;

public class UserNfcData {
	
      int id;
      /**
       * nfcCardId use as primary key as cardId_timestamp
       */
      String nfcCardId;
      String cardType;
      String iUserId;     
      /**
       * iUserServiceTermId use actual as cardId
       */
      String iUserServiceTermId;     
	  String flgDirty;
      String flgDeleted;     
      String dtCreated;    
      String dtUpdated;      
      String iTenantId;	    
      String iShipId;
      
 public UserNfcData (){}     
      
	public UserNfcData(String nfcCardId, String cardType, String iUserId,
			String iUserServiceTermId, String flgDirty, String flgDeleted,
			String dtCreated, String dtUpdated, String iTenantId,
			String iShipId) {
		this.nfcCardId = nfcCardId;
		this.cardType = cardType;
		this.iUserId = iUserId;
		this.iUserServiceTermId = iUserServiceTermId;
		this.flgDirty = flgDirty;
		this.flgDeleted = flgDeleted;
		this.dtCreated = dtCreated;
		this.dtUpdated = dtUpdated;
		this.iTenantId = iTenantId;
		this.iShipId = iShipId;
	}
	public UserNfcData(int id, String nfcCardId, String cardType,
			String iUserId, String iUserServiceTermId, String flgDirty,
			String flgDeleted, String dtCreated, String dtUpdated,
			String iTenantId, String iShipId) {
		this.id = id;
		this.nfcCardId = nfcCardId;
		this.cardType = cardType;
		this.iUserId = iUserId;
		this.iUserServiceTermId = iUserServiceTermId;
		this.flgDirty = flgDirty;
		this.flgDeleted = flgDeleted;
		this.dtCreated = dtCreated;
		this.dtUpdated = dtUpdated;
		this.iTenantId = iTenantId;
		this.iShipId = iShipId;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getNfcCardId() {
		return nfcCardId;
	}
	public void setNfcCardId(String nfcCardId) {
		this.nfcCardId = nfcCardId;
	}
	public String getCardType() {
		return cardType;
	}
	public void setCardType(String cardType) {
		this.cardType = cardType;
	}
	public String getiUserId() {
		return iUserId;
	}
	public void setiUserId(String iUserId) {
		this.iUserId = iUserId;
	}
	public String getiUserServiceTermId() {
		return iUserServiceTermId;
	}
	public void setiUserServiceTermId(String iUserServiceTermId) {
		this.iUserServiceTermId = iUserServiceTermId;
	}
	public String getFlgDirty() {
		return flgDirty;
	}
	public void setFlgDirty(String flgDirty) {
		this.flgDirty = flgDirty;
	}
	public String getFlgDeleted() {
		return flgDeleted;
	}
	public void setFlgDeleted(String flgDeleted) {
		this.flgDeleted = flgDeleted;
	}
	public String getDtCreated() {
		return dtCreated;
	}
	public void setDtCreated(String dtCreated) {
		this.dtCreated = dtCreated;
	}
	public String getDtUpdated() {
		return dtUpdated;
	}
	public void setDtUpdated(String dtUpdated) {
		this.dtUpdated = dtUpdated;
	}
	public String getiTenantId() {
		return iTenantId;
	}
	public void setiTenantId(String iTenantId) {
		this.iTenantId = iTenantId;
	}
	public String getiShipId() {
		return iShipId;
	}
	public void setiShipId(String iShipId) {
		this.iShipId = iShipId;
	}            

		
	
	
}
