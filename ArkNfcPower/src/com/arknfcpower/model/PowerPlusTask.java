package com.arknfcpower.model;

public class PowerPlusTask {

	
	int id;
	String iPowerPlusTaskId;
	String approvedBy;
	String dtCreated;
	String dtUpdated;    //PICK FROM SHIP MASTER
	String flgDeleted;     //PICK FROM SHIP MASTER
	String flgIsAllReceive;
	String flgIsDirty;
	String flgStatus;    //PICK FROM SHIP MASTER
	String iSequence;
	String powerPoints;
	String providedBy;
	String receivedBy;
	String strAdditionalOne;
	String strAdditionalTwo;
	String strDescription;
	String strTitle;       //PICK FROM TENENT
	String iPowerPlusCategoryId;
	String iPowerPlusPeriodicityId;
	String iTenantId;
	
	
	public PowerPlusTask(int id, String iPowerPlusTaskId, String approvedBy,
			String dtCreated, String dtUpdated, String flgDeleted,
			String flgIsAllReceive, String flgIsDirty, String flgStatus,
			String iSequence, String powerPoints, String providedBy,
			String receivedBy, String strAdditionalOne,
			String strAdditionalTwo, String strDescription, String strTitle,
			String iPowerPlusCategoryId, String iPowerPlusPeriodicityId,
			String iTenantId) {
		super();
		this.id = id;
		this.iPowerPlusTaskId = iPowerPlusTaskId;
		this.approvedBy = approvedBy;
		this.dtCreated = dtCreated;
		this.dtUpdated = dtUpdated;
		this.flgDeleted = flgDeleted;
		this.flgIsAllReceive = flgIsAllReceive;
		this.flgIsDirty = flgIsDirty;
		this.flgStatus = flgStatus;
		this.iSequence = iSequence;
		this.powerPoints = powerPoints;
		this.providedBy = providedBy;
		this.receivedBy = receivedBy;
		this.strAdditionalOne = strAdditionalOne;
		this.strAdditionalTwo = strAdditionalTwo;
		this.strDescription = strDescription;
		this.strTitle = strTitle;
		this.iPowerPlusCategoryId = iPowerPlusCategoryId;
		this.iPowerPlusPeriodicityId = iPowerPlusPeriodicityId;
		this.iTenantId = iTenantId;
	}
	
	public PowerPlusTask(String iPowerPlusTaskId, String approvedBy,
			String dtCreated, String dtUpdated, String flgDeleted,
			String flgIsAllReceive, String flgIsDirty, String flgStatus,
			String iSequence, String powerPoints, String providedBy,
			String receivedBy, String strAdditionalOne,
			String strAdditionalTwo, String strDescription, String strTitle,
			String iPowerPlusCategoryId, String iPowerPlusPeriodicityId,
			String iTenantId) {
		this.iPowerPlusTaskId = iPowerPlusTaskId;
		this.approvedBy = approvedBy;
		this.dtCreated = dtCreated;
		this.dtUpdated = dtUpdated;
		this.flgDeleted = flgDeleted;
		this.flgIsAllReceive = flgIsAllReceive;
		this.flgIsDirty = flgIsDirty;
		this.flgStatus = flgStatus;
		this.iSequence = iSequence;
		this.powerPoints = powerPoints;
		this.providedBy = providedBy;
		this.receivedBy = receivedBy;
		this.strAdditionalOne = strAdditionalOne;
		this.strAdditionalTwo = strAdditionalTwo;
		this.strDescription = strDescription;
		this.strTitle = strTitle;
		this.iPowerPlusCategoryId = iPowerPlusCategoryId;
		this.iPowerPlusPeriodicityId = iPowerPlusPeriodicityId;
		this.iTenantId = iTenantId;
	}

	public PowerPlusTask() {}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getiPowerPlusTaskId() {
		return iPowerPlusTaskId;
	}
	public void setiPowerPlusTaskId(String iPowerPlusTaskId) {
		this.iPowerPlusTaskId = iPowerPlusTaskId;
	}
	public String getApprovedBy() {
		return approvedBy;
	}
	public void setApprovedBy(String approvedBy) {
		this.approvedBy = approvedBy;
	}
	public String getDtCreated() {
		return dtCreated;
	}
	public void setDtCreated(String dtCreated) {
		this.dtCreated = dtCreated;
	}
	public String getDtUpdated() {
		return dtUpdated;
	}
	public void setDtUpdated(String dtUpdated) {
		this.dtUpdated = dtUpdated;
	}
	public String getFlgDeleted() {
		return flgDeleted;
	}
	public void setFlgDeleted(String flgDeleted) {
		this.flgDeleted = flgDeleted;
	}
	public String getFlgIsAllReceive() {
		return flgIsAllReceive;
	}
	public void setFlgIsAllReceive(String flgIsAllReceive) {
		this.flgIsAllReceive = flgIsAllReceive;
	}
	public String getFlgIsDirty() {
		return flgIsDirty;
	}
	public void setFlgIsDirty(String flgIsDirty) {
		this.flgIsDirty = flgIsDirty;
	}
	public String getFlgStatus() {
		return flgStatus;
	}
	public void setFlgStatus(String flgStatus) {
		this.flgStatus = flgStatus;
	}
	public String getiSequence() {
		return iSequence;
	}
	public void setiSequence(String iSequence) {
		this.iSequence = iSequence;
	}
	public String getPowerPoints() {
		return powerPoints;
	}
	public void setPowerPoints(String powerPoints) {
		this.powerPoints = powerPoints;
	}
	public String getProvidedBy() {
		return providedBy;
	}
	public void setProvidedBy(String providedBy) {
		this.providedBy = providedBy;
	}
	public String getReceivedBy() {
		return receivedBy;
	}
	public void setReceivedBy(String receivedBy) {
		this.receivedBy = receivedBy;
	}
	public String getStrAdditionalOne() {
		return strAdditionalOne;
	}
	public void setStrAdditionalOne(String strAdditionalOne) {
		this.strAdditionalOne = strAdditionalOne;
	}
	public String getStrAdditionalTwo() {
		return strAdditionalTwo;
	}
	public void setStrAdditionalTwo(String strAdditionalTwo) {
		this.strAdditionalTwo = strAdditionalTwo;
	}
	public String getStrDescription() {
		return strDescription;
	}
	public void setStrDescription(String strDescription) {
		this.strDescription = strDescription;
	}
	public String getStrTitle() {
		return strTitle;
	}
	public void setStrTitle(String strTitle) {
		this.strTitle = strTitle;
	}
	public String getiPowerPlusCategoryId() {
		return iPowerPlusCategoryId;
	}
	public void setiPowerPlusCategoryId(String iPowerPlusCategoryId) {
		this.iPowerPlusCategoryId = iPowerPlusCategoryId;
	}
	public String getiPowerPlusPeriodicityId() {
		return iPowerPlusPeriodicityId;
	}
	public void setiPowerPlusPeriodicityId(String iPowerPlusPeriodicityId) {
		this.iPowerPlusPeriodicityId = iPowerPlusPeriodicityId;
	}
	public String getiTenantId() {
		return iTenantId;
	}
	public void setiTenantId(String iTenantId) {
		this.iTenantId = iTenantId;
	}

}
