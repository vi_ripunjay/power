package com.arknfcpower.model;

public class CardTypeData {
    int id;
    
	String strCardId;
	String strCardType;
	
	
	public CardTypeData() {
}
	
	public CardTypeData(String strCardId, String strCardType) {
		this.strCardId = strCardId;
		this.strCardType = strCardType;
	}
	public CardTypeData(int id, String strCardId, String strCardType) {
		this.id = id;
		this.strCardId = strCardId;
		this.strCardType = strCardType;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getStrCardId() {
		return strCardId;
	}
	public void setStrCardId(String strCardId) {
		this.strCardId = strCardId;
	}
	public String getStrCardType() {
		return strCardType;
	}
	public void setStrCardType(String strCardType) {
		this.strCardType = strCardType;
	}
		

}
