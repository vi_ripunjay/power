package com.arknfcpower.model;

public class TrainingTransaction {

	int id;
	private String traningTransactionId;
	private String traningDataId;
	private String traningDate;
	private String trainingTime;
	private String strDescription;
	private String flgDeleted;
	private String iShipId;
	private String iTenantId;
	private String dtCreated;
	private String dtUpdated;
	private String flgStatus;
	private String flgIsDirty;
	private String strParentId;

	public TrainingTransaction(String traningTransactionId,
			String traningDataId, String traningDate, String trainingTime,
			String strDescription, String flgDeleted, String iShipId,
			String iTenantId, String dtCreated, String dtUpdated,
			String flgStatus, String flgIsDirty, String strParentId) {
		super();
		this.traningTransactionId = traningTransactionId;
		this.traningDataId = traningDataId;
		this.traningDate = traningDate;
		this.trainingTime = trainingTime;
		this.strDescription = strDescription;
		this.flgDeleted = flgDeleted;
		this.iShipId = iShipId;
		this.iTenantId = iTenantId;
		this.dtCreated = dtCreated;
		this.dtUpdated = dtUpdated;
		this.flgStatus = flgStatus;
		this.flgIsDirty = flgIsDirty;
		this.strParentId = strParentId;
	}

	public TrainingTransaction() {
		// TODO Auto-generated constructor stub
	}

	public String getTraningDataId() {
		return traningDataId;
	}

	public void setTraningDataId(String traningDataId) {
		this.traningDataId = traningDataId;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTraningTransactionId() {
		return traningTransactionId;
	}

	public void setTraningTransactionId(String traningTransactionId) {
		this.traningTransactionId = traningTransactionId;
	}

	public String getTraningDate() {
		return traningDate;
	}

	public void setTraningDate(String traningDate) {
		this.traningDate = traningDate;
	}

	public String getTrainingTime() {
		return trainingTime;
	}

	public void setTrainingTime(String trainingTime) {
		this.trainingTime = trainingTime;
	}

	public String getStrDescription() {
		return strDescription;
	}

	public void setStrDescription(String strDescription) {
		this.strDescription = strDescription;
	}

	public String getFlgDeleted() {
		return flgDeleted;
	}

	public void setFlgDeleted(String flgDeleted) {
		this.flgDeleted = flgDeleted;
	}

	public String getiShipId() {
		return iShipId;
	}

	public void setiShipId(String iShipId) {
		this.iShipId = iShipId;
	}

	public String getiTenantId() {
		return iTenantId;
	}

	public void setiTenantId(String iTenantId) {
		this.iTenantId = iTenantId;
	}

	public String getDtCreated() {
		return dtCreated;
	}

	public void setDtCreated(String dtCreated) {
		this.dtCreated = dtCreated;
	}

	public String getDtUpdated() {
		return dtUpdated;
	}

	public void setDtUpdated(String dtUpdated) {
		this.dtUpdated = dtUpdated;
	}

	public String getFlgStatus() {
		return flgStatus;
	}

	public void setFlgStatus(String flgStatus) {
		this.flgStatus = flgStatus;
	}

	public String getFlgIsDirty() {
		return flgIsDirty;
	}

	public void setFlgIsDirty(String flgIsDirty) {
		this.flgIsDirty = flgIsDirty;
	}

	public String getStrParentId() {
		return strParentId;
	}

	public void setStrParentId(String strParentId) {
		this.strParentId = strParentId;
	}

}
