package com.arknfcpower.model;

public class TrainingDataItem {

    private String text;
    private String trainingDataId;
    public void setText(String text) {
		this.text = text;
	}
	public void setTrainingDataId(String trainingDataId) {
		this.trainingDataId = trainingDataId;
	}
	public void setiTenantId(String iTenantId) {
		this.iTenantId = iTenantId;
	}


	private String iTenantId;
    
    public String getiTenantId() {
		return iTenantId;
	}
	public TrainingDataItem(String text, String item,String iTenantId) {
            this.text = text;
            this.trainingDataId = item;
            this.iTenantId = iTenantId;
            
    }
    public String getText() {
        return text;
    }
    public String getTrainingDataId() {
        return trainingDataId;
    }
    
    
    @Override
    public String toString() {
        return getText();
    }

}
