package com.arknfcpower.model;



public class PowerPlusCategory {

	int id;
	
	String iPowerPlusCategoryId;
	String dtCreated;
	String dtUpdated;    
	String flgDeleted;   
	String flgIsDirty;
	String flgStatus;    
	String iSequence;
	String strDescription;
	String strTitle;      
	String iTenantId;
	String strCategoryCode;
	
	
	
	public PowerPlusCategory(int id, String iPowerPlusCategoryId,
			String dtCreated, String dtUpdated, String flgDeleted,
			String flgIsDirty, String flgStatus, String iSequence,
			String strDescription, String strTitle, String iTenantId) {
		this.id = id;
		this.iPowerPlusCategoryId = iPowerPlusCategoryId;
		this.dtCreated = dtCreated;
		this.dtUpdated = dtUpdated;
		this.flgDeleted = flgDeleted;
		this.flgIsDirty = flgIsDirty;
		this.flgStatus = flgStatus;
		this.iSequence = iSequence;
		this.strDescription = strDescription;
		this.strTitle = strTitle;
		this.iTenantId = iTenantId;
	}

	public PowerPlusCategory(String iPowerPlusCategoryId, String dtCreated,
			String dtUpdated, String flgDeleted, String flgIsDirty,
			String flgStatus, String iSequence, String strDescription,
			String strTitle, String iTenantId, String strCategoryCode) {
		super();
		this.iPowerPlusCategoryId = iPowerPlusCategoryId;
		this.dtCreated = dtCreated;
		this.dtUpdated = dtUpdated;
		this.flgDeleted = flgDeleted;
		this.flgIsDirty = flgIsDirty;
		this.flgStatus = flgStatus;
		this.iSequence = iSequence;
		this.strDescription = strDescription;
		this.strTitle = strTitle;
		this.iTenantId = iTenantId;
		this.strCategoryCode = strCategoryCode;
	}



	public PowerPlusCategory(){}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getiPowerPlusCategoryId() {
		return iPowerPlusCategoryId;
	}
	public void setiPowerPlusCategoryId(String iPowerPlusCategoryId) {
		this.iPowerPlusCategoryId = iPowerPlusCategoryId;
	}
	public String getDtCreated() {
		return dtCreated;
	}
	public void setDtCreated(String dtCreated) {
		this.dtCreated = dtCreated;
	}
	public String getDtUpdated() {
		return dtUpdated;
	}
	public void setDtUpdated(String dtUpdated) {
		this.dtUpdated = dtUpdated;
	}
	public String getFlgDeleted() {
		return flgDeleted;
	}
	public void setFlgDeleted(String flgDeleted) {
		this.flgDeleted = flgDeleted;
	}
	public String getFlgIsDirty() {
		return flgIsDirty;
	}
	public void setFlgIsDirty(String flgIsDirty) {
		this.flgIsDirty = flgIsDirty;
	}
	public String getFlgStatus() {
		return flgStatus;
	}
	public void setFlgStatus(String flgStatus) {
		this.flgStatus = flgStatus;
	}
	public String getiSequence() {
		return iSequence;
	}
	public void setiSequence(String iSequence) {
		this.iSequence = iSequence;
	}
	public String getStrDescription() {
		return strDescription;
	}
	public void setStrDescription(String strDescription) {
		this.strDescription = strDescription;
	}
	public String getStrTitle() {
		return strTitle;
	}
	public void setStrTitle(String strTitle) {
		this.strTitle = strTitle;
	}
	public String getiTenantId() {
		return iTenantId;
	}
	public void setiTenantId(String iTenantId) {
		this.iTenantId = iTenantId;
	}

	public String getStrCategoryCode() {
		return strCategoryCode;
	}

	public void setStrCategoryCode(String strCategoryCode) {
		this.strCategoryCode = strCategoryCode;
	}
	
}
