package com.arknfcpower.model;

public class TrainingData {

	int id;
	private String traningDataId;
	private String strParentId;
	private String strTitle;
	private String strDescription;
	private String flgDeleted;
	private String iShipId;
	private String iTenantId;      
	private String dtCreated;
	private String dtUpdated;
	private String flgStatus="0";
	private String flgIsDirty;
	private String iShipCategoryId;
	private int iSequence; 
	
	public TrainingData(String traningDataId, String strParentId,
			String strTitle, String strDescription, String flgDeleted,
			String iShipId, String iTenantId, String dtCreated,
			String dtUpdated, String flgStatus, String flgIsDirty,
			String iShipCategoryId, int iSequence) {
		super();
		this.traningDataId = traningDataId;
		this.strParentId = strParentId;
		this.strTitle = strTitle;
		this.strDescription = strDescription;
		this.flgDeleted = flgDeleted;
		this.iShipId = iShipId;
		this.iTenantId = iTenantId;
		this.dtCreated = dtCreated;
		this.dtUpdated = dtUpdated;
		this.flgStatus = flgStatus;
		this.flgIsDirty = flgIsDirty;
		this.iShipCategoryId = iShipCategoryId;
		this.iSequence = iSequence;
	}
	public TrainingData(int id, String traningDataId, String strParentId,
			String strTitle, String strDescription, String flgDeleted,
			String iShipId, String iTenantId, String dtCreated,
			String dtUpdated, String flgStatus, String flgIsDirty) {
		super();
		this.id = id;
		this.traningDataId = traningDataId;
		this.strParentId = strParentId;
		this.strTitle = strTitle;
		this.strDescription = strDescription;
		this.flgDeleted = flgDeleted;
		this.iShipId = iShipId;
		this.iTenantId = iTenantId;
		this.dtCreated = dtCreated;
		this.dtUpdated = dtUpdated;
		this.flgStatus = flgStatus;
		this.flgIsDirty = flgIsDirty;
	}
	public TrainingData() {
		// TODO Auto-generated constructor stub
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTraningDataId() {
		return traningDataId;
	}
	public void setTraningDataId(String traningDataId) {
		this.traningDataId = traningDataId;
	}
	public String getStrParentId() {
		return strParentId;
	}
	public void setStrParentId(String strParentId) {
		this.strParentId = strParentId;
	}
	public String getStrTitle() {
		return strTitle;
	}
	public void setStrTitle(String strTitle) {
		this.strTitle = strTitle;
	}
	public String getStrDescription() {
		return strDescription;
	}
	public void setStrDescription(String strDescription) {
		this.strDescription = strDescription;
	}
	public String getFlgDeleted() {
		return flgDeleted;
	}
	public void setFlgDeleted(String flgDeleted) {
		this.flgDeleted = flgDeleted;
	}
	public String getiShipId() {
		return iShipId;
	}
	public void setiShipId(String iShipId) {
		this.iShipId = iShipId;
	}
	public String getiTenantId() {
		return iTenantId;
	}
	public void setiTenantId(String iTenantId) {
		this.iTenantId = iTenantId;
	}
	public String getDtCreated() {
		return dtCreated;
	}
	public void setDtCreated(String dtCreated) {
		this.dtCreated = dtCreated;
	}
	public String getDtUpdated() {
		return dtUpdated;
	}
	public void setDtUpdated(String dtUpdated) {
		this.dtUpdated = dtUpdated;
	}
	public String getFlgStatus() {
		return flgStatus;
	}
	public void setFlgStatus(String flgStatus) {
		this.flgStatus = flgStatus;
	}
	public String getFlgIsDirty() {
		return flgIsDirty;
	}
	public void setFlgIsDirty(String flgIsDirty) {
		this.flgIsDirty = flgIsDirty;
	}
	public String getiShipCategoryId() {
		return iShipCategoryId;
	}
	public void setiShipCategoryId(String iShipCategoryId) {
		this.iShipCategoryId = iShipCategoryId;
	}
	public int getiSequence() {
		return iSequence;
	}
	public void setiSequence(int iSequence) {
		this.iSequence = iSequence;
	}

}
