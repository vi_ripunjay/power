package com.arknfcpower.model;

public class TraingAttendance {

	int id;
	private String traningAttendanceId;
	private String traningTransactionId;
	private String traningDataId;
	private String strParentId;
	private String traningDate;
	private String trainingTime;
	private String strDescription;
	private String flgDeleted;
	private String iShipId;
	private String iTenantId;      
	private String dtCreated;
	private String dtUpdated;
	private String flgStatus;    // 0 atten completed done 1 for panding atten
	private String flgIsDirty;
	private String iUserId;
	private String iUserServiceTermId;

	
	
	
	public String getTraningTransactionId() {
		return traningTransactionId;
	}
	public void setTraningTransactionId(String traningTransactionId) {
		this.traningTransactionId = traningTransactionId;
	}

	
	
	public String getStrDescription() {
		return strDescription;
	}
	public void setStrDescription(String strDescription) {
		this.strDescription = strDescription;
	}
	
	
	
	
	public TraingAttendance(String traningAttendanceId,String traningTransactionId, String traningDataId,
			String strParentId, String traningDate, String trainingTime,String strDescription,
			String flgDeleted, String iShipId, String iTenantId,
			String dtCreated, String dtUpdated, String flgStatus,
			String flgIsDirty, String iUserId, String iUserServiceTermId) {
		super();
		this.traningAttendanceId = traningAttendanceId;
		this.traningTransactionId = traningTransactionId;
		this.traningDataId = traningDataId;
		this.strParentId = strParentId;
		this.traningDate = traningDate;
		this.trainingTime = trainingTime;
		this.strDescription = strDescription;
		this.flgDeleted = flgDeleted;
		this.iShipId = iShipId;
		this.iTenantId = iTenantId;
		this.dtCreated = dtCreated;
		this.dtUpdated = dtUpdated;
		this.flgStatus = flgStatus;
		this.flgIsDirty = flgIsDirty;
		this.iUserId = iUserId;
		this.iUserServiceTermId = iUserServiceTermId;
	}
	public TraingAttendance(int id, String traningAttendanceId,String traningTransactionId,
			String traningDataId, String strParentId, String traningDate,
			String trainingTime,String strDescription, String flgDeleted, String iShipId,
			String iTenantId, String dtCreated, String dtUpdated,
			String flgStatus, String flgIsDirty, String iUserId,
			String iUserServiceTermId) {
		super();
		this.id = id;
		this.traningAttendanceId = traningAttendanceId;
		this.traningTransactionId = traningTransactionId;
		this.traningDataId = traningDataId;
		this.strParentId = strParentId;
		this.traningDate = traningDate;
		this.trainingTime = trainingTime;
		this.strDescription = strDescription;
		this.flgDeleted = flgDeleted;
		this.iShipId = iShipId;
		this.iTenantId = iTenantId;
		this.dtCreated = dtCreated;
		this.dtUpdated = dtUpdated;
		this.flgStatus = flgStatus;
		this.flgIsDirty = flgIsDirty;
		this.iUserId = iUserId;
		this.iUserServiceTermId = iUserServiceTermId;
	}
	public TraingAttendance() {
		// TODO Auto-generated constructor stub
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getTraningAttendanceId() {
		return traningAttendanceId;
	}
	public void setTraningAttendanceId(String traningAttendanceId) {
		this.traningAttendanceId = traningAttendanceId;
	}
	public String getTraningDataId() {
		return traningDataId;
	}
	public void setTraningDataId(String traningDataId) {
		this.traningDataId = traningDataId;
	}
	public String getStrParentId() {
		return strParentId;
	}
	public void setStrParentId(String strParentId) {
		this.strParentId = strParentId;
	}
	public String getTraningDate() {
		return traningDate;
	}
	public void setTraningDate(String traningDate) {
		this.traningDate = traningDate;
	}
	public String getTrainingTime() {
		return trainingTime;
	}
	public void setTrainingTime(String trainingTime) {
		this.trainingTime = trainingTime;
	}
	public String getFlgDeleted() {
		return flgDeleted;
	}
	public void setFlgDeleted(String flgDeleted) {
		this.flgDeleted = flgDeleted;
	}
	public String getiShipId() {
		return iShipId;
	}
	public void setiShipId(String iShipId) {
		this.iShipId = iShipId;
	}
	public String getiTenantId() {
		return iTenantId;
	}
	public void setiTenantId(String iTenantId) {
		this.iTenantId = iTenantId;
	}
	public String getDtCreated() {
		return dtCreated;
	}
	public void setDtCreated(String dtCreated) {
		this.dtCreated = dtCreated;
	}
	public String getDtUpdated() {
		return dtUpdated;
	}
	public void setDtUpdated(String dtUpdated) {
		this.dtUpdated = dtUpdated;
	}
	public String getFlgStatus() {
		return flgStatus;
	}
	public void setFlgStatus(String flgStatus) {
		this.flgStatus = flgStatus;
	}
	public String getFlgIsDirty() {
		return flgIsDirty;
	}
	public void setFlgIsDirty(String flgIsDirty) {
		this.flgIsDirty = flgIsDirty;
	}
	public String getiUserId() {
		return iUserId;
	}
	public void setiUserId(String iUserId) {
		this.iUserId = iUserId;
	}
	public String getiUserServiceTermId() {
		return iUserServiceTermId;
	}
	public void setiUserServiceTermId(String iUserServiceTermId) {
		this.iUserServiceTermId = iUserServiceTermId;
	}
	
	

}
