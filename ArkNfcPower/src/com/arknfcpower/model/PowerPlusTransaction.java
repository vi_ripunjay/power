package com.arknfcpower.model;

public class PowerPlusTransaction {

	int id;

	String iPowerPlusTransactionId;
	String dtCreated;
	String dtUpdated;
	String flgDeleted;
	String flgIsDirty;
	String flgIsTransationInvalid;
	String flgStatus;
	String iSequence;
	String powerPoints;
	String strAdditionalOne;
	String strAdditionalTwo;
	String strApproverId;
	String strApproverName;
	String strDescription;
	String strProviderId;
	String strProviderName;
	String strTitle;
	String iPowerPlusTaskId;
	String iShipId;
	String iTenantId;
	String iUserId;
	String strReceiverName;
	String flgScoreStatus;
	String cetogoryId;
	String parentTransactionId;
	String iRoleId;
	String iUserServiceTermId;
	String processedAuto;

	public PowerPlusTransaction() {
		
	}

	public PowerPlusTransaction(int id, String iPowerPlusTransactionId,
			String dtCreated, String dtUpdated, String flgDeleted,
			String flgIsDirty, String flgIsTransationInvalid, String flgStatus,
			String iSequence, String powerPoints, String strAdditionalOne,
			String strAdditionalTwo, String strApproverId,
			String strApproverName, String strDescription,
			String strProviderId, String strProviderName, String strTitle,
			String iPowerPlusTaskId, String iShipId, String iTenantId,
			String iUserId, String flgScoreStatus, String cetogoryId,
			String parentTransactionId) {
		super();
		this.id = id;
		this.iPowerPlusTransactionId = iPowerPlusTransactionId;
		this.dtCreated = dtCreated;
		this.dtUpdated = dtUpdated;
		this.flgDeleted = flgDeleted;
		this.flgIsDirty = flgIsDirty;
		this.flgIsTransationInvalid = flgIsTransationInvalid;
		this.flgStatus = flgStatus;
		this.iSequence = iSequence;
		this.powerPoints = powerPoints;
		this.strAdditionalOne = strAdditionalOne;
		this.strAdditionalTwo = strAdditionalTwo;
		this.strApproverId = strApproverId;
		this.strApproverName = strApproverName;
		this.strDescription = strDescription;
		this.strProviderId = strProviderId;
		this.strProviderName = strProviderName;
		this.strTitle = strTitle;
		this.iPowerPlusTaskId = iPowerPlusTaskId;
		this.iShipId = iShipId;
		this.iTenantId = iTenantId;
		this.iUserId = iUserId;
		this.flgScoreStatus = flgScoreStatus;
		this.cetogoryId = cetogoryId;
		this.parentTransactionId = parentTransactionId;

	}

	public PowerPlusTransaction(String iPowerPlusTransactionId,
			String dtCreated, String dtUpdated, String flgDeleted,
			String flgIsDirty, String flgIsTransationInvalid, String flgStatus,
			String iSequence, String powerPoints, String strAdditionalOne,
			String strAdditionalTwo, String strApproverId,
			String strApproverName, String strDescription,
			String strProviderId, String strProviderName, String strTitle,
			String iPowerPlusTaskId, String iShipId, String iTenantId,
			String iUserId, String flgScoreStatus, String cetogoryId,
			String parentTransactionId, String iRoleId,
			String iUserServiceTermId, String processedAuto, String strReceiverName) {
		super();
		this.iPowerPlusTransactionId = iPowerPlusTransactionId;
		this.dtCreated = dtCreated;
		this.dtUpdated = dtUpdated;
		this.flgDeleted = flgDeleted;
		this.flgIsDirty = flgIsDirty;
		this.flgIsTransationInvalid = flgIsTransationInvalid;
		this.flgStatus = flgStatus;
		this.iSequence = iSequence;
		this.powerPoints = powerPoints;
		this.strAdditionalOne = strAdditionalOne;
		this.strAdditionalTwo = strAdditionalTwo;
		this.strApproverId = strApproverId;
		this.strApproverName = strApproverName;
		this.strDescription = strDescription;
		this.strProviderId = strProviderId;
		this.strProviderName = strProviderName;
		this.strTitle = strTitle;
		this.iPowerPlusTaskId = iPowerPlusTaskId;
		this.iShipId = iShipId;
		this.iTenantId = iTenantId;
		this.iUserId = iUserId;
		this.flgScoreStatus = flgScoreStatus;
		this.cetogoryId = cetogoryId;
		this.parentTransactionId = parentTransactionId;
		this.iRoleId = iRoleId;
		this.iUserServiceTermId = iUserServiceTermId;
		this.processedAuto = processedAuto;
		this.strReceiverName = strReceiverName;
	}

	public String getCetogoryId() {
		return cetogoryId;
	}

	public void setCetogoryId(String cetogoryId) {
		this.cetogoryId = cetogoryId;
	}

	public String getParentTransactionId() {
		return parentTransactionId;
	}

	public void setParentTransactionId(String parentTransactionId) {
		this.parentTransactionId = parentTransactionId;
	}

	public String getFlgScoreStatus() {
		return flgScoreStatus;
	}

	public void setFlgScoreStatus(String flgScoreStatus) {
		this.flgScoreStatus = flgScoreStatus;
	}

	public String getiUserId() {
		return iUserId;
	}

	public void setiUserId(String iUserId) {
		this.iUserId = iUserId;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getiPowerPlusTransactionId() {
		return iPowerPlusTransactionId;
	}

	public void setiPowerPlusTransactionId(String iPowerPlusTransactionId) {
		this.iPowerPlusTransactionId = iPowerPlusTransactionId;
	}

	public String getDtCreated() {
		return dtCreated;
	}

	public void setDtCreated(String dtCreated) {
		this.dtCreated = dtCreated;
	}

	public String getDtUpdated() {
		return dtUpdated;
	}

	public void setDtUpdated(String dtUpdated) {
		this.dtUpdated = dtUpdated;
	}

	public String getFlgDeleted() {
		return flgDeleted;
	}

	public void setFlgDeleted(String flgDeleted) {
		this.flgDeleted = flgDeleted;
	}

	public String getFlgIsDirty() {
		return flgIsDirty;
	}

	public void setFlgIsDirty(String flgIsDirty) {
		this.flgIsDirty = flgIsDirty;
	}

	public String getFlgIsTransationInvalid() {
		return flgIsTransationInvalid;
	}

	public void setFlgIsTransationInvalid(String flgIsTransationInvalid) {
		this.flgIsTransationInvalid = flgIsTransationInvalid;
	}

	public String getFlgStatus() {
		return flgStatus;
	}

	public void setFlgStatus(String flgStatus) {
		this.flgStatus = flgStatus;
	}

	public String getiSequence() {
		return iSequence;
	}

	public void setiSequence(String iSequence) {
		this.iSequence = iSequence;
	}

	public String getPowerPoints() {
		return powerPoints;
	}

	public void setPowerPoints(String powerPoints) {
		this.powerPoints = powerPoints;
	}

	public String getStrAdditionalOne() {
		return strAdditionalOne;
	}

	public void setStrAdditionalOne(String strAdditionalOne) {
		this.strAdditionalOne = strAdditionalOne;
	}

	public String getStrAdditionalTwo() {
		return strAdditionalTwo;
	}

	public void setStrAdditionalTwo(String strAdditionalTwo) {
		this.strAdditionalTwo = strAdditionalTwo;
	}

	public String getStrApproverId() {
		return strApproverId;
	}

	public void setStrApproverId(String strApproverId) {
		this.strApproverId = strApproverId;
	}

	public String getStrApproverName() {
		return strApproverName;
	}

	public void setStrApproverName(String strApproverName) {
		this.strApproverName = strApproverName;
	}

	public String getStrDescription() {
		return strDescription;
	}

	public void setStrDescription(String strDescription) {
		this.strDescription = strDescription;
	}

	public String getStrProviderId() {
		return strProviderId;
	}

	public void setStrProviderId(String strProviderId) {
		this.strProviderId = strProviderId;
	}

	public String getStrProviderName() {
		return strProviderName;
	}

	public void setStrProviderName(String strProviderName) {
		this.strProviderName = strProviderName;
	}

	public String getStrTitle() {
		return strTitle;
	}

	public void setStrTitle(String strTitle) {
		this.strTitle = strTitle;
	}

	public String getiPowerPlusTaskId() {
		return iPowerPlusTaskId;
	}

	public void setiPowerPlusTaskId(String iPowerPlusTaskId) {
		this.iPowerPlusTaskId = iPowerPlusTaskId;
	}

	public String getiShipId() {
		return iShipId;
	}

	public void setiShipId(String iShipId) {
		this.iShipId = iShipId;
	}

	public String getiTenantId() {
		return iTenantId;
	}

	public void setiTenantId(String iTenantId) {
		this.iTenantId = iTenantId;
	}

	public String getiRoleId() {
		return iRoleId;
	}

	public void setiRoleId(String iRoleId) {
		this.iRoleId = iRoleId;
	}

	public String getiUserServiceTermId() {
		return iUserServiceTermId;
	}

	public void setiUserServiceTermId(String iUserServiceTermId) {
		this.iUserServiceTermId = iUserServiceTermId;
	}

	public String getProcessedAuto() {
		return processedAuto;
	}

	public void setProcessedAuto(String processedAuto) {
		this.processedAuto = processedAuto;
	}

	public String getStrReceiverName() {
		return strReceiverName;
	}

	public void setStrReceiverName(String strReceiverName) {
		this.strReceiverName = strReceiverName;
	}

}
