package com.arknfcpower.model;

public class PowerPlusRoleRankCategory {

	int id;
	private String iPowerPlusRoleRankCategoryId;
	private String dtCreated;
	private String dtUpdated;
	private int flgDeleted;
	private String strRankCategory;
	private String strRolesId;
	private String iTenantId;
	private String strField1;
	private String strField2;

	public PowerPlusRoleRankCategory() {
		super();

	}

	public PowerPlusRoleRankCategory(String iPowerPlusRoleRankCategoryId,
			String dtCreated, String dtUpdated, int flgDeleted,
			String strRankCategory, String strRolesId, String iTenantId,
			String strField1, String strField2) {
		super();
		this.iPowerPlusRoleRankCategoryId = iPowerPlusRoleRankCategoryId;
		this.dtCreated = dtCreated;
		this.dtUpdated = dtUpdated;
		this.flgDeleted = flgDeleted;
		this.strRankCategory = strRankCategory;
		this.strRolesId = strRolesId;
		this.iTenantId = iTenantId;
		this.strField1 = strField1;
		this.strField2 = strField2;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getiPowerPlusRoleRankCategoryId() {
		return iPowerPlusRoleRankCategoryId;
	}

	public void setiPowerPlusRoleRankCategoryId(
			String iPowerPlusRoleRankCategoryId) {
		this.iPowerPlusRoleRankCategoryId = iPowerPlusRoleRankCategoryId;
	}

	public String getDtCreated() {
		return dtCreated;
	}

	public void setDtCreated(String dtCreated) {
		this.dtCreated = dtCreated;
	}

	public String getDtUpdated() {
		return dtUpdated;
	}

	public void setDtUpdated(String dtUpdated) {
		this.dtUpdated = dtUpdated;
	}

	public int getFlgDeleted() {
		return flgDeleted;
	}

	public void setFlgDeleted(int flgDeleted) {
		this.flgDeleted = flgDeleted;
	}

	public String getStrRankCategory() {
		return strRankCategory;
	}

	public void setStrRankCategory(String strRankCategory) {
		this.strRankCategory = strRankCategory;
	}

	public String getStrRolesId() {
		return strRolesId;
	}

	public void setStrRolesId(String strRolesId) {
		this.strRolesId = strRolesId;
	}

	public String getiTenantId() {
		return iTenantId;
	}

	public void setiTenantId(String iTenantId) {
		this.iTenantId = iTenantId;
	}

	public String getStrField1() {
		return strField1;
	}

	public void setStrField1(String strField1) {
		this.strField1 = strField1;
	}

	public String getStrField2() {
		return strField2;
	}

	public void setStrField2(String strField2) {
		this.strField2 = strField2;
	}

}
