package com.arknfcpower.model;

public class Score {
	
	int _id ;
	String _sid;
	String _sType;
	String _description;
	String _s_value;
	
    public String get_s_value() {
		return _s_value;
	}

	public void set_s_value(String _s_value) {
		this._s_value = _s_value;
	}

	public Score(int id,String _pid, String _sType, String _description,String s_value){  
        this._id = id;
     	this._sid = _pid;
        this._sType = _sType;  
        this._description = _description;
        this._s_value = s_value;
        
    }  
   
    public Score(String _pid, String _sType, String _description,String s_value){
    	this._sid = _pid;
        this._sType = _sType;  
        this._description = _description;  
        this._s_value = s_value;

    }  

	public Score() {
		// TODO Auto-generated constructor stub
	}

	public int get_id() {
		return _id;
	}
	public void set_id(int _id) {
		this._id = _id;
	}
	public String get_sid() {
		return _sid;
	}
	public void set_sid(String _pid) {
		this._sid = _pid;
	}
	public String get_sType() {
		return _sType;
	}
	public void set_sType(String _sType) {
		this._sType = _sType;
	}
	public String get_description() {
		return _description;
	}
	public void set_description(String _description) {
		this._description = _description;
	}

}
