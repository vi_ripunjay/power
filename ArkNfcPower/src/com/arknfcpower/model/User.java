package com.arknfcpower.model;

public class User {  
	    int _id;
	    String _uid; 
	    String _rank;
	    String _ship;
	    
	    public String get_rank() {
			return _rank;
		}
		public void set_rank(String _rank) {
			this._rank = _rank;
		}
		public String get_ship() {
			return _ship;
		}
		public void set_ship(String _ship) {
			this._ship = _ship;
		}
		public String get_uid() {
			return _uid;
		}
		public void set_uid(String _uid) {
			this._uid = _uid;
		}

		String _name;  
	    String _role;  
	     public User(){   }  
	    public User(int id,String uid, String name,String rank,String ship, String _role){  
	        this._id = id;
	        this._uid = uid;
	        this._name = name;  
	        this._rank = rank;  
	        this._ship = ship;  
	        this._role = _role;  
	    }  
	   
	    public User(String uid, String name,String rank,String ship, String _role){
	    	this._uid = uid;
	        this._name = name;  
	        this._rank = rank;  
	        this._ship = ship; 
	        this._role = _role;  
	    }  
	    public int getID(){  
	        return this._id;  
	    }  
	  
	    public void setID(int id){  
	        this._id = id;  
	    }  
	  
	    public String getName(){  
	        return this._name;  
	    }  
	  
	    public void setName(String name){  
	        this._name = name;  
	    }  
	  
	    public String getRole(){  
	        return this._role;  
	    }  
	   
	    public void setRole(String role){  
	        this._role = role;  
	    }  
	}  