package com.arknfcpower.model;


public class TabletRoleFunctions {
	
	int id;
	private String iTabletRoleFunctionsId;
	private String dtCreated;
	private String dtUpdated;
	private int flgDeleted;
	private String strFunctionCode;
	private String iRoleId;
	private String iTenantId;
	private String strField1;
	private String strField2;
	
	public TabletRoleFunctions() {
		
	}

	public TabletRoleFunctions(String iTabletRoleFunctionsId, String dtCreated,
			String dtUpdated, int flgDeleted, String strFunctionCode,
			String iRoleId, String iTenantId, String strField1, String strField2) {
		super();
		this.iTabletRoleFunctionsId = iTabletRoleFunctionsId;
		this.dtCreated = dtCreated;
		this.dtUpdated = dtUpdated;
		this.flgDeleted = flgDeleted;
		this.strFunctionCode = strFunctionCode;
		this.iRoleId = iRoleId;
		this.iTenantId = iTenantId;
		this.strField1 = strField1;
		this.strField2 = strField2;
	}


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getiTabletRoleFunctionsId() {
		return iTabletRoleFunctionsId;
	}

	public void setiTabletRoleFunctionsId(String iTabletRoleFunctionsId) {
		this.iTabletRoleFunctionsId = iTabletRoleFunctionsId;
	}

	public String getDtCreated() {
		return dtCreated;
	}

	public void setDtCreated(String dtCreated) {
		this.dtCreated = dtCreated;
	}

	public String getDtUpdated() {
		return dtUpdated;
	}

	public void setDtUpdated(String dtUpdated) {
		this.dtUpdated = dtUpdated;
	}

	public int getFlgDeleted() {
		return flgDeleted;
	}

	public void setFlgDeleted(int flgDeleted) {
		this.flgDeleted = flgDeleted;
	}


	public String getStrField1() {
		return strField1;
	}

	public void setStrField1(String strField1) {
		this.strField1 = strField1;
	}

	public String getStrField2() {
		return strField2;
	}

	public void setStrField2(String strField2) {
		this.strField2 = strField2;
	}

	public String getStrFunctionCode() {
		return strFunctionCode;
	}

	public void setStrFunctionCode(String strFunctionCode) {
		this.strFunctionCode = strFunctionCode;
	}

	public String getiRoleId() {
		return iRoleId;
	}

	public void setiRoleId(String iRoleId) {
		this.iRoleId = iRoleId;
	}

	public String getiTenantId() {
		return iTenantId;
	}

	public void setiTenantId(String iTenantId) {
		this.iTenantId = iTenantId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime
				* result
				+ ((iTabletRoleFunctionsId == null) ? 0
						: iTabletRoleFunctionsId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TabletRoleFunctions other = (TabletRoleFunctions) obj;
		if (iTabletRoleFunctionsId == null) {
			if (other.iTabletRoleFunctionsId != null)
				return false;
		} else if (!iTabletRoleFunctionsId.equals(other.iTabletRoleFunctionsId))
			return false;
		return true;
	}

}
