package com.arknfcpower.model;

public class PowerPlusPeriodicity {

	int id;
	String iPowerPlusPeriodicityId;
	String dtCreated;
	String dtUpdated;   
	String flgDeleted;    
	String flgIsDirty;
	String flgStatus; 	
	String iInterval;
	String iSequence;
	String iTimes;
	String strDescription;
	String strPattern;
	String strTitle;     
	String iTenantId;
	
	
	
	public PowerPlusPeriodicity(int id, 
			String iPowerPlusPeriodicityId, String dtCreated, String dtUpdated,
			String flgDeleted, String flgIsDirty, String flgStatus,
			String iInterval, String iSequence, String iTimes,
			String strDescription, String strPattern, String strTitle,
			String iTenantId) {
		this.id = id;
		this.iPowerPlusPeriodicityId = iPowerPlusPeriodicityId;
		this.dtCreated = dtCreated;
		this.dtUpdated = dtUpdated;
		this.flgDeleted = flgDeleted;
		this.flgIsDirty = flgIsDirty;
		this.flgStatus = flgStatus;
		this.iInterval = iInterval;
		this.iSequence = iSequence;
		this.iTimes = iTimes;
		this.strDescription = strDescription;
		this.strPattern = strPattern;
		this.strTitle = strTitle;
		this.iTenantId = iTenantId;
	}
	
	public PowerPlusPeriodicity(
			String iPowerPlusPeriodicityId, String dtCreated, String dtUpdated,
			String flgDeleted, String flgIsDirty, String flgStatus,
			String iInterval, String iSequence, String iTimes,
			String strDescription, String strPattern, String strTitle,
			String iTenantId) {
		this.iPowerPlusPeriodicityId = iPowerPlusPeriodicityId;
		this.dtCreated = dtCreated;
		this.dtUpdated = dtUpdated;
		this.flgDeleted = flgDeleted;
		this.flgIsDirty = flgIsDirty;
		this.flgStatus = flgStatus;
		this.iInterval = iInterval;
		this.iSequence = iSequence;
		this.iTimes = iTimes;
		this.strDescription = strDescription;
		this.strPattern = strPattern;
		this.strTitle = strTitle;
		this.iTenantId = iTenantId;
	}
	
	public PowerPlusPeriodicity(){}
	
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getiPowerPlusPeriodicityId() {
		return iPowerPlusPeriodicityId;
	}
	public void setiPowerPlusPeriodicityId(String iPowerPlusPeriodicityId) {
		this.iPowerPlusPeriodicityId = iPowerPlusPeriodicityId;
	}
	public String getDtCreated() {
		return dtCreated;
	}
	public void setDtCreated(String dtCreated) {
		this.dtCreated = dtCreated;
	}
	public String getDtUpdated() {
		return dtUpdated;
	}
	public void setDtUpdated(String dtUpdated) {
		this.dtUpdated = dtUpdated;
	}
	public String getFlgDeleted() {
		return flgDeleted;
	}
	public void setFlgDeleted(String flgDeleted) {
		this.flgDeleted = flgDeleted;
	}
	public String getFlgIsDirty() {
		return flgIsDirty;
	}
	public void setFlgIsDirty(String flgIsDirty) {
		this.flgIsDirty = flgIsDirty;
	}
	public String getFlgStatus() {
		return flgStatus;
	}
	public void setFlgStatus(String flgStatus) {
		this.flgStatus = flgStatus;
	}
	public String getiInterval() {
		return iInterval;
	}
	public void setiInterval(String iInterval) {
		this.iInterval = iInterval;
	}
	public String getiSequence() {
		return iSequence;
	}
	public void setiSequence(String iSequence) {
		this.iSequence = iSequence;
	}
	public String getiTimes() {
		return iTimes;
	}
	public void setiTimes(String iTimes) {
		this.iTimes = iTimes;
	}
	public String getStrDescription() {
		return strDescription;
	}
	public void setStrDescription(String strDescription) {
		this.strDescription = strDescription;
	}
	public String getStrPattern() {
		return strPattern;
	}
	public void setStrPattern(String strPattern) {
		this.strPattern = strPattern;
	}
	public String getStrTitle() {
		return strTitle;
	}
	public void setStrTitle(String strTitle) {
		this.strTitle = strTitle;
	}
	public String getiTenantId() {
		return iTenantId;
	}
	public void setiTenantId(String iTenantId) {
		this.iTenantId = iTenantId;
	}

		
}
