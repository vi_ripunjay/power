package com.arknfcpower;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.nfc.FormatException;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.arknfcpower.db.DatabaseSupport;
import com.arknfcpower.model.UserNfcData;
import com.arknfcpower.model.UserServiceTermRoleData;

public class UserCardRegistration extends Activity {

	
	Intent intent;

	String nameAndRole = "";
	String userId = "";
	String name = "";
	String rank = "";
	String cardType = "1";
	
	
	Button register; 
	String message = "";
	String msg = "";
	String userShipId = ""; 
	String userTenantId = ""; 
	
	String strRoleNameArr[] = null;
	Integer cardTypeOnCard = null;
	String userIdOnCard = "";
	
	NfcAdapter adapter;
	PendingIntent pendingIntent;
	IntentFilter writeTagFilters[];
	boolean writeMode;
	Tag mytag;
	Context ctx;
	
    TextView userNameForRegistration;
    TextView registerText;
    
    AlertDialog alertDialog ;
    String dialogTitle = "";
    boolean registered = false;
	
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_user_card_registration);
		 ActionBar ab = getActionBar();
		 ab.setTitle(" User Card Registration");
	        ColorDrawable colorDrawable = new ColorDrawable(Color.parseColor("#009bff"));     
	        ab.setBackgroundDrawable(colorDrawable);
			/**
			 * work on 4.4.2
			 */
			//ab.setDisplayUseLogoEnabled(false);
			
			ab.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM | ActionBar.DISPLAY_SHOW_HOME | ActionBar.DISPLAY_SHOW_TITLE);
			View homeIcon = findViewById(
		            Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB ? 
		                    android.R.id.home : android.support.v7.appcompat.R.id.home);
		    ((View) homeIcon.getParent()).setVisibility(View.GONE);
		    ((View) homeIcon).setVisibility(View.GONE);
		
		intent = getIntent();
	    userId = intent.getStringExtra("userId"); 
	    nameAndRole = intent.getStringExtra("nameAndRole");
	    name = intent.getStringExtra("name");
	    rank = intent.getStringExtra("rank");
	    Log.i("TheArkNFCCARD", "Rank" + rank);
	    Log.i("TheArkNFCCARD", "Name" + name);
		
	   // System.out.println("User ID : " + userId);
	    //System.out.println("Name And Role : " + nameAndRole);
	    
		setContentView(R.layout.activity_user_card_registration);
		ctx=this;
		
		registerText = (TextView) findViewById(R.id.registerText);
		String text = "Registering Card for " + nameAndRole + ". Please hold the card behind the device and click on the Register Card button.";
		registerText.setText(text);
		
		//Button For Register Card.
		register = (Button) findViewById(R.id.register);
		register.setBackgroundColor(Color.WHITE);
		//Textiew For Setting Name

		userNameForRegistration = (TextView) findViewById(R.id.userName);
		//userNameForRegistration.setText(nameAndRole);
		userNameForRegistration.setVisibility(View.VISIBLE);
		
		
		
		//This Messsage Write On card;
		//message = nameAndRole;
		message = userId + "#" + rank + "#" + cardType + "#" + name;
		msg = "";
		dialogTitle = "";
		registered = false;
		
		
		
		register.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				try {
					if(mytag==null){
						Toast.makeText(ctx, ctx.getString(R.string.error_detected), Toast.LENGTH_LONG ).show();
					}else{
						
						DatabaseSupport db = new DatabaseSupport(getApplicationContext());
						
						ArrayList<UserNfcData> nfcData = new ArrayList<UserNfcData>();
						nfcData = db.getUserNFCSingleRow(CardInfo.getId(getApplicationContext()));
						//String title = CardInfo.getTitle(getApplicationContext());
						//String tag = CardInfo.getTag(getApplicationContext());
						//Toast.makeText(ctx, nameAndRole, 5000 ).show();
						//Toast.makeText(ctx, tag, 5000 ).show();
						Log.i("TheArkNFCCARD", "userId " + CardInfo.getUserId(getApplicationContext()));
						if("".equals(CardInfo.getUserId(getApplicationContext())))
						{
							
							write(message,mytag);
							
							DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
							Date curDate = new Date(); 
							
							if(CardInfo.getShipId(getApplicationContext()) == null || CardInfo.getShipId(getApplicationContext()).equalsIgnoreCase("1"))
							{
								List<UserServiceTermRoleData> serviceTermRoleData = new ArrayList<UserServiceTermRoleData>();
								/**
								 * 1.5.1
								 */
								//serviceTermRoleData = db.getUserServiceTermRoleRowBiId(userId,df.format(curDate));
								
								/**
								 * 1.5.6
								 */
								serviceTermRoleData = db.getUserServiceTermRoleRowBiIdForregisteruser(userId,df.format(curDate));
								
								UserServiceTermRoleData userSrvTrmRlData =  new UserServiceTermRoleData();
								if(serviceTermRoleData != null && serviceTermRoleData.size() > 0){
									userSrvTrmRlData = serviceTermRoleData.get(0);
									CardInfo.setShipId(getApplicationContext(), userSrvTrmRlData.getiShipId());
									CardInfo.setTenantId(getApplicationContext(), userSrvTrmRlData.getiTenantId());
									
									db.updateUserNFCRowByUsrid(userId, CardInfo.getTenantId(getApplicationContext()), CardInfo.getShipId(getApplicationContext()));
								}
							
							}
							
							
							ArrayList<UserNfcData> data = db.getUserNFCSingleRowByUserId(userId);						
							if(data != null && data.size() > 0){
								for(UserNfcData und : data) {
									und.setFlgDeleted("1");
									und.setFlgDirty("1");
									db.updateUserNFCRow(und);
								}							
							}
							
											
							
							//overwrite	
							  
							/* ArrayList<UserNfcData> nfcData1 = new ArrayList<UserNfcData>();
							 nfcData1 = db.getUserNFCSingleRow(CardInfo.getId(getApplicationContext()));		
							 if(nfcData1 == null && nfcData1.size() > 0)
							 {
							
							 for(UserNfcData und : nfcData1) {
									und.setFlgDeleted("1");
									und.setFlgDirty("1");
									db.updateUserNFCRow(und);
								}	
							}*/
							
							
							/*						
							db.addUserNFCRow(new UserNfcData(CardInfo.getId(getApplicationContext()), "User", userId, "S1", "1", "0", df.format(curDate), df.format(curDate), CardInfo.getTenantId(getApplicationContext()), CardInfo.getShipId(getApplicationContext())));
							*/
							 /**
							 * Above code comment due to card id use as primary key which is duplicate.
							 * So change pk format and cardid use in iuserservicetermid 
							 */
							String pk = CardInfo.getId(getApplicationContext())+"_"+curDate.getTime();
							db.addUserNFCRow(new UserNfcData(pk, "User", userId, CardInfo
									.getId(getApplicationContext()), "1",
									"0", df.format(curDate), df.format(curDate), CardInfo
											.getTenantId(getApplicationContext()), CardInfo
											.getShipId(getApplicationContext())));
							
							//Toast.makeText(ctx, ctx.getString(R.string.ok_writing) + nameAndRole + ctx.getString(R.string.ok_writing1), Toast.LENGTH_LONG ).show();
							msg = ctx.getString(R.string.ok_writing) + " " + nameAndRole + " " +  ctx.getString(R.string.ok_writing1);
							dialogTitle = "Card Registered";
							registered = true;
							showAlertDialogBox(msg);
							
						}
						else{
							//Toast.makeText(ctx, "This card cannot be overwrite with other crew.", 2000 ).show();
							//registerText.setText("This card cannot be overwrite with other crew, tap other blank card for register.");
							String nameRank="";
							if(strRoleNameArr !=null && strRoleNameArr.length > 0){
								
								nameRank = strRoleNameArr[3]+" / "+strRoleNameArr[1];
							}
							String msg = "";
							dialogTitle = "Registration Error";
							msg = "This card is registered to "+nameRank+".";
							//msg = msg + " and cannot be reused for another person. ";
							msg = msg + " Please register a fresh card for  ";
							msg = msg + nameAndRole;
							
							showAlertDialogBox(msg);
							//registerText.setText(msg);
							
							//registerText.invalidate();
							//userNameForRegistration.setVisibility(View.GONE);
						}
				
					}
				} catch (IOException e) {
					Toast.makeText(ctx, ctx.getString(R.string.error_writing), Toast.LENGTH_LONG ).show();
					e.printStackTrace();
				} catch (FormatException e) {
					Toast.makeText(ctx, ctx.getString(R.string.error_writing) , Toast.LENGTH_LONG ).show();
					e.printStackTrace();
				}
				
				
				
			}
			
		});
		
	    adapter = NfcAdapter.getDefaultAdapter(this);
		pendingIntent = PendingIntent.getActivity(this, 0, new Intent(this, getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
		IntentFilter tagDetected = new IntentFilter(NfcAdapter.ACTION_TAG_DISCOVERED);
		tagDetected.addCategory(Intent.CATEGORY_DEFAULT);
		writeTagFilters = new IntentFilter[] { tagDetected };
		
	}
	
	
	private void showAlertDialogBox(final String nameAndRole) {
		// TODO Auto-generated method stub
		
		alertDialog = new AlertDialog.Builder(this)
	    .setTitle(dialogTitle)
	    .setMessage(nameAndRole)
	    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
	        public void onClick(DialogInterface dialog, int which) { 
	            // continue with delete
	        	alertDialog.dismiss();
	        	if(registered){
	        		Intent i = new Intent(UserCardRegistration.this , AdminMainActivity.class);
	        		startActivity(i);
	        		finish();
	        	}
	        }
	     }).show();
		
		
	}
	
	private void write(String text, Tag tag) throws IOException, FormatException {

		NdefRecord[] records = { createRecord(text) };
		NdefMessage  message = new NdefMessage(records);
		// Get an instance of Ndef for the tag.
		
		Ndef ndef = Ndef.get(tag);
		// Enable I/O
		ndef.connect();
		// Write the message
		ndef.writeNdefMessage(message);
		
		//Make read only
		/**
		 * For development comment below line.
		 */
		//Log.i("TheArkNFCCARD", "writing into NFC :" + ndef.canMakeReadOnly());
		if(ndef.canMakeReadOnly()){
			//Log.i("TheArkNFCCARD", "writing NFC :" + ndef.canMakeReadOnly());
			ndef.makeReadOnly();
		}
		//Log.i("TheArkNFCCARD", "Exit NFC :" + ndef.canMakeReadOnly());
		
		// Close the connection
		ndef.close();
	}



	private NdefRecord createRecord(String text) throws UnsupportedEncodingException {
		String lang       = "en";
		byte[] textBytes  = text.getBytes();
		byte[] langBytes  = lang.getBytes("US-ASCII");
		int    langLength = langBytes.length;
		int    textLength = textBytes.length;
		byte[] payload    = new byte[1 + langLength + textLength];

		// set status byte (see NDEF spec for actual bits)
		payload[0] = (byte) langLength;

		// copy langbytes and textbytes into payload
		System.arraycopy(langBytes, 0, payload, 1,              langLength);
		System.arraycopy(textBytes, 0, payload, 1 + langLength, textLength);

		NdefRecord recordNFC = new NdefRecord(NdefRecord.TNF_WELL_KNOWN,  NdefRecord.RTD_TEXT,  new byte[0], payload);

		return recordNFC;
	}


	@Override
	protected void onNewIntent(Intent intent){
		Log.i("TheArkNFCCARD", "On New Intent");
		CardInfo.setUserId(getApplicationContext(),"");
		if(NfcAdapter.ACTION_TAG_DISCOVERED.equals(intent.getAction())){
			mytag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);    
			 byte[] id = mytag.getId();
             long ids = getDec(id);
             String i = String.valueOf(ids);
             CardInfo.setId(ctx, i);
             NdefMessage[] msgs;
            Parcelable[] rawMsgs = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
 			if (rawMsgs != null) {
             msgs = new NdefMessage[rawMsgs.length];
				for (int x = 0; x < rawMsgs.length; x++) {

					msgs[x] = (NdefMessage) rawMsgs[x];
				}
				String m = getMessage(msgs);
				Log.i("TheArkNFCCARD", "Message" + m);
				
				if(m != null && m.startsWith("en")){
					//m = m.substring(3, m.length()-1);
			    	m = m.substring(3, m.length());
			    }
				
				
				
				strRoleNameArr = null;
			    cardTypeOnCard = null;
				userIdOnCard = "";
				if(m != null && !"".equals(m)){
					strRoleNameArr = m.split("#");
					userIdOnCard = strRoleNameArr[0];
					CardInfo.setUserId(getApplicationContext(),
							strRoleNameArr[0]);
				}
            // Toast.makeText(ctx, "Card Id : " + i, Toast.LENGTH_LONG).show();
             
 			}
		}
	}
	
	
	
	private String getMessage(NdefMessage[] msgs) {
		// TODO Auto-generated method stub
		String tags = "";
		if (msgs == null || msgs.length == 0) {
			return "";
		}

		NdefRecord[] records = msgs[0].getRecords();
		for (final NdefRecord record : records) {
			tags = new String(record.getPayload());
		}
		return tags.toString();
	}
	

    private long getDec(byte[] bytes) {
        long result = 0;
        long factor = 1;
        for (int i = 0; i < bytes.length; ++i) {
            long value = bytes[i] & 0xffl;
            result += value * factor;
            factor *= 256l;
        }
        return result;
    }
	@Override
	public void onPause(){
		super.onPause();
		WriteModeOff();
	}

	@Override
	public void onResume(){
		super.onResume();
		WriteModeOn();
	}

	private void WriteModeOn(){
		writeMode = true;
		adapter.enableForegroundDispatch(this, pendingIntent, writeTagFilters, null);
	}

	private void WriteModeOff(){
		writeMode = false;
		adapter.disableForegroundDispatch(this);
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		//super.onBackPressed();
		Intent i = new Intent(UserCardRegistration.this , AdminMainActivity.class);
		startActivity(i);
		finish();
	}

/*	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.user_card_registration, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
*/
	
}
