package com.arknfcpower.sync;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringEscapeUtils;

import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;
import net.lingala.zip4j.model.ZipParameters;
import net.lingala.zip4j.util.Zip4jConstants;
import android.annotation.SuppressLint;
import android.content.Context;
import android.database.Cursor;
import android.os.Environment;
import android.telephony.TelephonyManager;

import com.arknfcpower.CardInfo;
import com.arknfcpower.L;
import com.arknfcpower.R;
import com.arknfcpower.db.DatabaseSupport;
import com.arknfcpower.model.SyncHistory;

public class GenerateXmlService {

	/**
	 * @author Ripunjay Shukla
	 * @since 29-06-2015 This class write for generate zip of xml for sync.
	 */

	private Map<String, Set<String>> tableColumnMap;

	private Map<String, String[][]> tableValueMap;

	private Context context;

	@SuppressWarnings("unused")
	private int lineCounter = 0;
	@SuppressWarnings("unused")
	private int recordNum = 0;

	private String baseFoldername = "";

	private StringBuffer logMessages;

	public GenerateXmlService() {

	}

	public GenerateXmlService(Context mcontext) {

		this.context = mcontext;
	}

	public void generateXmlForDirtyRecord() {

		DatabaseSupport db = new DatabaseSupport(context);
		List<SyncHistory> shList = new ArrayList<SyncHistory>();
		shList = db.getSyncHistoryRow();
		
		cleanProcessDirectory(new File(getXmlSendPath()));
		cleanProcessDirectory(getXmlProcessPath());
		
		if (shList == null || shList.size() == 0) {

			File sourceFile = generateZipForInitialSync(context);

			String destinationFile = getInitialZipProcessPath(baseFoldername);
			createZipFileofFolder(sourceFile.getAbsolutePath(), destinationFile);
			mooveFiletoSendFolder(destinationFile);
			cleanProcessDirectory(getXmlProcessPath());

		} else {
			String iShipId  = new Integer(shList.get(0).getiShipId()).toString();
			String iTenantId = new Integer(shList.get(0).getiTenantId()).toString();
			CardInfo.setShipId(context, iShipId);
			CardInfo.setTenantId(context, iTenantId);
			File sourceFile = createOneTablerecord(context);

			String destinationFile = getZipProcessPath(baseFoldername);
			createZipFileofFolder(sourceFile.getAbsolutePath(), destinationFile);
			
			cleanProcessDirectory(new File(getXmlSendPath()));
			mooveFiletoSendFolder(destinationFile);
			
			cleanProcessDirectory(getXmlProcessPath());
			//updateDirtyFlagOfTables();  //update dirty record as undirty after parsing data.
		}

		/*
		 * DatabaseSupport db = new DatabaseSupport(mContext);
		 * 
		 * List<String[]> tblDetails = db
		 * .getDbTableColumnDetails("PowerPlusTransaction"); if (tblDetails !=
		 * null && tblDetails.size() > 0) {
		 * 
		 * for (String[] tblDet : tblDetails) { for (int i = 0; i <
		 * tblDet.length; i++) {
		 * 
		 * L.fv("" + tblDet[i]); }
		 * 
		 * }
		 * 
		 * } db.close();
		 */

	}

	public File getXmlProcessPath() {

		StringBuffer sb = new StringBuffer(
				Environment.getExternalStorageDirectory() + "/xmlProcess/");
		File procFile = new File(sb.toString());
		return procFile;
	}

	public String getZipProcessPath(String strFilename) {

		StringBuffer sb = new StringBuffer(
				Environment.getExternalStorageDirectory() + "/xmlProcess/"
						+ strFilename + getMacId()
						+ "_Database-Synchronization_.zip");

		return sb.toString();
	}

	public String getInitialZipProcessPath(String strFilename) {

		StringBuffer sb = new StringBuffer(
				Environment.getExternalStorageDirectory() + "/xmlProcess/"
						+ strFilename + getMacId()
						+ "_Initial-Synchronization_.zip");

		return sb.toString();
	}

	public String getXmlSendPath() {

		StringBuffer sb = new StringBuffer(
				Environment.getExternalStorageDirectory() + "/TheArk/Export/");
		File file = new File(sb.toString());
		if (!file.exists()) {
			file.mkdirs();
		}

		/**
		 * below for only creation directory
		 */
		StringBuffer sbRec = new StringBuffer(
				Environment.getExternalStorageDirectory() + "/TheArk/Import/");
		File fileRec = new File(sbRec.toString());
		if (!fileRec.exists()) {
			fileRec.mkdirs();
		}

		return sb.toString();
	}

	public File getBaseFolderpath() {
		StringBuffer baseFolderName = new StringBuffer("TABLETSHIP");
		baseFolderName.append("_" + 0 + "_" + CardInfo.getTenantId(context)
				+ "_" + CardInfo.getShipId(context) + "_");

		File file = new File(Environment.getExternalStorageDirectory()
				+ "/xmlProcess/" + baseFolderName);
		if (!file.exists()) {
			file.mkdirs();
		}
		baseFoldername = baseFolderName.toString();
		return file;
	}

	/**
	 * @author ripunjay
	 * @return pass table for generate xml and comes in sync.
	 */
	public String[] getTablename() {
		String[] tablename = { "PowerPlusTransaction", "TrainingAttendance",
				"TrainingTransaction", "PowerPlusTabletSyncHistory","UserNfc"};

		return tablename;
	}

	/**
	 * @author Ripunjay
	 * @param mContext
	 * @return create xml of given tables
	 */
	public File createOneTablerecord(Context mContext) {
		try {
			context = mContext;
			boolean createFileWriteOnfirstRecord = true;
			Writer fileWriter = null;
			StringBuffer dataXml = new StringBuffer();
			int recordcount = 0;
			logMessages = new StringBuffer();
			String shipid = CardInfo.getShipId(mContext);
			String tenantid = CardInfo.getTenantId(mContext);

			File sourceFile = getBaseFolderpath();

			DatabaseSupport db = new DatabaseSupport(mContext);
			
			//db.updateDirtyRecordInTables("PowerPlusTransaction");//23-12-2015 for testing
			
			String[] tablename = getTablename();

			for (int i = 0; i < tablename.length; i++) {

				recordcount=0;
				lineCounter =0;
				//recordNum = 0;
				
				createFileWriteOnfirstRecord = true;
				Cursor cursor = db.getDataForSync(tablename[i]);

				List<String[]> tblDetails = db
						.getDbTableColumnDetails(tablename[i]);

				if (tblDetails != null && tblDetails.size() > 0) {

					// looping through all rows and adding to list
					if (cursor != null && cursor.moveToFirst()) {

						if (createFileWriteOnfirstRecord) {

							String pk = tblDetails.get(0)[0];

							dataXml = new StringBuffer("<tables iTenantId='" + tenantid
									+ "' ishipId='" + shipid + "' pk='" + pk
									+ "'>");
							dataXml.append("<" + tablename[i] + " pk='"
									+ pk + "'>");
							fileWriter = new BufferedWriter(
									new OutputStreamWriter(
											new FileOutputStream(
													sourceFile
															.getAbsolutePath()
															+ "/"
															+ tablename[i]
															+ ".xml"), "UTF-8"));

							createFileWriteOnfirstRecord = false;
						}

						do {
							recordcount++;
							recordNum++;
							lineCounter++;
							
							if(recordcount > Integer.parseInt(context.getResources().getString(R.string.recordCounter))){
								
								recordcount = 1;
								dataXml.append("</" + tablename[i] + ">");
								dataXml.append("</tables>");
								fileWriter.write(dataXml.toString());
								fileWriter.close();
								
								String pk = tblDetails.get(0)[0];

								dataXml = new StringBuffer("<tables iTenantId='" + tenantid
										+ "' ishipId='" + shipid + "' pk='" + pk
										+ "'>");
								dataXml.append("<" + tablename[i] + " pk='"
										+ pk + "'>");
								fileWriter = new BufferedWriter(
										new OutputStreamWriter(
												new FileOutputStream(
														sourceFile
																.getAbsolutePath()
																+ "/"
																+ tablename[i] + new Date().getTime()
																+ ".xml"), "UTF-8"));
							}
							
							
							dataXml.append("<r" + recordcount + ">");

							for (String[] tblDet : tblDetails) {
								// for(int count =0; count < tblDet.length;
								// count++){

								if (tblDet[1].equalsIgnoreCase("INTEGER")) {
									dataXml.append("<"
											+ tblDet[0]
											+ " t='"
											+ tblDet[1]
											+ "'>"
											+ cursor.getInt(cursor
													.getColumnIndex(tblDet[0]))
											+ "</" + tblDet[0] + ">");
								} else if (tblDet[1].equalsIgnoreCase("TEXT")) {

									/*dataXml.append("<"
											+ tblDet[0]
											+ " t='VARCHAR'>"
											+ cursor.getString(cursor
													.getColumnIndex(tblDet[0]))
											+ "</" + tblDet[0] + ">");*/
									String data = cursor.getString(cursor
											.getColumnIndex(tblDet[0]));
									try {
										if (data != null
												&& !"null".equals(data)
												&& !"".equals(data)) {
											data = StringEscapeUtils
													.escapeXml(data);
										}
									} catch (Exception e) {
										e.printStackTrace();
									}

									dataXml.append("<"
											+ tblDet[0]
											+ " t='VARCHAR'>"
											+ ((data != null
													&& !"null".equals(data) && !""
														.equals(data)) ? data
													.replaceAll("&apos;",
															"&apos;&apos;")
													: data) + "</"
											+ tblDet[0] + ">");

								} else if (tblDet[1].equalsIgnoreCase("DATE")) {

									dataXml .append("<"
											+ tblDet[0]
											+ " t='"
											+ tblDet[1]
											+ "'>"
											+ cursor.getString(cursor
													.getColumnIndex(tblDet[0]))
											+ "</" + tblDet[0] + ">");
								}

								// }

							}
							dataXml.append("</r" + recordcount + ">");
							
						} while (cursor.moveToNext());

						dataXml.append("</" + tablename[i] + ">");
						dataXml.append("</tables>");
						fileWriter.write(dataXml.toString());
						fileWriter.close();

						logMessages.append(" " + tablename[i] + " : record : "
								+ recordcount);
					}

				}
				cursor.close();
				
			}
			if(recordNum > 0){
				insertSyncHistoryRecord(logMessages.toString(), "progress",
						"TABLETSHIP", baseFoldername,
						Integer.parseInt(tenantid), Integer.parseInt(shipid));
			}
			
			db.close();
			return sourceFile;
		} catch (UnsupportedEncodingException e) {

			e.printStackTrace();
		} catch (FileNotFoundException e) {

			e.printStackTrace();
		} catch (Exception e) {

			e.printStackTrace();
		}

		return null;

	}

	@SuppressLint("SimpleDateFormat")
	public void insertSyncHistoryRecord(String logMessage, String progress,
			String generator, String strFilename, int iTenantId, int iShipId) {

		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		Date curDate = new Date();
		/*
		 * SyncHistory syncHistory = new SyncHistory(getPk(),
		 * df.format(curDate), logMessage, progress, generator, strFilename, 1,
		 * 0, iTenantId, iShipId);
		 */

		DatabaseSupport db = new DatabaseSupport(context);
		List<SyncHistory> shList = new ArrayList<SyncHistory>();
		shList = db.getSyncHistoryRow();
		SyncHistory syncHistory = new SyncHistory();
		if(shList != null && shList.size() >0){
			syncHistory = shList.get(0);
			syncHistory.setLogMessage(logMessage);
		}
		/*else{		
			syncHistory = new SyncHistory(getPk(), df.format(curDate),
					logMessage, progress, generator, strFilename, 1, 0, null, null,
					null, getMacId(), "strRegisterTabletId", iTenantId, iShipId);
		}*/
		
		if (shList != null && shList.size() > 0) {
			//syncHistory.setIsyncHistoryId(shList.get(0).getIsyncHistoryId());
			syncHistory.setFlgIsDirty(1);
			if(syncHistory.getDtAcknowledgeDate() == null){
				syncHistory.setDtAcknowledgeDate(syncHistory.getDtGeneratedDate());
			}
			db.updateSyncHistorydata(syncHistory);
		}/* else {
			syncHistory.setFlgIsDirty(1);
			if(syncHistory.getDtAcknowledgeDate() == null){
				syncHistory.setDtAcknowledgeDate(""+curDate.getTime());
			}
			db.insertSyncHistorydata(syncHistory);
		}*/

		db.close();

	}

	public File generateZipForInitialSync(Context mContext) {
		/**
		 * Write initial sync zip code here.
		 */
		try {

			String baseFolderName = "TABLETSHIP_0_0_0_";
			File sourceFile = new File(
					Environment.getExternalStorageDirectory() + "/xmlProcess/"
							+ baseFolderName);
			if (!sourceFile.exists()) {
				sourceFile.mkdirs();
			}

			baseFoldername = baseFolderName;

			context = mContext;
			Writer fileWriter = null;
			String dataXml = "";
			logMessages = new StringBuffer();
			String shipid = CardInfo.getShipId(mContext);
			String tenantid = CardInfo.getTenantId(mContext);

			dataXml = "<tables iTenantId='" + tenantid + "' ishipId='" + shipid
					+ "' pk=''>";

			fileWriter = new BufferedWriter(new OutputStreamWriter(
					new FileOutputStream(sourceFile.getAbsolutePath()
							+ "/initialSync.xml"), "UTF-8"));

			dataXml = dataXml + "</tables>";

			fileWriter.write(dataXml);
			fileWriter.close();

			/*insertSyncHistoryRecord("Initial Sync", "progress", "TABLETSHIP",
					baseFoldername, Integer.parseInt("1"),
					Integer.parseInt("1"));*/

			return sourceFile;

		} catch (UnsupportedEncodingException e) {

			e.printStackTrace();
		} catch (FileNotFoundException e) {

			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

	public void updateDirtyFlagOfTables() {
		String[] tables = getTablename();
		DatabaseSupport db = new DatabaseSupport(context);
		if (tables != null && tables.length > 0) {
			for (String tbl : tables) {
				db.updateDirtyRecordInTables(tbl);
			}
			db.close();
		}
	}

	public Map<String, Set<String>> getTableColumnMap() {
		return tableColumnMap;
	}

	public void setTableColumnMap(Map<String, Set<String>> tableColumnMap) {
		this.tableColumnMap = tableColumnMap;
	}

	public Map<String, String[][]> getTableValueMap() {
		return tableValueMap;
	}

	public void setTableValueMap(Map<String, String[][]> tableValueMap) {
		this.tableValueMap = tableValueMap;
	}

	public void createZipFileofFolder(String folderPath, String zipFilePath) {
		try {
			File fileToAdd = new File(folderPath);
			if (fileToAdd.exists()) {
				ZipFile zipFile = new ZipFile(zipFilePath);
				ZipParameters parameters = new ZipParameters();
				parameters.setCompressionMethod(Zip4jConstants.COMP_DEFLATE);
				parameters
						.setCompressionLevel(Zip4jConstants.DEFLATE_LEVEL_NORMAL);
				parameters.setEncryptFiles(true);
				parameters
						.setEncryptionMethod(Zip4jConstants.ENC_METHOD_STANDARD);
				parameters.setPassword("prashant");
				String[] fileArray = fileToAdd.list();
				for (int i = 0; i < fileArray.length; i++) {
					L.fv("Added in zip" + fileArray[i]);
					File tobeAdded = new File(fileToAdd.getAbsolutePath()
							+ File.separator + fileArray[i]);
					if (tobeAdded.isDirectory()) {
						zipFile.addFolder(new File(fileToAdd.getAbsolutePath()
								+ File.separator + fileArray[i]), parameters);
					} else {
						zipFile.addFile(new File(fileToAdd.getAbsolutePath()
								+ File.separator + fileArray[i]), parameters);
					}
				}
			}

		} catch (ZipException e) {
			e.printStackTrace();

		}
	}

	public int cleanProcessDirectory(File dir) {
		if (dir.isDirectory()) {
			String[] children = dir.list();
			for (int i = 0; i < children.length; i++) {
				deleteDir(new File(dir, children[i]));

			}
		}
		if (dir.list() != null) {
			return dir.list().length;
		} else {
			return 0;
		}
	}

	public boolean deleteDir(File dir) {
		if (dir.isDirectory()) {
			String[] children = dir.list();
			for (int i = 0; i < children.length; i++) {
				boolean success = deleteDir(new File(dir, children[i]));
				if (!success) {
					return false;
				}
			}
		}
		return dir.delete();
	}

	public boolean mooveFiletoSendFolder(String filetomoove) {
		File file = new File(filetomoove);
		File dir = new File(getXmlSendPath());
		return file.renameTo(new File(dir, file.getName()));

	}

	public String getMacId() {
		String strmacId = "macid";
		TelephonyManager tm = (TelephonyManager) context
				.getSystemService(Context.TELEPHONY_SERVICE);
		strmacId = tm.getDeviceId();
		return strmacId;
	}

	public String getPk() {
		String pk = "";
		Date curDate = new Date();
		TelephonyManager tm = (TelephonyManager) context
				.getSystemService(Context.TELEPHONY_SERVICE);
		pk = tm.getDeviceId() + "_" + curDate.getTime();

		return pk;
	}

}
