package com.arknfcpower.sync;

import java.io.File;
import java.io.FilenameFilter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import net.lingala.zip4j.core.ZipFile;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.content.ContentValues;
import android.content.Context;
import android.os.Environment;
import android.os.Handler;
import android.telephony.TelephonyManager;

import com.arknfcpower.CardInfo;
import com.arknfcpower.L;
import com.arknfcpower.db.DatabaseSupport;
import com.arknfcpower.model.ShipMasterData;
import com.arknfcpower.model.TrainingTransaction;
import com.arknfcpower.model.UserServiceTermRoleData;

public class ParseXmlService {

	/**
	 * @author Ripunjay SHukla
	 * @see parse xml of given zip and save and update data in db
	 */

	Handler handler;
	public static Context context;

	private String xmlProcessPath = "";
	private String strDownloadDate;

	public ParseXmlService() {

	}

	public ParseXmlService(Context context) {
		super();
		this.context = context;
	}

	public void startParseXmlAfterZip() {
		try {

			parseXmlForSync();
			
			updateDirtyFlagOfTables(); 
			
			deleteTransactionData();
			
			deleteAllShipRelatedData();
			
			setDeviceTime();
						
			/*handler = new Handler();
			Runnable runnable = new Runnable() {
				int counter = 0;

				public void run() {
					counter++;
					if (fileExist()) {
						
						parseXmlForSync();
					} else {
						try {
							
							Thread.sleep(3000);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						if (counter <= 10) {
							run();
						}
					}
				}
			};
			handler.postDelayed(runnable, 3000);*/
		} catch (Exception e) {

			e.printStackTrace();
		}

	}
	
	public void updateDirtyFlagOfTables() {
		String[] tables = getTablename();
		DatabaseSupport db = new DatabaseSupport(context);
		if (tables != null && tables.length > 0) {
			for (String tbl : tables) {
				db.updateDirtyRecordInTables(tbl);
			}
			db.close();
		}
	}
	
	/**
	 * @author ripunjay
	 * @return pass table for generate xml and comes in sync.
	 */
	public String[] getTablename() {
		String[] tablename = { "PowerPlusTransaction", "TrainingAttendance",
				"TrainingTransaction", "PowerPlusTabletSyncHistory"};

		return tablename;
	}
	
	/**
	 * @author ripunjay
	 * @return pass table for generate xml and comes in sync.
	 */
	public String[] getTablenameForDelete() {
		String[] tablename = { "PowerPlusTransaction", "TrainingTransaction"};

		return tablename;
	}
	
	public boolean fileExist() {
		boolean exist = false;
		xmlProcessPath = getXmlReceivePath();
		String[] allzipFileFromXmlProcessPath = new File(xmlProcessPath)
				.list(new FilenameFilter() {
					@Override
					public boolean accept(File dir, String name) {
						String startingStringForFileNameFilter = "SHIPTABLET";

						return (name.toUpperCase()
								.startsWith(startingStringForFileNameFilter))
								&& name.toUpperCase().endsWith(".ZIP");
					}
				});

		if (allzipFileFromXmlProcessPath != null
				&& allzipFileFromXmlProcessPath.length > 0) {
			exist = true;
		}
		return exist;
	}

	@SuppressLint("DefaultLocale")
	public void parseXmlForSync() {

		Map<String, List<String>> totalShiporShoreDatatoSync = new HashMap<String, List<String>>();
		List<String> syncOrderIdsForShip = new ArrayList<String>();

		xmlProcessPath = getXmlReceivePath();
		final String strMacId = getMacId();
		
		String[] allzipFileFromXmlProcessPath = new File(xmlProcessPath)
				.list(new FilenameFilter() {
					@Override
					public boolean accept(File dir, String name) {
						String startingStringForFileNameFilter = "SHIPTABLET";

						return (name.toUpperCase()
								.startsWith(startingStringForFileNameFilter))
								&& name.toUpperCase().endsWith(".ZIP") && name.contains(strMacId);
					}
				});

		if (allzipFileFromXmlProcessPath != null
				&& allzipFileFromXmlProcessPath.length > 0) {

			for (String fileName : allzipFileFromXmlProcessPath) {

				String[] splitFileNameArray = fileName.split("_");

				String keystr = splitFileNameArray[3].replaceAll(".zip", "")
						.replaceAll(".ZIP", "") + "_" + splitFileNameArray[2];

				// order tenant ship
				List<String> list = totalShiporShoreDatatoSync.get(keystr);
				if (list == null) {
					list = new ArrayList<String>();
				}
				list.add(fileName);

				syncOrderIdsForShip.add(splitFileNameArray[3].replaceAll(
						".zip", "").replaceAll(".ZIP", "")
						+ "_" + splitFileNameArray[1]);
				totalShiporShoreDatatoSync.put(keystr, list);

				for (Iterator<String> iterator = totalShiporShoreDatatoSync
						.keySet().iterator(); iterator.hasNext();) {

					String keyStrValue = iterator.next();
					String tenantid = keyStrValue.split("_")[1];
					String shipid = keyStrValue.split("_")[0];

					List<String> totalFileForthisTenantShip = totalShiporShoreDatatoSync
							.get(keyStrValue);
					// overriding the sort method to do integer sorting for a
					// list of
					// strings passed
					Collections.sort(totalFileForthisTenantShip,
							new Comparator<String>() {
								@Override
								public int compare(String o1, String o2) {
									return Integer.parseInt(o1.split("_")[1])
											- Integer.parseInt(o2.split("_")[1]);

								}
							});

					for (Iterator<String> iterator2 = totalFileForthisTenantShip
							.iterator(); iterator2.hasNext();) {

						String zipFileName = iterator2.next();
						String[] arr1 = zipFileName.split("_");

						String extractPath = xmlProcessPath + "/extractZip"
								+ System.currentTimeMillis();

						try {
							extractZipFile(xmlProcessPath + "/" + zipFileName,
									extractPath);
						} catch (Exception e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}

						String allXmlFile[] = new File(extractPath)
								.list(new FilenameFilter() {
									@Override
									public boolean accept(File dir, String name) {
										return name.toUpperCase().endsWith(
												".XML");

									}
								});

						// cleanProcessDirectory(new File(extractPath));

						if (allXmlFile != null) {
							Arrays.sort(allXmlFile);
							for (String filenameInExtractedDir : allXmlFile) {
								System.out.println("xmlFileName : ================ "+filenameInExtractedDir);
								try {
									parseXmlFile(extractPath + "/"
											+ filenameInExtractedDir);

								} catch (Exception e) {

									e.printStackTrace();
								}

							}
						}

					}

				}
			}
		}

		cleanProcessDirectory(new File(xmlProcessPath));

	}

	/**
	 * This method is called for each file to be parsed and updated in the
	 * database
	 * 
	 * @param filename
	 */
	public void parseXmlFile(String filename) {
		String strIshipid = null;
		String strItenantid = null;
		String tablename = null;
		try {

			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			Document doc = db.parse("file://" + filename);
			doc.normalize();
			Node parentNode = doc.getFirstChild();
			strIshipid = parentNode.getAttributes().getNamedItem("ishipId")
					.getNodeValue();
			strItenantid = parentNode.getAttributes().getNamedItem("iTenantId")
					.getNodeValue();

			NodeList nodeList = parentNode.getChildNodes();

			for (int i = 0; i < nodeList.getLength(); i++) {

				Node node = nodeList.item(i);

				String pk = parentNode.getAttributes().getNamedItem("pk")
						.getNodeValue();

				if (node.getNodeType() == Node.ELEMENT_NODE) {
					tablename = node.getNodeName();

					List<String> lst = new ArrayList<String>();
					DatabaseSupport dataBase = new DatabaseSupport(context);
					lst = dataBase.getPkDataForSync(pk, tablename);
					dataBase.close();

					NodeList nodeList2 = node.getChildNodes();

					for (int j = 0; j < nodeList2.getLength(); j++) {
						Node tempNode = nodeList2.item(j);
						NodeList nodeList21 = tempNode.getChildNodes();

						Map<String, String> onerecordData = new HashMap<String, String>();

						for (int j1 = 0; j1 < nodeList21.getLength(); j1++) {
							Node tempNode1 = nodeList21.item(j1);
							onerecordData.put(tempNode1.getNodeName(),
									tempNode1.getTextContent()
											+ "____"
											+ tempNode1.getAttributes()
													.getNamedItem("t")
													.getNodeValue());
						}

						if (lst.contains(onerecordData.get(pk).split("____")[0])) {

							if (onerecordData.get(pk).split("____")[1]
									.equalsIgnoreCase("int")
									|| onerecordData.get(pk).split("____")[1]
											.equalsIgnoreCase("double")) {

								/**
								 * UPDATE
								 */
								dataBase = new DatabaseSupport(context);
								dataBase.updateRecordInTables(
										tablename,
										pk,
										onerecordData.get(pk).split("____")[0],
										getContentValueOfTable(onerecordData, 1));
								dataBase.close();

							} else {
								/**
								 * UPDATE
								 */
								ContentValues contentValues = getContentValueOfTable(onerecordData, 1);
								if(tablename.equalsIgnoreCase("PowerPlusTabletSyncHistory")){
									DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
									Date curDate = new Date();
									
									if(contentValues.getAsInteger("flgDeleted") == 0){
										strDownloadDate = contentValues.getAsString("dtGeneratedDate");
									}
									
									contentValues.put("dtAcknowledgeDate", contentValues.getAsString("dtGeneratedDate"));
									contentValues.put("flgIsDirty", 1);
								}
								
								dataBase = new DatabaseSupport(context);
								dataBase.updateRecordInTables(
										tablename,
										pk,
										onerecordData.get(pk).split("____")[0],
										contentValues);
								dataBase.close();

							}
						} else {

							/**
							 * insert
							 */
							ContentValues contentValues = getContentValueOfTable(onerecordData, 0);
							if(tablename.equalsIgnoreCase("PowerPlusTabletSyncHistory")){
								//DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
								//Date curDate = new Date();
								if(contentValues.getAsInteger("flgDeleted") == 0){
									strDownloadDate = contentValues.getAsString("dtGeneratedDate");
								}
								
								contentValues.put("dtAcknowledgeDate", contentValues.getAsString("dtGeneratedDate"));
								contentValues.put("flgIsDirty", 1);
							}
							
							dataBase = new DatabaseSupport(context);
							dataBase.insertRecordInTables(tablename,contentValues);
							dataBase.close();

						}

					}
					lst = null;

				}
			}

		} catch (Exception e) {
			System.out.println("expection in file parse  ======== : "+e.getMessage());
			e.printStackTrace();

		} 
	}

	public ContentValues getContentValueOfTable(
			Map<String, String> onerecordData, int flg) {

		ContentValues values = new ContentValues();

		StringBuffer sqlstr = new StringBuffer();
		// String sqlstr = "(";
		sqlstr.append("(");
		Set<String> allkeyset = onerecordData.keySet();
		if (flg == 0) {

			for (Iterator<String> iterator = allkeyset.iterator(); iterator
					.hasNext();) {

				String key = iterator.next();
				String[] val = onerecordData.get(key).split("____");

				values.put(key, val[0]);

			}

		}
		if (flg == 1) {
			// sqlstr = " SET ";
			sqlstr = new StringBuffer();
			sqlstr.append(" SET ");
			for (Iterator<String> iterator = allkeyset.iterator(); iterator
					.hasNext();) {
				String key = iterator.next();
				String[] val = onerecordData.get(key).split("____");

				values.put(key, val[0]);

			}
		}
		return values;
	}

	public void extractZipFile(String zipPath, String extractPath)
			throws Exception {

		ZipFile zipFile = new ZipFile(zipPath);
		if (zipFile.isEncrypted()) {
			zipFile.setPassword("prashant");
		}
		zipFile.extractAll(extractPath);

	}

	/**
	 * Delete transaction data from device which have transaction date before 30 days
	 */
	@SuppressLint("SimpleDateFormat")
	public void deleteTransactionData() {
		//String[] tables = getTablenameForDelete();
		try {
			DatabaseSupport db = new DatabaseSupport(context);
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
			Calendar cal = new GregorianCalendar();
			cal.setTime(new Date());
			cal.add(Calendar.DAY_OF_MONTH, -30);
			
			/**
			 * Training Transaction And Attandance
			 */
				List<TrainingTransaction> ttList = db.getTrainingTrainsactionRowByDate(df.format(cal.getTime()));
				if(ttList != null && ttList.size() > 0){
				for(TrainingTransaction tt : ttList) {
					db.deleteRecordFromTrainingTransation(tt.getTraningTransactionId());
				}
				
				/**
				 * UserMasterRelated Data and PowerPlusTransaction
				 */				
				
					List<UserServiceTermRoleData> serviceTermRoleData = new ArrayList<UserServiceTermRoleData>();
					serviceTermRoleData = db.getUserServiceTermRoleRowBiIdDate(df.format(cal.getTime()));					
					if(serviceTermRoleData != null && serviceTermRoleData.size() > 0){
						
						for(UserServiceTermRoleData usrD : serviceTermRoleData) {	
							
							List<UserServiceTermRoleData> serviceTermCurrentRoleData = new ArrayList<UserServiceTermRoleData>();
							serviceTermCurrentRoleData = db.getUserServiceTermRoleRowBiId(usrD.getIuserId(), df.format(new Date()));
							if(serviceTermCurrentRoleData == null || serviceTermCurrentRoleData.size() == 0)
							{
								db.deleteUserMasterRelatedData(usrD.getIuserId());
							}
														
						}
						
						
						
					}
			
				}
				db.close();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void deleteAllShipRelatedData() {
		
		try {
			List<ShipMasterData> shipList = new ArrayList<ShipMasterData>();
			DatabaseSupport  db = new DatabaseSupport(context);
			shipList = db.getShipMasterDeletedRow();
			
			if(shipList != null && shipList.size() > 0){
				L.fv("Ship Record is : "+shipList.size());
				for(ShipMasterData smd : shipList){
					L.fv("Ship Name is : "+smd.getStrShipName());
					if(smd.getFlgDeleted().equalsIgnoreCase("1")){
						db.deleteAllShipRelatedData(smd.getiShipId());
					}
				}
			}
			db.close();
		} catch (Exception e) {
			
			e.printStackTrace();
		}
	}
	
	public boolean deleteDir(File dir) {
		if (dir.isDirectory()) {
			String[] children = dir.list();
			for (int i = 0; i < children.length; i++) {
				boolean success = deleteDir(new File(dir, children[i]));
				if (!success) {
					return false;
				}
			}
		}
		return dir.delete();
	}

	public int cleanProcessDirectory(File dir) {
		if (dir.isDirectory()) {
			String[] children = dir.list();
			for (int i = 0; i < children.length; i++) {
				deleteDir(new File(dir, children[i]));

			}
		}
		if (dir.list() != null) {
			return dir.list().length;
		} else {
			return 0;
		}
	}

	public String getXmlReceivePath() {
		String strprocessPath = "";

		File sourceFile = new File(Environment.getExternalStorageDirectory()
				+ "/TheArk/Import/");
		/*
		 * if (!sourceFile.exists()) { sourceFile.mkdirs(); }
		 */

		return sourceFile.getAbsolutePath();
	}

	public String getXmlBackupPath() {
		String strBackupPath = "";

		return strBackupPath;
	}
	
	@SuppressWarnings("static-access")
	public void setDeviceTime() {
		try {
			if(strDownloadDate != null && !"".equals(strDownloadDate))
			{
				Calendar c = Calendar.getInstance();
				//c.set(2013, 8, 15, 12, 34, 56);
				c.setTimeInMillis(Long.parseLong(strDownloadDate));
				AlarmManager am = (AlarmManager) context.getSystemService(context.ALARM_SERVICE);
				am.setTime(c.getTimeInMillis());
			}
		} catch (Exception e) {
			
			e.printStackTrace();
		}
	}
	
	public String getMacId() {
		String strmacId = "macid";
		TelephonyManager tm = (TelephonyManager) context
				.getSystemService(Context.TELEPHONY_SERVICE);
		strmacId = tm.getDeviceId();
		return strmacId;
	}

}
