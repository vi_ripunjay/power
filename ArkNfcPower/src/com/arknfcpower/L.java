package com.arknfcpower;

import android.content.Context;

import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

public class L {
	static String  tag ="NFC";
    public static void fv(String msg) {
    	Log.println(Log.VERBOSE, tag, msg);
        i(msg);
    }

    public static void fi(Context cx, String eventId, String msg) {
        try {
            Map<String, String> parameters = new HashMap<String, String>();
            parameters.put("class", cx.getClass().getCanonicalName());
            parameters.put("info-msg", msg);
            //
            i(cx.getClass().getCanonicalName() + " : " + msg);
        } catch (Exception e) {
            e.printStackTrace();
            i(msg);
        }
    }

    public static void fw(Context cx, String eventId, String msg) {
        try {
            Map<String, String> parameters = new HashMap<String, String>();
            parameters.put("class", cx.getClass().getCanonicalName());
            parameters.put("warn-msg", msg);
            //
            w(cx.getClass().getCanonicalName() + " : " + msg);
        } catch (Exception e) {
            e.printStackTrace();
            w(msg);
        }
    }

    public static void fe(Context cx, String eventId, String msg) {
        try {
            e(cx.getClass().getCanonicalName() + " : " + msg);
        } catch (Exception e) {
            e.printStackTrace();
            e(msg);
        }
    }

    public static void fe(Context cx, String eventId, Throwable tr) {
        try {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            tr.printStackTrace(pw);
            //
            sw.close();
            pw.close();
            //
            e(tr);
        } catch (Exception e) {
            e.printStackTrace();
            e(tr);
        }
    }

    public static void fwtf(Context cx, String msg) {
        try {
            wtf(cx.getClass().getCanonicalName() + " : " + msg);
        } catch (Exception e) {
            e.printStackTrace();
            wtf(msg);
        }
    }

    public static void fwtf(Context cx, Throwable tr) {
        try {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            tr.printStackTrace(pw);
            //
            sw.close();
            pw.close();
            //
            wtf(tr);
        } catch (Exception e) {
            e.printStackTrace();
            wtf(tr);
        }
    }

    private static void i(String msg) {
        if (!BuildConfig.DEBUG)
            return;
        Log.i(tag, msg);
        logToFile("INFO", msg, null);
    }

    private static void w(String msg) {
        if (!BuildConfig.DEBUG)
            return;
        Log.w(tag, msg);
        logToFile("WARNING", msg, null);
    }

    private static void e(String msg) {
        Log.e(tag, msg);
        logToFile("EXCEPTION", msg, null);
    }

    private static void e(Throwable tr) {
        Log.e(tag, tr.getMessage(), tr);
        logToFile("EXCEPTION", tr.toString(), tr);
    }

    private static void wtf(String msg) {
        Log.println(Log.ASSERT, tag, msg);
        logToFile("WTF", msg, null);
    }

    private static void wtf(Throwable tr) {
        Log.println(Log.ASSERT, tag, tr.toString());
        logToFile("WTF", tr.toString(), tr);
    }

    private static void logToFile(String type, String msg, Throwable tr) {
        try {
            File sdCard = Environment.getExternalStorageDirectory();
            File PinigLogDir = new File(sdCard.getAbsoluteFile(), File.separator + tag + File.separator + "logs");
            if (!PinigLogDir.exists())
                PinigLogDir.mkdirs();
            File PinigLog = new File(PinigLogDir, "nfc.txt");
            if (!PinigLog.exists())
                PinigLog.createNewFile();
            if (null == tr) {
                msg += "\n";
                FileOutputStream fos = new FileOutputStream(PinigLog, true);
                fos.write("\n".getBytes());
                fos.write(msg.getBytes());
                fos.close();
            } else {
                StringWriter sw = new StringWriter();
                sw.append("\n").append(type).append("\n");
                PrintWriter pw = new PrintWriter(sw);
                tr.printStackTrace(pw);
                //
                FileOutputStream fos = new FileOutputStream(PinigLog, true);
                fos.write("\n".getBytes());
                fos.write(sw.toString().getBytes());
                sw.close();
                pw.close();
                fos.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
