package com.arknfcpower;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemSelectedListener;

import com.arknfcpower.db.DatabaseHandler;
import com.arknfcpower.db.DatabaseSupport;
import com.arknfcpower.model.PowerPlusTask;
import com.arknfcpower.model.PowerPlusTransaction;
import com.arknfcpower.model.TabletRoleFunctions;
import com.arknfcpower.model.UserMaster;
import com.arknfcpower.model.UserSelectItem;
import com.arknfcpower.model.UserServiceTermRoleData;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
@SuppressLint("NewApi")
public class Main extends Fragment {
	
	TextView welcomemsg = null;
	TextView enteredScore = null;
	TextView validatedScore = null;
	TextView validatedPonit = null;
	TextView enteredPonit = null;
	TextView validatedTextPonit = null;
	TextView enteredTextPonit = null;
	TextView welcomerank = null;
	ImageView powerUp;
	ImageView lDRPowerUp;
	ImageView turboPowerUp;
	ImageView powerDown;
	ImageView validator;

	
	public Main(){
		}
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
 
        View rootView = inflater.inflate(R.layout.activity_mai, container, false);
    
        welcomemsg = (TextView) rootView.findViewById(R.id.welcomename);
        welcomerank = (TextView) rootView.findViewById(R.id.welcomerank);
        
        enteredScore = (TextView) rootView.findViewById(R.id.enteredscore);
        validatedScore = (TextView) rootView.findViewById(R.id.validatedscore);
        enteredPonit = (TextView) rootView.findViewById(R.id.enteredPoint);
        validatedPonit = (TextView) rootView.findViewById(R.id.validatedPoint);
        enteredTextPonit = (TextView) rootView.findViewById(R.id.enteredTextPoint);
        validatedTextPonit = (TextView) rootView.findViewById(R.id.validatedTextPoint);
			
        powerUp = (ImageView) rootView.findViewById(R.id.imageViewPowerUp);
        lDRPowerUp = (ImageView) rootView.findViewById(R.id.imageViewLeaderPowerUp);
        turboPowerUp = (ImageView) rootView.findViewById(R.id.imageViewTurboPowerUp);
        powerDown = (ImageView) rootView.findViewById(R.id.imageViewPowerDown);
        validator = (ImageView) rootView.findViewById(R.id.imageViewValidator);

        DatabaseSupport db =new DatabaseSupport(getActivity());
        UserMaster user = new UserMaster();
		user = db.getUserMasterSingleRow(
				CardInfo.getUserId(getActivity())).get(0);
		/*String strRoleType = db.getRoleSingleRow(user.getiRoleId())
				.getStrRoleType();*/
		List<TabletRoleFunctions> trFunList = db.getTabletRoleFunctionsRowByRoleId(user.getiRoleId());
		String strRoleType ="Crew";
		if(trFunList != null && trFunList.size() > 0){
			strRoleType = trFunList.get(0).getStrFunctionCode();
		}
		

		if (strRoleType != null
				&& strRoleType.equalsIgnoreCase("Validator")) {
         
			lDRPowerUp.setVisibility(View.VISIBLE);
			validator.setVisibility(View.VISIBLE);
		}else 
			if (strRoleType != null
			&& strRoleType.equalsIgnoreCase("Crew")) {

				lDRPowerUp.setVisibility(View.GONE);
				validator.setVisibility(View.GONE);

			}

        powerUp.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
		
			
				((PowerPlusMain) getActivity()).setTitle("Power Up");
				PowerUp newFragment = new PowerUp();
            	FragmentTransaction transaction = getFragmentManager().beginTransaction();
            	transaction.replace(R.id.frame_container, newFragment);
            	transaction.addToBackStack(null);
            	transaction.commit();
			
			}
		});
        
        lDRPowerUp.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				((PowerPlusMain) getActivity()).setTitle("Leadership Power Up");
				LeadershipPowerUp newFragment = new LeadershipPowerUp();
            	FragmentTransaction transaction = getFragmentManager().beginTransaction();
            	transaction.replace(R.id.frame_container, newFragment);
            	transaction.addToBackStack(null);
            	transaction.commit();	
			}
		});
        
        turboPowerUp.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				((PowerPlusMain) getActivity()).setTitle("Turbo Power Up");
				TurboPowerUp newFragment = new TurboPowerUp();
            	FragmentTransaction transaction = getFragmentManager().beginTransaction();
            	transaction.replace(R.id.frame_container, newFragment);
            	transaction.addToBackStack(null);
            	transaction.commit();					
			}
		});
        
        powerDown.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				((PowerPlusMain) getActivity()).setTitle("Power Down");
				PowerDown newFragment = new PowerDown();
            	FragmentTransaction transaction = getFragmentManager().beginTransaction();
            	transaction.replace(R.id.frame_container, newFragment);
            	transaction.addToBackStack(null);
            	transaction.commit();					
				
			}
		});
        
        validator.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				((PowerPlusMain) getActivity()).setTitle("Validate Score");
				Validator newFragment = new Validator();
            	FragmentTransaction transaction = getFragmentManager().beginTransaction();
            	transaction.replace(R.id.frame_container, newFragment);
            	transaction.addToBackStack(null);
            	transaction.commit();					
				
			}
		});

        
        UserMaster um = new UserMaster();
        List<UserMaster> umList = db.getUserMasterSingleRow(CardInfo.getUserId(getActivity()));
        um = umList.get(0);
        
		Date curDate = new Date();
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd");		
		List<UserServiceTermRoleData> serviceTermRoleData = new ArrayList<UserServiceTermRoleData>();
		serviceTermRoleData = db.getUserServiceTermRoleRowBiId(um.getiUserId(),format.format(curDate));
		UserServiceTermRoleData userSrvTrmRlData = null;
		if(serviceTermRoleData != null && serviceTermRoleData.size() > 0){
			userSrvTrmRlData = new UserServiceTermRoleData();
			userSrvTrmRlData = serviceTermRoleData.get(0);
		}  
        
        
        welcomemsg.setText("Welcome  "+ um.getStrFirstName() + " " +um.getStrLastName());
        welcomerank.setText("("+db.getRoleSingleRow(um.getiRoleId()).getStrRole()+") ");
        
        
        enteredScore.setText("Logged in score ");
        enteredPonit.setText(db.getEnteredScore(um.getiUserId(),(userSrvTrmRlData != null ? userSrvTrmRlData.getIuserServiceTermId() : null)));
     //   enteredTextPonit.setText("Points");
        validatedScore.setText("Validated score ");
        validatedPonit.setText(db.getValidatedScore(um.getiUserId(),(userSrvTrmRlData != null ? userSrvTrmRlData.getIuserServiceTermId() : null)));
     //   validatedTextPonit.setText("Points");
        
        db.close();        
        
        
        return rootView;
    }
}
