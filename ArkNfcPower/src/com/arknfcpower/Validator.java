package com.arknfcpower;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Fragment;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.arknfc.adapter.ValidatorAdapter;
import com.arknfcpower.db.DatabaseSupport;
import com.arknfcpower.model.PowerPlusCategory;
import com.arknfcpower.model.PowerPlusTransaction;
import com.arknfcpower.model.UserMaster;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
@SuppressLint("NewApi")
public class Validator extends Fragment {
	ListView lv;
	TextView listItemText;
	TextView listItemText2;
	TextView validator_score;
	ImageView imageView;

	List<PowerPlusTransaction> powerPlusTransLeaderShipList;
	public static String strPowerPlustaskId;
	PowerPlusCategory ppCat;

	public Validator() {
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.activity_validator,
				container, false);

		lv = (ListView) rootView.findViewById(R.id.validatorlist);

		DatabaseSupport db = new DatabaseSupport(getActivity());

		List<PowerPlusCategory> ppcList = new ArrayList<PowerPlusCategory>();
		ppCat = new PowerPlusCategory();
		// ppcList = db.getPowerPlusCategoryRowByTitle("Leadership Power");
		ppcList = db.getPowerPlusCategoryRowByCatCode("leadershippower");
		if (ppcList != null && ppcList.size() > 0) {
			ppCat = ppcList.get(0);
		}

		ArrayList<PowerPlusTransaction> list = (ArrayList<PowerPlusTransaction>) db
				.getPowerPlusTransactionForValidate(
						CardInfo.getUserId(getActivity()),
						(ppCat.getiPowerPlusCategoryId() != null ? ppCat
								.getiPowerPlusCategoryId() : "0002"));

		if (list == null)
			list = new ArrayList<PowerPlusTransaction>();

		PowerPlusTransaction pptForInfo = new PowerPlusTransaction();
		pptForInfo.setStrTitle(getResources().getString(R.string.validatetext));

		list.add(0, pptForInfo);

		ValidatorAdapter adapter = new ValidatorAdapter(list, getActivity());

		// handle listview and assign adapter

		lv.setAdapter(adapter);
		db.close();

		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {

				// ListView Clicked item index
				@SuppressWarnings("unused")
				int itemPosition = position;

				listItemText = (TextView) view.findViewById(R.id.description);
				listItemText2 = (TextView) view.findViewById(R.id.namerank);
				validator_score = (TextView) view
						.findViewById(R.id.validator_score);

				imageView = ((ImageView) view.findViewById(R.id.selection));
				if (position > 0) {
					DatabaseSupport db = new DatabaseSupport(getActivity());
					// ListView Clicked item value
					PowerPlusTransaction itemValue = (PowerPlusTransaction) lv
							.getItemAtPosition(position);
					if (db.getScoreInEnterModeForValidateWithTrans(
							itemValue.getiPowerPlusTransactionId(),
							itemValue.getiUserId(),
							itemValue.getiPowerPlusTaskId(), "Validated") > 0) {
						itemValue.setFlgStatus("Rejected");
						itemValue.setStrApproverId(CardInfo
								.getUserId(getActivity()));

						imageView.setVisibility(View.VISIBLE);
						imageView.setImageResource(R.drawable.reject_icon);
						imageView.invalidate();

						listItemText.setTextColor(getResources().getColor(
								R.color.white));
						listItemText2.setTextColor(getResources().getColor(
								R.color.white));
						validator_score.setTextColor(getResources().getColor(
								R.color.white));

						view.setBackgroundColor(Color.parseColor("#ff0000")); // red

					} else if (db.getScoreInEnterModeForValidateWithTrans(
							itemValue.getiPowerPlusTransactionId(),
							itemValue.getiUserId(),
							itemValue.getiPowerPlusTaskId(), "Rejected") > 0) {
						itemValue.setFlgStatus("Entered");
						itemValue.setStrApproverId(CardInfo
								.getUserId(getActivity()));

						imageView.setVisibility(View.GONE);

						listItemText.setTextColor(getResources().getColor(
								R.color.black));
						listItemText2.setTextColor(getResources().getColor(
								R.color.black));
						validator_score.setTextColor(getResources().getColor(
								R.color.black));

						view.setBackgroundColor(Color.parseColor("#ffffff")); // white

					} else if (db.getScoreInEnterModeForValidateWithTrans(
							itemValue.getiPowerPlusTransactionId(),
							itemValue.getiUserId(),
							itemValue.getiPowerPlusTaskId(), "Entered") > 0) {
						itemValue.setFlgStatus("Validated");
						itemValue.setStrApproverId(CardInfo
								.getUserId(getActivity()));

						imageView.setVisibility(View.VISIBLE);
						imageView.setImageResource(R.drawable.select_icon);
						imageView.invalidate();

						listItemText.setTextColor(getResources().getColor(
								R.color.white));
						listItemText2.setTextColor(getResources().getColor(
								R.color.white));
						validator_score.setTextColor(getResources().getColor(
								R.color.white));

						view.setBackgroundColor(Color.parseColor("#06dc1a"));
					}

					itemValue.setFlgIsDirty("1");
					Date curDate = new Date();
					DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
					itemValue.setDtUpdated(format.format(curDate));
					// L.fv("dtUpdated : "+itemValue.getDtUpdated());
					// L.fv("Approver id : "+itemValue.getStrApproverId());

					L.fv("Status : " + itemValue.getFlgStatus());

					db.close();
					DatabaseSupport db1 = new DatabaseSupport(getActivity());
					db1.UpdatePowerPlusTransactionRow(itemValue);
					db1.close();

					if (itemValue.getCetogoryId().equalsIgnoreCase(
							ppCat.getiPowerPlusCategoryId() != null ? ppCat
									.getiPowerPlusCategoryId() : "0002")) {
						/**
						 * This is need for rejected and entered mode
						 */
						if (!(strPowerPlustaskId != null && strPowerPlustaskId
								.equalsIgnoreCase(itemValue
										.getiPowerPlusTaskId()))) {
							powerPlusTransLeaderShipList = new ArrayList<PowerPlusTransaction>();
							db1 = new DatabaseSupport(getActivity());
							powerPlusTransLeaderShipList = db1
									.getPowerPlusTransactionByTaskIdAndStatus(
											itemValue.getiPowerPlusTaskId(),
											"Entered");
							db1.close();

							L.fv("powerPlusTransLeaderShipList test : "
									+ (powerPlusTransLeaderShipList != null ? powerPlusTransLeaderShipList
											.size() : "no data"));
						}

						db1 = new DatabaseSupport(getActivity());

						L.fv("ledership====== : "
								+ itemValue.getiPowerPlusTaskId());

						for (PowerPlusTransaction ppTran : powerPlusTransLeaderShipList) {
							ppTran.setFlgStatus(itemValue.getFlgStatus());
							ppTran.setFlgIsDirty("1");
							ppTran.setDtUpdated(itemValue.getDtUpdated());
							/**
							 * Set approver Id.
							 */
							ppTran.setStrApproverId(CardInfo
									.getUserId(getActivity()));

							L.fv("Task id : " + ppTran.getiPowerPlusTaskId());
							L.fv("Approver id ppTran: "
									+ itemValue.getStrApproverId());
							db1.UpdatePowerPlusTransactionRow(ppTran);
						}

					} else {

						// db1.UpdatePowerPlusTransactionRow(itemValue);
					}

					strPowerPlustaskId = itemValue.getiPowerPlusTaskId();
					db1.close();

				}
			}
		});

		return rootView;
	}

	public List<UserMaster> getLeadershipUser(String receivedById) {
		DatabaseSupport db = new DatabaseSupport(getActivity());
		/**
		 * StringBuffer roleIds = new StringBuffer("'0'"); List<RoleData>
		 * roleDataList = db.getRoleRow("Validator"); if (roleDataList != null
		 * && roleDataList.size() > 0) { for (RoleData roleData : roleDataList)
		 * { roleIds.append(",'" + roleData.getiRoleId() + "'"); } }
		 * 
		 * List<UserMaster> umList =
		 * db.getUserMasterRowByRoleIds(roleIds.toString());
		 */
		List<UserMaster> umList = db.getUserMasterRowByRoleIds(receivedById);

		return umList;
	}

}