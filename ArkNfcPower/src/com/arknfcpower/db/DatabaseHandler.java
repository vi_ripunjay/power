package com.arknfcpower.db;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.arknfcpower.model.Score;
import com.arknfcpower.model.User;
import com.arknfcpower.model.UserScore;

public class DatabaseHandler extends SQLiteOpenHelper {
	private static final int DATABASE_VERSION = 1;
	private static final String DATABASE_NAME = "arknfc";
	// table user and its field
	private static final String TABLE_USER = "user";
	private static final String KEY_ID = "id";
	private static final String KEY_UID = "uid";
	private static final String KEY_NAME = "name";
	private static final String KEY_RANK = "rank";
	private static final String KEY_SHIP = "ship";
	private static final String KEY_ROLE = "role";

	// table safety_score and its field
	private static final String TABLE_SCORE = "scores";
	private static final String KEY_SID = "s_id";
	private static final String KEY_TYPE = "s_type";
	private static final String KEY_DESCRIPTION = "description";
	private static final String KEY_VALUE = "value";

	// table safety_score and its field
	private static final String TABLE_USER_SCORE = "user_score";
	private static final String KEY_USER_ID = "u_id";
	private static final String KEY_SCORE_ID = "score_id";
	private static final String KEY_U_TYPE = "score_type";
	private static final String KEY_U_STATUS = "score_status";
	private static final String KEY_U_VALUE = "u_value";

	public DatabaseHandler(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
		// 3rd argument to be passed is CursorFactory instance
	}

	// Creating Tables
	@Override
	public void onCreate(SQLiteDatabase db) {
		String CREATE_USER_TABLE = "CREATE TABLE " + TABLE_USER + "(" + KEY_ID
				+ " INTEGER PRIMARY KEY," + KEY_UID + " TEXT," + KEY_NAME
				+ " TEXT," + KEY_RANK + " TEXT," + KEY_SHIP + " TEXT,"
				+ KEY_ROLE + " TEXT" + ")";

		String CREATE_SS_TABLE = "CREATE TABLE " + TABLE_SCORE + "(" + KEY_ID
				+ " INTEGER PRIMARY KEY," + KEY_SID + " TEXT," + KEY_TYPE
				+ " TEXT," + KEY_DESCRIPTION + " TEXT," + KEY_VALUE + " TEXT"
				+ ")";

		String CREATE_USER_SCORE_TABLE = "CREATE TABLE " + TABLE_USER_SCORE
				+ "(" + KEY_ID + " INTEGER PRIMARY KEY," + KEY_USER_ID
				+ " TEXT," + KEY_SCORE_ID + " TEXT," + KEY_U_TYPE + " TEXT,"
				+ KEY_U_STATUS + " TEXT," + KEY_U_VALUE + " TEXT" + ")";

		db.execSQL(CREATE_USER_TABLE);
		db.execSQL(CREATE_SS_TABLE);
		db.execSQL(CREATE_USER_SCORE_TABLE);

	}

	// Upgrading database
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// Drop older table if existed
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_SCORE);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER_SCORE);

		// Create tables again
		onCreate(db);
	}

	@Override
	public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_SCORE);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER_SCORE);

		// Create tables again
		onCreate(db);

	}

	public int updateCurrentUserScore(UserScore uc) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();

		System.out.println("uc.get_id() " + uc.get_id());
		System.out.println("uc.get_id() " + uc.getU_id());
		System.out.println("uc.getScore_id() " + uc.getScore_id());
		System.out.println("uc.getScore_type() " + uc.getScore_type());
		System.out.println("uc.getScore_status() " + uc.getScore_status());
		System.out.println("uc.getU_value() " + uc.getU_value());

		values.put(KEY_ID, uc.get_id());
		values.put(KEY_USER_ID, uc.getU_id());
		values.put(KEY_SCORE_ID, uc.getScore_id());
		values.put(KEY_U_TYPE, uc.getScore_type());
		values.put(KEY_U_STATUS, uc.getScore_status());
		values.put(KEY_U_VALUE, uc.getU_value());

		// updating row

		String whereClause = KEY_ID + " =  '" + uc.get_id() + "'";

		return db.update(TABLE_USER_SCORE, values, whereClause, null);

	}

	// code to add the new user
	public void addUserRow(User user) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_UID, user.get_uid());
		values.put(KEY_NAME, user.getName());
		values.put(KEY_RANK, user.get_rank());
		values.put(KEY_SHIP, user.get_ship());
		values.put(KEY_ROLE, user.getRole());

		// Inserting Row
		db.insert(TABLE_USER, null, values);
		// 2nd argument is String containing nullColumnHack
		db.close(); // Closing database connection
	}

	// code to add the new safety score row
	public void addScoreRow(Score ss) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_SID, ss.get_sid());
		values.put(KEY_TYPE, ss.get_sType());
		values.put(KEY_DESCRIPTION, ss.get_description());
		values.put(KEY_VALUE, ss.get_s_value());

		// Inserting Row
		db.insert(TABLE_SCORE, null, values);
		// 2nd argument is String containing nullColumnHack
		db.close(); // Closing database connection
	}

	// code to add the new contact
	public void addUserScoreRow(UserScore user) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_SCORE_ID, user.getScore_id());
		values.put(KEY_USER_ID, user.getU_id());
		values.put(KEY_U_TYPE, user.getScore_type());
		values.put(KEY_U_STATUS, user.getScore_status());
		values.put(KEY_U_VALUE, user.getU_value());

		// Inserting Row
		db.insert(TABLE_USER_SCORE, null, values);
		// 2nd argument is String containing nullColumnHack
		db.close(); // Closing database connection
	}

	// code to get all contacts in a list view
	public List<User> getAllUser() {
		List<User> contactList = new ArrayList<User>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + TABLE_USER;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				User contact = new User();
				contact.setID(Integer.parseInt(cursor.getString(0)));
				contact.set_uid(cursor.getString(1));
				contact.setName(cursor.getString(2));
				contact.set_rank(cursor.getString(3));
				contact.set_ship(cursor.getString(4));
				contact.setRole(cursor.getString(5));

				// Adding contact to list
				contactList.add(contact);
			} while (cursor.moveToNext());
		}

		// return contact list
		return contactList;
	}

	// code to get all contacts in a list view
	public List<Score> getAllScore() {
		List<Score> pendingList = new ArrayList<Score>();
		// Select All Query

		String selectQuery = "SELECT  * FROM " + TABLE_SCORE;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				Score pl = new Score();
				pl.set_id(Integer.parseInt(cursor.getString(0)));
				pl.set_sid(cursor.getString(1));
				pl.set_sType(cursor.getString(2));
				pl.set_description(cursor.getString(3));
				pl.set_s_value(cursor.getString(4));

				// Adding contact to list
				pendingList.add(pl);
			} while (cursor.moveToNext());
		}

		// return contact list
		return pendingList;
	}

	// code to get all contacts in a list view
	public String getRole(String uid) {
		// Select All Query
		System.out.println("hello role");
		String role = "";
		try {
			String selectQuery = "SELECT " + KEY_ROLE + "  FROM " + TABLE_USER
					+ " WHERE " + KEY_UID + " = " + "'" + uid + "'";
			System.out.println("Query :" + selectQuery);
			SQLiteDatabase db = this.getWritableDatabase();
			Cursor cursor = db.rawQuery(selectQuery, null);
			if (cursor.getCount() > 0) {

				cursor.moveToFirst();
				role = cursor.getString(cursor.getColumnIndex(KEY_ROLE));
			}
			db.close();
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("Exception :" + e);
		}
		// return contact list
		System.out.println("Role :" + role);
		return role;

	}

	// code to get all contacts in a list view
	public String getDescription(String sid) {
		// Select All Query
		System.out.println("i m here ");
		String des = "";
		try {
			String selectQuery = "SELECT " + KEY_DESCRIPTION + "  FROM "
					+ TABLE_SCORE + " WHERE " + KEY_SID + " = " + "'" + sid
					+ "'";
			System.out.println("Query :" + selectQuery);
			SQLiteDatabase db = this.getWritableDatabase();
			Cursor cursor = db.rawQuery(selectQuery, null);
			if (cursor.getCount() > 0) {

				cursor.moveToFirst();
				des = cursor.getString(cursor.getColumnIndex(KEY_DESCRIPTION));
			}
			db.close();
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("Exception :" + e);
		}
		System.out.println("des :" + des);
		// return contact list
		return des;

	}

	// code to get all contacts in a list view
	public String getRank(String uid) {
		// Select All Query
		String rank = "";
		try {
			String selectQuery = "SELECT " + KEY_RANK + "  FROM " + TABLE_USER
					+ " WHERE " + KEY_UID + " = " + "'" + uid + "'";
			System.out.println("Rank Query :" + selectQuery);
			SQLiteDatabase db = this.getWritableDatabase();
			Cursor cursor = db.rawQuery(selectQuery, null);
			if (cursor.getCount() > 0) {

				cursor.moveToFirst();
				rank = cursor.getString(cursor.getColumnIndex(KEY_RANK));
			}
			db.close();
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("Exception :" + e);
		}
		// return contact list
		System.out.println("Rank :" + rank);
		return rank;

	}

	public int getScoreInEnterMode(String uid, String sid, String status) {
		// Select All Query
		int count = 0;
		try {
			String selectQuery = "SELECT " + KEY_U_VALUE + "  FROM "
					+ TABLE_USER_SCORE + " WHERE " + KEY_USER_ID + " = " + "'"
					+ uid + "' and  " + KEY_SCORE_ID + " = '" + sid + "' and  "
					+ KEY_U_STATUS + " = '" + status + "'";
			System.out.println("Entered Query :" + selectQuery);
			SQLiteDatabase db = this.getWritableDatabase();
			Cursor cursor = db.rawQuery(selectQuery, null);

			if (cursor.moveToFirst()) {
				do {

					count++;
				} while (cursor.moveToNext());

			}
			db.close();
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("Exception :" + e);
		}
		// return contact list
		System.out.println("return count vALUE :" + count);
		return count;

	} // code to get all contacts in a list view

	public UserScore getUserScoreById(String uid, String sid, String status) {
		// Select All Query
		int count = 0;
		UserScore us = new UserScore();
		try {
			String selectQuery = "SELECT *  FROM " + TABLE_USER_SCORE
					+ " WHERE " + KEY_USER_ID + " = " + "'" + uid + "' and  "
					+ KEY_SCORE_ID + " = '" + sid + "' and  " + KEY_U_STATUS
					+ " = '" + status + "'";
			System.out.println("Entered Query :" + selectQuery);
			SQLiteDatabase db = this.getWritableDatabase();
			Cursor cursor = db.rawQuery(selectQuery, null);

			if (cursor.moveToFirst()) {
				do {

					us.set_id(Integer.parseInt(cursor.getString(0)));
					us.setU_id(cursor.getString(1));
					us.setScore_id(cursor.getString(2));

					us.setScore_type(cursor.getString(3));
					us.setScore_status(cursor.getString(4));
					us.setU_value(cursor.getString(5));

				} while (cursor.moveToNext());

			}
			db.close();
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("Exception :" + e);
		}
		// return contact list
		System.out.println("return count vALUE :" + count);
		return us;

	} // code to get all contacts in a list view

	// code to get all contacts in a list view
	public String getEnteredScore(String uid) {
		// Select All Query

		int enteredScore = 0;
		int enteredNegativeScore = 0;
		try {
			String selectQuery = "SELECT " + KEY_U_VALUE + "  FROM "
					+ TABLE_USER_SCORE + " WHERE " + KEY_USER_ID + " = " + "'"
					+ uid + "' and " + KEY_U_TYPE + "='P' and " + KEY_U_STATUS
					+ " != 'Rejected'";
			System.out.println("Entered Query :" + selectQuery);
			SQLiteDatabase db = this.getWritableDatabase();
			Cursor cursor = db.rawQuery(selectQuery, null);

			if (cursor.moveToFirst()) {
				do {
					enteredScore = enteredScore
							+ Integer.parseInt(cursor.getString(cursor
									.getColumnIndex(KEY_U_VALUE)));
				} while (cursor.moveToNext());

			}

			String selectNegQuery = "SELECT " + KEY_U_VALUE + "  FROM "
					+ TABLE_USER_SCORE + " WHERE " + KEY_USER_ID + " = " + "'"
					+ uid + "' and " + KEY_U_TYPE + "='N' and " + KEY_U_STATUS
					+ " != 'Rejected'";
			System.out.println("Entered Neg Query :" + selectNegQuery);

			Cursor cursorNeg = db.rawQuery(selectNegQuery, null);

			if (cursorNeg.moveToFirst()) {
				do {
					enteredNegativeScore = enteredNegativeScore
							+ Integer.parseInt(cursorNeg.getString(cursorNeg
									.getColumnIndex(KEY_U_VALUE)));
				} while (cursorNeg.moveToNext());

			}

			db.close();
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("Exception :" + e);
		}

		enteredScore = enteredScore - enteredNegativeScore;
		// return contact list
		System.out.println("return ENTERED vALUE :" + enteredScore);
		return String.valueOf(enteredScore);

	} // code to get all contacts in a list view

	public String getValidatedScore(String uid) {
		//String rank = "";
		System.out.println("in validated");
		int validatededScore = 0;
		int negativeValidatedScore = 0;
		try {
			String selectQuery = "SELECT " + KEY_U_VALUE + "  FROM "
					+ TABLE_USER_SCORE + " WHERE " + KEY_USER_ID + " = " + "'"
					+ uid + "' and " + KEY_U_STATUS + " = 'Validated' and "
					+ KEY_U_TYPE + "='P'";
			System.out.println("validated Query :" + selectQuery);
			SQLiteDatabase db = this.getWritableDatabase();
			Cursor cursor = db.rawQuery(selectQuery, null);
			if (cursor.moveToFirst()) {
				do {
					validatededScore = validatededScore
							+ Integer.parseInt(cursor.getString(cursor
									.getColumnIndex(KEY_U_VALUE)));
				} while (cursor.moveToNext());

			}

			/**
			 * For Negative score N
			 */

			String negQuery = "SELECT " + KEY_U_VALUE + "  FROM "
					+ TABLE_USER_SCORE + " WHERE " + KEY_USER_ID + " = " + "'"
					+ uid + "' and " + KEY_U_STATUS + " = 'Validated' and "
					+ KEY_U_TYPE + "='N'";
			System.out.println("negQuery :" + negQuery);
			Cursor cursorNeg = db.rawQuery(negQuery, null);
			if (cursorNeg.moveToFirst()) {
				do {
					negativeValidatedScore = negativeValidatedScore
							+ Integer.parseInt(cursorNeg.getString(cursorNeg
									.getColumnIndex(KEY_U_VALUE)));
				} while (cursorNeg.moveToNext());

			}

			db.close();
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("Exception :" + e);
		}

		validatededScore = validatededScore - negativeValidatedScore;
		System.out.println("return validated vALUE :" + validatededScore);
		// return contact list
		return String.valueOf(validatededScore);

	}

	public String getUser(String uid) {
		// Select All Query

		String user = "";
		try {
			String selectQuery = "SELECT  name  FROM " + TABLE_USER + " WHERE "
					+ KEY_UID + " = " + "'" + uid + "'";
			System.out.println("Query :" + selectQuery);
			SQLiteDatabase db = this.getWritableDatabase();
			Cursor cursor = db.rawQuery(selectQuery, null);
			if (cursor.getCount() > 0) {
				cursor.moveToFirst();
				user = cursor.getString(cursor.getColumnIndex(KEY_NAME));
			}
			db.close();
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("Exception :" + e);
		}
		// return contact list
		System.out.println("User " + user);
		return user;

	}

	public List<Score> getAllPositiveScore() {
		List<Score> pendingList = new ArrayList<Score>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + TABLE_SCORE + " WHERE "
				+ KEY_TYPE + " = " + "'" + "P" + "'";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				Score pl = new Score();
				pl.set_id(Integer.parseInt(cursor.getString(0)));
				pl.set_sid(cursor.getString(1));
				pl.set_sType(cursor.getString(2));
				pl.set_description(cursor.getString(3));
				pl.set_s_value(cursor.getString(4));

				// Adding contact to list
				pendingList.add(pl);
			} while (cursor.moveToNext());
		}

		// return contact list
		return pendingList;
	}

	public List<UserScore> getValidatedUserScore(String uid) {
		List<UserScore> pendingList = new ArrayList<UserScore>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + TABLE_USER_SCORE + " WHERE "
				+ KEY_U_STATUS + " = 'Entered' and " + KEY_USER_ID + " != '"
				+ uid + "'";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				UserScore pl = new UserScore();
				pl.set_id(Integer.parseInt(cursor.getString(0)));
				pl.setU_id(cursor.getString(1));
				pl.setScore_id(cursor.getString(2));

				pl.setScore_type(cursor.getString(3));
				pl.setScore_status(cursor.getString(4));
				pl.setU_value(cursor.getString(5));

				// Adding contact to list
				pendingList.add(pl);
			} while (cursor.moveToNext());
		}

		// return contact list
		return pendingList;
	}

	public List<Score> getAllNegitiveScore() {
		List<Score> pendingList = new ArrayList<Score>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + TABLE_SCORE + " WHERE "
				+ KEY_TYPE + " = " + "'" + "N" + "'";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				Score pl = new Score();
				pl.set_id(Integer.parseInt(cursor.getString(0)));
				pl.set_sid(cursor.getString(1));
				pl.set_sType(cursor.getString(2));
				pl.set_description(cursor.getString(3));
				pl.set_s_value(cursor.getString(4));

				// Adding contact to list
				pendingList.add(pl);
			} while (cursor.moveToNext());
		}

		// return contact list
		return pendingList;
	}

	public void deleteContact(User contact) {
		SQLiteDatabase db = this.getWritableDatabase();
		db.delete(TABLE_USER, KEY_ID + " = ?",
				new String[] { String.valueOf(contact.getID()) });
		db.close();
	}

	// Getting contacts Count
	public int getContactsCount() {
		String countQuery = "SELECT  * FROM " + TABLE_USER;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(countQuery, null);
		cursor.close();

		// return count
		return cursor.getCount();
	}

}