package com.arknfcpower.db;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.arknfc.adapter.UserServiceTermDateComparator;
import com.arknfcpower.L;
import com.arknfcpower.model.CardTypeData;
import com.arknfcpower.model.ModelData;
import com.arknfcpower.model.PowerPlusCategory;
import com.arknfcpower.model.PowerPlusPeriodicity;
import com.arknfcpower.model.PowerPlusRoleRankCategory;
import com.arknfcpower.model.PowerPlusTask;
import com.arknfcpower.model.PowerPlusTransaction;
import com.arknfcpower.model.RoleData;
import com.arknfcpower.model.ShipMasterData;
import com.arknfcpower.model.SyncHistory;
import com.arknfcpower.model.TabletRoleFunctions;
import com.arknfcpower.model.TenantData;
import com.arknfcpower.model.TraingAttendance;
import com.arknfcpower.model.TrainingData;
import com.arknfcpower.model.TrainingTransaction;
import com.arknfcpower.model.UserMaster;
import com.arknfcpower.model.UserNfcData;
import com.arknfcpower.model.UserServiceTermData;
import com.arknfcpower.model.UserServiceTermRoleData;

public class DatabaseSupport extends SQLiteOpenHelper {
	
	
	public static final int DATABASE_VERSION = 1;
	public static final String DATABASE_NAME = "nfcpower";
	// table user and its field
	public static final String KEY_ID = "id";

	// Table TENANT and its field
	public static final String TABLE_TENANT = "Tenant";
	public static final String KEY_TENANT_ID = "iTenantId";
	public static final String KEY_STR_COMPANY_NAME = "strCompanyName";

	// Table Model and its field
	public static final String TABLE_MODEL = "Model";
	public static final String KEY_MODEL_ID = "strModelId";
	public static final String KEY_MODEL_NAME = "strModelName";

	// Table SHIP_MASTER and its field
	public static final String TABLE_SHIP_MASTER = "ShipMaster";
	public static final String KEY_SHIP_ID = "iShipId";
	public static final String KEY_SOCIETY_ID = "iClassificationSocietyId";
	public static final String KEY_SHIP_TYPE_ID = "iShipTypeId";
	public static final String KEY_SHIP_NAME = "strShipName";
	public static final String KEY_S_DESCRIPTION = "strDescription";
	public static final String KEY_DT_CREATED = "dtCreated";
	public static final String KEY_DT_UPDATED = "dtUpdated";
	public static final String KEY_FLG_STATUS = "flgStatus";
	public static final String KEY_FLG_DELETED = "flgDeleted";
	public static final String KEY_FLG_IS_DIRTY = "flgIsDirty";
	public static final String KEY_RULE_LIST_ID = "iRuleListId";
	public static final String KEY_SHIP_IMO_NUMBER = "iShipIMONumber";
	public static final String KEY_STR_FLAG = "strFlag";
	public static final String KEY_LOGO = "strLogo";
	public static final String KEY_FILE_SIZE = "fileSize";
	public static final String KEY_FILE_NAME = "strFileName";
	public static final String KEY_FILE_PATH = "strFilePath";
	public static final String KEY_FILE_TYPE = "strFileType";
	public static final String KEY_SHIP_CODE = "strShipCode";

	// Table ROLE and its field
	public static final String TABLE_ROLE = "Roles";
	public static final String KEY_ROLE_ID = "iRoleId";
	public static final String KEY_ROLE = "strRole";
	public static final String KEY_STR_TYPE = "strType";
	public static final String KEY_TXT_DESCRIPTION = "txtDescription";
	public static final String KEY_RANK_PRIORITY = "iRankPriority";
	public static final String KEY_STR_ROLE_TYPE = "strRoleType"; // PICK FROM
																	// TENENT

	// Table UserMaster and its field
	public static final String TABLE_USER_MASTER = "UserMaster";
	public static final String KEY_iUSER_ID = "iUserId";
	public static final String KEY_USER_NAME = "strUserName";
	public static final String KEY_USER_PASSWORD = "strPassword";
	public static final String KEY_FIRST_NAME = "strFirstName";
	public static final String KEY_LAST_NAME = "strLastName";
	public static final String KEY_DATE_OF_BIRTH = "dtDateOfBirth";
	public static final String KEY_EMAIL = "strEmail";
	public static final String KEY_SHIP_HOLIDAY_LIST_ID = "iShipHolidayListId";
	public static final String KEY_MIN_WORK_HOUR_WEEK_DAYS = "fltMinWorkHourWeekDays";
	public static final String KEY_MIN_WORK_HOUR_SATUREDAY = "fltMinWorkHourSaturdays";
	public static final String KEY_MIN_WORK_HOUR_SUNDAY = "fltMinWorkHourSundays";
	public static final String KEY_MIN_WORK_HOUR_HOLIDAY = "fltMinWorkHourHolidays";
	public static final String KEY_FLTOT_INCLUDED_IN_WAGE = "fltOTIncludedInWage";
	public static final String KEY_FLTOT_RATE_PER_HOUR = "fltOTRatePerHour";
	public static final String KEY_FLG_IS_OVER_TIME_ENABLED = "flgIsOverTimeEnabled";
	public static final String KEY_FLG_IS_WATCHKEEPER = "flgIsWatchkeeper";
	public static final String KEY_FAX_NUMBER = "faxNumber";
	public static final String KEY_HAND_PHONE = "handPhone";
	public static final String KEY_LANDLINE_NUMBER = "landLineNumber";
	public static final String KEY_PAN_NUMBER = "panNumber";
	public static final String KEY_PIN_CODE = "pinCode";
	public static final String KEY_ADDRESS_FIRST = "addressFirst";
	public static final String KEY_ADDRESS_SECOND = "addressSecond";
	public static final String KEY_ADDRESS_THIRD = "addressThird";
	public static final String KEY_CITY = "city";
	public static final String KEY_STATE = "state";
	public static final String KEY_COUNTRY_ID = "iCountryId";
	
	//new field
	public static final String KEY_EMPLOYEE_NO = "strEmployeeNo";
	public static final String KEY_MIDDLE_NAME = "strMiddleName";

	// Table UserServiceTerm and its field
	public static final String TABLE_USER_SERVICE_TERM = "UserServiceTerm";
	public static final String KEY_USER_SERVICE_TERM_ID = "iUserServiceTermId";
	public static final String KEY_DT_TERM_FROM = "dtTermFrom";
	public static final String KEY_DT_TERM_TO = "dtTermTo";

	// Table UserServiceTermRole and its field
	public static final String TABLE_USER_SERVICE_TERM_ROLE = "UserServiceTermRole";
	public static final String KEY_USER_SERVICE_TERM_ROLE_ID = "strUserServiceTermRoleId";
	public static final String POWER_PLUS_SCORE = "powerPlusScore";
	public static final String FIELD_ONE = "strField1";
	public static final String FIELD_TWO = "strField2";

	// Table CARD TYPE and its field
	public static final String TABLE_CARD_TYPE = "CardType";
	public static final String KEY_CARD_ID = "strCardId";
	public static final String KEY_CARD_TYPE = "strCardType";

	// Table UserNfc and its field
	public static final String TABLE_USER_NFC = "UserNfc";

	public static final String KEY_NFC_CARD_ID = "nfcCardId";
	public static final String KEY_NFC_CARD_TYPE = "strCardType";

	public static final String KEY_FLG_DIRTY = "flgDirty";

	// PowerPlusCategory Table

	public static final String TABLE_POWER_PLUS_CATEGOEY = "PowerPlusCategory";

	public static final String KEY_I_POWER_PLUS_CATEGORY_ID = "iPowerPlusCategoryId";
	public static final String KEY_I_SEQUENCE = "iSequence";
	public static final String KEY_STR_DESCRIPTION = "strDescription";
	public static final String KEY_STR_TITLE = "strTitle"; // PICK FROM TENENT
	public static final String KEY_STR_CATEGORY_CODE = "strCategoryCode";

	//PowerPlusPeriodicity Table

	// Table PowerPlusPeriodicity and its field
	public static final String TABLE_POWER_PLUS_PERIODICITY = "PowerPlusPeriodicity";

	public static final String KEY_I_POWER_PLUS_PERIODICITY_ID = "iPowerPlusPeriodicityId";
	public static final String KEY_I_INTERVAL = "iInterval";
	public static final String KEY_I_TIMES = "iTimes";
	public static final String KEY_STR_PATTERN = "strPattern";

	//PowerPlusTask Table

	public static final String TABLE_POWER_PLUS_TASK = "PowerPlusTask";

	public static final String KEY_I_POWER_PLUS_TASK_ID = "iPowerPlusTaskId";
	public static final String KEY_APPROVED_BY = "approvedBy";
	public static final String KEY_FLG_IS_ALL_RECEIVE = "flgIsAllReceive";
	public static final String KEY_POWER_POINTS = "powerPoints";
	public static final String KEY_PROVIDED_BY = "providedBy";
	public static final String KEY_RECEIVED_BY = "receivedBy";
	public static final String KEY_STR_ADDITIONAL_ONE = "strAdditionalOne";
	public static final String KEY_STR_ADDITIONAL_TWO = "strAdditionalTwo";

	//PowerPlusTransaction Table

	public static final String TABLE_POWER_PLUS_TRANSACTION = "PowerPlusTransaction";

	public static final String KEY_I_POWER_PLUS_TRANSACTION_ID = "iPowerPlusTransactionId";
	public static final String KEY_FLG_IS_TRANSACTION_INVALID = "flgIsTransactionInvalid";
	public static final String KEY_STR_APPROVER_ID = "strApproverId";
	public static final String KEY_STR_APPROVER_NAME = "strApproverName";
	public static final String KEY_STR_PROVIDER_ID = "strProviderId";
	public static final String KEY_STR_PROVIDER_NAME = "strProviderName";
	public static final String KEY_FLG_SCORE_STATUS = "strScoreStatus";
	public static final String KEY_PARENT_TRANSACTION_ID = "parentTransactionId";
	public static final String KEY_PROCESSED_AUTO = "processedAuto";
	public static final String KEY_STR_RECEIVER_ID = "strReceiverId";
	public static final String KEY_STR_RECEIVER_NAME = "strReceiverName";

	public static final String TABLE_TRAINING = "TrainingData";

	public static final String KEY_TRAINING_DATA_ID = "iTrainingDataId";
	public static final String KEY_STR_PARENT_ID = "strParentId";

	public static final String TABLE_TRAINING_ATTENDANCE = "TrainingAttendance";

	public static final String KEY_TRAINING_ATTENDANCE_ID = "iTrainingAttendanceId";
	public static final String KEY_TRAINING_TRANSACTION_ID = "iTrainingTransactionId";
	public static final String KEY_TRAINING_DATE = "trainingDate";
	public static final String KEY_TRAINING_TIME = "trainingTime";
	public static final String KEY_SHIP_CATEGORY = "iShipCategoryId";

	public static final String TABLE_TRAINING_TRANSACTION = "TrainingTransaction";

	/**
	 * SyncHistory
	 * 
	 * @param context
	 */
	public static final String TABLE_SYNC_HISTORY = "PowerPlusTabletSyncHistory";

	/**
	 * Column SyncHistory
	 * 
	 * @param context
	 */
	public static final String KEY_SYNC_HISTORY_ID = "iPowerPlusTabletSyncHistoryId";
	public static final String SYNC_DATE = "dtSyncDate";
	public static final String LOG_MSG = "logMessage";
	public static final String SYNC_PROGRESS = "progress";
	public static final String SYNC_GENERATOR = "generator";
	public static final String SYNC_FILE_NAME = "strFilename";
	public static final String SYNC_ORDER_ID = "syncOrderid";
	public static final String SYNC_ACK_DATE = "dtAcknowledgeDate";
	public static final String SYNC_LAST_DOWN_DATE = "lastDownLoadDate";
	public static final String SYNC_GEN_DATE = "dtGeneratedDate";
	public static final String MAC_ID = "strMacId";
	public static final String REG_TABLET_ID = "strRegisterTabletId";

	/**
	 * RoleTabletFunctions
	 * 
	 * @param context
	 */
	public static final String TABLE_ROLE_FUNCTION = "RoleTabletFunctions";

	/**
	 * Column RoleTabletFunctions
	 * 
	 * @param context
	 */
	public static final String KEY_ROLE_FUNCTION_ID = "iRoleTabletFunctionsId";
	public static final String ROLE_FUNCTION_CODE = "strFunctionCode";

	/**
	 * PowerPlusRoleRankCategory
	 * 
	 * @param context
	 */
	public static final String TABLE_POWERPLUS_ROLERANK_CATEGORY = "PowerPlusRoleRankCategory";

	/**
	 * Column PowerPlusRoleRankCategory
	 * 
	 * @param context
	 */
	public static final String KEY_POWERPLUS_ROLERANK_CATEGORY_ID = "iPowerPlusRoleRankCategoryId";
	public static final String RANK_CATEGORY = "strRankCategory";
	public static final String ROLES_IDS = "strRolesId";

	public DatabaseSupport(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	// Creating Tables
	@Override
	public void onCreate(SQLiteDatabase db) {

		String CREATE_TENANT_TABLE = "CREATE TABLE " + TABLE_TENANT + "("
				+ KEY_ID + " INTEGER PRIMARY KEY," + KEY_TENANT_ID + " TEXT,"
				+ KEY_STR_COMPANY_NAME + " TEXT" + ")";

		String CREATE_MODEL_TABLE = "CREATE TABLE " + TABLE_MODEL + "("
				+ KEY_ID + " INTEGER PRIMARY KEY," + KEY_MODEL_ID + " TEXT,"
				+ KEY_MODEL_NAME + " TEXT" + ")";

		System.out.println("CREATE_TENANT_TABLE :" + CREATE_TENANT_TABLE);

		String CREATE_POWERPLUS_ROLERANK_CATEGORY_TABLE = "CREATE TABLE "
				+ TABLE_POWERPLUS_ROLERANK_CATEGORY + "(" + KEY_ID
				+ " INTEGER PRIMARY KEY," + KEY_POWERPLUS_ROLERANK_CATEGORY_ID
				+ " TEXT," + KEY_DT_CREATED + " TEXT," + KEY_DT_UPDATED
				+ " TEXT," + KEY_FLG_DELETED + " INTEGER," + RANK_CATEGORY
				+ " TEXT," + ROLES_IDS + " TEXT," + FIELD_ONE + " TEXT,"
				+ FIELD_TWO + " TEXT," + KEY_TENANT_ID + " TEXT" + ")";

		String CREATE_ROLE_FUNCTION_TABLE = "CREATE TABLE "
				+ TABLE_ROLE_FUNCTION + "(" + KEY_ID + " INTEGER PRIMARY KEY,"
				+ KEY_ROLE_FUNCTION_ID + " TEXT," + KEY_DT_CREATED + " TEXT,"
				+ KEY_DT_UPDATED + " TEXT," + KEY_FLG_DELETED + " INTEGER,"
				+ ROLE_FUNCTION_CODE + " TEXT," + KEY_ROLE_ID + " TEXT,"
				+ FIELD_ONE + " TEXT," + FIELD_TWO + " TEXT," + KEY_TENANT_ID
				+ " TEXT" + ")";

		String CREATE_SYNC_HISTORY_TABLE = "CREATE TABLE " + TABLE_SYNC_HISTORY
				+ "(" + KEY_ID + " INTEGER PRIMARY KEY," + KEY_SYNC_HISTORY_ID
				+ " TEXT," + SYNC_DATE + " DATE," + LOG_MSG + " TEXT,"
				+ SYNC_PROGRESS + " TEXT," + SYNC_GENERATOR + " TEXT,"
				+ SYNC_FILE_NAME + " TEXT," + SYNC_ORDER_ID + " INTEGER,"
				+ KEY_FLG_DELETED + " INTEGER," + KEY_FLG_IS_DIRTY + " TEXT,"
				+ SYNC_ACK_DATE + " TEXT," + SYNC_LAST_DOWN_DATE + " DATE,"
				+ SYNC_GEN_DATE + " TEXT," + MAC_ID + " TEXT,"
				+ REG_TABLET_ID + " TEXT," + KEY_SHIP_ID + " TEXT,"
				+ KEY_TENANT_ID + " TEXT" + ")";

		String CREATE_SHIP_MASTER_TABLE = "CREATE TABLE " + TABLE_SHIP_MASTER
				+ "(" + KEY_ID + " INTEGER PRIMARY KEY," + KEY_SHIP_ID
				+ " TEXT," + KEY_TENANT_ID + " TEXT," + KEY_SOCIETY_ID
				+ " TEXT," + KEY_SHIP_TYPE_ID + " TEXT," + KEY_SHIP_NAME
				+ " TEXT," + KEY_S_DESCRIPTION + " TEXT," + KEY_DT_CREATED
				+ " TEXT," + KEY_DT_UPDATED + " TEXT," + KEY_FLG_STATUS
				+ " TEXT," + KEY_FLG_DELETED + " TEXT," + KEY_FLG_IS_DIRTY
				+ " TEXT," + KEY_RULE_LIST_ID + " TEXT," + KEY_SHIP_IMO_NUMBER
				+ " TEXT," + KEY_STR_FLAG + " TEXT," + KEY_LOGO + " TEXT,"
				+ KEY_FILE_SIZE + " TEXT," + KEY_FILE_NAME + " TEXT,"
				+ KEY_FILE_PATH + " TEXT," + KEY_FILE_TYPE + " TEXT,"
				+ KEY_SHIP_CODE + " TEXT" + ")";

		String CREATE_ROLE_TABLE = "CREATE TABLE " + TABLE_ROLE + "(" + KEY_ID
				+ " INTEGER PRIMARY KEY," + KEY_ROLE_ID + " TEXT," + KEY_ROLE
				+ " TEXT," + KEY_DT_CREATED + " TEXT," + KEY_DT_UPDATED
				+ " TEXT," + KEY_FLG_IS_DIRTY + " TEXT," + KEY_STR_TYPE
				+ " TEXT," + KEY_FLG_DELETED + " TEXT," + KEY_TXT_DESCRIPTION
				+ " TEXT," + KEY_RANK_PRIORITY + " TEXT," + KEY_TENANT_ID
				+ " TEXT," + KEY_STR_ROLE_TYPE + " TEXT" + ")";

		String CREATE_USER_MASTER_TABLE = "CREATE TABLE " + TABLE_USER_MASTER
				+ "(" + KEY_ID + " INTEGER PRIMARY KEY," + KEY_iUSER_ID
				+ " TEXT," + KEY_TENANT_ID + " TEXT," + KEY_USER_NAME
				+ " TEXT," + KEY_USER_PASSWORD + " TEXT," + KEY_FIRST_NAME
				+ " TEXT," + KEY_LAST_NAME + " TEXT," + KEY_DATE_OF_BIRTH
				+ " TEXT," + KEY_TXT_DESCRIPTION + " TEXT," + KEY_EMAIL
				+ " TEXT," + KEY_DT_CREATED + " TEXT," + KEY_DT_UPDATED
				+ " TEXT," + KEY_FLG_STATUS + " TEXT," + KEY_FLG_DELETED
				+ " TEXT," + KEY_FLG_IS_DIRTY + " TEXT,"
				+ KEY_SHIP_HOLIDAY_LIST_ID + " TEXT,"
				+ KEY_MIN_WORK_HOUR_WEEK_DAYS + " TEXT,"
				+ KEY_MIN_WORK_HOUR_SATUREDAY + " TEXT,"
				+ KEY_MIN_WORK_HOUR_SUNDAY + " TEXT,"
				+ KEY_MIN_WORK_HOUR_HOLIDAY + " TEXT,"
				+ KEY_FLTOT_INCLUDED_IN_WAGE + " TEXT,"
				+ KEY_FLTOT_RATE_PER_HOUR + " TEXT,"
				+ KEY_FLG_IS_OVER_TIME_ENABLED + " TEXT," + KEY_ROLE_ID
				+ " TEXT," + KEY_FLG_IS_WATCHKEEPER + " TEXT," + KEY_FAX_NUMBER
				+ " TEXT," + KEY_HAND_PHONE + " TEXT," + KEY_LANDLINE_NUMBER
				+ " TEXT," + KEY_PAN_NUMBER + " TEXT," + KEY_PIN_CODE
				+ " TEXT," + KEY_ADDRESS_FIRST + " TEXT," + KEY_ADDRESS_SECOND
				+ " TEXT," + KEY_ADDRESS_THIRD + " TEXT," + KEY_CITY + " TEXT,"
				+ KEY_STATE + " TEXT," + KEY_COUNTRY_ID + " TEXT," 
				+ KEY_EMPLOYEE_NO + " TEXT," + KEY_MIDDLE_NAME + " TEXT"+ ")";

		String CREATE_USER_SERVICE_TERM_TABLE = "CREATE TABLE "
				+ TABLE_USER_SERVICE_TERM + "(" + KEY_ID
				+ " INTEGER PRIMARY KEY," + KEY_USER_SERVICE_TERM_ID + " TEXT,"
				+ KEY_iUSER_ID + " TEXT," + KEY_DT_TERM_FROM + " DATE,"
				+ KEY_DT_TERM_TO + " DATE," + KEY_FLG_DELETED + " TEXT,"
				+ KEY_FLG_IS_DIRTY + " TEXT," + KEY_DT_CREATED + " TEXT,"
				+ KEY_DT_UPDATED + " TEXT," + KEY_SHIP_ID + " TEXT,"
				+ KEY_TENANT_ID + " TEXT" + ")";

		String CREATE_USER_SERVICE_TERM_ROLE_TABLE = "CREATE TABLE "
				+ TABLE_USER_SERVICE_TERM_ROLE + "(" + KEY_ID
				+ " INTEGER PRIMARY KEY," + KEY_USER_SERVICE_TERM_ROLE_ID
				+ " TEXT," + KEY_DT_CREATED + " TEXT," + KEY_DT_UPDATED
				+ " TEXT," + KEY_FLG_DELETED + " TEXT," + KEY_FLG_IS_DIRTY
				+ " TEXT," + KEY_ROLE_ID + " TEXT," + KEY_SHIP_ID + " TEXT,"
				+ KEY_TENANT_ID + " TEXT," + KEY_iUSER_ID + " TEXT,"
				+ KEY_USER_SERVICE_TERM_ID + " TEXT," + KEY_DT_TERM_FROM
				+ " DATE," + KEY_DT_TERM_TO + " DATE," + POWER_PLUS_SCORE
				+ " INTEGER," + FIELD_ONE + " INTEGER," + FIELD_TWO
				+ " INTEGER" + ")";

		String CREATE_CARD_TYPE_TABLE = "CREATE TABLE " + TABLE_CARD_TYPE + "("
				+ KEY_ID + " INTEGER PRIMARY KEY," + KEY_CARD_ID + " TEXT,"
				+ KEY_CARD_TYPE + " TEXT" + ")";

		String CREATE_USER_NFC_TABLE = "CREATE TABLE " + TABLE_USER_NFC + "("
				+ KEY_ID + " INTEGER PRIMARY KEY," + KEY_NFC_CARD_ID + " TEXT,"
				+ KEY_NFC_CARD_TYPE + " TEXT," + KEY_iUSER_ID + " TEXT,"
				+ KEY_USER_SERVICE_TERM_ID + " TEXT," + KEY_FLG_DIRTY
				+ " TEXT," + KEY_FLG_DELETED + " TEXT," + KEY_DT_CREATED
				+ " TEXT," + KEY_DT_UPDATED + " TEXT," + KEY_TENANT_ID
				+ " TEXT," + KEY_SHIP_ID + " TEXT" + ")";

		String CREATE_POWER_PLUS_CATEGORY_TABLE = "CREATE TABLE "
				+ TABLE_POWER_PLUS_CATEGOEY + "(" + KEY_ID
				+ " INTEGER PRIMARY KEY," + KEY_I_POWER_PLUS_CATEGORY_ID
				+ " TEXT," + KEY_DT_CREATED + " TEXT," + KEY_DT_UPDATED
				+ " TEXT," + KEY_FLG_DELETED + " TEXT," + KEY_FLG_IS_DIRTY
				+ " TEXT," + KEY_FLG_STATUS + " TEXT," + KEY_I_SEQUENCE
				+ " TEXT," + KEY_STR_DESCRIPTION + " TEXT," + KEY_STR_TITLE
				+ " TEXT," + KEY_TENANT_ID + " TEXT," + KEY_STR_CATEGORY_CODE
				+ " TEXT" + ")";

		String CREATE_POWER_PLUS_PERIODICITY_TABLE = "CREATE TABLE "
				+ TABLE_POWER_PLUS_PERIODICITY + "(" + KEY_ID
				+ " INTEGER PRIMARY KEY," + KEY_I_POWER_PLUS_PERIODICITY_ID
				+ " TEXT," + KEY_DT_CREATED + " TEXT," + KEY_DT_UPDATED
				+ " TEXT," + KEY_FLG_DELETED + " TEXT," + KEY_FLG_IS_DIRTY
				+ " TEXT," + KEY_FLG_STATUS + " TEXT," + KEY_I_INTERVAL
				+ " TEXT," + KEY_I_SEQUENCE + " TEXT," + KEY_I_TIMES + " TEXT,"
				+ KEY_STR_DESCRIPTION + " TEXT," + KEY_STR_PATTERN + " TEXT,"
				+ KEY_STR_TITLE + " TEXT," + KEY_TENANT_ID + " TEXT" + ")";

		String CREATE_POWER_PLUS_TASK_TABLE = "CREATE TABLE "
				+ TABLE_POWER_PLUS_TASK + "(" + KEY_ID
				+ " INTEGER PRIMARY KEY," + KEY_I_POWER_PLUS_TASK_ID + " TEXT,"
				+ KEY_APPROVED_BY + " TEXT," + KEY_DT_CREATED + " TEXT,"
				+ KEY_DT_UPDATED + " TEXT," + KEY_FLG_DELETED + " TEXT,"
				+ KEY_FLG_IS_ALL_RECEIVE + " TEXT," + KEY_FLG_IS_DIRTY
				+ " TEXT," + KEY_FLG_STATUS + " TEXT," + KEY_I_SEQUENCE
				+ " TEXT," + KEY_POWER_POINTS + " TEXT," + KEY_PROVIDED_BY
				+ " TEXT," + KEY_RECEIVED_BY + " TEXT,"
				+ KEY_STR_ADDITIONAL_ONE + " TEXT," + KEY_STR_ADDITIONAL_TWO
				+ " TEXT," + KEY_STR_DESCRIPTION + " TEXT," + KEY_STR_TITLE
				+ " TEXT," + KEY_I_POWER_PLUS_CATEGORY_ID + " TEXT,"
				+ KEY_I_POWER_PLUS_PERIODICITY_ID + " TEXT," + KEY_TENANT_ID
				+ " TEXT" + ")";

		String CREATE_POWER_PLUS_TRANSACTION_TABLE = "CREATE TABLE "
				+ TABLE_POWER_PLUS_TRANSACTION + "(" + KEY_ID
				+ " INTEGER PRIMARY KEY," + KEY_I_POWER_PLUS_TRANSACTION_ID
				+ " TEXT," + KEY_DT_CREATED + " DATE," + KEY_DT_UPDATED
				+ " DATE," + KEY_FLG_DELETED + " TEXT," + KEY_FLG_IS_DIRTY
				+ " TEXT," + KEY_FLG_IS_TRANSACTION_INVALID + " TEXT,"
				+ KEY_FLG_STATUS + " TEXT," + KEY_I_SEQUENCE + " TEXT,"
				+ KEY_POWER_POINTS + " TEXT," + KEY_STR_ADDITIONAL_ONE
				+ " TEXT," + KEY_STR_ADDITIONAL_TWO + " TEXT,"
				+ KEY_STR_APPROVER_ID + " TEXT," + KEY_STR_APPROVER_NAME
				+ " TEXT," + KEY_STR_DESCRIPTION + " TEXT,"
				+ KEY_STR_PROVIDER_ID + " TEXT," + KEY_STR_PROVIDER_NAME
				+ " TEXT," + KEY_STR_TITLE + " TEXT,"
				+ KEY_I_POWER_PLUS_TASK_ID + " TEXT," + KEY_SHIP_ID + " TEXT,"
				+ KEY_TENANT_ID + " TEXT," + KEY_STR_RECEIVER_ID + " TEXT,"
				+ KEY_STR_RECEIVER_NAME + " TEXT," + KEY_FLG_SCORE_STATUS
				+ " TEXT," + KEY_I_POWER_PLUS_CATEGORY_ID + " TEXT,"
				+ KEY_PARENT_TRANSACTION_ID + " TEXT,"
				+ KEY_USER_SERVICE_TERM_ID + " TEXT," + KEY_ROLE_ID + " TEXT,"
				+ KEY_PROCESSED_AUTO + " TEXT" + ")";

		String CREATE_TRAINING_TABLE = "CREATE TABLE " + TABLE_TRAINING + "("
				+ KEY_ID + " INTEGER PRIMARY KEY," + KEY_TRAINING_DATA_ID
				+ " TEXT," + KEY_STR_PARENT_ID + " TEXT," + KEY_STR_TITLE
				+ " TEXT," + KEY_STR_DESCRIPTION + " TEXT," + KEY_FLG_DELETED
				+ " TEXT," + KEY_SHIP_ID + " TEXT," + KEY_TENANT_ID + " TEXT,"
				+ KEY_DT_CREATED + " TEXT," + KEY_DT_UPDATED + " TEXT,"
				+ KEY_FLG_STATUS + " TEXT," + KEY_FLG_IS_DIRTY + " TEXT,"
				+ KEY_I_SEQUENCE + " INTEGER," + KEY_SHIP_CATEGORY + " TEXT "
				+ ")";

		String CREATE_TRAINING_ATTENDANCE_TABLE = "CREATE TABLE "
				+ TABLE_TRAINING_ATTENDANCE + "(" + KEY_ID
				+ " INTEGER PRIMARY KEY," + KEY_TRAINING_ATTENDANCE_ID
				+ " TEXT," + KEY_TRAINING_TRANSACTION_ID + " TEXT,"
				+ KEY_TRAINING_DATA_ID + " TEXT," + KEY_STR_PARENT_ID
				+ " TEXT," + KEY_TRAINING_DATE + " TEXT," + KEY_TRAINING_TIME
				+ " TEXT," + KEY_STR_DESCRIPTION + " TEXT," + KEY_FLG_DELETED
				+ " TEXT," + KEY_SHIP_ID + " TEXT," + KEY_TENANT_ID + " TEXT,"
				+ KEY_DT_CREATED + " TEXT," + KEY_DT_UPDATED + " TEXT,"
				+ KEY_FLG_STATUS + " TEXT," + KEY_FLG_IS_DIRTY + " TEXT,"
				+ KEY_iUSER_ID + " TEXT," + KEY_USER_SERVICE_TERM_ID + " TEXT"
				+ ")";

		String CREATE_TRAINING_TRANSACTION_TABLE = "CREATE TABLE "
				+ TABLE_TRAINING_TRANSACTION + "(" + KEY_ID
				+ " INTEGER PRIMARY KEY," + KEY_TRAINING_TRANSACTION_ID
				+ " TEXT," + KEY_TRAINING_DATA_ID + " TEXT,"
				+ KEY_TRAINING_DATE + " DATE," + KEY_TRAINING_TIME + " TEXT,"
				+ KEY_STR_DESCRIPTION + " TEXT," + KEY_FLG_DELETED + " TEXT,"
				+ KEY_SHIP_ID + " TEXT," + KEY_TENANT_ID + " TEXT,"
				+ KEY_DT_CREATED + " DATE," + KEY_DT_UPDATED + " DATE,"
				+ KEY_FLG_STATUS + " TEXT," + KEY_FLG_IS_DIRTY + " TEXT,"
				+ KEY_STR_PARENT_ID + " TEXT" + ")";

		db.execSQL(CREATE_TENANT_TABLE);
		db.execSQL(CREATE_SHIP_MASTER_TABLE);
		db.execSQL(CREATE_USER_MASTER_TABLE);
		db.execSQL(CREATE_ROLE_TABLE);
		db.execSQL(CREATE_USER_SERVICE_TERM_TABLE);
		db.execSQL(CREATE_USER_SERVICE_TERM_ROLE_TABLE);
		db.execSQL(CREATE_CARD_TYPE_TABLE);
		db.execSQL(CREATE_USER_NFC_TABLE);
		db.execSQL(CREATE_MODEL_TABLE);

		// power plus
		db.execSQL(CREATE_POWER_PLUS_CATEGORY_TABLE);
		db.execSQL(CREATE_POWER_PLUS_PERIODICITY_TABLE);
		db.execSQL(CREATE_POWER_PLUS_TASK_TABLE);
		db.execSQL(CREATE_POWER_PLUS_TRANSACTION_TABLE);

		// Traing Plus
		db.execSQL(CREATE_TRAINING_TABLE);
		db.execSQL(CREATE_TRAINING_ATTENDANCE_TABLE);
		db.execSQL(CREATE_TRAINING_TRANSACTION_TABLE);

		// SYNC HISTORY
		db.execSQL(CREATE_SYNC_HISTORY_TABLE);

		// SYNC ROLEFUNCTION
		db.execSQL(CREATE_ROLE_FUNCTION_TABLE);

		// SYNC PowerPlusRoleRankCategory
		db.execSQL(CREATE_POWERPLUS_ROLERANK_CATEGORY_TABLE);

	}
	
	public void updateDataBase() {
		SQLiteDatabase db = this.getWritableDatabase();
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_TENANT);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_SHIP_MASTER);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_ROLE);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER_SERVICE_TERM);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER_SERVICE_TERM_ROLE);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER_MASTER);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_CARD_TYPE);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER_NFC);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_MODEL);

		// power Plus
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_POWER_PLUS_CATEGOEY);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_POWER_PLUS_PERIODICITY);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_POWER_PLUS_TASK);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_POWER_PLUS_TRANSACTION);

		// Traing Plus
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_TRAINING);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_TRAINING_ATTENDANCE);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_TRAINING_TRANSACTION);

		// SYNC HISTORY
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_SYNC_HISTORY);

		// ROLE FUNCTION
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_ROLE_FUNCTION);

		// ROLE PowerPlusRoleRankCategory
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_POWERPLUS_ROLERANK_CATEGORY);

		// Create tables again
		onCreate(db);
	}

	// Upgrading database
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// Drop older table if existed
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_TENANT);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_SHIP_MASTER);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_ROLE);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER_SERVICE_TERM);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER_SERVICE_TERM_ROLE);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER_MASTER);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_CARD_TYPE);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER_NFC);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_MODEL);

		// power Plus
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_POWER_PLUS_CATEGOEY);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_POWER_PLUS_PERIODICITY);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_POWER_PLUS_TASK);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_POWER_PLUS_TRANSACTION);

		// Traing Plus
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_TRAINING);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_TRAINING_ATTENDANCE);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_TRAINING_TRANSACTION);

		// SYNC HISTORY
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_SYNC_HISTORY);

		// ROLE FUNCTION
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_ROLE_FUNCTION);

		// ROLE PowerPlusRoleRankCategory
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_POWERPLUS_ROLERANK_CATEGORY);

		// Create tables again
		onCreate(db);
	}

	@Override
	public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		// TODO Auto-generated method stub

		db.execSQL("DROP TABLE IF EXISTS " + TABLE_TENANT);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_SHIP_MASTER);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_ROLE);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER_SERVICE_TERM);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER_SERVICE_TERM_ROLE);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER_MASTER);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_CARD_TYPE);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_USER_NFC);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_MODEL);

		// power Plus
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_POWER_PLUS_CATEGOEY);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_POWER_PLUS_PERIODICITY);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_POWER_PLUS_TASK);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_POWER_PLUS_TRANSACTION);

		// Training Plus
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_TRAINING);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_TRAINING_ATTENDANCE);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_TRAINING_TRANSACTION);

		// SYNC HISTORY
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_SYNC_HISTORY);

		// ROLE FUNCTION
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_ROLE_FUNCTION);

		// ROLE PowerPlusRoleRankCategory
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_POWERPLUS_ROLERANK_CATEGORY);

		// Create tables again
		onCreate(db);

	}

	public int getScoreInEnterMode(String uid, String taskId, String status) {
		// Select All Query
		int count = 0;
		try {
			String selectQuery = "SELECT " + KEY_STR_TITLE + "  FROM "
					+ TABLE_POWER_PLUS_TRANSACTION + " WHERE "
					+ KEY_STR_PROVIDER_ID + " = " + "'" + uid + "' and  "
					+ KEY_I_POWER_PLUS_TASK_ID + " = '" + taskId + "' and  "
					+ KEY_FLG_STATUS + " = '" + status + "' and "+ KEY_FLG_DELETED +" ='0'";
			System.out.println("Entered Query :" + selectQuery);
			SQLiteDatabase db = this.getWritableDatabase();
			Cursor cursor = db.rawQuery(selectQuery, null);

			if (cursor.moveToFirst()) {
				do {

					count++;
				} while (cursor.moveToNext());

			}
			db.close();
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("Exception :" + e);
		}
		
		System.out.println("return count vALUE :" + count);
		return count;

	}

	public int getScoreInEnterModeForValidate(String uid, String taskId,
			String status) {
		// Select All Query
		int count = 0;
		try {
			String selectQuery = "SELECT " + KEY_STR_TITLE + "  FROM "
					+ TABLE_POWER_PLUS_TRANSACTION + " WHERE "
					+ KEY_STR_RECEIVER_ID + " = " + "'" + uid + "' and  "
					+ KEY_I_POWER_PLUS_TASK_ID + " = '" + taskId + "' and  "
					+ KEY_FLG_STATUS + " = '" + status + "' and "+ KEY_FLG_DELETED +" ='0'";
			System.out.println("Entered Query :" + selectQuery);
			SQLiteDatabase db = this.getWritableDatabase();
			Cursor cursor = db.rawQuery(selectQuery, null);

			if (cursor.moveToFirst()) {
				do {

					count++;
				} while (cursor.moveToNext());

			}
			db.close();
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("Exception :" + e);
		}
		// return contact list
		System.out.println("return count vALUE :" + count);
		return count;

	} // code to get all contacts in a list view

	public int getScoreInEnterModeForValidateWithTrans(
			String strPowerplustransactionid, String uid, String taskId,
			String status) {
		// Select All Query
		int count = 0;
		try {
			String selectQuery = "SELECT " + KEY_STR_TITLE + "  FROM "
					+ TABLE_POWER_PLUS_TRANSACTION + " WHERE "
					+ KEY_STR_RECEIVER_ID + " = " + "'" + uid + "' and "
					+ KEY_I_POWER_PLUS_TRANSACTION_ID + " = '"
					+ strPowerplustransactionid + "' and  "
					+ KEY_I_POWER_PLUS_TASK_ID + " = '" + taskId + "' and  "
					+ KEY_FLG_STATUS + " = '" + status + "' and "+ KEY_FLG_DELETED +" ='0'";
			System.out.println("Entered Query :" + selectQuery);
			SQLiteDatabase db = this.getWritableDatabase();
			Cursor cursor = db.rawQuery(selectQuery, null);

			if (cursor.moveToFirst()) {
				do {

					count++;
				} while (cursor.moveToNext());

			}
			db.close();
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("Exception :" + e);
		}
		// return contact list
		System.out.println("return count vALUE :" + count);
		return count;

	} // code to get all contacts in a list view

	public PowerPlusTransaction getPowerPlusTransactionById(String uid,
			String taskId, String status) {
		// Select All Query
		int count = 0;
		PowerPlusTransaction data = new PowerPlusTransaction();
		try {
			String selectQuery = "SELECT *  FROM "
					+ TABLE_POWER_PLUS_TRANSACTION + " WHERE "
					+ KEY_STR_RECEIVER_ID + " = " + "'" + uid + "' and  "
					+ KEY_I_POWER_PLUS_TASK_ID + " = '" + taskId + "' and  "
					+ KEY_FLG_STATUS + " = '" + status + "' and "+ KEY_FLG_DELETED +" ='0'";
			System.out.println("Entered Query :" + selectQuery);
			SQLiteDatabase db = this.getWritableDatabase();
			Cursor cursor = db.rawQuery(selectQuery, null);

			if (cursor.moveToFirst()) {
				do {

					data.setId(Integer.parseInt(cursor.getString(0)));
					data.setiPowerPlusTransactionId(cursor.getString(1));
					data.setDtCreated(cursor.getString(2));
					data.setDtUpdated(cursor.getString(3));
					data.setFlgDeleted(cursor.getString(4));
					data.setFlgIsDirty(cursor.getString(5));
					data.setFlgIsTransationInvalid(cursor.getString(6));
					data.setFlgStatus(cursor.getString(7));
					data.setiSequence(cursor.getString(8));
					data.setPowerPoints(cursor.getString(9));
					data.setStrAdditionalOne(cursor.getString(10));
					data.setStrAdditionalTwo(cursor.getString(11));
					data.setStrApproverId(cursor.getString(12));
					data.setStrApproverName(cursor.getString(13));
					data.setStrDescription(cursor.getString(14));
					data.setStrProviderId(cursor.getString(15));
					data.setStrProviderName(cursor.getString(16));
					data.setStrTitle(cursor.getString(17));
					data.setiPowerPlusTaskId(cursor.getString(18));
					data.setiShipId(cursor.getString(19));
					data.setiTenantId(cursor.getString(20));
					data.setiUserId(cursor.getString(21));
					data.setStrReceiverName(cursor.getString(22));
					data.setFlgScoreStatus(cursor.getString(23));
					data.setCetogoryId(cursor.getString(24));
					data.setParentTransactionId(cursor.getString(25));
					data.setiUserServiceTermId(cursor.getString(26));
					data.setiRoleId(cursor.getString(27));
					data.setProcessedAuto(cursor.getString(28));

				} while (cursor.moveToNext());

			}
			db.close();
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("Exception :" + e);
		}
		// return contact list
		System.out.println("return count vALUE :" + count);
		return data;

	}

	public PowerPlusTransaction getPowerPlusTransactionByTransId(
			String strTransId, String uid, String taskId, String status) {
		// Select All Query
		int count = 0;
		PowerPlusTransaction data = new PowerPlusTransaction();
		try {
			String selectQuery = "SELECT *  FROM "
					+ TABLE_POWER_PLUS_TRANSACTION + " WHERE "
					+ KEY_STR_RECEIVER_ID + " = " + "'" + uid + "' and  "
					+ KEY_I_POWER_PLUS_TASK_ID + " = '" + taskId + "' and  "
					+ KEY_FLG_STATUS + " = '" + status + "' and "
					+ KEY_I_POWER_PLUS_TRANSACTION_ID + " ='" + strTransId
					+ "'" + " and " + KEY_FLG_DELETED + " ='0'";
			System.out.println("Entered Query :" + selectQuery);
			SQLiteDatabase db = this.getWritableDatabase();
			Cursor cursor = db.rawQuery(selectQuery, null);

			if (cursor.moveToFirst()) {
				do {

					data.setId(Integer.parseInt(cursor.getString(0)));
					data.setiPowerPlusTransactionId(cursor.getString(1));
					data.setDtCreated(cursor.getString(2));
					data.setDtUpdated(cursor.getString(3));
					data.setFlgDeleted(cursor.getString(4));
					data.setFlgIsDirty(cursor.getString(5));
					data.setFlgIsTransationInvalid(cursor.getString(6));
					data.setFlgStatus(cursor.getString(7));
					data.setiSequence(cursor.getString(8));
					data.setPowerPoints(cursor.getString(9));
					data.setStrAdditionalOne(cursor.getString(10));
					data.setStrAdditionalTwo(cursor.getString(11));
					data.setStrApproverId(cursor.getString(12));
					data.setStrApproverName(cursor.getString(13));
					data.setStrDescription(cursor.getString(14));
					data.setStrProviderId(cursor.getString(15));
					data.setStrProviderName(cursor.getString(16));
					data.setStrTitle(cursor.getString(17));
					data.setiPowerPlusTaskId(cursor.getString(18));
					data.setiShipId(cursor.getString(19));
					data.setiTenantId(cursor.getString(20));
					data.setiUserId(cursor.getString(21));
					data.setStrReceiverName(cursor.getString(22));
					data.setFlgScoreStatus(cursor.getString(23));
					data.setCetogoryId(cursor.getString(24));
					data.setParentTransactionId(cursor.getString(25));
					data.setiUserServiceTermId(cursor.getString(26));
					data.setiRoleId(cursor.getString(27));
					data.setProcessedAuto(cursor.getString(28));

				} while (cursor.moveToNext());

			}
			db.close();
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("Exception :" + e);
		}
		// return contact list
		System.out.println("return count vALUE :" + count);
		return data;

	}

	public List<PowerPlusTransaction> getPowerPlusTransactionByTaskIdAndStatus(
			String strTaskid, String status) {
		// Select All Query
		int count = 0;
		List<PowerPlusTransaction> list = new ArrayList<PowerPlusTransaction>();

		try {
			String selectQuery = "SELECT *  FROM "
					+ TABLE_POWER_PLUS_TRANSACTION + " WHERE "
					+ KEY_I_POWER_PLUS_TASK_ID + " = '" + strTaskid + "' and  "
					+ KEY_FLG_STATUS + " = '" + status + "' and "+ KEY_FLG_DELETED +" ='0'";
			System.out.println("Entered Query :" + selectQuery);
			SQLiteDatabase db = this.getWritableDatabase();
			Cursor cursor = db.rawQuery(selectQuery, null);

			if (cursor.moveToFirst()) {
				do {
					PowerPlusTransaction data = new PowerPlusTransaction();

					data.setId(Integer.parseInt(cursor.getString(0)));
					data.setiPowerPlusTransactionId(cursor.getString(1));
					data.setDtCreated(cursor.getString(2));
					data.setDtUpdated(cursor.getString(3));
					data.setFlgDeleted(cursor.getString(4));
					data.setFlgIsDirty(cursor.getString(5));
					data.setFlgIsTransationInvalid(cursor.getString(6));
					data.setFlgStatus(cursor.getString(7));
					data.setiSequence(cursor.getString(8));
					data.setPowerPoints(cursor.getString(9));
					data.setStrAdditionalOne(cursor.getString(10));
					data.setStrAdditionalTwo(cursor.getString(11));
					data.setStrApproverId(cursor.getString(12));
					data.setStrApproverName(cursor.getString(13));
					data.setStrDescription(cursor.getString(14));
					data.setStrProviderId(cursor.getString(15));
					data.setStrProviderName(cursor.getString(16));
					data.setStrTitle(cursor.getString(17));
					data.setiPowerPlusTaskId(cursor.getString(18));
					data.setiShipId(cursor.getString(19));
					data.setiTenantId(cursor.getString(20));
					data.setiUserId(cursor.getString(21));
					data.setStrReceiverName(cursor.getString(22));
					data.setFlgScoreStatus(cursor.getString(23));
					data.setCetogoryId(cursor.getString(24));
					data.setParentTransactionId(cursor.getString(25));
					data.setiUserServiceTermId(cursor.getString(26));
					data.setiRoleId(cursor.getString(27));
					data.setProcessedAuto(cursor.getString(28));

					list.add(data);

				} while (cursor.moveToNext());

			}
			db.close();
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("Exception :" + e);
		}
		// return contact list
		System.out.println("return count vALUE :" + count);
		return list;

	}
	
	public List<PowerPlusTransaction> getPowerPlusTransactionByTaskIdLoginUserStatusAndTurboUserForToday(
			String strTaskid, String strLoginUserId, String strTurboUserId, String status, String strDate) {
		
		//L.fv("user for turbo is  : "+strTurboUserId +" and login user is "+strLoginUserId);
		//L.fv("task is  : "+strTaskid +" and date  is "+strDate+" and status is "+status);
		// Select All Query
		int count = 0;
		List<PowerPlusTransaction> list = new ArrayList<PowerPlusTransaction>();

		try {
			String selectQuery = "SELECT *  FROM "
					+ TABLE_POWER_PLUS_TRANSACTION + " WHERE "
					+ KEY_I_POWER_PLUS_TASK_ID + " = '" + strTaskid + "' and  "
					+ KEY_FLG_STATUS + " = '" + status + "' and "+ KEY_FLG_DELETED +" ='0' and "
					+ KEY_STR_PROVIDER_ID + " = '"+strLoginUserId+"' and "+KEY_STR_RECEIVER_ID+" = '"+strTurboUserId+ "' and "
					+ KEY_DT_CREATED+"='"+strDate+"'";
			System.out.println("Entered Query :" + selectQuery);
			SQLiteDatabase db = this.getWritableDatabase();
			Cursor cursor = db.rawQuery(selectQuery, null);

			if (cursor.moveToFirst()) {
				do {
					PowerPlusTransaction data = new PowerPlusTransaction();

					data.setId(Integer.parseInt(cursor.getString(0)));
					data.setiPowerPlusTransactionId(cursor.getString(1));
					data.setDtCreated(cursor.getString(2));
					data.setDtUpdated(cursor.getString(3));
					data.setFlgDeleted(cursor.getString(4));
					data.setFlgIsDirty(cursor.getString(5));
					data.setFlgIsTransationInvalid(cursor.getString(6));
					data.setFlgStatus(cursor.getString(7));
					data.setiSequence(cursor.getString(8));
					data.setPowerPoints(cursor.getString(9));
					data.setStrAdditionalOne(cursor.getString(10));
					data.setStrAdditionalTwo(cursor.getString(11));
					data.setStrApproverId(cursor.getString(12));
					data.setStrApproverName(cursor.getString(13));
					data.setStrDescription(cursor.getString(14));
					data.setStrProviderId(cursor.getString(15));
					data.setStrProviderName(cursor.getString(16));
					data.setStrTitle(cursor.getString(17));
					data.setiPowerPlusTaskId(cursor.getString(18));
					data.setiShipId(cursor.getString(19));
					data.setiTenantId(cursor.getString(20));
					data.setiUserId(cursor.getString(21));
					data.setStrReceiverName(cursor.getString(22));
					data.setFlgScoreStatus(cursor.getString(23));
					data.setCetogoryId(cursor.getString(24));
					data.setParentTransactionId(cursor.getString(25));
					data.setiUserServiceTermId(cursor.getString(26));
					data.setiRoleId(cursor.getString(27));
					data.setProcessedAuto(cursor.getString(28));

					list.add(data);

				} while (cursor.moveToNext());

			}
			db.close();
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("Exception :" + e);
		}
		// return contact list
		System.out.println("return count vALUE :" + count);
		//L.fv("List size is :"+list);
		return list;

	}

	// Code to add the Tenant Row
	public void addModelRow(ModelData md) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_ID, md.getId());
		values.put(KEY_MODEL_ID, md.getModeId());
		values.put(KEY_MODEL_NAME, md.getModelName());
		// Inserting Row
		db.insert(TABLE_MODEL, null, values);
		// 2nd argument is String containing nullColumnHack
		db.close(); // Closing database connection
	}

	// Code to add the Tenant Row
	public void addTenantRow(TenantData td) {

		try {
			L.fv("I m Here");
			SQLiteDatabase db = this.getWritableDatabase();

			ContentValues values = new ContentValues();

			values.put(KEY_TENANT_ID, td.getiTenantId());
			values.put(KEY_STR_COMPANY_NAME, td.getStrCompanyName());
			// Inserting Row
			L.fv("KEY_ID" + td.getId());
			L.fv("KEY_TENANT_ID" + td.getiTenantId());
			db.insert(TABLE_TENANT, null, values);
			L.fv("i m");

			// 2nd argument is String containing nullColumnHack
			db.close(); // Closing database connection
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			L.fv("Exception :" + e);
		}

	}

	// Code to add the new Ship Master row
	public void addShipMasterRow(ShipMasterData smd) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_SHIP_ID, smd.getiShipId());
		values.put(KEY_TENANT_ID, smd.getiTenantId());
		values.put(KEY_SOCIETY_ID, smd.getiClassificationSocietyId());
		values.put(KEY_SHIP_TYPE_ID, smd.getiShipTypeId());
		values.put(KEY_SHIP_NAME, smd.getStrShipName());
		values.put(KEY_S_DESCRIPTION, smd.getStrDescription());
		values.put(KEY_DT_CREATED, smd.getDtCreated());
		values.put(KEY_DT_UPDATED, smd.getDtUpdated());
		values.put(KEY_FLG_STATUS, smd.getFlgStatus());
		values.put(KEY_FLG_DELETED, smd.getFlgDeleted());
		values.put(KEY_FLG_IS_DIRTY, smd.getFlgIsDirty());
		values.put(KEY_RULE_LIST_ID, smd.getiRuleListId());
		values.put(KEY_SHIP_IMO_NUMBER, smd.getiShipIMONumber());
		values.put(KEY_STR_FLAG, smd.getStrFlag());
		values.put(KEY_LOGO, smd.getStrLogo());
		values.put(KEY_FILE_SIZE, smd.getFileSize());
		values.put(KEY_FILE_NAME, smd.getStrFileName());
		values.put(KEY_FILE_PATH, smd.getStrFilePath());
		values.put(KEY_FILE_TYPE, smd.getStrFileType());
		values.put(KEY_SHIP_CODE, smd.getStrShipCode());

		// Inserting Row
		db.insert(TABLE_SHIP_MASTER, null, values);
		// 2nd argument is String containing nullColumnHack
		db.close(); // Closing database connection
	}

	// Code to add the new Role Row
	public void addRoleRow(RoleData rd) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_ROLE_ID, rd.getiRoleId());
		values.put(KEY_ROLE, rd.getStrRole());
		values.put(KEY_DT_CREATED, rd.getDtCreated());
		values.put(KEY_DT_UPDATED, rd.getDtUpdated());
		values.put(KEY_FLG_IS_DIRTY, rd.getFlgIsDirty());
		values.put(KEY_STR_TYPE, rd.getStrType());
		values.put(KEY_FLG_DELETED, rd.getFlgDeleted());
		values.put(KEY_TXT_DESCRIPTION, rd.getTxtDescription());
		values.put(KEY_RANK_PRIORITY, rd.getiRankPriority());
		values.put(KEY_TENANT_ID, rd.getiTenantId());
		values.put(KEY_STR_ROLE_TYPE, rd.getStrRoleType());

		// Inserting Row
		db.insert(TABLE_ROLE, null, values);
		// 2nd argument is String containing nullColumnHack
		db.close(); // Closing database connection
	}

	// Code to add the new User Master Row
	public void addUserMasterRow(UserMaster umd) {
		SQLiteDatabase db = this.getWritableDatabase();
		ContentValues values = new ContentValues();
		values.put(KEY_iUSER_ID, umd.getiUserId());
		values.put(KEY_TENANT_ID, umd.getiTenantId());
		values.put(KEY_USER_NAME, umd.getStrUserName());
		values.put(KEY_USER_PASSWORD, umd.getStrPassword());
		values.put(KEY_FIRST_NAME, umd.getStrFirstName());
		values.put(KEY_LAST_NAME, umd.getStrLastName());
		values.put(KEY_DATE_OF_BIRTH, umd.getDtDateOfBirth());
		values.put(KEY_TXT_DESCRIPTION, umd.getTxtDescription());
		values.put(KEY_EMAIL, umd.getStrEmail());
		values.put(KEY_DT_CREATED, umd.getDtCreated());
		values.put(KEY_DT_UPDATED, umd.getDtUpdated());
		values.put(KEY_FLG_STATUS, umd.getFlgStatus());
		values.put(KEY_FLG_DELETED, umd.getFlgDeleted());
		values.put(KEY_FLG_IS_DIRTY, umd.getFlgIsDirty());
		values.put(KEY_SHIP_HOLIDAY_LIST_ID, umd.getiShipHolidayListId());
		values.put(KEY_MIN_WORK_HOUR_WEEK_DAYS, umd.getFltMinWorkHourWeekDays());
		values.put(KEY_MIN_WORK_HOUR_SATUREDAY,
				umd.getFltMinWorkHourSaturdays());
		values.put(KEY_MIN_WORK_HOUR_SUNDAY, umd.getFltMinWorkHourSundays());
		values.put(KEY_MIN_WORK_HOUR_HOLIDAY, umd.getFltMinWorkHourHolidays());
		values.put(KEY_FLTOT_INCLUDED_IN_WAGE, umd.getFltOTIncludedInWage());
		values.put(KEY_FLTOT_RATE_PER_HOUR, umd.getFltOTRatePerHour());
		values.put(KEY_FLG_IS_OVER_TIME_ENABLED, umd.getFlgIsOverTimeEnabled());
		values.put(KEY_ROLE_ID, umd.getiRoleId());
		values.put(KEY_FLG_IS_WATCHKEEPER, umd.getFltMinWorkHourHolidays());
		values.put(KEY_FAX_NUMBER, umd.getFaxNumber());
		values.put(KEY_HAND_PHONE, umd.getHandPhone());
		values.put(KEY_LANDLINE_NUMBER, umd.getLandLineNumber());
		values.put(KEY_PAN_NUMBER, umd.getPanNumber());
		values.put(KEY_PIN_CODE, umd.getPinCode());
		values.put(KEY_ADDRESS_FIRST, umd.getAddressFirst());
		values.put(KEY_ADDRESS_SECOND, umd.getAddressSecond());
		values.put(KEY_ADDRESS_THIRD, umd.getAddressThird());
		values.put(KEY_CITY, umd.getCity());
		values.put(KEY_STATE, umd.getState());
		values.put(KEY_COUNTRY_ID, umd.getiCountryId());
		
		values.put(KEY_EMPLOYEE_NO, umd.getStrEmployeeNo());
		values.put(KEY_MIDDLE_NAME, umd.getStrMiddleName());
		
		// Inserting Row
		db.insert(TABLE_USER_MASTER, null, values);
		// 2nd argument is String containing nullColumnHack
		db.close(); // Closing database connection
	}

	// Code to add the New UserServiceTerm Row
	public void addUserServiceTermRow(UserServiceTermData usrSData) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_USER_SERVICE_TERM_ID, usrSData.getiUserServiceTermId());
		values.put(KEY_iUSER_ID, usrSData.getiUserId());
		values.put(KEY_DT_TERM_FROM, usrSData.getDtTermFrom());
		values.put(KEY_DT_TERM_TO, usrSData.getDtTermTo());
		values.put(KEY_FLG_DELETED, usrSData.getFlgDeleted());
		values.put(KEY_FLG_IS_DIRTY, usrSData.getFlgIsDirty());
		values.put(KEY_DT_CREATED, usrSData.getDtCreated());
		values.put(KEY_DT_UPDATED, usrSData.getDtUpdated());
		values.put(KEY_SHIP_ID, usrSData.getiShipId());
		values.put(KEY_TENANT_ID, usrSData.getiTenantId());

		// Inserting Row
		db.insert(TABLE_USER_SERVICE_TERM, null, values);
		// 2nd argument is String containing nullColumnHack
		db.close(); // Closing database connection
	}

	// Code to add the New UserServiceTermRole row
	public void addUserServiceTermRoleRow(UserServiceTermRoleData usrSRData) {
		SQLiteDatabase db = this.getWritableDatabase();
		long cnt = 0;
		try {
			ContentValues values = new ContentValues();
			values.put(KEY_USER_SERVICE_TERM_ROLE_ID,
					usrSRData.getStrUserServiceTermRoleId());
			values.put(KEY_DT_CREATED, usrSRData.getDtCreated());
			values.put(KEY_DT_UPDATED, usrSRData.getDtUpdated());
			values.put(KEY_FLG_DELETED, usrSRData.getFlgDeleted());
			values.put(KEY_FLG_IS_DIRTY, usrSRData.getFlgIsDirty());
			values.put(KEY_ROLE_ID, usrSRData.getiRoleId());
			values.put(KEY_SHIP_ID, usrSRData.getiShipId());
			values.put(KEY_TENANT_ID, usrSRData.getiTenantId());
			values.put(KEY_iUSER_ID, usrSRData.getIuserId());
			values.put(KEY_USER_SERVICE_TERM_ID,
					usrSRData.getIuserServiceTermId());
			values.put(KEY_DT_TERM_FROM, usrSRData.getDtTermFrom());
			values.put(KEY_DT_TERM_TO, usrSRData.getDtTermTo());
			values.put(POWER_PLUS_SCORE, usrSRData.getPowerPlusScore());
			values.put(FIELD_ONE, usrSRData.getField1());
			values.put(FIELD_TWO, usrSRData.getField2());

			// Inserting Row
			cnt = db.insert(TABLE_USER_SERVICE_TERM_ROLE, null, values);
			System.out.println("cnt cnt :===================== " + cnt);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		// 2nd argument is String containing nullColumnHack
		db.close(); // Closing database connection
	}

	// Code to add New Card Type
	public void addCardTypeRow(CardTypeData ctd) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_CARD_ID, ctd.getStrCardId());
		values.put(KEY_CARD_TYPE, ctd.getStrCardType());
		// Inserting Row
		db.insert(TABLE_CARD_TYPE, null, values);
		// 2nd argument is String containing nullColumnHack
		db.close(); // Closing database connection
	}

	// Code to add the new User NFC row
	public void addUserNFCRow(UserNfcData usrNFCData) {
		try {
			
			List<UserNfcData> list = getUserNFCRowByUserIdAndCardId(usrNFCData.getiUserId(), usrNFCData.getiUserServiceTermId());
			
			SQLiteDatabase db = this.getWritableDatabase();

			if(list == null || list.size() == 0)
			{
				ContentValues values = new ContentValues();
				values.put(KEY_NFC_CARD_ID, usrNFCData.getNfcCardId());
				values.put(KEY_CARD_TYPE, usrNFCData.getCardType());
				values.put(KEY_iUSER_ID, usrNFCData.getiUserId());
				values.put(KEY_USER_SERVICE_TERM_ID, usrNFCData.getiUserServiceTermId());
				values.put(KEY_FLG_DIRTY, usrNFCData.getFlgDirty());
				values.put(KEY_FLG_DELETED, usrNFCData.getFlgDeleted());
				values.put(KEY_DT_CREATED, usrNFCData.getDtCreated());
				values.put(KEY_DT_UPDATED, usrNFCData.getDtUpdated());
				values.put(KEY_TENANT_ID, usrNFCData.getiTenantId());
				values.put(KEY_SHIP_ID, usrNFCData.getiShipId());
				
				
				/*L.fv(""+usrNFCData.getNfcCardId());
				L.fv(""+ usrNFCData.getCardType());
				L.fv(""+usrNFCData.getiUserId());
				L.fv(""+usrNFCData.getiUserServiceTermId());
				L.fv(""+usrNFCData.getFlgDirty());
				L.fv(""+ usrNFCData.getFlgDeleted());
				L.fv(""+ usrNFCData.getDtCreated());
				L.fv(""+usrNFCData.getDtUpdated());
				L.fv(""+usrNFCData.getiTenantId());
				L.fv(""+usrNFCData.getiShipId());*/
				

				// Inserting Row
				long counter = db.insert(TABLE_USER_NFC, null, values);
			}
			
			db.close();
		} catch (Exception e) {			
			e.printStackTrace();
		} 
	}

	// Code to add the new User NFC row
	public void updateUserNFCRowByUsrid(String strUserid, String tenantid,
			String shipId) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_TENANT_ID, tenantid);
		values.put(KEY_SHIP_ID, shipId);

		String whereClause = KEY_iUSER_ID + " =  '" + strUserid + "'";

		db.update(TABLE_USER_NFC, values, whereClause, null);

		db.close(); // Closing database connection
	}

	// Code to delete the new User NFC row
	public void updateUserNFCRow(UserNfcData und) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_FLG_DIRTY, "1");
		values.put(KEY_FLG_DELETED, "1");
		String whereClause = KEY_NFC_CARD_ID + " =  '" + und.getNfcCardId()
				+ "'";
		db.update(TABLE_USER_NFC, values, whereClause, null);

		db.close(); // Closing database connection
	}

	// Code to add the new Ship Master row
	public void addPowerPlusCategoryRow(PowerPlusCategory smd) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_I_POWER_PLUS_CATEGORY_ID, smd.getiPowerPlusCategoryId());
		values.put(KEY_DT_CREATED, smd.getDtCreated());
		values.put(KEY_DT_UPDATED, smd.getDtUpdated());
		values.put(KEY_FLG_DELETED, smd.getFlgDeleted());
		values.put(KEY_FLG_IS_DIRTY, smd.getFlgIsDirty());
		values.put(KEY_FLG_STATUS, smd.getFlgStatus());
		values.put(KEY_I_SEQUENCE, smd.getiSequence());
		values.put(KEY_STR_DESCRIPTION, smd.getStrDescription());
		values.put(KEY_STR_TITLE, smd.getStrTitle());
		values.put(KEY_TENANT_ID, smd.getiTenantId());
		values.put(KEY_STR_CATEGORY_CODE, smd.getStrCategoryCode());

		// Inserting Row
		db.insert(TABLE_POWER_PLUS_CATEGOEY, null, values);
		// 2nd argument is String containing nullColumnHack
		db.close(); // Closing database connection
	}

	// Code to add the n row
	public void addPowerPlusPeriodicityRow(PowerPlusPeriodicity ppp) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_I_POWER_PLUS_PERIODICITY_ID,
				ppp.getiPowerPlusPeriodicityId());
		values.put(KEY_DT_CREATED, ppp.getDtCreated());
		values.put(KEY_DT_UPDATED, ppp.getDtUpdated());
		values.put(KEY_FLG_DELETED, ppp.getFlgDeleted());
		values.put(KEY_FLG_IS_DIRTY, ppp.getFlgIsDirty());
		values.put(KEY_FLG_STATUS, ppp.getFlgStatus());
		values.put(KEY_I_INTERVAL, ppp.getiInterval());
		values.put(KEY_I_SEQUENCE, ppp.getiSequence());
		values.put(KEY_I_TIMES, ppp.getiTimes());
		values.put(KEY_STR_DESCRIPTION, ppp.getStrDescription());
		values.put(KEY_STR_PATTERN, ppp.getStrPattern());
		values.put(KEY_STR_TITLE, ppp.getStrTitle());
		values.put(KEY_TENANT_ID, ppp.getiTenantId());

		// Inserting Row
		db.insert(TABLE_POWER_PLUS_PERIODICITY, null, values);
		// 2nd argument is String containing nullColumnHack
		db.close(); // Closing database connection
	}

	// Code to add the new Ship Master row
	public void addPowerPlusTaskRow(PowerPlusTask ppt) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_I_POWER_PLUS_TASK_ID, ppt.getiPowerPlusTaskId());
		values.put(KEY_APPROVED_BY, ppt.getApprovedBy());
		values.put(KEY_DT_CREATED, ppt.getDtCreated());
		values.put(KEY_DT_UPDATED, ppt.getDtUpdated());
		values.put(KEY_FLG_DELETED, ppt.getFlgDeleted());
		values.put(KEY_FLG_IS_ALL_RECEIVE, ppt.getFlgIsAllReceive());
		values.put(KEY_FLG_IS_DIRTY, ppt.getFlgIsDirty());
		values.put(KEY_FLG_STATUS, ppt.getFlgStatus());
		values.put(KEY_I_SEQUENCE, ppt.getiSequence());
		values.put(KEY_POWER_POINTS, ppt.getPowerPoints());
		values.put(KEY_PROVIDED_BY, ppt.getProvidedBy());
		values.put(KEY_RECEIVED_BY, ppt.getReceivedBy());
		values.put(KEY_STR_ADDITIONAL_ONE, ppt.getStrAdditionalOne());
		values.put(KEY_STR_ADDITIONAL_TWO, ppt.getStrAdditionalTwo());
		values.put(KEY_STR_DESCRIPTION, ppt.getStrDescription());
		values.put(KEY_STR_TITLE, ppt.getStrTitle());
		values.put(KEY_I_POWER_PLUS_CATEGORY_ID, ppt.getiPowerPlusCategoryId());
		values.put(KEY_I_POWER_PLUS_PERIODICITY_ID,
				ppt.getiPowerPlusPeriodicityId());
		values.put(KEY_TENANT_ID, ppt.getiTenantId());

		// Inserting Row
		db.insert(TABLE_POWER_PLUS_TASK, null, values);
		// 2nd argument is String containing nullColumnHack
		db.close(); // Closing database connection
	}

	// Code to add the new Ship Master row
	public void addPowerPlusTransactionRow(PowerPlusTransaction ppt) {
		SQLiteDatabase db = this.getWritableDatabase();

		 L.fv("Approver id ppTran: "+ppt.getStrApproverId());	
		 
		ContentValues values = new ContentValues();
		values.put(KEY_I_POWER_PLUS_TRANSACTION_ID,
				ppt.getiPowerPlusTransactionId());
		values.put(KEY_DT_CREATED, ppt.getDtCreated());
		values.put(KEY_DT_UPDATED, ppt.getDtUpdated());
		values.put(KEY_FLG_DELETED, ppt.getFlgDeleted());
		values.put(KEY_FLG_IS_DIRTY, ppt.getFlgIsDirty());
		values.put(KEY_FLG_IS_TRANSACTION_INVALID,
				ppt.getFlgIsTransationInvalid());
		values.put(KEY_FLG_STATUS, ppt.getFlgStatus());
		values.put(KEY_I_SEQUENCE, ppt.getiSequence());
		values.put(KEY_POWER_POINTS, ppt.getPowerPoints());
		values.put(KEY_STR_ADDITIONAL_ONE, ppt.getStrAdditionalOne());
		values.put(KEY_STR_ADDITIONAL_TWO, ppt.getStrAdditionalTwo());
		values.put(KEY_STR_APPROVER_ID, ppt.getStrApproverId());
		values.put(KEY_STR_APPROVER_NAME, ppt.getStrApproverName());
		values.put(KEY_STR_DESCRIPTION, ppt.getStrDescription());
		values.put(KEY_STR_PROVIDER_ID, ppt.getStrProviderId());
		values.put(KEY_STR_PROVIDER_NAME, ppt.getStrProviderName());
		values.put(KEY_STR_TITLE, ppt.getStrTitle());
		values.put(KEY_I_POWER_PLUS_TASK_ID, ppt.getiPowerPlusTaskId());
		values.put(KEY_SHIP_ID, ppt.getiShipId());
		values.put(KEY_TENANT_ID, ppt.getiTenantId());
		values.put(KEY_STR_RECEIVER_ID, ppt.getiUserId());
		values.put(KEY_STR_RECEIVER_NAME, ppt.getStrReceiverName());
		values.put(KEY_FLG_SCORE_STATUS, ppt.getFlgScoreStatus());
		values.put(KEY_I_POWER_PLUS_CATEGORY_ID, ppt.getCetogoryId());
		values.put(KEY_PARENT_TRANSACTION_ID, ppt.getParentTransactionId());
		values.put(KEY_ROLE_ID, ppt.getiRoleId());
		values.put(KEY_USER_SERVICE_TERM_ID, ppt.getiUserServiceTermId());
		values.put(KEY_PROCESSED_AUTO, ppt.getProcessedAuto());

		// Inserting Row
		db.insert(TABLE_POWER_PLUS_TRANSACTION, null, values);
		// 2nd argument is String containing nullColumnHack
		db.close(); // Closing database connection
	}

	// Code to add the new Ship Master row
	public long UpdatePowerPlusTransactionRow(PowerPlusTransaction ppt) {
		SQLiteDatabase db = this.getWritableDatabase();

		// L.fv("Approver id ppTran: "+ppt.getStrApproverId());	
		 
		ContentValues values = new ContentValues();
		values.put(KEY_I_POWER_PLUS_TRANSACTION_ID,
				ppt.getiPowerPlusTransactionId());
		values.put(KEY_DT_CREATED, ppt.getDtCreated());
		values.put(KEY_DT_UPDATED, ppt.getDtUpdated());
		values.put(KEY_FLG_DELETED, ppt.getFlgDeleted());
		values.put(KEY_FLG_IS_DIRTY, ppt.getFlgIsDirty());
		values.put(KEY_FLG_IS_TRANSACTION_INVALID,
				ppt.getFlgIsTransationInvalid());
		values.put(KEY_FLG_STATUS, ppt.getFlgStatus());
		values.put(KEY_I_SEQUENCE, ppt.getiSequence());
		values.put(KEY_POWER_POINTS, ppt.getPowerPoints());
		values.put(KEY_STR_ADDITIONAL_ONE, ppt.getStrAdditionalOne());
		values.put(KEY_STR_ADDITIONAL_TWO, ppt.getStrAdditionalTwo());
		values.put(KEY_STR_APPROVER_ID, ppt.getStrApproverId());
		values.put(KEY_STR_APPROVER_NAME, ppt.getStrApproverName());
		values.put(KEY_STR_DESCRIPTION, ppt.getStrDescription());
		values.put(KEY_STR_PROVIDER_ID, ppt.getStrProviderId());
		values.put(KEY_STR_PROVIDER_NAME, ppt.getStrProviderName());
		values.put(KEY_STR_TITLE, ppt.getStrTitle());
		values.put(KEY_I_POWER_PLUS_TASK_ID, ppt.getiPowerPlusTaskId());
		values.put(KEY_SHIP_ID, ppt.getiShipId());
		values.put(KEY_TENANT_ID, ppt.getiTenantId());
		values.put(KEY_STR_RECEIVER_ID, ppt.getiUserId());
		values.put(KEY_STR_RECEIVER_NAME, ppt.getStrReceiverName());
		values.put(KEY_FLG_SCORE_STATUS, ppt.getFlgScoreStatus());
		values.put(KEY_I_POWER_PLUS_CATEGORY_ID, ppt.getCetogoryId());
		values.put(KEY_PARENT_TRANSACTION_ID, ppt.getParentTransactionId());
		values.put(KEY_ROLE_ID, ppt.getiRoleId());
		values.put(KEY_USER_SERVICE_TERM_ID, ppt.getiUserServiceTermId());
		values.put(KEY_PROCESSED_AUTO, ppt.getProcessedAuto());

		// String whereClause = KEY_I_POWER_PLUS_TASK_ID + " =  '" +
		// ppt.getiPowerPlusTaskId() + "'";

		String whereClause = KEY_I_POWER_PLUS_TRANSACTION_ID + " =  '"
				+ ppt.getiPowerPlusTransactionId() + "'";

		long ii = db.update(TABLE_POWER_PLUS_TRANSACTION, values, whereClause,
				null);

		db.close();
		return ii;
	}

	/*
	 * >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
	 * >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
	 * >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
	 * >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
	 * >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
	 * >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>Add Training Related Data
	 * Row>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
	 * >>>>>>>>>>>>>>>>>>>>>
	 * >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
	 * >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
	 * >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
	 * >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
	 * >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
	 */

	// Code to add the new Training row
	public void addTrainingRow(TrainingData ppt) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_TRAINING_DATA_ID, ppt.getTraningDataId());
		values.put(KEY_STR_PARENT_ID, ppt.getStrParentId());
		values.put(KEY_STR_TITLE, ppt.getStrTitle());
		values.put(KEY_STR_DESCRIPTION, ppt.getStrDescription());
		values.put(KEY_FLG_DELETED, ppt.getFlgDeleted());
		values.put(KEY_SHIP_ID, ppt.getiShipId());
		values.put(KEY_TENANT_ID, ppt.getiTenantId());
		values.put(KEY_DT_CREATED, ppt.getDtCreated());
		values.put(KEY_DT_UPDATED, ppt.getDtUpdated());
		values.put(KEY_FLG_STATUS, ppt.getFlgStatus());
		values.put(KEY_FLG_IS_DIRTY, ppt.getFlgIsDirty());

		// Inserting Row
		db.insert(TABLE_TRAINING, null, values);
		// 2nd argument is String containing nullColumnHack
		db.close(); // Closing database connection
	}

	// Code to add the new TrainingAttendance row
	public void addTrainingAttendanceRow(TraingAttendance ppt) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_TRAINING_ATTENDANCE_ID, ppt.getTraningAttendanceId());
		values.put(KEY_TRAINING_TRANSACTION_ID, ppt.getTraningTransactionId());
		values.put(KEY_TRAINING_DATA_ID, ppt.getTraningDataId());
		values.put(KEY_STR_PARENT_ID, ppt.getStrParentId());
		values.put(KEY_TRAINING_DATE, ppt.getTraningDate());
		values.put(KEY_TRAINING_TIME, ppt.getTrainingTime());
		values.put(KEY_STR_DESCRIPTION, ppt.getStrDescription());
		values.put(KEY_FLG_DELETED, ppt.getFlgDeleted());
		values.put(KEY_SHIP_ID, ppt.getiShipId());
		values.put(KEY_TENANT_ID, ppt.getiTenantId());
		values.put(KEY_DT_CREATED, ppt.getDtCreated());
		values.put(KEY_DT_UPDATED, ppt.getDtUpdated());
		values.put(KEY_FLG_STATUS, ppt.getFlgStatus());
		values.put(KEY_FLG_IS_DIRTY, ppt.getFlgIsDirty());
		values.put(KEY_iUSER_ID, ppt.getiUserId());
		values.put(KEY_USER_SERVICE_TERM_ID, ppt.getiUserServiceTermId());

		// Inserting Row
		db.insert(TABLE_TRAINING_ATTENDANCE, null, values);
		// 2nd argument is String containing nullColumnHack
		db.close(); // Closing database connection
	}

	// Code to add the new TrainingAttendance row
	public void updateTrainingAttendanceRow(TraingAttendance ppt) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_TRAINING_ATTENDANCE_ID, ppt.getTraningAttendanceId());
		values.put(KEY_TRAINING_TRANSACTION_ID, ppt.getTraningTransactionId());
		values.put(KEY_TRAINING_DATA_ID, ppt.getTraningDataId());
		values.put(KEY_STR_PARENT_ID, ppt.getStrParentId());
		values.put(KEY_TRAINING_DATE, ppt.getTraningDate());
		values.put(KEY_TRAINING_TIME, ppt.getTrainingTime());
		values.put(KEY_STR_DESCRIPTION, ppt.getStrDescription());
		values.put(KEY_FLG_DELETED, ppt.getFlgDeleted());
		values.put(KEY_SHIP_ID, ppt.getiShipId());
		values.put(KEY_TENANT_ID, ppt.getiTenantId());
		values.put(KEY_DT_CREATED, ppt.getDtCreated());
		values.put(KEY_DT_UPDATED, ppt.getDtUpdated());
		values.put(KEY_FLG_STATUS, ppt.getFlgStatus());
		values.put(KEY_FLG_IS_DIRTY, ppt.getFlgIsDirty());
		values.put(KEY_iUSER_ID, ppt.getiUserId());
		values.put(KEY_USER_SERVICE_TERM_ID, ppt.getiUserServiceTermId());

		String whereClause = KEY_TRAINING_ATTENDANCE_ID + " = '"
				+ ppt.getTraningAttendanceId() + "'";
		// Inserting Row
		db.update(TABLE_TRAINING_ATTENDANCE, values, whereClause, null);
		// 2nd argument is String containing nullColumnHack
		db.close(); // Closing database connection
	}
	
	// Code to add the new TrainingAttendance row
	public void updateTrainingAttendanceByTransactionid(String strTransactionid) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();	
		values.put(KEY_FLG_DELETED, "1");
		values.put(KEY_FLG_IS_DIRTY, "1");
		String whereClause = KEY_TRAINING_TRANSACTION_ID + " = '"
				+ strTransactionid + "'";
		// Inserting Row
		db.update(TABLE_TRAINING_ATTENDANCE, values, whereClause, null);
		// 2nd argument is String containing nullColumnHack
		db.close(); // Closing database connection
	}

	// Code to add the new TrainingAttendance row
	public void addTrainingTransactionRow(TrainingTransaction ppt) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_TRAINING_TRANSACTION_ID, ppt.getTraningTransactionId());
		values.put(KEY_TRAINING_DATA_ID, ppt.getTraningDataId());
		values.put(KEY_TRAINING_DATE, ppt.getTraningDate());
		values.put(KEY_TRAINING_TIME, ppt.getTrainingTime());
		values.put(KEY_STR_DESCRIPTION, ppt.getStrDescription());
		values.put(KEY_FLG_DELETED, ppt.getFlgDeleted());
		values.put(KEY_SHIP_ID, ppt.getiShipId());
		values.put(KEY_TENANT_ID, ppt.getiTenantId());
		values.put(KEY_DT_CREATED, ppt.getDtCreated());
		values.put(KEY_DT_UPDATED, ppt.getDtUpdated());
		values.put(KEY_FLG_STATUS, ppt.getFlgStatus());
		values.put(KEY_FLG_IS_DIRTY, ppt.getFlgIsDirty());
		values.put(KEY_STR_PARENT_ID, ppt.getStrParentId());

		// Inserting Row
		db.insert(TABLE_TRAINING_TRANSACTION, null, values);
		// 2nd argument is String containing nullColumnHack
		db.close(); // Closing database connection
	}

	// Code to add the new TrainingAttendance row
	public void UpdateTrainingTransactionRow(TrainingTransaction ppt) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_TRAINING_TRANSACTION_ID, ppt.getTraningTransactionId());
		values.put(KEY_TRAINING_DATA_ID, ppt.getTraningDataId());
		values.put(KEY_TRAINING_DATE, ppt.getTraningDate());
		values.put(KEY_TRAINING_TIME, ppt.getTrainingTime());
		values.put(KEY_STR_DESCRIPTION, ppt.getStrDescription());
		values.put(KEY_FLG_DELETED, ppt.getFlgDeleted());
		values.put(KEY_SHIP_ID, ppt.getiShipId());
		values.put(KEY_TENANT_ID, ppt.getiTenantId());
		values.put(KEY_DT_CREATED, ppt.getDtCreated());
		values.put(KEY_DT_UPDATED, ppt.getDtUpdated());
		values.put(KEY_FLG_STATUS, ppt.getFlgStatus());
		values.put(KEY_FLG_IS_DIRTY, ppt.getFlgIsDirty());
		values.put(KEY_STR_PARENT_ID, ppt.getStrParentId());

		String whereClause = KEY_TRAINING_TRANSACTION_ID + " = '"
				+ ppt.getTraningTransactionId() + "'";
		// Inserting Row
		db.update(TABLE_TRAINING_TRANSACTION, values, whereClause, null);
		// 2nd argument is String containing nullColumnHack
		db.close(); // Closing database connection
	}

	/*
	 * >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
	 * >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
	 * >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
	 * >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
	 * >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
	 * >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>Get All Rows of Training Related Data
	 * >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
	 * >>>>>>>>>>>>>>>>>>>>>>>>
	 * >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
	 * >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
	 * >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
	 * >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
	 * >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
	 */

	public List<TraingAttendance> getTrainingAttendanceByTransactionId(
			String transactionId, String flgStatus) {
		List<TraingAttendance> shipMasterDataList = new ArrayList<TraingAttendance>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + TABLE_TRAINING_ATTENDANCE
				+ " where " + KEY_TRAINING_TRANSACTION_ID + " = '"
				+ transactionId + "' and " + KEY_FLG_STATUS + " = '"
				+ flgStatus + "' and " + KEY_FLG_DELETED + " = '0'";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				TraingAttendance data = new TraingAttendance();
				data.setId(Integer.parseInt(cursor.getString(0)));
				data.setTraningAttendanceId(cursor.getString(1));
				data.setTraningTransactionId(cursor.getString(2));
				data.setTraningDataId(cursor.getString(3));
				data.setStrParentId(cursor.getString(4));
				data.setTraningDate(cursor.getString(5));
				data.setTrainingTime(cursor.getString(6));
				data.setStrDescription(cursor.getString(7));
				data.setFlgDeleted(cursor.getString(8));
				data.setiShipId(cursor.getString(9));
				data.setiTenantId(cursor.getString(10));
				data.setDtCreated(cursor.getString(11));
				data.setDtUpdated(cursor.getString(12));
				data.setFlgStatus(cursor.getString(13));
				data.setFlgIsDirty(cursor.getString(14));
				data.setiUserId(cursor.getString(15));
				data.setiUserServiceTermId(cursor.getString(16));
				// Adding contact to list
				shipMasterDataList.add(data);
			} while (cursor.moveToNext());
		}

		db.close();
		// return contact list
		return shipMasterDataList;
	}

	// Code to get all TrainingAttendance row in a list
	public List<TraingAttendance> getTrainingAttendanceByTransactionAndUserId(
			String transactionId, String userId, String flgStatus) {
		List<TraingAttendance> shipMasterDataList = new ArrayList<TraingAttendance>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + TABLE_TRAINING_ATTENDANCE
				+ " where " + KEY_TRAINING_TRANSACTION_ID + " = '"
				+ transactionId + "' and " + KEY_iUSER_ID + " = '" + userId
				+ "' and " + KEY_FLG_STATUS + " = '" + flgStatus + "' and "
				+ KEY_FLG_DELETED + " = '0'";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				TraingAttendance data = new TraingAttendance();
				data.setId(Integer.parseInt(cursor.getString(0)));
				data.setTraningAttendanceId(cursor.getString(1));
				data.setTraningTransactionId(cursor.getString(2));
				data.setTraningDataId(cursor.getString(3));
				data.setStrParentId(cursor.getString(4));
				data.setTraningDate(cursor.getString(5));
				data.setTrainingTime(cursor.getString(6));
				data.setStrDescription(cursor.getString(7));
				data.setFlgDeleted(cursor.getString(8));
				data.setiShipId(cursor.getString(9));
				data.setiTenantId(cursor.getString(10));
				data.setDtCreated(cursor.getString(11));
				data.setDtUpdated(cursor.getString(12));
				data.setFlgStatus(cursor.getString(13));
				data.setFlgIsDirty(cursor.getString(14));
				data.setiUserId(cursor.getString(15));
				data.setiUserServiceTermId(cursor.getString(16));
				// Adding contact to list
				shipMasterDataList.add(data);
			} while (cursor.moveToNext());
		}

		db.close();
		// return contact list
		return shipMasterDataList;
	}

	// Code to get all TrainingAttendance row in a list
	public List<TraingAttendance> getTrainingAttendanceByTransactionAndUserIdByTwo(
			String transactionId, String userId) {
		List<TraingAttendance> shipMasterDataList = new ArrayList<TraingAttendance>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + TABLE_TRAINING_ATTENDANCE
				+ " where " + KEY_TRAINING_TRANSACTION_ID + " = '"
				+ transactionId + "' and " + KEY_iUSER_ID + " = '" + userId
				+ "' and " + KEY_FLG_DELETED + " = '0'";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				TraingAttendance data = new TraingAttendance();
				data.setId(Integer.parseInt(cursor.getString(0)));
				data.setTraningAttendanceId(cursor.getString(1));
				data.setTraningTransactionId(cursor.getString(2));
				data.setTraningDataId(cursor.getString(3));
				data.setStrParentId(cursor.getString(4));
				data.setTraningDate(cursor.getString(5));
				data.setTrainingTime(cursor.getString(6));
				data.setStrDescription(cursor.getString(7));
				data.setFlgDeleted(cursor.getString(8));
				data.setiShipId(cursor.getString(9));
				data.setiTenantId(cursor.getString(10));
				data.setDtCreated(cursor.getString(11));
				data.setDtUpdated(cursor.getString(12));
				data.setFlgStatus(cursor.getString(13));
				data.setFlgIsDirty(cursor.getString(14));
				data.setiUserId(cursor.getString(15));
				data.setiUserServiceTermId(cursor.getString(16));
				// Adding contact to list
				shipMasterDataList.add(data);
			} while (cursor.moveToNext());
		}

		db.close();
		// return contact list
		return shipMasterDataList;
	}

	// Code to get all TrainingAttendance row in a list
	public ArrayList<TraingAttendance> getTrainingAttendanceByUserIdAndStatus(
			String userId, String flgStatus) {
		ArrayList<TraingAttendance> shipMasterDataList = new ArrayList<TraingAttendance>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + TABLE_TRAINING_ATTENDANCE
				+ " where " + KEY_iUSER_ID + " = '" + userId + "' and "
				+ KEY_FLG_STATUS + " = '" + flgStatus + "' and "
				+ KEY_FLG_DELETED + " = '0'";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				TraingAttendance data = new TraingAttendance();
				data.setId(Integer.parseInt(cursor.getString(0)));
				data.setTraningAttendanceId(cursor.getString(1));
				data.setTraningTransactionId(cursor.getString(2));
				data.setTraningDataId(cursor.getString(3));
				data.setStrParentId(cursor.getString(4));
				data.setTraningDate(cursor.getString(5));
				data.setTrainingTime(cursor.getString(6));
				data.setStrDescription(cursor.getString(7));
				data.setFlgDeleted(cursor.getString(8));
				data.setiShipId(cursor.getString(9));
				data.setiTenantId(cursor.getString(10));
				data.setDtCreated(cursor.getString(11));
				data.setDtUpdated(cursor.getString(12));
				data.setFlgStatus(cursor.getString(13));
				data.setFlgIsDirty(cursor.getString(14));
				data.setiUserId(cursor.getString(15));
				data.setiUserServiceTermId(cursor.getString(16));
				// Adding contact to list
				shipMasterDataList.add(data);
			} while (cursor.moveToNext());
		}

		db.close();
		// return contact list
		return shipMasterDataList;
	}

	// Code to get all TrainingAttendance row in a list
	public List<TraingAttendance> getTrainingAttendanceRow() {
		List<TraingAttendance> shipMasterDataList = new ArrayList<TraingAttendance>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + TABLE_TRAINING_ATTENDANCE + " where "+ KEY_FLG_DELETED +" ='0'";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				TraingAttendance data = new TraingAttendance();
				data.setId(Integer.parseInt(cursor.getString(0)));
				data.setTraningAttendanceId(cursor.getString(1));
				data.setTraningTransactionId(cursor.getString(2));
				data.setTraningDataId(cursor.getString(3));
				data.setStrParentId(cursor.getString(4));
				data.setTraningDate(cursor.getString(5));
				data.setTrainingTime(cursor.getString(6));
				data.setStrDescription(cursor.getString(7));
				data.setFlgDeleted(cursor.getString(8));
				data.setiShipId(cursor.getString(9));
				data.setiTenantId(cursor.getString(10));
				data.setDtCreated(cursor.getString(11));
				data.setDtUpdated(cursor.getString(12));
				data.setFlgStatus(cursor.getString(13));
				data.setFlgIsDirty(cursor.getString(14));
				data.setiUserId(cursor.getString(15));
				data.setiUserServiceTermId(cursor.getString(16));
				// Adding contact to list
				shipMasterDataList.add(data);
			} while (cursor.moveToNext());
		}

		db.close();
		// return contact list
		return shipMasterDataList;
	}

	// Code to get all TrainingAttendance row in a list
	public List<TrainingData> getTrainingRow() {
		List<TrainingData> trainingList = new ArrayList<TrainingData>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + TABLE_TRAINING + " where "+ KEY_FLG_DELETED +" ='0'";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				TrainingData data = new TrainingData();
				data.setId(Integer.parseInt(cursor.getString(0)));
				data.setTraningDataId(cursor.getString(1));
				data.setStrParentId(cursor.getString(2));
				data.setStrTitle(cursor.getString(3));
				data.setStrDescription(cursor.getString(4));
				data.setFlgDeleted(cursor.getString(5));
				data.setiShipId(cursor.getString(6));
				data.setiTenantId(cursor.getString(7));
				data.setDtCreated(cursor.getString(8));
				data.setDtUpdated(cursor.getString(9));
				data.setFlgStatus(cursor.getString(10));
				data.setFlgIsDirty(cursor.getString(11));
				// Adding contact to list
				trainingList.add(data);
			} while (cursor.moveToNext());
		}

		db.close();
		// return contact list
		return trainingList;
	}

	// Code to get all TrainingAttendance row in a list
	public ArrayList<TrainingData> getTrainingDataById(String trDataId) {
		ArrayList<TrainingData> trainingList = new ArrayList<TrainingData>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + TABLE_TRAINING + " where "
				+ KEY_TRAINING_DATA_ID + " = '" + trDataId + "' and "+ KEY_FLG_DELETED +" ='0'";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				TrainingData data = new TrainingData();
				data.setId(Integer.parseInt(cursor.getString(0)));
				data.setTraningDataId(cursor.getString(1));
				data.setStrParentId(cursor.getString(2));
				data.setStrTitle(cursor.getString(3));
				data.setStrDescription(cursor.getString(4));
				data.setFlgDeleted(cursor.getString(5));
				data.setiShipId(cursor.getString(6));
				data.setiTenantId(cursor.getString(7));
				data.setDtCreated(cursor.getString(8));
				data.setDtUpdated(cursor.getString(9));
				data.setFlgStatus(cursor.getString(10));
				data.setFlgIsDirty(cursor.getString(11));
				// Adding contact to list
				trainingList.add(data);
			} while (cursor.moveToNext());
		}

		db.close();
		// return contact list
		return trainingList;
	}

	// Code to get all TrainingAttendance row in a list
	public ArrayList<TrainingData> getTrainingDataForCategory(String parentId) {
		ArrayList<TrainingData> trainingList = new ArrayList<TrainingData>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + TABLE_TRAINING + " where "
				+ KEY_STR_PARENT_ID + " = '" + parentId + "' and "+ KEY_FLG_DELETED +" ='0' order by "+KEY_I_SEQUENCE;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				TrainingData data = new TrainingData();
				data.setId(Integer.parseInt(cursor.getString(0)));
				data.setTraningDataId(cursor.getString(1));
				data.setStrParentId(cursor.getString(2));
				data.setStrTitle(cursor.getString(3));
				data.setStrDescription(cursor.getString(4));
				data.setFlgDeleted(cursor.getString(5));
				data.setiShipId(cursor.getString(6));
				data.setiTenantId(cursor.getString(7));
				data.setDtCreated(cursor.getString(8));
				data.setDtUpdated(cursor.getString(9));
				data.setFlgStatus(cursor.getString(10));
				data.setFlgIsDirty(cursor.getString(11));
				// Adding contact to list
				trainingList.add(data);
			} while (cursor.moveToNext());
		}

		db.close();
		// return contact list
		return trainingList;
	}
	
	/**
	 * Select training data for BBS category as deleted=5 and status=99
	 */
	public ArrayList<TrainingData> getTrainingDataForCategoryForBBS(String strTitle, String strTrainingCode) {
		ArrayList<TrainingData> trainingList = new ArrayList<TrainingData>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + TABLE_TRAINING + " where "
				+ KEY_STR_TITLE + " = '" + strTitle + "' and "+ KEY_FLG_DELETED +" ='0' and "+KEY_FLG_STATUS+" = '99' order by "+KEY_I_SEQUENCE;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				TrainingData data = new TrainingData();
				data.setId(Integer.parseInt(cursor.getString(0)));
				data.setTraningDataId(cursor.getString(1));
				data.setStrParentId(cursor.getString(2));
				data.setStrTitle(cursor.getString(3));
				data.setStrDescription(cursor.getString(4));
				data.setFlgDeleted(cursor.getString(5));
				data.setiShipId(cursor.getString(6));
				data.setiTenantId(cursor.getString(7));
				data.setDtCreated(cursor.getString(8));
				data.setDtUpdated(cursor.getString(9));
				data.setFlgStatus(cursor.getString(10));
				data.setFlgIsDirty(cursor.getString(11));
				// Adding contact to list
				trainingList.add(data);
			} while (cursor.moveToNext());
		}

		db.close();
		// return contact list
		return trainingList;
	}

	// Code to get all TrainingAttendance row in a list
	public List<TrainingTransaction> getTrainingTrainsactionRow() {
		List<TrainingTransaction> trainingTransactionList = new ArrayList<TrainingTransaction>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + TABLE_TRAINING_TRANSACTION +" where "+ KEY_FLG_DELETED +" ='0'";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				TrainingTransaction data = new TrainingTransaction();
				data.setId(Integer.parseInt(cursor.getString(0)));
				data.setTraningTransactionId(cursor.getString(1));
				data.setTraningDataId(cursor.getString(2));
				data.setTraningDate(cursor.getString(3));
				data.setTrainingTime(cursor.getString(4));
				data.setStrDescription(cursor.getString(5));
				data.setFlgDeleted(cursor.getString(6));
				data.setiShipId(cursor.getString(7));
				data.setiTenantId(cursor.getString(8));
				data.setDtCreated(cursor.getString(9));
				data.setDtUpdated(cursor.getString(10));
				data.setFlgStatus(cursor.getString(11));
				data.setFlgIsDirty(cursor.getString(12));
				data.setStrParentId(cursor.getString(13));
				// Adding contact to list
				trainingTransactionList.add(data);
			} while (cursor.moveToNext());
		}

		db.close();
		// return contact list
		return trainingTransactionList;
	}
	
	// Code to get all TrainingAttendance row in a list
	public List<TrainingTransaction> getTrainingTrainsactionRowByDate(String strDate) {
		List<TrainingTransaction> trainingTransactionList = new ArrayList<TrainingTransaction>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + TABLE_TRAINING_TRANSACTION +" where "+ KEY_DT_CREATED+" <='"+strDate+"' and "+ KEY_TRAINING_DATE+" <='"+strDate+"' and "+KEY_FLG_IS_DIRTY+"='0'";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				TrainingTransaction data = new TrainingTransaction();
				data.setId(Integer.parseInt(cursor.getString(0)));
				data.setTraningTransactionId(cursor.getString(1));
				data.setTraningDataId(cursor.getString(2));
				data.setTraningDate(cursor.getString(3));
				data.setTrainingTime(cursor.getString(4));
				data.setStrDescription(cursor.getString(5));
				data.setFlgDeleted(cursor.getString(6));
				data.setiShipId(cursor.getString(7));
				data.setiTenantId(cursor.getString(8));
				data.setDtCreated(cursor.getString(9));
				data.setDtUpdated(cursor.getString(10));
				data.setFlgStatus(cursor.getString(11));
				data.setFlgIsDirty(cursor.getString(12));
				data.setStrParentId(cursor.getString(13));
				// Adding contact to list
				trainingTransactionList.add(data);
			} while (cursor.moveToNext());
		}

		db.close();
		// return contact list
		return trainingTransactionList;
	}

	// Code to get all TrainingAttendance row in a list
	public List<TrainingTransaction> getTrainingTrainsactionByDataIdDateTime(
			String dataId, String date, String time) {
		List<TrainingTransaction> trainingTransactionList = new ArrayList<TrainingTransaction>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + TABLE_TRAINING_TRANSACTION
				+ " where " + KEY_TRAINING_DATA_ID + " = '" + dataId + "' and "
				+ KEY_TRAINING_DATE + " = '" + date + "' and "
				+ KEY_TRAINING_TIME + " = '" + time + "' and "+ KEY_FLG_DELETED +" ='0'";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				TrainingTransaction data = new TrainingTransaction();
				data.setId(Integer.parseInt(cursor.getString(0)));
				data.setTraningTransactionId(cursor.getString(1));
				data.setTraningDataId(cursor.getString(2));
				data.setTraningDate(cursor.getString(3));
				data.setTrainingTime(cursor.getString(4));
				data.setStrDescription(cursor.getString(5));
				data.setFlgDeleted(cursor.getString(6));
				data.setiShipId(cursor.getString(7));
				data.setiTenantId(cursor.getString(8));
				data.setDtCreated(cursor.getString(9));
				data.setDtUpdated(cursor.getString(10));
				data.setFlgStatus(cursor.getString(11));
				data.setFlgIsDirty(cursor.getString(12));
				data.setStrParentId(cursor.getString(13));
				// Adding contact to list
				trainingTransactionList.add(data);
			} while (cursor.moveToNext());
		}

		db.close();
		// return contact list
		return trainingTransactionList;
	}

	// Code to get all TrainingAttendance row in a list
	public List<TrainingTransaction> getTrainingTrainsactionByDataId(
			String dataId) {
		List<TrainingTransaction> trainingTransactionList = new ArrayList<TrainingTransaction>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + TABLE_TRAINING_TRANSACTION
				+ " where " + KEY_TRAINING_TRANSACTION_ID + " = '" + dataId
				+ "' and "+ KEY_FLG_DELETED +" ='0'";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				TrainingTransaction data = new TrainingTransaction();
				data.setId(Integer.parseInt(cursor.getString(0)));
				data.setTraningTransactionId(cursor.getString(1));
				data.setTraningDataId(cursor.getString(2));
				data.setTraningDate(cursor.getString(3));
				data.setTrainingTime(cursor.getString(4));
				data.setStrDescription(cursor.getString(5));
				data.setFlgDeleted(cursor.getString(6));
				data.setiShipId(cursor.getString(7));
				data.setiTenantId(cursor.getString(8));
				data.setDtCreated(cursor.getString(9));
				data.setDtUpdated(cursor.getString(10));
				data.setFlgStatus(cursor.getString(11));
				data.setFlgIsDirty(cursor.getString(12));
				data.setStrParentId(cursor.getString(13));
				// Adding contact to list
				trainingTransactionList.add(data);
			} while (cursor.moveToNext());
		}

		db.close();
		// return contact list
		return trainingTransactionList;
	}

	/*******End Training Related Data **********/

	// Code to get all UserNfc row in a list
	public List<UserNfcData> getUserNFCRow() {
		List<UserNfcData> nfcDataList = new ArrayList<UserNfcData>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + TABLE_USER_NFC;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				UserNfcData data = new UserNfcData();
				data.setId(Integer.parseInt(cursor.getString(0)));
				data.setNfcCardId(cursor.getString(1));
				data.setCardType(cursor.getString(2));
				data.setiUserId(cursor.getString(3));
				data.setiUserServiceTermId(cursor.getString(4));
				data.setFlgDirty(cursor.getString(5));
				data.setFlgDeleted(cursor.getString(6));
				data.setDtCreated(cursor.getString(7));
				data.setDtUpdated(cursor.getString(8));
				data.setiTenantId(cursor.getString(9));
				data.setiShipId(cursor.getString(10));

				// Adding contact to list
				nfcDataList.add(data);
			} while (cursor.moveToNext());
		}

		db.close();
		// return contact list
		return nfcDataList;
	}

	// Code to get all UserNfc row in a list
	public ArrayList<UserNfcData> getUserNFCSingleRow(String id) {
		ArrayList<UserNfcData> nfcDataList = new ArrayList<UserNfcData>();
		// Select All Query
		/*String selectQuery = "SELECT  * FROM " + TABLE_USER_NFC + " WHERE "
				+ KEY_NFC_CARD_ID + " = " + id + " and " + KEY_FLG_DELETED
				+ " = '0'";*/
		String selectQuery = "SELECT  * FROM " + TABLE_USER_NFC + " WHERE "
				+ KEY_USER_SERVICE_TERM_ID + " = " + id + " and " + KEY_FLG_DELETED
				+ " = '0'";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				UserNfcData data = new UserNfcData();
				data.setId(Integer.parseInt(cursor.getString(0)));
				data.setNfcCardId(cursor.getString(1));
				data.setCardType(cursor.getString(2));
				data.setiUserId(cursor.getString(3));
				data.setiUserServiceTermId(cursor.getString(4));
				data.setFlgDirty(cursor.getString(5));
				data.setFlgDeleted(cursor.getString(6));
				data.setDtCreated(cursor.getString(7));
				data.setDtUpdated(cursor.getString(8));
				data.setiTenantId(cursor.getString(9));
				data.setiShipId(cursor.getString(10));

				// Adding contact to list
				nfcDataList.add(data);
			} while (cursor.moveToNext());
		}

		db.close();
		// return contact list
		return nfcDataList;
	}

	// Code to get all UserNfc row in a list
	public ArrayList<UserNfcData> getUserNFCSingleRowByUserId(String id) {
		ArrayList<UserNfcData> nfcDataList = new ArrayList<UserNfcData>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + TABLE_USER_NFC + " WHERE "
				+ KEY_iUSER_ID + " = '" + id + "' and " + KEY_FLG_DELETED
				+ " = '0'";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				UserNfcData data = new UserNfcData();
				data.setId(Integer.parseInt(cursor.getString(0)));
				data.setNfcCardId(cursor.getString(1));
				data.setCardType(cursor.getString(2));
				data.setiUserId(cursor.getString(3));
				data.setiUserServiceTermId(cursor.getString(4));
				data.setFlgDirty(cursor.getString(5));
				data.setFlgDeleted(cursor.getString(6));
				data.setDtCreated(cursor.getString(7));
				data.setDtUpdated(cursor.getString(8));
				data.setiTenantId(cursor.getString(9));
				data.setiShipId(cursor.getString(10));

				// Adding contact to list
				nfcDataList.add(data);
			} while (cursor.moveToNext());
		}

		db.close();
		// return contact list
		return nfcDataList;
	}
	
	/**
	 * get data on the basis of userid and card id getUserNFCRowByUserIdAndCardId
	 * @param id
	 * @return
	 */
	public ArrayList<UserNfcData> getUserNFCRowByUserIdAndCardId(String userid, String strCardId) {
		ArrayList<UserNfcData> nfcDataList = new ArrayList<UserNfcData>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + TABLE_USER_NFC + " WHERE "
				+ KEY_iUSER_ID + " = '" + userid + "' and " + KEY_FLG_DELETED
				+ " = '0' and "+KEY_USER_SERVICE_TERM_ID+" = '"+strCardId+"'";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				UserNfcData data = new UserNfcData();
				data.setId(Integer.parseInt(cursor.getString(0)));
				data.setNfcCardId(cursor.getString(1));
				data.setCardType(cursor.getString(2));
				data.setiUserId(cursor.getString(3));
				data.setiUserServiceTermId(cursor.getString(4));
				data.setFlgDirty(cursor.getString(5));
				data.setFlgDeleted(cursor.getString(6));
				data.setDtCreated(cursor.getString(7));
				data.setDtUpdated(cursor.getString(8));
				data.setiTenantId(cursor.getString(9));
				data.setiShipId(cursor.getString(10));

				// Adding contact to list
				nfcDataList.add(data);
			} while (cursor.moveToNext());
		}

		cursor.close();
		db.close();
		// return contact list
		return nfcDataList;
	}

	// Code to get all UserNfc row in a list
	public ArrayList<TenantData> getTenantRow() {
		ArrayList<TenantData> tenantDataList = new ArrayList<TenantData>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + TABLE_TENANT;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				TenantData data = new TenantData();
				data.setId(Integer.parseInt(cursor.getString(0)));
				data.setiTenantId(cursor.getString(1));
				data.setStrCompanyName(cursor.getString(2));
				// Adding contact to list
				tenantDataList.add(data);
			} while (cursor.moveToNext());
		}

		db.close();
		// return contact list
		return tenantDataList;
	}

	// Code to get all UserNfc row in a list
	public List<ModelData> getModelRow() {
		List<ModelData> modelDataList = new ArrayList<ModelData>();
		// Select All Query
		String selectQuery = "SELECT * FROM " + TABLE_MODEL;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				ModelData data = new ModelData();
				data.setId(Integer.parseInt(cursor.getString(0)));
				data.setModeId(cursor.getString(1));
				data.setModelName(cursor.getString(2));
				// Adding contact to list
				modelDataList.add(data);
			} while (cursor.moveToNext());
		}

		db.close();
		// return contact list
		return modelDataList;
	}

	// Code to get all CardType row in a list
	public List<CardTypeData> getCardTypeRow() {
		List<CardTypeData> cardDataList = new ArrayList<CardTypeData>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + TABLE_CARD_TYPE;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				CardTypeData data = new CardTypeData();
				data.setId(Integer.parseInt(cursor.getString(0)));
				data.setStrCardId(cursor.getString(1));
				data.setStrCardType(cursor.getString(2));
				// Adding contact to list
				cardDataList.add(data);
			} while (cursor.moveToNext());
		}

		db.close();
		// return contact list
		return cardDataList;
	}

	// Code to get all Ship Master row in a list
	public List<ShipMasterData> getShipMasterRow() {
		List<ShipMasterData> shipMasterDataList = new ArrayList<ShipMasterData>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + TABLE_SHIP_MASTER;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				ShipMasterData data = new ShipMasterData();
				data.setId(Integer.parseInt(cursor.getString(0)));
				data.setiShipId(cursor.getString(1));
				data.setiTenantId(cursor.getString(2));
				data.setiClassificationSocietyId(cursor.getString(3));
				data.setiShipTypeId(cursor.getString(4));
				data.setStrShipName(cursor.getString(5));
				data.setStrDescription(cursor.getString(6));
				data.setDtCreated(cursor.getString(7));
				data.setDtUpdated(cursor.getString(8));
				data.setFlgStatus(cursor.getString(9));
				data.setFlgDeleted(cursor.getString(10));
				data.setFlgIsDirty(cursor.getString(11));
				data.setiRuleListId(cursor.getString(12));
				data.setiShipIMONumber(cursor.getString(13));
				data.setStrFlag(cursor.getString(14));
				data.setStrLogo(cursor.getString(15));
				data.setFileSize(cursor.getString(16));
				data.setStrFileName(cursor.getString(17));
				data.setStrFilePath(cursor.getString(18));
				data.setStrFileType(cursor.getString(19));
				data.setStrShipCode(cursor.getString(20));

				// Adding contact to list
				shipMasterDataList.add(data);
			} while (cursor.moveToNext());
		}

		db.close();
		// return contact list
		return shipMasterDataList;
	}

	/**
	 *  Code to get all Ship Master deleted row in a list
	 * @return
	 */
	public List<ShipMasterData> getShipMasterDeletedRow() {
		List<ShipMasterData> shipMasterDataList = new ArrayList<ShipMasterData>();
		
		String selectQuery = "SELECT  * FROM " + TABLE_SHIP_MASTER +" where "+ KEY_FLG_DELETED +" ='1'";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		if (cursor.moveToFirst()) {
			do {
				ShipMasterData data = new ShipMasterData();
				data.setId(Integer.parseInt(cursor.getString(0)));
				data.setiShipId(cursor.getString(1));
				data.setiTenantId(cursor.getString(2));
				data.setiClassificationSocietyId(cursor.getString(3));
				data.setiShipTypeId(cursor.getString(4));
				data.setStrShipName(cursor.getString(5));
				data.setStrDescription(cursor.getString(6));
				data.setDtCreated(cursor.getString(7));
				data.setDtUpdated(cursor.getString(8));
				data.setFlgStatus(cursor.getString(9));
				data.setFlgDeleted(cursor.getString(10));
				data.setFlgIsDirty(cursor.getString(11));
				data.setiRuleListId(cursor.getString(12));
				data.setiShipIMONumber(cursor.getString(13));
				data.setStrFlag(cursor.getString(14));
				data.setStrLogo(cursor.getString(15));
				data.setFileSize(cursor.getString(16));
				data.setStrFileName(cursor.getString(17));
				data.setStrFilePath(cursor.getString(18));
				data.setStrFileType(cursor.getString(19));
				data.setStrShipCode(cursor.getString(20));

				// Adding contact to list
				shipMasterDataList.add(data);
			} while (cursor.moveToNext());
		}

		db.close();
		// return contact list
		return shipMasterDataList;
	}

	// Code to get all Role table row in a list
	public List<RoleData> getRoleRow() {
		List<RoleData> roleDataList = new ArrayList<RoleData>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + TABLE_ROLE +" where "+ KEY_FLG_DELETED +" ='0'";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				RoleData data = new RoleData();
				data.setId(Integer.parseInt(cursor.getString(0)));
				data.setiRoleId(cursor.getString(1));
				data.setStrRole(cursor.getString(2));
				data.setDtCreated(cursor.getString(3));
				data.setDtUpdated(cursor.getString(4));
				data.setFlgIsDirty(cursor.getString(5));
				data.setStrType(cursor.getString(6));
				data.setFlgDeleted(cursor.getString(7));
				data.setTxtDescription(cursor.getString(8));
				data.setiRankPriority(cursor.getString(9));
				data.setiTenantId(cursor.getString(10));
				data.setStrRoleType(cursor.getString(11));

				// Adding contact to list
				roleDataList.add(data);
			} while (cursor.moveToNext());
		}

		db.close();
		// return contact list
		return roleDataList;
	}

	// Code to get all Role table row in a list
	public List<RoleData> getRoleRow(String roletype) {
		List<RoleData> roleDataList = new ArrayList<RoleData>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + TABLE_ROLE + " where "
				+ KEY_STR_ROLE_TYPE + " = '" + roletype + "'";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				RoleData data = new RoleData();
				data.setId(Integer.parseInt(cursor.getString(0)));
				data.setiRoleId(cursor.getString(1));
				data.setStrRole(cursor.getString(2));
				data.setDtCreated(cursor.getString(3));
				data.setDtUpdated(cursor.getString(4));
				data.setFlgIsDirty(cursor.getString(5));
				data.setStrType(cursor.getString(6));
				data.setFlgDeleted(cursor.getString(7));
				data.setTxtDescription(cursor.getString(8));
				data.setiRankPriority(cursor.getString(9));
				data.setiTenantId(cursor.getString(10));
				data.setStrRoleType(cursor.getString(11));

				// Adding contact to list
				roleDataList.add(data);
			} while (cursor.moveToNext());
		}

		db.close();
		// return contact list
		return roleDataList;
	}

	// Code to get all Role table row in a list
	public RoleData getRoleSingleRow(String roleId) {
		// Select All Query
		String selectQuery = "SELECT  * FROM " + TABLE_ROLE + " where "
				+ KEY_ROLE_ID + " = '" + roleId + "'";
		RoleData data = new RoleData();
		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				data.setId(Integer.parseInt(cursor.getString(0)));
				data.setiRoleId(cursor.getString(1));
				data.setStrRole(cursor.getString(2));
				data.setDtCreated(cursor.getString(3));
				data.setDtUpdated(cursor.getString(4));
				data.setFlgIsDirty(cursor.getString(5));
				data.setStrType(cursor.getString(6));
				data.setFlgDeleted(cursor.getString(7));
				data.setTxtDescription(cursor.getString(8));
				data.setiRankPriority(cursor.getString(9));
				data.setiTenantId(cursor.getString(10));
				data.setStrRoleType(cursor.getString(11));

			} while (cursor.moveToNext());
		}

		db.close();
		// return contact list
		return data;
	}

	/**
	 * Code to get all User Master row in a list
	 * 
	 * @param roleId
	 * @return
	 */
	public List<UserMaster> getUserMasterForRoles(String roleId) {
		List<UserMaster> userMasterDataList = new ArrayList<UserMaster>();

		String selectQuery = "SELECT  *  FROM " + TABLE_USER_MASTER + " WHERE "
				+ KEY_ROLE_ID + " in( " + roleId + ") and "+ KEY_FLG_DELETED +" ='0'";
		System.out.println("Query : " + selectQuery);

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				UserMaster data = new UserMaster();
				data.setId(Integer.parseInt(cursor.getString(0)));
				data.setiUserId(cursor.getString(1));
				data.setiTenantId(cursor.getString(2));
				data.setStrUserName(cursor.getString(3));
				data.setStrPassword(cursor.getString(4));
				data.setStrFirstName(cursor.getString(5));
				data.setStrLastName(cursor.getString(6));
				data.setDtDateOfBirth(cursor.getString(7));
				data.setTxtDescription(cursor.getString(8));
				data.setStrEmail(cursor.getString(9));
				data.setDtCreated(cursor.getString(10));
				data.setDtUpdated(cursor.getString(11));
				data.setFlgStatus(cursor.getString(12));
				data.setFlgDeleted(cursor.getString(13));
				data.setFlgIsDirty(cursor.getString(14));
				data.setiShipHolidayListId(cursor.getString(15));
				data.setFltMinWorkHourWeekDays(cursor.getString(16));
				data.setFltMinWorkHourSaturdays(cursor.getString(17));
				data.setFltMinWorkHourSundays(cursor.getString(18));
				data.setFltMinWorkHourHolidays(cursor.getString(19));
				data.setFltOTIncludedInWage(cursor.getString(20));
				data.setFltOTRatePerHour(cursor.getString(21));
				data.setFlgIsOverTimeEnabled(cursor.getString(22));
				data.setiRoleId(cursor.getString(23));
				data.setFlgIsWatchkeeper(cursor.getString(24));
				data.setFaxNumber(cursor.getString(25));
				data.setHandPhone(cursor.getString(26));
				data.setLandLineNumber(cursor.getString(27));
				data.setPanNumber(cursor.getString(28));
				data.setPinCode(cursor.getString(29));
				data.setAddressFirst(cursor.getString(30));
				data.setAddressSecond(cursor.getString(31));
				data.setAddressThird(cursor.getString(32));
				data.setCity(cursor.getString(33));
				data.setState(cursor.getString(34));
				data.setiCountryId(cursor.getString(35));
				
				data.setStrEmployeeNo(cursor.getString(36));
				data.setStrMiddleName(cursor.getString(37));

				RoleData rd = getRoleSingleRow(data.getiRoleId());
				data.setRankPriority(rd != null
						&& rd.getiRankPriority() != null ? Integer.parseInt(rd
						.getiRankPriority()) : 0);

				// Adding contact to list
				userMasterDataList.add(data);
			} while (cursor.moveToNext());
		}

		cursor.close();
		db.close();
		// return contact list

		Collections.sort(userMasterDataList, new Comparator<UserMaster>() {
			@Override
			public int compare(UserMaster lhs, UserMaster rhs) {
				// TODO Auto-generated method stub
				return lhs.getRankPriority().compareTo(rhs.getRankPriority());
			}
		});

		return userMasterDataList;
	}

	// Code to get all User Master row in a list
	public ArrayList<UserMaster> getUserMasterRow() {
		ArrayList<UserMaster> userMasterDataList = new ArrayList<UserMaster>();
		// Select All Query
		String selectQuery = "SELECT * FROM " + TABLE_USER_MASTER +" where "+ KEY_FLG_DELETED +" ='0'";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				UserMaster data = new UserMaster();
				data.setId(Integer.parseInt(cursor.getString(0)));
				data.setiUserId(cursor.getString(1));
				data.setiTenantId(cursor.getString(2));
				data.setStrUserName(cursor.getString(3));
				data.setStrPassword(cursor.getString(4));
				data.setStrFirstName(cursor.getString(5));
				data.setStrLastName(cursor.getString(6));
				data.setDtDateOfBirth(cursor.getString(7));
				data.setTxtDescription(cursor.getString(8));
				data.setStrEmail(cursor.getString(9));
				data.setDtCreated(cursor.getString(10));
				data.setDtUpdated(cursor.getString(11));
				data.setFlgStatus(cursor.getString(12));
				data.setFlgDeleted(cursor.getString(13));
				data.setFlgIsDirty(cursor.getString(14));
				data.setiShipHolidayListId(cursor.getString(15));
				data.setFltMinWorkHourWeekDays(cursor.getString(16));
				data.setFltMinWorkHourSaturdays(cursor.getString(17));
				data.setFltMinWorkHourSundays(cursor.getString(18));
				data.setFltMinWorkHourHolidays(cursor.getString(19));
				data.setFltOTIncludedInWage(cursor.getString(20));
				data.setFltOTRatePerHour(cursor.getString(21));
				data.setFlgIsOverTimeEnabled(cursor.getString(22));
				data.setiRoleId(cursor.getString(23));
				data.setFlgIsWatchkeeper(cursor.getString(24));
				data.setFaxNumber(cursor.getString(25));
				data.setHandPhone(cursor.getString(26));
				data.setLandLineNumber(cursor.getString(27));
				data.setPanNumber(cursor.getString(28));
				data.setPinCode(cursor.getString(29));
				data.setAddressFirst(cursor.getString(30));
				data.setAddressSecond(cursor.getString(31));
				data.setAddressThird(cursor.getString(32));
				data.setCity(cursor.getString(33));
				data.setState(cursor.getString(34));
				data.setiCountryId(cursor.getString(35));
				
				data.setStrEmployeeNo(cursor.getString(36));
				data.setStrMiddleName(cursor.getString(37));


				RoleData rd = getRoleSingleRow(data.getiRoleId());
				data.setRankPriority(rd != null
						&& rd.getiRankPriority() != null ? Integer.parseInt(rd
						.getiRankPriority()) : 0);

				// Adding contact to list
				userMasterDataList.add(data);
			} while (cursor.moveToNext());
		}

		cursor.close();
		db.close();

		Collections.sort(userMasterDataList, new Comparator<UserMaster>() {
			@Override
			public int compare(UserMaster lhs, UserMaster rhs) {
				// TODO Auto-generated method stub
				return lhs.getRankPriority().compareTo(rhs.getRankPriority());
			}
		});

		// return contact list
		return userMasterDataList;
	}

	public ArrayList<UserMaster> getUserMasterRowByRoleIds(String rolesid) {
		ArrayList<UserMaster> userMasterDataList = new ArrayList<UserMaster>();
		// Select All Query
		String selectQuery = "SELECT * FROM " + TABLE_USER_MASTER + " where "
				+ KEY_ROLE_ID + " in (" + rolesid + ") and "+ KEY_FLG_DELETED +" ='0'";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				UserMaster data = new UserMaster();
				data.setId(Integer.parseInt(cursor.getString(0)));
				data.setiUserId(cursor.getString(1));
				data.setiTenantId(cursor.getString(2));
				data.setStrUserName(cursor.getString(3));
				data.setStrPassword(cursor.getString(4));
				data.setStrFirstName(cursor.getString(5));
				data.setStrLastName(cursor.getString(6));
				data.setDtDateOfBirth(cursor.getString(7));
				data.setTxtDescription(cursor.getString(8));
				data.setStrEmail(cursor.getString(9));
				data.setDtCreated(cursor.getString(10));
				data.setDtUpdated(cursor.getString(11));
				data.setFlgStatus(cursor.getString(12));
				data.setFlgDeleted(cursor.getString(13));
				data.setFlgIsDirty(cursor.getString(14));
				data.setiShipHolidayListId(cursor.getString(15));
				data.setFltMinWorkHourWeekDays(cursor.getString(16));
				data.setFltMinWorkHourSaturdays(cursor.getString(17));
				data.setFltMinWorkHourSundays(cursor.getString(18));
				data.setFltMinWorkHourHolidays(cursor.getString(19));
				data.setFltOTIncludedInWage(cursor.getString(20));
				data.setFltOTRatePerHour(cursor.getString(21));
				data.setFlgIsOverTimeEnabled(cursor.getString(22));
				data.setiRoleId(cursor.getString(23));
				data.setFlgIsWatchkeeper(cursor.getString(24));
				data.setFaxNumber(cursor.getString(25));
				data.setHandPhone(cursor.getString(26));
				data.setLandLineNumber(cursor.getString(27));
				data.setPanNumber(cursor.getString(28));
				data.setPinCode(cursor.getString(29));
				data.setAddressFirst(cursor.getString(30));
				data.setAddressSecond(cursor.getString(31));
				data.setAddressThird(cursor.getString(32));
				data.setCity(cursor.getString(33));
				data.setState(cursor.getString(34));
				data.setiCountryId(cursor.getString(35));
				
				data.setStrEmployeeNo(cursor.getString(36));
				data.setStrMiddleName(cursor.getString(37));


				RoleData rd = getRoleSingleRow(data.getiRoleId());
				data.setRankPriority(rd != null
						&& rd.getiRankPriority() != null ? Integer.parseInt(rd
						.getiRankPriority()) : 0);

				// Adding contact to list
				userMasterDataList.add(data);
			} while (cursor.moveToNext());
		}

		cursor.close();
		db.close();

		Collections.sort(userMasterDataList, new Comparator<UserMaster>() {
			@Override
			public int compare(UserMaster lhs, UserMaster rhs) {
				// TODO Auto-generated method stub
				return lhs.getRankPriority().compareTo(rhs.getRankPriority());
			}
		});

		// return contact list
		return userMasterDataList;
	}

	// Code to get all User Master row in a list
	public ArrayList<UserMaster> getUserMasterSingleRow(String id) {
		ArrayList<UserMaster> userMasterDataList = new ArrayList<UserMaster>();
		// Select All Query
		String selectQuery = "SELECT  *  FROM " + TABLE_USER_MASTER + " WHERE "
				+ KEY_iUSER_ID + " = " + "'" + id + "' and "+ KEY_FLG_DELETED +" ='0'";
		System.out.println("Query : " + selectQuery);

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				UserMaster data = new UserMaster();
				data.setId(Integer.parseInt(cursor.getString(0)));
				data.setiUserId(cursor.getString(1));
				data.setiTenantId(cursor.getString(2));
				data.setStrUserName(cursor.getString(3));
				data.setStrPassword(cursor.getString(4));
				data.setStrFirstName(cursor.getString(5));
				data.setStrLastName(cursor.getString(6));
				data.setDtDateOfBirth(cursor.getString(7));
				data.setTxtDescription(cursor.getString(8));
				data.setStrEmail(cursor.getString(9));
				data.setDtCreated(cursor.getString(10));
				data.setDtUpdated(cursor.getString(11));
				data.setFlgStatus(cursor.getString(12));
				data.setFlgDeleted(cursor.getString(13));
				data.setFlgIsDirty(cursor.getString(14));
				data.setiShipHolidayListId(cursor.getString(15));
				data.setFltMinWorkHourWeekDays(cursor.getString(16));
				data.setFltMinWorkHourSaturdays(cursor.getString(17));
				data.setFltMinWorkHourSundays(cursor.getString(18));
				data.setFltMinWorkHourHolidays(cursor.getString(19));
				data.setFltOTIncludedInWage(cursor.getString(20));
				data.setFltOTRatePerHour(cursor.getString(21));
				data.setFlgIsOverTimeEnabled(cursor.getString(22));
				data.setiRoleId(cursor.getString(23));
				data.setFlgIsWatchkeeper(cursor.getString(24));
				data.setFaxNumber(cursor.getString(25));
				data.setHandPhone(cursor.getString(26));
				data.setLandLineNumber(cursor.getString(27));
				data.setPanNumber(cursor.getString(28));
				data.setPinCode(cursor.getString(29));
				data.setAddressFirst(cursor.getString(30));
				data.setAddressSecond(cursor.getString(31));
				data.setAddressThird(cursor.getString(32));
				data.setCity(cursor.getString(33));
				data.setState(cursor.getString(34));
				data.setiCountryId(cursor.getString(35));
				
				data.setStrEmployeeNo(cursor.getString(36));
				data.setStrMiddleName(cursor.getString(37));


				RoleData rd = getRoleSingleRow(data.getiRoleId());
				data.setRankPriority(rd != null
						&& rd.getiRankPriority() != null ? Integer.parseInt(rd
						.getiRankPriority()) : 0);

				// Adding contact to list
				userMasterDataList.add(data);
			} while (cursor.moveToNext());
		}

		cursor.close();
		db.close();

		Collections.sort(userMasterDataList, new Comparator<UserMaster>() {
			@Override
			public int compare(UserMaster lhs, UserMaster rhs) {
				// TODO Auto-generated method stub
				return lhs.getRankPriority().compareTo(rhs.getRankPriority());
			}
		});

		// return contact list
		return userMasterDataList;
	}

	public String getCurrentUserServiceTermIdFor(String userId) {
		// Select All Query

		String userServiceTermId = "";
		try {
			Date curDate = new Date();
			String selectQuery = "SELECT " + KEY_USER_SERVICE_TERM_ID
					+ " FROM " + TABLE_USER_SERVICE_TERM + " WHERE "
					+ KEY_iUSER_ID + " = " + "'" + userId + "' and "
					+ KEY_DT_TERM_FROM + " <='" + curDate + "' and "
					+ KEY_DT_TERM_TO + ">='" + curDate + "' and "+ KEY_FLG_DELETED +" ='0'";
			System.out.println("Query :" + selectQuery);
			SQLiteDatabase db = this.getWritableDatabase();
			Cursor cursor = db.rawQuery(selectQuery, null);
			if (cursor.getCount() > 0) {
				cursor.moveToFirst();
				userServiceTermId = cursor.getString(cursor
						.getColumnIndex(KEY_USER_SERVICE_TERM_ID));
			}
			db.close();
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("Exception :" + e);
		}
		// return contact list
		System.out.println("UserServiceTermId :" + userServiceTermId);
		return userServiceTermId;

	}

	// Code to get all UserServiceTermData row in a list
	public List<UserServiceTermData> getUserServiceTermRow() {
		List<UserServiceTermData> userServiceTermDataList = new ArrayList<UserServiceTermData>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + TABLE_USER_SERVICE_TERM +" where "+ KEY_FLG_DELETED +" ='0'";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				UserServiceTermData data = new UserServiceTermData();
				data.setId(Integer.parseInt(cursor.getString(0)));
				data.setiUserServiceTermId(cursor.getString(1));
				data.setiUserId(cursor.getString(2));
				data.setDtTermFrom(cursor.getString(3));
				data.setDtTermTo(cursor.getString(4));
				data.setFlgDeleted(cursor.getString(5));
				data.setFlgIsDirty(cursor.getString(6));
				data.setDtCreated(cursor.getString(7));
				data.setDtUpdated(cursor.getString(8));
				data.setiShipId(cursor.getString(9));
				data.setiTenantId(cursor.getString(10));

				// Adding contact to list
				userServiceTermDataList.add(data);
			} while (cursor.moveToNext());
		}

		db.close();
		// return contact list
		return userServiceTermDataList;
	}
	
	/**
	 * Get userserviceterm by user id.
	 * @return
	 */
	public List<UserServiceTermRoleData> getUserServiceTermRoleByUser(String strUserId, String strRoleId) {
		List<UserServiceTermRoleData> userServiceTermRoleDataList = new ArrayList<UserServiceTermRoleData>();
		Date curDate = new Date();
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd");		
		String selectQuery = "SELECT  * FROM " + TABLE_USER_SERVICE_TERM_ROLE +" where "+ KEY_FLG_DELETED +" ='0'"
				+ " and "+KEY_iUSER_ID+"='"+strUserId+"' and "+ KEY_DT_TERM_FROM + " <='" + format.format(curDate) + "'"
						+ " and "+KEY_ROLE_ID+" ='"+strRoleId+"'";
		
		L.fv("Device Date is : "+curDate +" and formated date is : "+format.format(curDate));

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				UserServiceTermRoleData data = new UserServiceTermRoleData();
				data.setId(Integer.parseInt(cursor.getString(0)));
				data.setStrUserServiceTermRoleId(cursor.getString(1));
				data.setDtCreated(cursor.getString(2));
				data.setDtUpdated(cursor.getString(3));
				data.setFlgDeleted(cursor.getString(4));
				data.setFlgIsDirty(cursor.getString(5));
				data.setiRoleId(cursor.getString(6));
				data.setiShipId(cursor.getString(7));
				data.setiTenantId(cursor.getString(8));
				data.setIuserId(cursor.getString(9));
				data.setIuserServiceTermId(cursor.getString(10));
				data.setDtTermFrom(cursor.getString(11));
				data.setDtTermTo(cursor.getString(12));
				data.setPowerPlusScore(cursor.getInt(13));
				data.setField1(cursor.getInt(14));
				data.setField2(cursor.getInt(15));

				// Adding contact to list
				userServiceTermRoleDataList.add(data);
			} while (cursor.moveToNext());
		}

		db.close();
		// return contact list
		return userServiceTermRoleDataList;
	}

	// Code to get all UserServiceTermRole row in a list
	public List<UserServiceTermRoleData> getUserServiceTermRoleRow() {
		List<UserServiceTermRoleData> userServiceTermRoleDataList = new ArrayList<UserServiceTermRoleData>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + TABLE_USER_SERVICE_TERM_ROLE +" where "+ KEY_FLG_DELETED +" ='0'";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				UserServiceTermRoleData data = new UserServiceTermRoleData();
				data.setId(Integer.parseInt(cursor.getString(0)));
				data.setStrUserServiceTermRoleId(cursor.getString(1));
				data.setDtCreated(cursor.getString(2));
				data.setDtUpdated(cursor.getString(3));
				data.setFlgDeleted(cursor.getString(4));
				data.setFlgIsDirty(cursor.getString(5));
				data.setiRoleId(cursor.getString(6));
				data.setiShipId(cursor.getString(7));
				data.setiTenantId(cursor.getString(8));
				data.setIuserId(cursor.getString(9));
				data.setIuserServiceTermId(cursor.getString(10));
				data.setDtTermFrom(cursor.getString(11));
				data.setDtTermTo(cursor.getString(12));
				data.setPowerPlusScore(cursor.getInt(13));
				data.setField1(cursor.getInt(14));
				data.setField2(cursor.getInt(15));

				// Adding contact to list
				userServiceTermRoleDataList.add(data);
			} while (cursor.moveToNext());
		}

		db.close();
		// return contact list
		return userServiceTermRoleDataList;
	}

	public List<UserServiceTermRoleData> getUserServiceTermRoleRowBiId(
			String strUserId, String strDate) {
		List<UserServiceTermRoleData> userServiceTermRoleDataList = new ArrayList<UserServiceTermRoleData>();
		// Select All Query
		String selectQuery = "SELECT * FROM " + TABLE_USER_SERVICE_TERM_ROLE
				+ " WHERE " + KEY_iUSER_ID + " = " + "'" + strUserId + "' and "
				+ KEY_DT_TERM_FROM + " <='" + strDate + "' and "
				+ KEY_DT_TERM_TO + ">='" + strDate + "' and "+ KEY_FLG_DELETED +" ='0'";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				UserServiceTermRoleData data = new UserServiceTermRoleData();
				data.setId(Integer.parseInt(cursor.getString(0)));
				data.setStrUserServiceTermRoleId(cursor.getString(1));
				data.setDtCreated(cursor.getString(2));
				data.setDtUpdated(cursor.getString(3));
				data.setFlgDeleted(cursor.getString(4));
				data.setFlgIsDirty(cursor.getString(5));
				data.setiRoleId(cursor.getString(6));
				data.setiShipId(cursor.getString(7));
				data.setiTenantId(cursor.getString(8));
				data.setIuserId(cursor.getString(9));
				data.setIuserServiceTermId(cursor.getString(10));
				data.setDtTermFrom(cursor.getString(11));
				data.setDtTermTo(cursor.getString(12));
				data.setPowerPlusScore(cursor.getInt(13));
				data.setField1(cursor.getInt(14));
				data.setField2(cursor.getInt(15));

				// Adding contact to list
				userServiceTermRoleDataList.add(data);
			} while (cursor.moveToNext());
		}

		db.close();
		// return contact list
		return userServiceTermRoleDataList;
	}
	
	public List<UserServiceTermRoleData> getUserServiceTermRoleRowBiIdForregisteruser(
			String strUserId, String strDate) {
		List<UserServiceTermRoleData> userServiceTermRoleDataList = new ArrayList<UserServiceTermRoleData>();
		// Select All Query
		String selectQuery = "SELECT * FROM " + TABLE_USER_SERVICE_TERM_ROLE
				+ " WHERE " + KEY_iUSER_ID + " = " + "'" + strUserId + "' and "
				/*+ KEY_DT_TERM_FROM + " <='" + strDate + "' and "*/
				+ KEY_DT_TERM_TO + ">='" + strDate + "' and "+ KEY_FLG_DELETED +" ='0'";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				UserServiceTermRoleData data = new UserServiceTermRoleData();
				data.setId(Integer.parseInt(cursor.getString(0)));
				data.setStrUserServiceTermRoleId(cursor.getString(1));
				data.setDtCreated(cursor.getString(2));
				data.setDtUpdated(cursor.getString(3));
				data.setFlgDeleted(cursor.getString(4));
				data.setFlgIsDirty(cursor.getString(5));
				data.setiRoleId(cursor.getString(6));
				data.setiShipId(cursor.getString(7));
				data.setiTenantId(cursor.getString(8));
				data.setIuserId(cursor.getString(9));
				data.setIuserServiceTermId(cursor.getString(10));
				data.setDtTermFrom(cursor.getString(11));
				data.setDtTermTo(cursor.getString(12));
				data.setPowerPlusScore(cursor.getInt(13));
				data.setField1(cursor.getInt(14));
				data.setField2(cursor.getInt(15));

				// Adding contact to list
				userServiceTermRoleDataList.add(data);
			} while (cursor.moveToNext());
		}

		db.close();
		// return contact list
		return userServiceTermRoleDataList;
	}

	public List<UserServiceTermRoleData> getUserServiceTermRoleRowBiIdDate(String strDate) {
		List<UserServiceTermRoleData> userServiceTermRoleDataList = new ArrayList<UserServiceTermRoleData>();
		// Select All Query
		String selectQuery = "SELECT * FROM " + TABLE_USER_SERVICE_TERM_ROLE
				+ " WHERE "	+ KEY_DT_TERM_TO + "<'" + strDate + "' and "+ KEY_FLG_DELETED +" ='0'";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				UserServiceTermRoleData data = new UserServiceTermRoleData();
				data.setId(Integer.parseInt(cursor.getString(0)));
				data.setStrUserServiceTermRoleId(cursor.getString(1));
				data.setDtCreated(cursor.getString(2));
				data.setDtUpdated(cursor.getString(3));
				data.setFlgDeleted(cursor.getString(4));
				data.setFlgIsDirty(cursor.getString(5));
				data.setiRoleId(cursor.getString(6));
				data.setiShipId(cursor.getString(7));
				data.setiTenantId(cursor.getString(8));
				data.setIuserId(cursor.getString(9));
				data.setIuserServiceTermId(cursor.getString(10));
				data.setDtTermFrom(cursor.getString(11));
				data.setDtTermTo(cursor.getString(12));
				data.setPowerPlusScore(cursor.getInt(13));
				data.setField1(cursor.getInt(14));
				data.setField2(cursor.getInt(15));

				// Adding contact to list
				userServiceTermRoleDataList.add(data);
			} while (cursor.moveToNext());
		}

		db.close();
		// return contact list
		return userServiceTermRoleDataList;
	}

	/**
	 * PowerPlusTransaction get on the basis of userId, userServiceTermId and
	 * powerPlusTaskId
	 * 
	 * @param uid
	 * @return count
	 */
	public int getPowerPlusTransactionCountForTask(String uid,
			String userServiceTermId, String powerPlusTaskId) {

		// Select All Query
		String selectQuery = "SELECT  * FROM " + TABLE_POWER_PLUS_TRANSACTION
				+ " where " + KEY_STR_RECEIVER_ID + " != '" + uid + "' and "
				+ KEY_USER_SERVICE_TERM_ID + " ='" + userServiceTermId
				+ "' and " + KEY_I_POWER_PLUS_TASK_ID + " ='" + powerPlusTaskId
				+ "' and " + KEY_FLG_STATUS + " != 'Rejected' and "+ KEY_FLG_DELETED +" ='0'";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		int count = cursor.getCount();
		cursor.close();
		db.close();
		return count;
	}

	/**
	 * PowerPlusTransaction get on the basis of userId, date from and date to
	 * 
	 * @param uid
	 * @return count
	 */
	public int getPowerPlusTransactionCountForValidate(String uid,
			String dtFrom, String dtTo, String strTaskId) {

		// Select All Query
		String selectQuery = "SELECT  * FROM " + TABLE_POWER_PLUS_TRANSACTION
				+ " where " + KEY_STR_RECEIVER_ID + " = '" + uid + "' and "
				+ KEY_I_POWER_PLUS_TASK_ID + " ='" + strTaskId + "' and "
				+ KEY_DT_CREATED + " >='" + dtFrom + "' and " + KEY_DT_CREATED
				+ " <='" + dtTo + "' and " + KEY_FLG_STATUS
				+ " == 'Validated' and " + KEY_FLG_DELETED + "= '0'";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		int count = cursor.getCount();
		cursor.close();
		return count;
	}

	public ArrayList<PowerPlusCategory> getPowerPlusCategoryRow() {
		ArrayList<PowerPlusCategory> shipMasterDataList = new ArrayList<PowerPlusCategory>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + TABLE_POWER_PLUS_CATEGOEY
				+ " where "+ KEY_FLG_DELETED +" ='0' ORDER BY " + KEY_I_SEQUENCE;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				PowerPlusCategory data = new PowerPlusCategory();
				data.setId(Integer.parseInt(cursor.getString(0)));
				data.setiPowerPlusCategoryId(cursor.getString(1));
				data.setDtCreated(cursor.getString(2));
				data.setDtUpdated(cursor.getString(3));
				data.setFlgDeleted(cursor.getString(4));
				data.setFlgIsDirty(cursor.getString(5));
				data.setFlgStatus(cursor.getString(6));
				data.setiSequence(cursor.getString(7));
				data.setStrDescription(cursor.getString(8));
				data.setStrTitle(cursor.getString(9));
				data.setiTenantId(cursor.getString(10));
				data.setStrCategoryCode(cursor.getString(11));

				// Adding contact to list
				shipMasterDataList.add(data);
			} while (cursor.moveToNext());
		}

		db.close();
		// return contact list
		return shipMasterDataList;
	}

	public ArrayList<PowerPlusCategory> getPowerPlusCategoryRowByTitle(
			String strCatTitle) {
		ArrayList<PowerPlusCategory> shipMasterDataList = new ArrayList<PowerPlusCategory>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + TABLE_POWER_PLUS_CATEGOEY
				+ " where " + KEY_STR_TITLE + " = '" + strCatTitle + "' and "
				+ KEY_FLG_DELETED + " = '0' ORDER BY " + KEY_I_SEQUENCE;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				PowerPlusCategory data = new PowerPlusCategory();
				data.setId(Integer.parseInt(cursor.getString(0)));
				data.setiPowerPlusCategoryId(cursor.getString(1));
				data.setDtCreated(cursor.getString(2));
				data.setDtUpdated(cursor.getString(3));
				data.setFlgDeleted(cursor.getString(4));
				data.setFlgIsDirty(cursor.getString(5));
				data.setFlgStatus(cursor.getString(6));
				data.setiSequence(cursor.getString(7));
				data.setStrDescription(cursor.getString(8));
				data.setStrTitle(cursor.getString(9));
				data.setiTenantId(cursor.getString(10));
				data.setStrCategoryCode(cursor.getString(11));

				// Adding contact to list
				shipMasterDataList.add(data);
			} while (cursor.moveToNext());
		}

		db.close();
		// return contact list
		return shipMasterDataList;
	}

	public ArrayList<PowerPlusCategory> getPowerPlusCategoryRowByCatCode(
			String strCatCode) {
		ArrayList<PowerPlusCategory> shipMasterDataList = new ArrayList<PowerPlusCategory>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + TABLE_POWER_PLUS_CATEGOEY
				+ " where " + KEY_STR_CATEGORY_CODE + " = '" + strCatCode
				+ "' and " + KEY_FLG_DELETED + " = '0' ORDER BY "
				+ KEY_I_SEQUENCE;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				PowerPlusCategory data = new PowerPlusCategory();
				data.setId(Integer.parseInt(cursor.getString(0)));
				data.setiPowerPlusCategoryId(cursor.getString(1));
				data.setDtCreated(cursor.getString(2));
				data.setDtUpdated(cursor.getString(3));
				data.setFlgDeleted(cursor.getString(4));
				data.setFlgIsDirty(cursor.getString(5));
				data.setFlgStatus(cursor.getString(6));
				data.setiSequence(cursor.getString(7));
				data.setStrDescription(cursor.getString(8));
				data.setStrTitle(cursor.getString(9));
				data.setiTenantId(cursor.getString(10));
				data.setStrCategoryCode(cursor.getString(11));

				// Adding contact to list
				shipMasterDataList.add(data);
			} while (cursor.moveToNext());
		}

		db.close();
		// return contact list
		return shipMasterDataList;
	}

	public List<PowerPlusPeriodicity> getPowerPlusPeriodicityRow() {
		List<PowerPlusPeriodicity> shipMasterDataList = new ArrayList<PowerPlusPeriodicity>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + TABLE_POWER_PLUS_PERIODICITY + " where "+ KEY_FLG_DELETED +" ='0'";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				PowerPlusPeriodicity data = new PowerPlusPeriodicity();
				data.setId(Integer.parseInt(cursor.getString(0)));
				data.setiPowerPlusPeriodicityId(cursor.getString(1));
				data.setDtCreated(cursor.getString(2));
				data.setDtUpdated(cursor.getString(3));
				data.setFlgDeleted(cursor.getString(4));
				data.setFlgIsDirty(cursor.getString(5));
				data.setFlgStatus(cursor.getString(6));
				data.setiInterval(cursor.getString(7));
				data.setiSequence(cursor.getString(8));
				data.setiTimes(cursor.getString(9));
				data.setStrDescription(cursor.getString(10));
				data.setStrPattern(cursor.getString(11));
				data.setStrTitle(cursor.getString(12));
				data.setiTenantId(cursor.getString(13));
				// Adding contact to list
				shipMasterDataList.add(data);
			} while (cursor.moveToNext());
		}

		db.close();
		// return contact list
		return shipMasterDataList;
	}

	public List<PowerPlusPeriodicity> getPowerPlusPeriodicityRowById(
			String strperiodicityId) {
		List<PowerPlusPeriodicity> shipMasterDataList = new ArrayList<PowerPlusPeriodicity>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + TABLE_POWER_PLUS_PERIODICITY
				+ " WHERE " + KEY_I_POWER_PLUS_PERIODICITY_ID + "='"
				+ strperiodicityId + "' and "+ KEY_FLG_DELETED +" ='0'";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				PowerPlusPeriodicity data = new PowerPlusPeriodicity();
				data.setId(Integer.parseInt(cursor.getString(0)));
				data.setiPowerPlusPeriodicityId(cursor.getString(1));
				data.setDtCreated(cursor.getString(2));
				data.setDtUpdated(cursor.getString(3));
				data.setFlgDeleted(cursor.getString(4));
				data.setFlgIsDirty(cursor.getString(5));
				data.setFlgStatus(cursor.getString(6));
				data.setiInterval(cursor.getString(7));
				data.setiSequence(cursor.getString(8));
				data.setiTimes(cursor.getString(9));
				data.setStrDescription(cursor.getString(10));
				data.setStrPattern(cursor.getString(11));
				data.setStrTitle(cursor.getString(12));
				data.setiTenantId(cursor.getString(13));
				// Adding contact to list
				shipMasterDataList.add(data);
			} while (cursor.moveToNext());
		}

		db.close();
		// return contact list
		return shipMasterDataList;
	}

	public List<PowerPlusTask> getPowerPlusTaskRow(String categoryId) {
		List<PowerPlusTask> shipMasterDataList = new ArrayList<PowerPlusTask>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + TABLE_POWER_PLUS_TASK
				+ " where " + KEY_FLG_DELETED + " ='0' and "
				+ KEY_I_POWER_PLUS_CATEGORY_ID + " = '" + categoryId + "'";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				PowerPlusTask data = new PowerPlusTask();
				data.setId(Integer.parseInt(cursor.getString(0)));
				data.setiPowerPlusTaskId(cursor.getString(1));
				data.setApprovedBy(cursor.getString(2));
				data.setDtCreated(cursor.getString(3));
				data.setDtUpdated(cursor.getString(4));
				data.setFlgDeleted(cursor.getString(5));
				data.setFlgIsAllReceive(cursor.getString(6));
				data.setFlgIsDirty(cursor.getString(7));
				data.setFlgStatus(cursor.getString(8));
				data.setiSequence(cursor.getString(9));
				data.setPowerPoints(cursor.getString(10));
				data.setProvidedBy(cursor.getString(11));
				data.setReceivedBy(cursor.getString(12));
				data.setStrAdditionalOne(cursor.getString(13));
				data.setStrAdditionalTwo(cursor.getString(14));
				data.setStrDescription(cursor.getString(15));
				data.setStrTitle(cursor.getString(16));
				data.setiPowerPlusCategoryId(cursor.getString(17));
				data.setiPowerPlusPeriodicityId(cursor.getString(18));
				data.setiTenantId(cursor.getString(19));

				// Adding contact to list
				shipMasterDataList.add(data);
			} while (cursor.moveToNext());
		}

		db.close();
		// return contact list
		return shipMasterDataList;
	}

	public List<PowerPlusTask> getPowerPlusTaskRowByCategoryRole(
			String categoryId, String strRoleRankCategoryId) {
		List<PowerPlusTask> shipMasterDataList = new ArrayList<PowerPlusTask>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + TABLE_POWER_PLUS_TASK
				+ " where " + KEY_FLG_DELETED + " ='0' and " + KEY_FLG_STATUS
				+ " = '0' and " + KEY_I_POWER_PLUS_CATEGORY_ID + " = '"
				+ categoryId + "' and " + KEY_RECEIVED_BY + " in "
				+ strRoleRankCategoryId;

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				PowerPlusTask data = new PowerPlusTask();
				data.setId(Integer.parseInt(cursor.getString(0)));
				data.setiPowerPlusTaskId(cursor.getString(1));
				data.setApprovedBy(cursor.getString(2));
				data.setDtCreated(cursor.getString(3));
				data.setDtUpdated(cursor.getString(4));
				data.setFlgDeleted(cursor.getString(5));
				data.setFlgIsAllReceive(cursor.getString(6));
				data.setFlgIsDirty(cursor.getString(7));
				data.setFlgStatus(cursor.getString(8));
				data.setiSequence(cursor.getString(9));
				data.setPowerPoints(cursor.getString(10));
				data.setProvidedBy(cursor.getString(11));
				data.setReceivedBy(cursor.getString(12));
				data.setStrAdditionalOne(cursor.getString(13));
				data.setStrAdditionalTwo(cursor.getString(14));
				data.setStrDescription(cursor.getString(15));
				data.setStrTitle(cursor.getString(16));
				data.setiPowerPlusCategoryId(cursor.getString(17));
				data.setiPowerPlusPeriodicityId(cursor.getString(18));
				data.setiTenantId(cursor.getString(19));

				// Adding contact to list
				shipMasterDataList.add(data);
			} while (cursor.moveToNext());
		}

		db.close();
		// return contact list
		return shipMasterDataList;
	}

	public List<PowerPlusTask> getPowerPlusTaskRowById(String strPowerPlusTaskId) {
		List<PowerPlusTask> shipMasterDataList = new ArrayList<PowerPlusTask>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + TABLE_POWER_PLUS_TASK
				+ " where " + KEY_FLG_DELETED + " ='0' and "
				+ KEY_I_POWER_PLUS_TASK_ID + " = '" + strPowerPlusTaskId + "'"
				+ " and " + KEY_FLG_DELETED + "='0'";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				PowerPlusTask data = new PowerPlusTask();
				data.setId(Integer.parseInt(cursor.getString(0)));
				data.setiPowerPlusTaskId(cursor.getString(1));
				data.setApprovedBy(cursor.getString(2));
				data.setDtCreated(cursor.getString(3));
				data.setDtUpdated(cursor.getString(4));
				data.setFlgDeleted(cursor.getString(5));
				data.setFlgIsAllReceive(cursor.getString(6));
				data.setFlgIsDirty(cursor.getString(7));
				data.setFlgStatus(cursor.getString(8));
				data.setiSequence(cursor.getString(9));
				data.setPowerPoints(cursor.getString(10));
				data.setProvidedBy(cursor.getString(11));
				data.setReceivedBy(cursor.getString(12));
				data.setStrAdditionalOne(cursor.getString(13));
				data.setStrAdditionalTwo(cursor.getString(14));
				data.setStrDescription(cursor.getString(15));
				data.setStrTitle(cursor.getString(16));
				data.setiPowerPlusCategoryId(cursor.getString(17));
				data.setiPowerPlusPeriodicityId(cursor.getString(18));
				data.setiTenantId(cursor.getString(19));

				// Adding contact to list
				shipMasterDataList.add(data);
			} while (cursor.moveToNext());
		}

		db.close();
		// return contact list
		return shipMasterDataList;
	}

	public List<PowerPlusTransaction> getPowerPlusTransactionRow() {
		List<PowerPlusTransaction> shipMasterDataList = new ArrayList<PowerPlusTransaction>();
		// Select All Query
		String selectQuery = "SELECT  * FROM " + TABLE_POWER_PLUS_TRANSACTION
				+ " where " + KEY_FLG_DELETED + " ='0'";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				PowerPlusTransaction data = new PowerPlusTransaction();
				data.setId(Integer.parseInt(cursor.getString(0)));
				data.setiPowerPlusTransactionId(cursor.getString(1));
				data.setDtCreated(cursor.getString(2));
				data.setDtUpdated(cursor.getString(3));
				data.setFlgDeleted(cursor.getString(4));
				data.setFlgIsDirty(cursor.getString(5));
				data.setFlgIsTransationInvalid(cursor.getString(6));
				data.setFlgStatus(cursor.getString(7));
				data.setiSequence(cursor.getString(8));
				data.setPowerPoints(cursor.getString(9));
				data.setStrAdditionalOne(cursor.getString(10));
				data.setStrAdditionalTwo(cursor.getString(11));
				data.setStrApproverId(cursor.getString(12));
				data.setStrApproverName(cursor.getString(13));
				data.setStrDescription(cursor.getString(14));
				data.setStrProviderId(cursor.getString(15));
				data.setStrProviderName(cursor.getString(16));
				data.setStrTitle(cursor.getString(17));
				data.setiPowerPlusTaskId(cursor.getString(18));
				data.setiShipId(cursor.getString(19));
				data.setiTenantId(cursor.getString(20));
				data.setiUserId(cursor.getString(21));
				data.setStrReceiverName(cursor.getString(22));
				data.setFlgScoreStatus(cursor.getString(23));
				data.setCetogoryId(cursor.getString(24));
				data.setParentTransactionId(cursor.getString(25));
				data.setiUserServiceTermId(cursor.getString(26));
				data.setiRoleId(cursor.getString(27));
				data.setProcessedAuto(cursor.getString(28));

				// Adding contact to list
				shipMasterDataList.add(data);
			} while (cursor.moveToNext());
		}

		db.close();
		// return contact list
		return shipMasterDataList;
	}

	// code to get all contacts in a list view
	public String getEnteredScore(String uid, String strServiceTermId) {
		// Select All Query

		/**
		 * This code is running from 5 Apr 2016, this shows all score of user
		 * of user service term including consecutive.
		 * change by PN Sir.
		 */
		String enteredScore = getServiceTermWiseEnteredScore(uid);
		
		/**
		 * Below code is running before 5 Apr 2016, this shows all score of user.
		 * change by PN Sir.
		 */
	/*	int enteredScore = 0;
		int enteredNegativeScore = 0;
		try {
			String selectQuery = "SELECT " + KEY_POWER_POINTS + "  FROM "
					+ TABLE_POWER_PLUS_TRANSACTION + " WHERE "
					+ KEY_STR_RECEIVER_ID + " = " + "'" + uid + "' and "
					+ KEY_FLG_SCORE_STATUS + "='P' and " + KEY_FLG_STATUS
					+ " != 'Rejected' and " + KEY_FLG_DELETED + " ='0' ";
			
			//and "	+ KEY_USER_SERVICE_TERM_ID + " ='" + strServiceTermId + "'
			
			//System.out.println("Entered Query :" + selectQuery);
			SQLiteDatabase db = this.getWritableDatabase();
			Cursor cursor = db.rawQuery(selectQuery, null);

			if (cursor.moveToFirst()) {
				do {
					enteredScore = enteredScore
							+ Integer.parseInt(cursor.getString(cursor
									.getColumnIndex(KEY_POWER_POINTS)));
				} while (cursor.moveToNext());

			}

			String selectNegQuery = "SELECT " + KEY_POWER_POINTS + "  FROM "
					+ TABLE_POWER_PLUS_TRANSACTION + " WHERE "
					+ KEY_STR_RECEIVER_ID + " = " + "'" + uid + "' and "
					+ KEY_FLG_SCORE_STATUS + "='N' and " + KEY_FLG_STATUS
					+ " != 'Rejected' and " + KEY_FLG_DELETED + " ='0'";

			// and "+ KEY_USER_SERVICE_TERM_ID + " ='" + strServiceTermId + "'
			//System.out.println("Entered Neg Query :" + selectNegQuery);

			Cursor cursorNeg = db.rawQuery(selectNegQuery, null);

			if (cursorNeg.moveToFirst()) {
				do {
					enteredNegativeScore = enteredNegativeScore
							+ Integer.parseInt(cursorNeg.getString(cursorNeg
									.getColumnIndex(KEY_POWER_POINTS)));
				} while (cursorNeg.moveToNext());

			}

			db.close();
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("Exception :" + e);
		}

		// enteredScore = enteredScore - enteredNegativeScore;
		enteredScore = enteredScore + enteredNegativeScore;
		// return contact list
		System.out.println("return ENTERED vALUE :" + enteredScore);
		return String.valueOf(enteredScore);
		*/
		
		return enteredScore;

	} 
	
	/**
	 * This method return user score with service term wise if
	 * service term as consecutive then include points also. 
	 * @return
	 */
	public String getServiceTermWiseEnteredScore(String uid){
		int enteredScore =0 ;
		int enteredNegativeScore = 0;
		
		List<UserServiceTermRoleData> userServiceTermDataList = getServiceTermIdListOfUser(uid);
		if(userServiceTermDataList != null && userServiceTermDataList.size() > 0){
			for(UserServiceTermRoleData ust : userServiceTermDataList){				
				
				try {
					String selectQuery = "SELECT " + KEY_POWER_POINTS + "  FROM "
							+ TABLE_POWER_PLUS_TRANSACTION + " WHERE "
							+ KEY_STR_RECEIVER_ID + " = " + "'" + uid + "' and "
							+ KEY_FLG_SCORE_STATUS + "='P' and " + KEY_FLG_STATUS
							+ " != 'Rejected' and " + KEY_FLG_DELETED + " ='0' and "
							+ KEY_USER_SERVICE_TERM_ID + " ='" + ust.getIuserServiceTermId() + "'";
					
					//System.out.println("Entered Query :" + selectQuery);
					SQLiteDatabase db = this.getWritableDatabase();
					Cursor cursor = db.rawQuery(selectQuery, null);

					if (cursor.moveToFirst()) {
						do {
							enteredScore = enteredScore
									+ Integer.parseInt(cursor.getString(cursor
											.getColumnIndex(KEY_POWER_POINTS)));
						} while (cursor.moveToNext());

					}

					String selectNegQuery = "SELECT " + KEY_POWER_POINTS + "  FROM "
							+ TABLE_POWER_PLUS_TRANSACTION + " WHERE "
							+ KEY_STR_RECEIVER_ID + " = " + "'" + uid + "' and "
							+ KEY_FLG_SCORE_STATUS + "='N' and " + KEY_FLG_STATUS
							+ " != 'Rejected' and " + KEY_FLG_DELETED + " ='0' and "
							+ KEY_USER_SERVICE_TERM_ID + " ='" + ust.getIuserServiceTermId() + "'";
					
					//System.out.println("Entered Neg Query :" + selectNegQuery);

					Cursor cursorNeg = db.rawQuery(selectNegQuery, null);

					if (cursorNeg.moveToFirst()) {
						do {
							enteredNegativeScore = enteredNegativeScore
									+ Integer.parseInt(cursorNeg.getString(cursorNeg
											.getColumnIndex(KEY_POWER_POINTS)));
						} while (cursorNeg.moveToNext());

					}

					db.close();
				} catch (Exception e) {
					// TODO: handle exception
					System.out.println("Exception :" + e);
				}

				// enteredScore = enteredScore - enteredNegativeScore;
				enteredScore = enteredScore + enteredNegativeScore;
			}
		}
		
		
		return String.valueOf(enteredScore);
	}

	public String getValidatedScore(String uid, String strServiceTermId) {
		// String rank = "";
		System.out.println("in validated");
		/**
		 * This code is running from 5 Apr 2016, this shows all score of user
		 * of user service term including consecutive.
		 * change by PN Sir.
		 */
		String validatededScore = getServiceTermWiseValidatedScore(uid);
		
		/**
		 * Below code is running before 5 Apr 2016, this shows all score of user.
		 * change by PN Sir.
		 */
		//================================================================
		/*
		  int validatededScore = 0;
		  int negativeValidatedScore = 0;
		try {
			String selectQuery = "SELECT " + KEY_POWER_POINTS + "  FROM "
					+ TABLE_POWER_PLUS_TRANSACTION + " WHERE "
					+ KEY_STR_RECEIVER_ID + " = " + "'" + uid + "' and "
					+ KEY_FLG_STATUS + " = 'Validated' and "
					+ KEY_FLG_SCORE_STATUS + "='P' and " + KEY_FLG_DELETED
					+ " ='0'";
			
			 //and " + KEY_USER_SERVICE_TERM_ID + " ='"+ strServiceTermId + "'
			//System.out.println("validated Query :" + selectQuery);
			SQLiteDatabase db = this.getWritableDatabase();
			Cursor cursor = db.rawQuery(selectQuery, null);
			if (cursor.moveToFirst()) {
				do {
					validatededScore = validatededScore
							+ Integer.parseInt(cursor.getString(cursor
									.getColumnIndex(KEY_POWER_POINTS)));
				} while (cursor.moveToNext());

			}

			*//**
			 * For Negative score N
			 *//*

			String negQuery = "SELECT " + KEY_POWER_POINTS + "  FROM "
					+ TABLE_POWER_PLUS_TRANSACTION + " WHERE "
					+ KEY_STR_RECEIVER_ID + " = " + "'" + uid + "' and "
					+ KEY_FLG_STATUS + " = 'Validated' and "
					+ KEY_FLG_SCORE_STATUS + "='N' and " + KEY_FLG_DELETED
					+ " ='0'";
			
			// and " + KEY_USER_SERVICE_TERM_ID + " ='"+ strServiceTermId + "'
			//System.out.println("negQuery :" + negQuery);
			Cursor cursorNeg = db.rawQuery(negQuery, null);
			if (cursorNeg.moveToFirst()) {
				do {
					negativeValidatedScore = negativeValidatedScore
							+ Integer.parseInt(cursorNeg.getString(cursorNeg
									.getColumnIndex(KEY_POWER_POINTS)));
				} while (cursorNeg.moveToNext());

			}

			db.close();
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("Exception :" + e);
		}

		// validatededScore = validatededScore - negativeValidatedScore;
		validatededScore = validatededScore + negativeValidatedScore;
		System.out.println("return validated vALUE :" + validatededScore);
		*/
		// return contact list
		//return String.valueOf(validatededScore);
		return validatededScore;

	}
	
	/**
	 * This method return user score with service term wise if
	 * service term as consecutive then include points also. 
	 * @return
	 */
	public String getServiceTermWiseValidatedScore(String uid){
		int validatededScore =0 ;
		int negativeValidatedScore = 0;
		List<UserServiceTermRoleData> userServiceTermDataList = getServiceTermIdListOfUser(uid);
		if(userServiceTermDataList != null && userServiceTermDataList.size() > 0){
			for(UserServiceTermRoleData ust : userServiceTermDataList){
				
				try {
					String selectQuery = "SELECT " + KEY_POWER_POINTS + "  FROM "
							+ TABLE_POWER_PLUS_TRANSACTION + " WHERE "
							+ KEY_STR_RECEIVER_ID + " = " + "'" + uid + "' and "
							+ KEY_FLG_STATUS + " = 'Validated' and "
							+ KEY_FLG_SCORE_STATUS + "='P' and " + KEY_FLG_DELETED
							+ " ='0' and " + KEY_USER_SERVICE_TERM_ID + " ='"+ ust.getIuserServiceTermId() + "'";
					
					//System.out.println("validated Query :" + selectQuery);
					SQLiteDatabase db = this.getWritableDatabase();
					Cursor cursor = db.rawQuery(selectQuery, null);
					if (cursor.moveToFirst()) {
						do {
							validatededScore = validatededScore
									+ Integer.parseInt(cursor.getString(cursor
											.getColumnIndex(KEY_POWER_POINTS)));
						} while (cursor.moveToNext());

					}

					/**
					 * For Negative score N
					 */

					String negQuery = "SELECT " + KEY_POWER_POINTS + "  FROM "
							+ TABLE_POWER_PLUS_TRANSACTION + " WHERE "
							+ KEY_STR_RECEIVER_ID + " = " + "'" + uid + "' and "
							+ KEY_FLG_STATUS + " = 'Validated' and "
							+ KEY_FLG_SCORE_STATUS + "='N' and " + KEY_FLG_DELETED
							+ " ='0' and " + KEY_USER_SERVICE_TERM_ID + " ='"+ ust.getIuserServiceTermId() + "'";
					
					//System.out.println("negQuery :" + negQuery);
					Cursor cursorNeg = db.rawQuery(negQuery, null);
					if (cursorNeg.moveToFirst()) {
						do {
							negativeValidatedScore = negativeValidatedScore
									+ Integer.parseInt(cursorNeg.getString(cursorNeg
											.getColumnIndex(KEY_POWER_POINTS)));
						} while (cursorNeg.moveToNext());

					}

					db.close();
				} catch (Exception e) {
					// TODO: handle exception
					System.out.println("Exception :" + e);
				}

				// validatededScore = validatededScore - negativeValidatedScore;
				validatededScore = validatededScore + negativeValidatedScore;
				System.out.println("return validated vALUE :" + validatededScore);
				
			}
		}
		
		
		return String.valueOf(validatededScore);
	}

	
	/**
	 * get all service term ids including
	 * consecutive service term bcoz all consecutive service term points added.
	 */
	public List<UserServiceTermRoleData> getServiceTermIdListOfUser(String strUserId){
		List<UserServiceTermRoleData> ustList = new ArrayList<UserServiceTermRoleData>();
		
		
		Date curDate = new Date();
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd");		
		List<UserServiceTermRoleData> serviceTermRoleData = new ArrayList<UserServiceTermRoleData>();
		serviceTermRoleData = getUserServiceTermRoleRowBiId(strUserId,format.format(curDate));
		UserServiceTermRoleData userSrvTrmRlData = null;
		if(serviceTermRoleData != null && serviceTermRoleData.size() > 0){
			userSrvTrmRlData = new UserServiceTermRoleData();
			userSrvTrmRlData = serviceTermRoleData.get(0);
		}  
        		
		ustList = getUserServiceTermRoleByUser(strUserId,(userSrvTrmRlData != null ? userSrvTrmRlData.getiRoleId() : "00"));
		Collections.sort(ustList,
                new UserServiceTermDateComparator());
		List<UserServiceTermRoleData> usetDataList = new ArrayList<UserServiceTermRoleData>();
		
		Date startDate=null;
		Date endDate = null;
		int counter =0;
		if(ustList != null && ustList.size() > 0)
		{
			for(UserServiceTermRoleData ust : ustList){
				
				L.fv("User Service term : "+counter+" date from : "+ust.getDtTermFrom()+" date to : "+ust.getDtTermTo() +" size of list : "+ustList.size());
				
				if(counter > 0){
					try {
						endDate = format.parse(ust.getDtTermTo());
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}	
					
					int dayDiff = getNumberOfDays(endDate, startDate, 0);
					
					L.fv("day difference : "+dayDiff);
					if(dayDiff > 1){
						break;
					}
					else{
						usetDataList.add(ust);
					}
				
				}
				else{
					usetDataList.add(ust);
				}
				
				try {
					startDate = format.parse(ust.getDtTermFrom());
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}		
				
				
				counter++;
				
			}
		}
		L.fv("Size of User added list :"+usetDataList.size());
		return usetDataList;
	}
	
	/**
	 * This method return number of days which have differenece of two UST.
	 * @param cFrom
	 * @param cTo
	 * @param flgInclude
	 * @return
	 */
	public int getNumberOfDays(Date cFrom, Date cTo, int flgInclude) {
        float days = 0;
        if (cTo.getTime() > new Date().getTime() && flgInclude > 0) {
                days = (removeTime(new Date()).getTime() - cFrom.getTime())
                                / (1000 * 60 * 60 * 24);
        } else {
                days = (cTo.getTime() - cFrom.getTime()) / (1000 * 60 * 60 * 24);
        }

        int days1 = 0;
        days1 = Math.round(days);
        days1 = days1 + flgInclude;
        L.fv("Number of days " + days);
        return days1;
	}
	
	/**
	 * Remove time for check
	 * @param date
	 * @return
	 */
	public Date removeTime(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
	}

	public List<PowerPlusTransaction> getPowerPlusTransactionForValidate(
			String uid, String strcategoryId) {
		List<PowerPlusTransaction> shipMasterDataList = new ArrayList<PowerPlusTransaction>();
		/**
		 * Select All Query get records except LeaderShip Category(0002)
		 */
		String selectQuery = "SELECT  * FROM " + TABLE_POWER_PLUS_TRANSACTION
				+ " where " + KEY_STR_RECEIVER_ID + " != '" + uid + "' and "
				+ KEY_FLG_STATUS + " = 'Entered' and " + KEY_FLG_DELETED
				+ "='0' and " + KEY_I_POWER_PLUS_CATEGORY_ID + " != '"
				+ strcategoryId + "'";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				PowerPlusTransaction data = new PowerPlusTransaction();
				data.setId(Integer.parseInt(cursor.getString(0)));
				data.setiPowerPlusTransactionId(cursor.getString(1));
				data.setDtCreated(cursor.getString(2));
				data.setDtUpdated(cursor.getString(3));
				data.setFlgDeleted(cursor.getString(4));
				data.setFlgIsDirty(cursor.getString(5));
				data.setFlgIsTransationInvalid(cursor.getString(6));
				data.setFlgStatus(cursor.getString(7));
				data.setiSequence(cursor.getString(8));
				data.setPowerPoints(cursor.getString(9));
				data.setStrAdditionalOne(cursor.getString(10));
				data.setStrAdditionalTwo(cursor.getString(11));
				data.setStrApproverId(cursor.getString(12));
				data.setStrApproverName(cursor.getString(13));
				data.setStrDescription(cursor.getString(14));
				data.setStrProviderId(cursor.getString(15));
				data.setStrProviderName(cursor.getString(16));
				data.setStrTitle(cursor.getString(17));
				data.setiPowerPlusTaskId(cursor.getString(18));
				data.setiShipId(cursor.getString(19));
				data.setiTenantId(cursor.getString(20));
				data.setiUserId(cursor.getString(21));
				data.setStrReceiverName(cursor.getString(22));
				data.setFlgScoreStatus(cursor.getString(23));
				data.setCetogoryId(cursor.getString(24));
				data.setParentTransactionId(cursor.getString(25));
				data.setiUserServiceTermId(cursor.getString(26));
				data.setiRoleId(cursor.getString(27));
				data.setProcessedAuto(cursor.getString(28));

				// Adding contact to list
				shipMasterDataList.add(data);
			} while (cursor.moveToNext());
		}

		/**
		 * get records of only leadership category group by task id
		 */
		selectQuery = "SELECT  * FROM " + TABLE_POWER_PLUS_TRANSACTION
				+ " where " + KEY_STR_PROVIDER_ID + " != '" + uid + "' and "
				+ KEY_FLG_STATUS + " = 'Entered' and " + KEY_FLG_DELETED
				+ "='0' and " + KEY_I_POWER_PLUS_CATEGORY_ID + " == '"
				+ strcategoryId + "'" + " group by " + KEY_I_POWER_PLUS_TASK_ID;

		Cursor cursorL = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursorL.moveToFirst()) {
			do {
				PowerPlusTransaction data = new PowerPlusTransaction();
				data.setId(Integer.parseInt(cursorL.getString(0)));
				data.setiPowerPlusTransactionId(cursorL.getString(1));
				data.setDtCreated(cursorL.getString(2));
				data.setDtUpdated(cursorL.getString(3));
				data.setFlgDeleted(cursorL.getString(4));
				data.setFlgIsDirty(cursorL.getString(5));
				data.setFlgIsTransationInvalid(cursorL.getString(6));
				data.setFlgStatus(cursorL.getString(7));
				data.setiSequence(cursorL.getString(8));
				data.setPowerPoints(cursorL.getString(9));
				data.setStrAdditionalOne(cursorL.getString(10));
				data.setStrAdditionalTwo(cursorL.getString(11));
				data.setStrApproverId(cursorL.getString(12));
				data.setStrApproverName(cursorL.getString(13));
				data.setStrDescription(cursorL.getString(14));
				data.setStrProviderId(cursorL.getString(15));
				data.setStrProviderName(cursorL.getString(16));
				data.setStrTitle(cursorL.getString(17));
				data.setiPowerPlusTaskId(cursorL.getString(18));
				data.setiShipId(cursorL.getString(19));
				data.setiTenantId(cursorL.getString(20));
				data.setiUserId(cursorL.getString(21));
				data.setStrReceiverName(cursorL.getString(22));
				data.setFlgScoreStatus(cursorL.getString(23));
				data.setCetogoryId(cursorL.getString(24));
				data.setParentTransactionId(cursorL.getString(25));
				data.setiUserServiceTermId(cursorL.getString(26));
				data.setiRoleId(cursorL.getString(27));
				data.setProcessedAuto(cursorL.getString(28));

				// Adding contact to list
				shipMasterDataList.add(data);
			} while (cursorL.moveToNext());
		}

		db.close();
		// return contact list
		return shipMasterDataList;
	}

	public String getRoleName(String rid) {
		// Select All Query

		String role = "";
		SQLiteDatabase db = this.getWritableDatabase();
		
		try {
			String selectQuery = "SELECT " + KEY_ROLE + " FROM " + TABLE_ROLE
					+ " WHERE " + KEY_ROLE_ID + " = " + "'" + rid + "'";
			System.out.println("Query :" + selectQuery);
			
			Cursor cursor = db.rawQuery(selectQuery, null);
			if (cursor.getCount() > 0) {
				cursor.moveToFirst();
				role = cursor.getString(cursor.getColumnIndex(KEY_ROLE));
			}
			
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("Exception :" + e);
		}
		// return contact list
		System.out.println("Role " + role);
		db.close();
		return role;

	}

	public String getUserServiceTermId(String id) {
		// Select All Query

		String userServiceTermId = "";
		try {
			String selectQuery = "SELECT " + KEY_USER_SERVICE_TERM_ID
					+ " FROM " + TABLE_USER_SERVICE_TERM + " WHERE "
					+ KEY_iUSER_ID + " = " + "'" + id + "'";
			System.out.println("Query :" + selectQuery);
			SQLiteDatabase db = this.getWritableDatabase();
			Cursor cursor = db.rawQuery(selectQuery, null);
			if (cursor.getCount() > 0) {
				cursor.moveToFirst();
				userServiceTermId = cursor.getString(cursor
						.getColumnIndex(KEY_USER_SERVICE_TERM_ID));
			}
			db.close();
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("Exception :" + e);
		}
		// return contact list
		System.out.println("UserServiceTermId :" + userServiceTermId);
		return userServiceTermId;

	}

	public String getShipId(String tenantId) {
		// Select All Query

		String shipId = "";
		try {
			String selectQuery = "SELECT " + KEY_SHIP_ID + " FROM "
					+ TABLE_SHIP_MASTER + " WHERE " + KEY_TENANT_ID + " = "
					+ "'" + tenantId + "'";
			System.out.println("Query :" + selectQuery);
			SQLiteDatabase db = this.getWritableDatabase();
			Cursor cursor = db.rawQuery(selectQuery, null);
			if (cursor.getCount() > 0) {
				cursor.moveToFirst();
				shipId = cursor.getString(cursor.getColumnIndex(KEY_SHIP_ID));
			}
			db.close();
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println("Exception :" + e);
		}
		// return contact list
		System.out.println(" :" + shipId);
		return shipId;

	}

	// Getting contacts Count
	public int getContactsCount() {
		String countQuery = "SELECT  * FROM " + TABLE_USER_NFC;
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery(countQuery, null);
		cursor.close();

		// return count
		return cursor.getCount();
	}

	/**
	 * return SyncHistory data
	 * 
	 * @return
	 */
	public List<SyncHistory> getSyncHistoryRow() {
		List<SyncHistory> syncHistoryList = new ArrayList<SyncHistory>();

		String selectQuery = "SELECT  * FROM " + TABLE_SYNC_HISTORY + " where "+KEY_FLG_DELETED+" ='0'";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				SyncHistory sh = new SyncHistory();
				sh.setId(Integer.parseInt(cursor.getString(0)));
				sh.setIsyncHistoryId(cursor.getString(1));
				sh.setDtSyncDate(cursor.getString(2));
				sh.setLogMessage(cursor.getString(3));
				sh.setProgress(cursor.getString(4));
				sh.setGenerator(cursor.getString(5));
				sh.setStrFilename(cursor.getString(6));
				sh.setSyncOrderid(cursor.getInt(7));
				sh.setFlgDeleted(cursor.getInt(8));
				sh.setFlgIsDirty(cursor.getInt(9));
				sh.setDtAcknowledgeDate(cursor.getString(10));
				sh.setDtGeneratedDate(cursor.getString(11));
				sh.setLastDownLoadDate(cursor.getString(12));
				sh.setStrMacId(cursor.getString(13));
				sh.setStrRegisterTabletId(cursor.getString(14));
				sh.setiShipId(Integer.parseInt(cursor.getString(15)));
				sh.setiTenantId(Integer.parseInt(cursor.getString(16)));

				// Adding contact to list
				syncHistoryList.add(sh);
			} while (cursor.moveToNext());
		}

		db.close();
		// return contact list
		return syncHistoryList;
	}

	/**
	 * @author ripunjay
	 * @param ppt
	 * @return
	 */
	public long insertSyncHistorydata(SyncHistory syncHistory) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_SYNC_HISTORY_ID, syncHistory.getSyncOrderid());
		values.put(SYNC_DATE, syncHistory.getDtSyncDate());
		values.put(LOG_MSG, syncHistory.getLogMessage());
		values.put(SYNC_PROGRESS, syncHistory.getProgress());
		values.put(SYNC_GENERATOR, syncHistory.getGenerator());
		values.put(SYNC_FILE_NAME, syncHistory.getStrFilename());
		values.put(KEY_FLG_IS_DIRTY, syncHistory.getFlgIsDirty());
		values.put(SYNC_ORDER_ID, syncHistory.getSyncOrderid());
		values.put(KEY_TENANT_ID, syncHistory.getiTenantId());
		values.put(KEY_SHIP_ID, syncHistory.getiShipId());
		values.put(KEY_FLG_DELETED, syncHistory.getFlgDeleted());
		values.put(SYNC_ACK_DATE, syncHistory.getDtAcknowledgeDate());
		values.put(SYNC_GEN_DATE, syncHistory.getDtGeneratedDate());
		values.put(SYNC_LAST_DOWN_DATE, syncHistory.getLastDownLoadDate());
		values.put(MAC_ID, syncHistory.getStrMacId());
		values.put(REG_TABLET_ID, syncHistory.getStrRegisterTabletId());

		long result = db.insert(TABLE_SYNC_HISTORY, null, values);
		db.close();
		return result;
	}

	/**
	 * @author ripunjay
	 * @param ppt
	 * @return
	 */
	public long updateSyncHistorydata(SyncHistory syncHistory) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();
		values.put(KEY_SYNC_HISTORY_ID, syncHistory.getIsyncHistoryId());
		values.put(SYNC_DATE, syncHistory.getDtSyncDate());
		values.put(LOG_MSG, syncHistory.getLogMessage());
		values.put(SYNC_PROGRESS, syncHistory.getProgress());
		values.put(SYNC_GENERATOR, syncHistory.getGenerator());
		values.put(SYNC_FILE_NAME, syncHistory.getStrFilename());
		values.put(KEY_FLG_IS_DIRTY, syncHistory.getFlgIsDirty());
		values.put(SYNC_ORDER_ID, syncHistory.getSyncOrderid());
		values.put(KEY_TENANT_ID, syncHistory.getiTenantId());
		values.put(KEY_SHIP_ID, syncHistory.getiShipId());
		values.put(KEY_FLG_DELETED, syncHistory.getFlgDeleted());
		values.put(SYNC_ACK_DATE, syncHistory.getDtAcknowledgeDate());
		values.put(SYNC_GEN_DATE, syncHistory.getDtGeneratedDate());
		values.put(SYNC_LAST_DOWN_DATE, syncHistory.getLastDownLoadDate());
		values.put(MAC_ID, syncHistory.getStrMacId());
		values.put(REG_TABLET_ID, syncHistory.getStrRegisterTabletId());

		String whereClause = KEY_SYNC_HISTORY_ID + " =  '"
				+ syncHistory.getIsyncHistoryId() + "'";

		long result = db.update(TABLE_SYNC_HISTORY, values, whereClause, null);
		db.close();
		return result;
	}

	/**
	 * @author ripunjay
	 * @param PowerPlusRoleRankCategory
	 * @return
	 */
	@SuppressLint("SimpleDateFormat")
	public long insertPowerPlusRoleRankCategory(
			PowerPlusRoleRankCategory powerPlusRoleRankCategory) {
		SQLiteDatabase db = this.getWritableDatabase();

		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

		ContentValues values = new ContentValues();
		values.put(KEY_POWERPLUS_ROLERANK_CATEGORY_ID,
				powerPlusRoleRankCategory.getiPowerPlusRoleRankCategoryId());
		values.put(KEY_DT_CREATED, powerPlusRoleRankCategory.getDtCreated());
		values.put(KEY_DT_UPDATED, powerPlusRoleRankCategory.getDtUpdated());
		values.put(KEY_FLG_DELETED, powerPlusRoleRankCategory.getFlgDeleted());
		values.put(RANK_CATEGORY,
				powerPlusRoleRankCategory.getStrRankCategory());
		values.put(ROLES_IDS, powerPlusRoleRankCategory.getStrRolesId());
		values.put(KEY_TENANT_ID, powerPlusRoleRankCategory.getiTenantId());
		values.put(FIELD_ONE, powerPlusRoleRankCategory.getStrField1());
		values.put(FIELD_TWO, powerPlusRoleRankCategory.getStrField2());

		long result = db
				.insert(TABLE_POWERPLUS_ROLERANK_CATEGORY, null, values);
		db.close();
		return result;
	}

	/**
	 * @author ripunjay
	 * @param PowerPlusRoleRankCategory
	 * @return
	 */
	@SuppressLint("SimpleDateFormat")
	public long updatePowerPlusRoleRankCategory(
			PowerPlusRoleRankCategory powerPlusRoleRankCategory) {
		SQLiteDatabase db = this.getWritableDatabase();

		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

		ContentValues values = new ContentValues();
		values.put(KEY_POWERPLUS_ROLERANK_CATEGORY_ID,
				powerPlusRoleRankCategory.getiPowerPlusRoleRankCategoryId());
		values.put(KEY_DT_CREATED, powerPlusRoleRankCategory.getDtCreated());
		values.put(KEY_DT_UPDATED, powerPlusRoleRankCategory.getDtUpdated());
		values.put(KEY_FLG_DELETED, powerPlusRoleRankCategory.getFlgDeleted());
		values.put(RANK_CATEGORY,
				powerPlusRoleRankCategory.getStrRankCategory());
		values.put(ROLES_IDS, powerPlusRoleRankCategory.getStrRolesId());
		values.put(KEY_TENANT_ID, powerPlusRoleRankCategory.getiTenantId());
		values.put(FIELD_ONE, powerPlusRoleRankCategory.getStrField1());
		values.put(FIELD_TWO, powerPlusRoleRankCategory.getStrField2());

		String whereClause = KEY_POWERPLUS_ROLERANK_CATEGORY_ID + " =  '"
				+ powerPlusRoleRankCategory.getiPowerPlusRoleRankCategoryId()
				+ "'";

		long result = db.update(TABLE_POWERPLUS_ROLERANK_CATEGORY, values,
				whereClause, null);
		db.close();
		return result;

	}

	/**
	 * return PowerPlusRoleRankCategory data
	 * 
	 * @return
	 */
	public List<PowerPlusRoleRankCategory> getPowerPlusRoleRankCategoryRow() {
		List<PowerPlusRoleRankCategory> powerPlusRoleRankCategoryList = new ArrayList<PowerPlusRoleRankCategory>();

		String selectQuery = "SELECT  * FROM "
				+ TABLE_POWERPLUS_ROLERANK_CATEGORY + " where "
				+ KEY_FLG_DELETED + " ='0'";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				PowerPlusRoleRankCategory data = new PowerPlusRoleRankCategory();

				data.setiPowerPlusRoleRankCategoryId(cursor.getString(1));
				data.setDtCreated(cursor.getString(2));
				data.setDtUpdated(cursor.getString(3));
				data.setFlgDeleted(cursor.getInt(4));
				data.setStrRankCategory(cursor.getString(5));
				data.setStrRolesId(cursor.getString(6));
				data.setiTenantId(cursor.getString(7));
				data.setStrField1(cursor.getString(8));
				data.setStrField2(cursor.getString(9));

				// Adding contact to list
				powerPlusRoleRankCategoryList.add(data);
			} while (cursor.moveToNext());
		}

		// return contact list
		db.close();
		return powerPlusRoleRankCategoryList;
	}

	/**
	 * return PowerPlusRoleRankCategory data
	 * 
	 * @return
	 */
	public List<PowerPlusRoleRankCategory> getPowerPlusRoleRankCategoryRowByRoleId(
			String strRoleid) {
		List<PowerPlusRoleRankCategory> powerPlusRoleRankCategoryList = new ArrayList<PowerPlusRoleRankCategory>();

		String selectQuery = "SELECT  * FROM "
				+ TABLE_POWERPLUS_ROLERANK_CATEGORY + " where "
				+ KEY_FLG_DELETED + " ='0' and " + ROLES_IDS + " like '%,"
				+ strRoleid + ",%'";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				PowerPlusRoleRankCategory data = new PowerPlusRoleRankCategory();

				data.setiPowerPlusRoleRankCategoryId(cursor.getString(1));
				data.setDtCreated(cursor.getString(2));
				data.setDtUpdated(cursor.getString(3));
				data.setFlgDeleted(cursor.getInt(4));
				data.setStrRankCategory(cursor.getString(5));
				data.setStrRolesId(cursor.getString(6));
				data.setiTenantId(cursor.getString(7));
				data.setStrField1(cursor.getString(8));
				data.setStrField2(cursor.getString(9));

				// Adding contact to list
				powerPlusRoleRankCategoryList.add(data);
			} while (cursor.moveToNext());
		}

		// return contact list
		db.close();
		return powerPlusRoleRankCategoryList;
	}

	/**
	 * return PowerPlusRoleRankCategory data
	 * 
	 * @return
	 */
	public List<PowerPlusRoleRankCategory> getPowerPlusRoleRankCategoryRowById(
			String strRolerankCategoryId) {
		List<PowerPlusRoleRankCategory> powerPlusRoleRankCategoryList = new ArrayList<PowerPlusRoleRankCategory>();

		String selectQuery = "SELECT  * FROM "
				+ TABLE_POWERPLUS_ROLERANK_CATEGORY + " where "
				+ KEY_FLG_DELETED + " ='0' and "
				+ KEY_POWERPLUS_ROLERANK_CATEGORY_ID + " ='"
				+ strRolerankCategoryId + "'";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				PowerPlusRoleRankCategory data = new PowerPlusRoleRankCategory();

				data.setiPowerPlusRoleRankCategoryId(cursor.getString(1));
				data.setDtCreated(cursor.getString(2));
				data.setDtUpdated(cursor.getString(3));
				data.setFlgDeleted(cursor.getInt(4));
				data.setStrRankCategory(cursor.getString(5));
				data.setStrRolesId(cursor.getString(6));
				data.setiTenantId(cursor.getString(7));
				data.setStrField1(cursor.getString(8));
				data.setStrField2(cursor.getString(9));

				// Adding contact to list
				powerPlusRoleRankCategoryList.add(data);
			} while (cursor.moveToNext());
		}

		// return contact list
		db.close();
		return powerPlusRoleRankCategoryList;
	}

	/**
	 * @author ripunjay
	 * @param ROLEFUNCTIONS
	 * @return
	 */
	@SuppressLint("SimpleDateFormat")
	public long insertRoleFunctiondata(TabletRoleFunctions tabletRoleFunctions) {
		SQLiteDatabase db = this.getWritableDatabase();

		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

		ContentValues values = new ContentValues();
		values.put(KEY_ROLE_FUNCTION_ID,
				tabletRoleFunctions.getiTabletRoleFunctionsId());
		values.put(KEY_DT_CREATED, tabletRoleFunctions.getDtCreated());
		values.put(KEY_DT_UPDATED, tabletRoleFunctions.getDtUpdated());
		values.put(KEY_FLG_DELETED, tabletRoleFunctions.getFlgDeleted());
		values.put(ROLE_FUNCTION_CODE, tabletRoleFunctions.getStrFunctionCode());
		values.put(KEY_ROLE_ID, tabletRoleFunctions.getiRoleId());
		values.put(KEY_TENANT_ID, tabletRoleFunctions.getiTenantId());
		values.put(FIELD_ONE, tabletRoleFunctions.getStrField1());
		values.put(FIELD_TWO, tabletRoleFunctions.getStrField2());

		long result = db.insert(TABLE_ROLE_FUNCTION, null, values);
		db.close();
		return result;

	}

	/**
	 * return TabletRoleFunctions data
	 * 
	 * @return
	 */
	public List<TabletRoleFunctions> getTabletRoleFunctionsRow() {
		List<TabletRoleFunctions> tabletRoleFunctionsList = new ArrayList<TabletRoleFunctions>();

		String selectQuery = "SELECT  * FROM " + TABLE_ROLE_FUNCTION
				+ " where " + KEY_FLG_DELETED + " ='0'";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				TabletRoleFunctions data = new TabletRoleFunctions();

				data.setiTabletRoleFunctionsId(cursor.getString(1));
				data.setDtCreated(cursor.getString(2));
				data.setDtUpdated(cursor.getString(3));
				data.setFlgDeleted(cursor.getInt(4));
				data.setStrFunctionCode(cursor.getString(5));
				data.setiRoleId(cursor.getString(6));
				data.setStrField1(cursor.getString(7));
				data.setStrField2(cursor.getString(8));
				data.setiTenantId(cursor.getString(9));

				// Adding contact to list
				tabletRoleFunctionsList.add(data);
			} while (cursor.moveToNext());
		}

		// return contact list
		db.close();
		return tabletRoleFunctionsList;
	}

	/**
	 * return TabletRoleFunctions data
	 * 
	 * @return
	 */
	public List<TabletRoleFunctions> getTabletRoleFunctionsRowByRoleId(
			String strRole) {
		List<TabletRoleFunctions> tabletRoleFunctionsList = new ArrayList<TabletRoleFunctions>();

		String selectQuery = "SELECT  * FROM " + TABLE_ROLE_FUNCTION
				+ " where " + KEY_FLG_DELETED + " ='0' and " + KEY_ROLE_ID
				+ " = '" + strRole + "'";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				TabletRoleFunctions data = new TabletRoleFunctions();

				data.setiTabletRoleFunctionsId(cursor.getString(1));
				data.setDtCreated(cursor.getString(2));
				data.setDtUpdated(cursor.getString(3));
				data.setFlgDeleted(cursor.getInt(4));
				data.setStrFunctionCode(cursor.getString(5));
				data.setiRoleId(cursor.getString(6));
				data.setStrField1(cursor.getString(7));
				data.setStrField2(cursor.getString(8));
				data.setiTenantId(cursor.getString(9));

				// Adding contact to list
				tabletRoleFunctionsList.add(data);
			} while (cursor.moveToNext());
		}

		db.close();
		// return contact list
		return tabletRoleFunctionsList;
	}

	/**
	 * return TabletRoleFunctions data
	 * 
	 * @return
	 */
	public List<TabletRoleFunctions> getTabletRoleFunctionsRowByRoleIdAndFunctionCode(
			String strRoleId, String strFunctionCode) {
		List<TabletRoleFunctions> tabletRoleFunctionsList = new ArrayList<TabletRoleFunctions>();

		String selectQuery = "SELECT  * FROM " + TABLE_ROLE_FUNCTION
				+ " where " + KEY_FLG_DELETED + " ='0' and " + KEY_ROLE_ID
				+ " = '" + strRoleId + "' and " + ROLE_FUNCTION_CODE + " ='"
				+ strFunctionCode + "'";

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		// looping through all rows and adding to list
		if (cursor.moveToFirst()) {
			do {
				TabletRoleFunctions data = new TabletRoleFunctions();

				data.setiTabletRoleFunctionsId(cursor.getString(1));
				data.setDtCreated(cursor.getString(2));
				data.setDtUpdated(cursor.getString(3));
				data.setFlgDeleted(cursor.getInt(4));
				data.setStrFunctionCode(cursor.getString(5));
				data.setiRoleId(cursor.getString(6));
				data.setStrField1(cursor.getString(7));
				data.setStrField2(cursor.getString(8));
				data.setiTenantId(cursor.getString(9));

				// Adding contact to list
				tabletRoleFunctionsList.add(data);
			} while (cursor.moveToNext());
		}

		db.close();
		// return contact list
		return tabletRoleFunctionsList;
	}

	/**
	 * Get all table Details from teh sqlite_master table in Db.
	 * 
	 * @return An ArrayList of table details.
	 */
	public ArrayList<String[]> getDbTableDetails() {
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor c = db.rawQuery(
				"SELECT name FROM sqlite_master WHERE type='table'", null);
		ArrayList<String[]> result = new ArrayList<String[]>();
		int i = 0;
		result.add(c.getColumnNames());
		for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()) {
			String[] temp = new String[c.getColumnCount()];
			for (i = 0; i < temp.length; i++) {
				temp[i] = c.getString(i);
			}
			result.add(temp);
		}

		db.close();
		return result;
	}

	/**
	 * Get all table Details from teh sqlite_master table in Db.
	 * 
	 * @return An ArrayList of table details.
	 */
	public ArrayList<String[]> getDbTableColumnDetails(String tableName) {
		ArrayList<String[]> result = new ArrayList<String[]>();
		SQLiteDatabase db = this.getReadableDatabase();
		Cursor cursor = db.rawQuery("PRAGMA table_info(" + tableName + ")",
				null);
		try {
			int nameIdx = cursor.getColumnIndexOrThrow("name");
			int typeIdx = cursor.getColumnIndexOrThrow("type");
			int notNullIdx = cursor.getColumnIndexOrThrow("notnull");
			int dfltValueIdx = cursor.getColumnIndexOrThrow("dflt_value");

			ArrayList<String> integerDefault1NotNull = new ArrayList<String>();

			while (cursor.moveToNext()) {
				String[] temp = new String[2];
				String type = cursor.getString(typeIdx);
				if (!cursor.getString(nameIdx).equalsIgnoreCase("id")) {
					if ("INTEGER".equals(type)) {
						// Integer column
						if (cursor.getInt(notNullIdx) == 1) {
							// NOT NULL
							String defaultValue = cursor
									.getString(dfltValueIdx);
							if ("1".equals(defaultValue)) {
								integerDefault1NotNull.add(cursor
										.getString(nameIdx));
							}
						}
					}
					temp[0] = cursor.getString(nameIdx);
					temp[1] = cursor.getString(typeIdx);

					result.add(temp);
				}
			}
			System.out
					.println("integerDefault1NotNull now contains a list of all columns "
							+ " defined as INTEGER NOT NULL DEFAULT 1, "
							+ integerDefault1NotNull);
		} finally {
			cursor.close();
		}

		//db.close();
		return result;
	}

	public Cursor getDataForSync(String tableName) {
		try {
			Cursor cursor ;
			StringBuffer selectQuery = null;
			if (!tableName.equalsIgnoreCase("PowerPlusTabletSyncHistory") && !tableName.equalsIgnoreCase("UserNfc")) {
				selectQuery = new StringBuffer("SELECT  * FROM " + tableName
						+ " where " + KEY_FLG_IS_DIRTY + "='1'");
			} else {
				selectQuery = new StringBuffer("SELECT  * FROM " + tableName);
			}

			SQLiteDatabase db = this.getWritableDatabase();
			cursor = db.rawQuery(selectQuery.toString(), null);

			if(cursor != null && cursor.getCount() > 0){
				
			}
			return cursor;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	public List<String> getPkDataForSync(String strPk, String tableName) {

		String selectQuery = "SELECT  " + strPk + " FROM " + tableName;// +" where "+
																		// KEY_FLG_DELETED+"='0'";

		List<String> pkList = new ArrayList<String>();

		SQLiteDatabase db = this.getWritableDatabase();
		Cursor cursor = db.rawQuery(selectQuery, null);

		if (cursor.moveToFirst()) {
			do {
				String strPkId = "";
				strPkId = cursor.getString(0);
				pkList.add(strPkId);

			} while (cursor.moveToNext());
		}

		db.close();
		return pkList;
	}

	public void insertRecordInTables(String strTablename, ContentValues values) {
		SQLiteDatabase db = this.getWritableDatabase();

		// Inserting Row
		db.insert(strTablename, null, values);
		// 2nd argument is String containing nullColumnHack
		db.close(); // Closing database connection
	}

	public long updateRecordInTables(String strTablename, String strPk_Nmae,
			String pkValues, ContentValues values) {
		SQLiteDatabase db = this.getWritableDatabase();

		String whereClause = strPk_Nmae + " =  '" + pkValues + "'";

		long result = db.update(strTablename, values, whereClause, null);
		db.close();
		return result;
	}

	public long updateDirtyRecordInTables(String strTablename) {
		SQLiteDatabase db = this.getWritableDatabase();

		ContentValues values = new ContentValues();	
		values.put(KEY_FLG_IS_DIRTY, "0"); 
		long result = db.update(strTablename, values, null, null);
		db.close();
		return result;
	}
	
	public long deleteRecordFromTables(String strTablename, String strDate) {
		SQLiteDatabase db = this.getWritableDatabase();
	
		String whereClause = KEY_DT_CREATED + " <  '" + strDate + "'";		
		long result = db.delete(strTablename, whereClause, null);
		
		
		db.close();
		return result;
	}
	
	public long deleteRecordFromTrainingTransation(String strTrainingTransationId) {
		SQLiteDatabase db = this.getWritableDatabase();
	
		String whereClause = KEY_TRAINING_TRANSACTION_ID + " =  '" + strTrainingTransationId + "'";		
		long result = db.delete(TABLE_TRAINING_TRANSACTION, whereClause, null);		
		
		long result1 = db.delete(TABLE_TRAINING_ATTENDANCE, whereClause, null);
		
		db.close();
		return result;
	}
	
	public long deleteRecordFromPowerPlusTransaction(String strTrainingTransation, String strDate) {
		SQLiteDatabase db = this.getWritableDatabase();
	
		//String whereClause = KEY_I_POWER_PLUS_TRANSACTION_ID + " = '" + strTrainingTransationId + "'";	
		String whereClause = KEY_DT_CREATED + " <=  '" + strDate + "'";	
		long result = db.delete(TABLE_POWER_PLUS_TRANSACTION, whereClause, null);
		
		
		db.close();
		return result;
	}
	
	public long deleteUserMasterRelatedData(String strUserId) {
		SQLiteDatabase db = this.getWritableDatabase();
	
		String whereClause = KEY_iUSER_ID + " =  '" + strUserId + "'";	
		
		String whereClause1 = KEY_STR_RECEIVER_ID + " =  '" + strUserId + "'";	
		
		long result4 = db.delete(TABLE_POWER_PLUS_TRANSACTION, whereClause1, null);
		
		long result = db.delete(TABLE_USER_SERVICE_TERM, whereClause, null);		
		
		long result1 = db.delete(TABLE_USER_SERVICE_TERM_ROLE, whereClause, null);
		
		long result2 = db.delete(TABLE_USER_NFC, whereClause, null);
		
		long result3 = db.delete(TABLE_USER_MASTER, whereClause, null);
		
		db.close();
		return result;
	}
	
	public long deleteAllShipRelatedData(String strShipIdId) {
		SQLiteDatabase db = this.getWritableDatabase();
	
		String whereClause = KEY_SHIP_ID + " =  '" + strShipIdId + "'";	
		String whereClause1 = KEY_iUSER_ID +" in(select iUserId from "+TABLE_USER_SERVICE_TERM
				+" where "+KEY_SHIP_ID+" ='"+strShipIdId+"') and "+KEY_iUSER_ID +" not in(select iUserId from "+TABLE_USER_SERVICE_TERM
				+" where "+KEY_SHIP_ID+" !='"+strShipIdId+"')"; 
		
		long result4 = db.delete(TABLE_POWER_PLUS_TRANSACTION, whereClause, null);		
		L.fv("Delected PowerPlusTransaction record count of result4 is : "+result4);
		
		long result5 = db.delete(TABLE_TRAINING_ATTENDANCE, whereClause, null);
		L.fv("Delected TrainingAttendance record count of result5 is : "+result5);
		
		long result6 = db.delete(TABLE_TRAINING_TRANSACTION, whereClause, null);
		L.fv("Delected TrainingTransaction record count of result6 is : "+result6);
		
		long result3 = db.delete(TABLE_USER_MASTER, whereClause1, null);
		L.fv("Delected UserMaster record count of result3 is : "+result3);
		
		long result = db.delete(TABLE_USER_SERVICE_TERM, whereClause, null);	
		L.fv("Delected UserServiceTerm record count of result is : "+result);
		
		long result1 = db.delete(TABLE_USER_SERVICE_TERM_ROLE, whereClause, null);
		L.fv("Delected UserServiceTermRole record count of result1 is : "+result1);
		
		long result2 = db.delete(TABLE_USER_NFC, whereClause, null);
		L.fv("Delected UserNfc record count of result2 is : "+result2);
		
		long result7 = db.delete(TABLE_SYNC_HISTORY, whereClause, null);
		L.fv("Delected SyncHistory record count of result7 is : "+result7); 
		
		long result8 = db.delete(TABLE_SHIP_MASTER, whereClause, null);
		L.fv("Delected ShipMaster record count of result8 is : "+result8);
		
		db.close();
		return result;
	}
}