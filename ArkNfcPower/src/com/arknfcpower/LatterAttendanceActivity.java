package com.arknfcpower;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.telephony.TelephonyManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.arknfc.adapter.TrainingAdapter;
import com.arknfcpower.db.DatabaseSupport;
import com.arknfcpower.model.TraingAttendance;
import com.arknfcpower.model.TrainingTransaction;
import com.arknfcpower.model.UserMaster;
import com.arknfcpower.model.UserServiceTermRoleData;

public class LatterAttendanceActivity extends ActionBarActivity {

	String trainingTransactionId = "";
	String trainingDataId = "";
	String trainingTime = "";
	String trainingDate = "";
	Intent intent;
	ListView lv;
	ImageView imageView;
	LinearLayout selectLout;
	TextView name;
	TextView roleName;
	
//	Button complete;
	private SimpleDateFormat dateFormatter;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	    ActionBar ab = getActionBar(); 
        ColorDrawable colorDrawable = new ColorDrawable(Color.parseColor("#009bff"));     
        ab.setBackgroundDrawable(colorDrawable);
		/**
		 * work on 4.4.2
		 */
		//ab.setDisplayUseLogoEnabled(false);
		
		ab.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM | ActionBar.DISPLAY_SHOW_HOME | ActionBar.DISPLAY_SHOW_TITLE);
		View homeIcon = findViewById(
	            Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB ? 
	                    android.R.id.home : android.support.v7.appcompat.R.id.home);
	    ((View) homeIcon.getParent()).setVisibility(View.GONE);
	    ((View) homeIcon).setVisibility(View.GONE);


		setContentView(R.layout.activity_latter_attendance);
		intent = getIntent();
		dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
        lv = (ListView) findViewById(R.id.userlist);
  //      complete = (Button) findViewById(R.id.button1);
        trainingTransactionId = intent.getStringExtra("trainingTransactionId"); 
		trainingDataId = intent.getStringExtra("trainingDataId"); 
		trainingDate = intent.getStringExtra("trainingDate"); 
		trainingTime = intent.getStringExtra("trainingTime"); 
		
	    DatabaseSupport db =new DatabaseSupport(getApplicationContext());
		ArrayList<UserMaster> umlist = db.getUserMasterRow();
		ArrayList<UserMaster> userList = new ArrayList<UserMaster>();
		
		UserMaster userMaster = new UserMaster();
		userMaster.setStrFirstName(getResources().getString(R.string.laterAttText));
		userList.add(0, userMaster);
		
		for(UserMaster um : umlist)	{
			
			Date curDate = new Date();
			DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			List<UserServiceTermRoleData> serviceTermRoleData = new ArrayList<UserServiceTermRoleData>();
			serviceTermRoleData = db.getUserServiceTermRoleRowBiId(um.getiUserId(),format.format(curDate));
			if(serviceTermRoleData != null && serviceTermRoleData.size() > 0){
				
				userList.add(um);
			}
		
		}
		
	    TrainingAdapter adapter = new TrainingAdapter(userList,getApplicationContext(),trainingTransactionId);
	    lv.setAdapter(adapter);  
	    db.close();
	    lv.setOnItemClickListener(new OnItemClickListener() {
	    	 
            @Override
            public void onItemClick(AdapterView<?> parent, View view,
               int position, long id) {
              
             // ListView Clicked item index
             int itemPosition     = position;
             
			 imageView=((ImageView)view.findViewById(R.id.select_icon));
			 name=((TextView)view.findViewById(R.id.strFirstNameLName));
			 roleName=((TextView)view.findViewById(R.id.roleName));
			 selectLout = (LinearLayout) view.findViewById(R.id.selectLout);


			 
			 if(position >0)
				 
			 {
			 
             DatabaseSupport db = new DatabaseSupport(getApplicationContext());  
             
             // ListView Clicked item value
             UserMaster  itemValue    =  (UserMaster) lv.getItemAtPosition(position);
             Date curDate = new Date();
             DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			 UserServiceTermRoleData userServiceTermRoleData = new UserServiceTermRoleData();
			 List<UserServiceTermRoleData> serviceTermRoleData = new ArrayList<UserServiceTermRoleData>();
			 serviceTermRoleData = db.getUserServiceTermRoleRowBiId(itemValue.getiUserId(),format.format(curDate));
			 if(serviceTermRoleData != null && serviceTermRoleData.size() > 0){
				
				userServiceTermRoleData = serviceTermRoleData.get(0);
			 }
             
 		
             
            if(db.getTrainingAttendanceByTransactionAndUserId(trainingTransactionId,itemValue.getiUserId() , "1").size() == 0)	
 	        {
            	 
        	        TelephonyManager  tm=(TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE); 
        	        String traningAttendanceId=tm.getDeviceId()+"_"+curDate.getTime(); 
 	        
 		          db.addTrainingAttendanceRow(new TraingAttendance(traningAttendanceId,trainingTransactionId , trainingDataId,"parentId", trainingDate, trainingTime ,
 				 "desc", "0", CardInfo.getShipId(getApplicationContext()), CardInfo.getTenantId(getApplicationContext()),dateFormatter.format(curDate) , dateFormatter.format(curDate),
 				 "1", "1",itemValue.getiUserId() , userServiceTermRoleData.getIuserServiceTermId()));
 		     	
 		          	selectLout.setVisibility(View.VISIBLE);
					imageView.setVisibility(View.VISIBLE);
					imageView.setImageResource(R.drawable.select_icon);
					imageView.invalidate();
					
					name.setTextColor(getResources().getColor(R.color.white));
					roleName.setTextColor(getResources().getColor(R.color.appcolor));

 		          view.setBackgroundColor(Color.parseColor("#06dc1a"));
 	        
 	        }else
 	        {
 	        	  String traningAttendanceId = db.getTrainingAttendanceByTransactionAndUserId(trainingTransactionId,itemValue.getiUserId() , "1").get(0).getTraningAttendanceId();
 	        	
 	        	  db.updateTrainingAttendanceRow(new TraingAttendance(traningAttendanceId,trainingTransactionId , trainingDataId,"parentId", trainingDate, trainingTime ,
 	     				 "desc", "1", CardInfo.getShipId(getApplicationContext()), CardInfo.getTenantId(getApplicationContext()),dateFormatter.format(curDate) , dateFormatter.format(curDate),
 	     				 "1", "1",itemValue.getiUserId() , userServiceTermRoleData.getIuserServiceTermId()));
 	     	            
 	        	imageView.setVisibility(View.GONE);
 	        	selectLout.setVisibility(View.GONE);
				name.setTextColor(getResources().getColor(R.color.black));
				roleName.setTextColor(getResources().getColor(R.color.appcolor));
				
 	        	view.setBackgroundColor(Color.parseColor("#ffffff"));
 	            //Update logic Apply here
 	        }
       
            }
            }
            
            
           });
            
	    
	    
	    
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.latter_attendance, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
         if (id == R.id.done) {
        	 Intent i = new Intent(LatterAttendanceActivity.this ,TrainingPlus.class );
		     startActivity(i);
		     finish();
			return true;
		}else 
	    if (id == R.id.close) {
	    	DatabaseSupport db = new DatabaseSupport(getApplicationContext());
	    	TrainingTransaction tt = new TrainingTransaction();
	    	tt = db.getTrainingTrainsactionByDataId(trainingTransactionId).get(0);
	    	tt.setFlgDeleted("1");
	    	tt.setFlgIsDirty("1");
	    	db.UpdateTrainingTransactionRow(tt);
	    	db.updateTrainingAttendanceByTransactionid(tt.getTraningTransactionId());
	    	
	    	Intent i = new Intent(LatterAttendanceActivity.this ,TrainingPlus.class );
			startActivity(i);
			finish();
				return true;
			}
		return super.onOptionsItemSelected(item);
	}
}
