package com.arknfcpower;

import android.support.v7.app.ActionBarActivity;
import android.app.ActionBar;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;

public class AdminAction extends ActionBarActivity {

	Button registerNewCard;
	Button sync;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	    ActionBar ab = getActionBar(); 
        ColorDrawable colorDrawable = new ColorDrawable(Color.parseColor("#009bff"));     
        ab.setBackgroundDrawable(colorDrawable);
		/**
		 * work on 4.4.2
		 */
		//ab.setDisplayUseLogoEnabled(false);
		
		ab.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM | ActionBar.DISPLAY_SHOW_HOME | ActionBar.DISPLAY_SHOW_TITLE);
		View homeIcon = findViewById(
	            Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB ? 
	                    android.R.id.home : android.support.v7.appcompat.R.id.home);
	    ((View) homeIcon.getParent()).setVisibility(View.GONE);
	    ((View) homeIcon).setVisibility(View.GONE);
        
       /* ab.setDisplayOptions(ab.getDisplayOptions()
                | ActionBar.DISPLAY_SHOW_CUSTOM);
        ImageView imageView = new ImageView(ab.getThemedContext());
        imageView.setScaleType(ImageView.ScaleType.CENTER);
        imageView.setImageResource(R.drawable.nyk_logo);
        ActionBar.LayoutParams layoutParams = new ActionBar.LayoutParams(
                ActionBar.LayoutParams.WRAP_CONTENT,
                ActionBar.LayoutParams.WRAP_CONTENT, Gravity.RIGHT
                        | Gravity.CENTER_VERTICAL);
        layoutParams.rightMargin = 5;
        imageView.setLayoutParams(layoutParams);
        ab.setCustomView(imageView);
*/
        
		setContentView(R.layout.activity_admin_action);
	    
		registerNewCard  = (Button) findViewById( R.id.registerNewCard);
		sync  = (Button) findViewById( R.id.Sync);
		
		registerNewCard.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(AdminAction.this, UserCardRegistrationUi.class);
				startActivity(i);
				finish();
			}
		});
		
	}

/*	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.admin_action, menu);
		return true;
	}
*/
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		/*if (id == R.id.action_settings) {
			return true;
		}
		*/
		return super.onOptionsItemSelected(item);
	}
}
