package com.arknfcpower;

import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.net.Uri;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.provider.Settings;
import android.support.v7.app.ActionBarActivity;
import android.telephony.TelephonyManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.arknfc.adapter.SpinnerAdapter;
import com.arknfcpower.db.DatabaseSupport;
import com.arknfcpower.model.TraingAttendance;
import com.arknfcpower.model.TrainingTransaction;
import com.arknfcpower.model.UserMaster;
import com.arknfcpower.model.UserNfcData;
import com.arknfcpower.model.UserSelectItem;
import com.arknfcpower.model.UserServiceTermRoleData;

public class AttendanceActivity extends ActionBarActivity implements
		AnimationListener {
	Intent intent;
	String trainingTransactionId = "";
	String trainingDataId = "";
	String trainingTime = "";
	String trainingDate = "";
	TextView welcomeName;
	TextView welcomeTap;

	VideoView attendancevideo;

	ArrayList<UserSelectItem> userAttendanceList;

	ListView lv;
	// ImageView roundedImage;

	// Button attendanceCompleted;
	Handler handler;

	private static final DateFormat TIME_FORMAT = SimpleDateFormat
			.getDateTimeInstance();
	// private LinearLayout mTagContent;

	private SimpleDateFormat dateFormatter;

	private NfcAdapter mAdapter;
	private PendingIntent mPendingIntent;
	private NdefMessage mNdefPushMessage;
	long ids;
	private AlertDialog mDialog;

	Animation animRound;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		ActionBar ab = getActionBar();
		ColorDrawable colorDrawable = new ColorDrawable(
				Color.parseColor("#009bff"));
		ab.setBackgroundDrawable(colorDrawable);
		/**
		 * work on 4.4.2
		 */
		// ab.setDisplayUseLogoEnabled(false);

		ab.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM
				| ActionBar.DISPLAY_SHOW_HOME | ActionBar.DISPLAY_SHOW_TITLE);
		View homeIcon = findViewById(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB ? android.R.id.home
				: android.support.v7.appcompat.R.id.home);
		((View) homeIcon.getParent()).setVisibility(View.GONE);
		((View) homeIcon).setVisibility(View.GONE);

		/*
		 * ab.setDisplayOptions(ab.getDisplayOptions() |
		 * ActionBar.DISPLAY_SHOW_CUSTOM); ImageView imageView = new
		 * ImageView(ab.getThemedContext());
		 * imageView.setScaleType(ImageView.ScaleType.CENTER);
		 * imageView.setImageResource(R.drawable.nyk_logo);
		 * ActionBar.LayoutParams layoutParams = new ActionBar.LayoutParams(
		 * ActionBar.LayoutParams.WRAP_CONTENT,
		 * ActionBar.LayoutParams.WRAP_CONTENT, Gravity.RIGHT |
		 * Gravity.CENTER_VERTICAL); layoutParams.rightMargin = 5;
		 * imageView.setLayoutParams(layoutParams); ab.setCustomView(imageView);
		 */
		setContentView(R.layout.activity_attendance);
		dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
		intent = getIntent();
		trainingTransactionId = intent.getStringExtra("trainingTransactionId");
		trainingDataId = intent.getStringExtra("trainingDataId");
		trainingDate = intent.getStringExtra("trainingDate");
		trainingTime = intent.getStringExtra("trainingTime");
		welcomeName = (TextView) findViewById(R.id.textView2);
		welcomeTap = (TextView) findViewById(R.id.textView1);
		// attendanceCompleted = (Button) findViewById(R.id.saveAttendance);
		lv = (ListView) findViewById(R.id.attendanceUserList);
		userAttendanceList = new ArrayList<UserSelectItem>();
		List<TraingAttendance> trAttList = getUpdateAttandanceList();
		if (trAttList != null && trAttList.size() > 0) {
			SpinnerAdapter spin = new SpinnerAdapter(userAttendanceList,
					getApplicationContext());
			lv.setAdapter(spin);
			lv.setVisibility(View.VISIBLE);

		} else {
			lv.setVisibility(View.GONE);
		}

		animRound = AnimationUtils.loadAnimation(getApplicationContext(),
				R.drawable.rotate);
		// set animation listener
		animRound.setAnimationListener(this);
		// roundedImage = (ImageView) findViewById(R.id.imageView1);

		// roundedImage.setVisibility(View.VISIBLE);
		// start the animation
		// roundedImage.startAnimation(animRound);

		resolveIntent(getIntent());

		// Video View
		attendancevideo = (VideoView) findViewById(R.id.attendanceVideo);
		Uri UriPath = Uri.parse("android.resource://com.arknfcpower/"
				+ R.raw.animation_new);
		// MediaController controller;
		// controller = new MediaController(AttendanceActivity.this);
		// video.setMediaController(controller);
		attendancevideo.setVideoURI(UriPath);

		attendancevideo.requestFocus();
		attendancevideo.start();
		attendancevideo.setOnCompletionListener(new OnCompletionListener() {

			@Override
			public void onCompletion(MediaPlayer mp) {
				mp.seekTo(0);
				mp.start();

			}
		});

		mDialog = new AlertDialog.Builder(this).setNeutralButton("Ok", null)
				.create();

		mAdapter = NfcAdapter.getDefaultAdapter(this);
		if (mAdapter == null) {
			showMessage(R.string.error, R.string.no_nfc);
			finish();
			return;
		}

		mPendingIntent = PendingIntent.getActivity(this, 0, new Intent(this,
				getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
		mNdefPushMessage = new NdefMessage(new NdefRecord[] { newTextRecord(
				"Message from NFC Reader :-)", Locale.ENGLISH, true) });

		/*
		 * attendanceCompleted.setOnClickListener(new OnClickListener() {
		 * 
		 * @Override public void onClick(View v) { // TODO Auto-generated method
		 * stub DatabaseSupport db =new
		 * DatabaseSupport(getApplicationContext());
		 * 
		 * UserMaster um = new UserMaster(); um =
		 * db.getUserMasterSingleRow(CardInfo
		 * .getUserId(getApplicationContext())).get(0); String username =
		 * um.getStrFirstName() +" "+ um.getStrLastName();
		 * 
		 * Intent i = new Intent(AttendanceActivity.this
		 * ,WelcomeTrainingPlus.class ); i.putExtra("name", username);
		 * startActivity(i); finish(); } });
		 */

	}

	private void showMessage(int title, int message) {
		mDialog.setTitle(title);
		mDialog.setMessage(getText(message));
		mDialog.show();
	}

	private NdefRecord newTextRecord(String text, Locale locale,
			boolean encodeInUtf8) {
		byte[] langBytes = locale.getLanguage().getBytes(
				Charset.forName("US-ASCII"));

		Charset utfEncoding = encodeInUtf8 ? Charset.forName("UTF-8") : Charset
				.forName("UTF-16");
		byte[] textBytes = text.getBytes(utfEncoding);

		int utfBit = encodeInUtf8 ? 0 : (1 << 7);
		char status = (char) (utfBit + langBytes.length);

		byte[] data = new byte[1 + langBytes.length + textBytes.length];
		data[0] = (byte) status;
		System.arraycopy(langBytes, 0, data, 1, langBytes.length);
		System.arraycopy(textBytes, 0, data, 1 + langBytes.length,
				textBytes.length);

		return new NdefRecord(NdefRecord.TNF_WELL_KNOWN, NdefRecord.RTD_TEXT,
				new byte[0], data);
	}

	@Override
	protected void onResume() {
		super.onResume();
		if (mAdapter != null) {
			if (!mAdapter.isEnabled()) {
				showWirelessSettingsDialog();
			}
			mAdapter.enableForegroundDispatch(this, mPendingIntent, null, null);
			mAdapter.enableForegroundNdefPush(this, mNdefPushMessage);
		}
	}

	@Override
	protected void onPause() {
		super.onPause();
		if (mAdapter != null) {
			mAdapter.disableForegroundDispatch(this);
			mAdapter.disableForegroundNdefPush(this);
		}
	}

	private void showWirelessSettingsDialog() {
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(R.string.nfc_disabled);
		builder.setPositiveButton(android.R.string.ok,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialogInterface, int i) {
						Intent intent = new Intent(
								Settings.ACTION_WIRELESS_SETTINGS);
						startActivity(intent);
					}
				});
		builder.setNegativeButton(android.R.string.cancel,
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialogInterface, int i) {
						finish();
					}
				});
		builder.create().show();
		return;
	}
	
	private void makeEntryInUsrNFC(String userId, String rank, String cardType) {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		Date curDate = new Date();
		DatabaseSupport db = new DatabaseSupport(this);
		
		List<UserServiceTermRoleData> serviceTermRoleData = new ArrayList<UserServiceTermRoleData>();
		serviceTermRoleData = db.getUserServiceTermRoleRowBiId(
				userId,
				df.format(curDate));
		if (serviceTermRoleData != null
				&& serviceTermRoleData.size() > 0) {
		
			String roleName = db
					.getRoleName(serviceTermRoleData.get(0)
							.getiRoleId());
			
			if(userId.equalsIgnoreCase(serviceTermRoleData.get(0).getIuserId()) && rank.equalsIgnoreCase(roleName)){
					Integer cardTypeOnCard = null;
					Integer cardTypeOnDB = serviceTermRoleData.get(0).getField2();
					try {
						cardTypeOnCard = Integer
								.parseInt(rank);
					} catch (Exception e) {
						e.printStackTrace();
					}
					
					if(cardTypeOnCard == null ||  cardTypeOnDB== null || cardTypeOnCard.intValue() == cardTypeOnDB.intValue()){
						
						if(CardInfo.getShipId(getApplicationContext()) == null || CardInfo.getShipId(getApplicationContext()).equalsIgnoreCase("1")){
							CardInfo.setShipId(getApplicationContext(), serviceTermRoleData.get(0).getiShipId());
							CardInfo.setTenantId(getApplicationContext(), serviceTermRoleData.get(0).getiTenantId());
						}
						
						
							
						/*db.addUserNFCRow(new UserNfcData(CardInfo
								.getId(getApplicationContext()), "User", userId, "S1", "1",
								"0", df.format(curDate), df.format(curDate), CardInfo
										.getTenantId(getApplicationContext()), CardInfo
										.getShipId(getApplicationContext())));*/
						/**
						 * Above code comment due to card id use as primary key which is duplicate.
						 * So change pk format and cardid use in iuserservicetermid 
						 */
						String pk = CardInfo.getId(getApplicationContext())+"_"+curDate.getTime();
						db.addUserNFCRow(new UserNfcData(pk, "User", userId, CardInfo
								.getId(getApplicationContext()), "1",
								"0", df.format(curDate), df.format(curDate), CardInfo
										.getTenantId(getApplicationContext()), CardInfo
										.getShipId(getApplicationContext())));
					}
			}
		}
	}
	
	private void resolveIntent(Intent intent) {
		String action = intent.getAction();
		if (NfcAdapter.ACTION_TAG_DISCOVERED.equals(action)
				|| NfcAdapter.ACTION_TECH_DISCOVERED.equals(action)
				|| NfcAdapter.ACTION_NDEF_DISCOVERED.equals(action)) {
			Parcelable[] rawMsgs = intent
					.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
			NdefMessage[] msgs;
			if (rawMsgs != null) {
				// IF TAG IS NOT NULL |
				Parcelable tag = intent
						.getParcelableExtra(NfcAdapter.EXTRA_TAG);
				Tag tag1 = (Tag) tag;
				byte[] id = tag1.getId();
				ids = getDec(id);
				CardInfo.setId(getApplicationContext(), String.valueOf(ids));
				// Toast.makeText(getApplicationContext(), "ID :" + ids,
				// Toast.LENGTH_LONG).show();
				msgs = new NdefMessage[rawMsgs.length];
				for (int i = 0; i < rawMsgs.length; i++) {

					msgs[i] = (NdefMessage) rawMsgs[i];
				}

				String m = getMessage(msgs);
				if(m != null && m.startsWith("en")){
					//m = m.substring(3, m.length()-1);
			    	m = m.substring(3, m.length());
			    }
				
				
				
				String strRoleNameArr[] = null;
				Integer cardTypeOnCard = null;
				String userIdOnCard = "";
				if(m != null && !"".equals(m)){
					strRoleNameArr = m.split("#");
					userIdOnCard = strRoleNameArr[0];
				}
				
				
				DatabaseSupport db = new DatabaseSupport(this);
				ArrayList<UserNfcData> data = db.getUserNFCSingleRowByUserId(userIdOnCard);
				
				if (data != null && data.size() > 0) {
 					if (strRoleNameArr!= null && strRoleNameArr.length >= 3) {
 						makeEntryInUsrNFC(strRoleNameArr[0],strRoleNameArr[1],strRoleNameArr[2]);
 					}
 					data = db.getUserNFCSingleRow(String
 							.valueOf(ids));
 				}

				if (userIdOnCard == null || "".equals(userIdOnCard)) {

				} else {
					
					Date curDate = new Date();
					DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
					List<UserServiceTermRoleData> serviceTermRoleData = new ArrayList<UserServiceTermRoleData>();
					serviceTermRoleData = db.getUserServiceTermRoleRowBiId(userIdOnCard,format.format(curDate));
					if (serviceTermRoleData != null
							&& serviceTermRoleData.size() > 0) {
					
						
						String roleName = db
								.getRoleName(serviceTermRoleData.get(0)
										.getiRoleId());
						
						String userIdInDB = serviceTermRoleData.get(0)
								.getIuserId();
						//String userIdOnCard = strRoleNameArr[0];

						if (roleName != null && strRoleNameArr != null
								&& strRoleNameArr.length >= 3) {
							

							if (userIdInDB.equals(userIdOnCard)
									&& roleName.trim().equalsIgnoreCase(
											strRoleNameArr[1].trim())) {				
					
					
								String uid = "";
								String userServiceTermId = "";
								for (int i = 0; i < serviceTermRoleData.size(); i++) {
									uid = serviceTermRoleData.get(i).getIuserId();
									userServiceTermId = serviceTermRoleData.get(i).getIuserServiceTermId();
									System.out.println("Uid :" + uid);
								}
								
								TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
								String traningAttendanceId = tm.getDeviceId() + "_"
										+ curDate.getTime();
			
								if (db.getTrainingAttendanceByTransactionAndUserId(
										trainingTransactionId, uid, "0").size() == 0)
			
								{
			
									db.addTrainingAttendanceRow(new TraingAttendance(
											traningAttendanceId, trainingTransactionId,
											trainingDataId, "parentId", trainingDate,
											trainingTime, "desc", "0", CardInfo.getShipId(getApplicationContext()), CardInfo.getTenantId(getApplicationContext()),
											dateFormatter.format(curDate), dateFormatter
													.format(curDate), "0", "1", uid,
											userServiceTermId));
			
									UserMaster um = new UserMaster();
									um = db.getUserMasterSingleRow(uid).get(0);
									welcomeName.setVisibility(View.VISIBLE);
									// welcomeTap.setVisibility(View.GONE);
									// roundedImage.setVisibility(View.GONE);
			
									welcomeName.setText("Welcome " + um.getStrFirstName()
											+ " " + um.getStrLastName());
			
									String roleid = um.getiRoleId();
									String name = um.getStrFirstName() + " "
											+ um.getStrLastName() + " / "
											+ db.getRoleName(roleid);
									userAttendanceList.add(0,new UserSelectItem(name, um
											.getiUserId(), CardInfo.getTenantId(getApplicationContext())));
			
									// ArrayAdapter<UserSelectItem> myAdapter = new
									// ArrayAdapter<UserSelectItem>(getActivity(),
									// android.R.layout.simple_spinner_item, userlist);
			
									SpinnerAdapter spin = new SpinnerAdapter(
											userAttendanceList, getApplicationContext());
									lv.setAdapter(spin);
			
									handler = new Handler();
									Runnable runnable = new Runnable() {
										int i = 0;
			
										public void run() {
											welcomeName.setVisibility(View.GONE);
											// welcomeTap.setVisibility(View.VISIBLE);
											lv.setVisibility(View.VISIBLE);
											// roundedImage.setVisibility(View.VISIBLE);
			
										}
									};
									handler.postDelayed(runnable, 3000);
			
								} else {
			
									UserMaster um = new UserMaster();
									um = db.getUserMasterSingleRow(uid).get(0);
									welcomeName.setVisibility(View.VISIBLE);
									// welcomeTap.setVisibility(View.GONE);
									// roundedImage.setVisibility(View.GONE);
			
									welcomeName.setText(um.getStrFirstName() + " "
											+ um.getStrLastName()
											+ " Your attendance is already marked. ");
									handler = new Handler();
									Runnable runnable = new Runnable() {
										int i = 0;
			
										public void run() {
											welcomeName.setVisibility(View.GONE);
											// welcomeTap.setVisibility(View.VISIBLE);
											// roundedImage.setVisibility(View.VISIBLE);
			
										}
									};
									handler.postDelayed(runnable, 3000);
			
								}
							 }
							 else{
								 Toast.makeText(getApplicationContext(), "You are not authorized for attendance.", Toast.LENGTH_LONG).show();
							 }
						 }
						 else{
							 Toast.makeText(getApplicationContext(), "You are not authorized for attendance.", Toast.LENGTH_LONG).show();
						 }
					}
				}

				
				// Toast.makeText(getApplicationContext(), "CardInfo :"
				// +CardInfo.getAdminId(getApplicationContext()) ,
				// Toast.LENGTH_LONG).show();
				System.out.println("Card Value : " + m);

			} else {
				// IF TAG IS NULL
				// Unknown tag type

				Toast.makeText(getApplicationContext(), "You are not authorize for attendance.", Toast.LENGTH_LONG).show();
				
			/*	byte[] empty = new byte[0];
				byte[] id = intent.getByteArrayExtra(NfcAdapter.EXTRA_ID);
				Parcelable tag = intent
						.getParcelableExtra(NfcAdapter.EXTRA_TAG);
				Tag tag1 = (Tag) tag;
				byte[] ida = tag1.getId();
				ids = getDec(ida);*/
				

				/*Intent i = new Intent(AttendanceActivity.this,
						CreateAdminActivity.class);
				startActivity(i);
				finish();*/
				/*
				 * byte[] payload = dumpTagData(tag).getBytes(); NdefRecord
				 * record = new NdefRecord(NdefRecord.TNF_UNKNOWN, empty, id,
				 * payload); NdefMessage msg = new NdefMessage(new NdefRecord[]
				 * { record }); msgs = new NdefMessage[] { msg };
				 */
			}
			// Setup the views
			// buildTagViews(msgs);
		}
	}

	private String getMessage(NdefMessage[] msgs) {
		// TODO Auto-generated method stub
		String tags = "";
		if (msgs == null || msgs.length == 0) {
			return "";
		}

		NdefRecord[] records = msgs[0].getRecords();
		for (final NdefRecord record : records) {
			tags = new String(record.getPayload());
		}
		return tags.toString();
	}

	private long getDec(byte[] bytes) {
		long result = 0;
		long factor = 1;
		for (int i = 0; i < bytes.length; ++i) {
			long value = bytes[i] & 0xffl;
			result += value * factor;
			factor *= 256l;
		}
		return result;
	}

	private List<TraingAttendance> getUpdateAttandanceList() {
		List<TraingAttendance> taList = new ArrayList<TraingAttendance>();
		DatabaseSupport db = new DatabaseSupport(getApplicationContext());
		taList = db.getTrainingAttendanceByTransactionId(trainingTransactionId,
				"0");

		if (taList != null && taList.size() > 0) {
			for (TraingAttendance ta : taList) {
				UserMaster um = new UserMaster();
				um = db.getUserMasterSingleRow(ta.getiUserId()).get(0);

				String roleid = um.getiRoleId();
				String name = um.getStrFirstName() + " " + um.getStrLastName()
						+ " / " + db.getRoleName(roleid);
				userAttendanceList.add(new UserSelectItem(name,
						um.getiUserId(), CardInfo.getTenantId(getApplicationContext())));

			}
		}

		return taList;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.attendance, menu);
		return true;
	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
	}

	@Override
	public void onNewIntent(Intent intent2) {
		setIntent(intent2);
		resolveIntent(intent2);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.done) {

			DatabaseSupport db = new DatabaseSupport(getApplicationContext());

			UserMaster um = new UserMaster();
			um = db.getUserMasterSingleRow(
					CardInfo.getUserId(getApplicationContext())).get(0);
			String username = um.getStrFirstName() + " " + um.getStrLastName();

			Intent i = new Intent(AttendanceActivity.this, TrainingPlus.class);
			i.putExtra("name", username);
			startActivity(i);
			finish();

			return true;
		} else if (id == R.id.close) {

			DatabaseSupport db = new DatabaseSupport(getApplicationContext());
			TrainingTransaction tt = new TrainingTransaction();
			tt = db.getTrainingTrainsactionByDataId(trainingTransactionId).get(
					0);
			tt.setFlgDeleted("1");
			tt.setFlgIsDirty("1");
			db.UpdateTrainingTransactionRow(tt);
			db.updateTrainingAttendanceByTransactionid(tt.getTraningTransactionId());

			Intent i = new Intent(AttendanceActivity.this, TrainingPlus.class);
			startActivity(i);
			finish();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onAnimationStart(Animation animation) {
		// TODO Auto-generated method stub

		if (animation == animRound) {
			/*
			 * Toast.makeText(getApplicationContext(), "Animation Stopped",
			 * Toast.LENGTH_SHORT).show();
			 */}

	}

	@Override
	public void onAnimationEnd(Animation animation) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onAnimationRepeat(Animation animation) {
		// TODO Auto-generated method stub

	}
}
