package com.arknfcpower;

import java.util.ArrayList;
import java.util.Date;

import com.arknfc.adapter.PendingAttendanceAdapter;
import com.arknfc.adapter.ScoreAdapter;
import com.arknfcpower.db.DatabaseSupport;
import com.arknfcpower.model.PowerPlusTask;
import com.arknfcpower.model.PowerPlusTransaction;
import com.arknfcpower.model.TraingAttendance;
import com.arknfcpower.model.UserMaster;

import android.support.v7.app.ActionBarActivity;
import android.telephony.TelephonyManager;
import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

public class PendingAttendance extends ActionBarActivity {

	ListView lv;
	ImageView imageView;
//	Button done;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	    ActionBar ab = getActionBar(); 
        ColorDrawable colorDrawable = new ColorDrawable(Color.parseColor("#009bff"));     
        ab.setBackgroundDrawable(colorDrawable);
		/**
		 * work on 4.4.2
		 */
		//ab.setDisplayUseLogoEnabled(false);
		
		ab.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM | ActionBar.DISPLAY_SHOW_HOME | ActionBar.DISPLAY_SHOW_TITLE);
		View homeIcon = findViewById(
	            Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB ? 
	                    android.R.id.home : android.support.v7.appcompat.R.id.home);
	    ((View) homeIcon.getParent()).setVisibility(View.GONE);
	    ((View) homeIcon).setVisibility(View.GONE);
   /*     ab.setDisplayOptions(ab.getDisplayOptions()
                | ActionBar.DISPLAY_SHOW_CUSTOM);
        ImageView imageView = new ImageView(ab.getThemedContext());
        imageView.setScaleType(ImageView.ScaleType.CENTER);
        imageView.setImageResource(R.drawable.nyk_logo);
        ActionBar.LayoutParams layoutParams = new ActionBar.LayoutParams(
                ActionBar.LayoutParams.WRAP_CONTENT,
                ActionBar.LayoutParams.WRAP_CONTENT, Gravity.RIGHT
                        | Gravity.CENTER_VERTICAL);
        layoutParams.rightMargin = 5;
        imageView.setLayoutParams(layoutParams);
        ab.setCustomView(imageView);
*/

		setContentView(R.layout.activity_pending_attendance);
		DatabaseSupport db = new DatabaseSupport(getApplicationContext());
		ArrayList<TraingAttendance>  attendanceList = db.getTrainingAttendanceByUserIdAndStatus(CardInfo.getUserId(getApplicationContext()), "1");
		
		   lv = (ListView) findViewById(R.id.pendingList);
	//	   done = (Button) findViewById(R.id.done);
	       PendingAttendanceAdapter adapter = new PendingAttendanceAdapter(attendanceList,getApplicationContext());
		   lv.setAdapter(adapter);  
		   db.close();
		   
	/*	   done.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(PendingAttendance.this ,WelcomeTrainingPlus.class );
				startActivity(i);
				finish();
			}
		});
	*/	   lv.setOnItemClickListener(new OnItemClickListener() {
		    	 
	            @Override
	            public void onItemClick(AdapterView<?> parent, View view,
	               int position, long id) {
	              
	             // ListView Clicked item index
	             int itemPosition     = position;
    
				 imageView=((ImageView)view.findViewById(R.id.select_padding_icon));
	             
	             DatabaseSupport db = new DatabaseSupport(getApplicationContext());  
	             
	             // ListView Clicked item value
	             TraingAttendance  itemValue    =  (TraingAttendance) lv.getItemAtPosition(position);
	             
	 			Date curDate = new Date();
	             
	             if(db.getTrainingAttendanceByTransactionAndUserId(itemValue.getTraningTransactionId(),itemValue.getiUserId() , "1").size() > 0)
	 	        {
	            	 
	            	 itemValue.setFlgStatus("0");
	            	 db.updateTrainingAttendanceRow(itemValue);
						imageView.setVisibility(View.VISIBLE);
						imageView.setImageResource(R.drawable.select_icon);
						imageView.invalidate();
						
	 		          view.setBackgroundColor(Color.parseColor("#06dc1a"));
	 	        
	 	        }else
	 	        {
	            	 itemValue.setFlgStatus("1");
	            	 db.updateTrainingAttendanceRow(itemValue);
	 	        	 imageView.setVisibility(View.GONE);

	 	             view.setBackgroundColor(Color.parseColor("#ffffff"));
	 	            //Update logic Apply here
	 	        }
	      
	            }
	           });
		
		
		
	}

/*	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.pending_attendance, menu);
		return true;
	}
*/
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
/*		if (id == R.id.action_settings) {
			return true;
		}
*/		return super.onOptionsItemSelected(item);
	}
}
