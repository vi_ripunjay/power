package com.arknfcpower;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ActionBar;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.arknfc.adapter.NavDrawerListAdapter;
import com.arknfc.adapter.TrainingAttendanceFragment;
import com.arknfcpower.model.NavDrawerItem;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
@SuppressLint("NewApi")
public class AdminMainActivity extends ActionBarActivity{
	private DrawerLayout mDrawerLayout;
	private ListView mDrawerList;
	private ActionBarDrawerToggle mDrawerToggle;

	// nav drawer title
	private CharSequence mDrawerTitle;

	// used to store app title
	private CharSequence mTitle;

	ProgressBar progressbar;
	EditText userName;
	EditText userPass;
	TextView errortext = null;

	// slide menu items
	private String[] navMenuTitles;
	private TypedArray navMenuIcons;
	private ArrayList<NavDrawerItem> navDrawerItems;
	private NavDrawerListAdapter adapter;

	private int pos;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		ActionBar ab = getActionBar();
		ColorDrawable colorDrawable = new ColorDrawable(
				Color.parseColor("#009bff"));
		ab.setBackgroundDrawable(colorDrawable);
		/**
		 * work on 4.4.2
		 */
		//ab.setDisplayUseLogoEnabled(false);
		
		ab.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM | ActionBar.DISPLAY_SHOW_HOME | ActionBar.DISPLAY_SHOW_TITLE);
		View homeIcon = findViewById(
	            Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB ? 
	                    android.R.id.home : android.support.v7.appcompat.R.id.home);
	    ((View) homeIcon.getParent()).setVisibility(View.GONE);
	    ((View) homeIcon).setVisibility(View.GONE);
	    
	    /**
		 * below code for hide the icon.
		 */
		ab.setIcon(new ColorDrawable(getResources().getColor(android.R.color.transparent)));
		/*
		 * ab.setDisplayOptions(ab.getDisplayOptions() |
		 * ActionBar.DISPLAY_SHOW_CUSTOM); ImageView imageView = new
		 * ImageView(ab.getThemedContext());
		 * imageView.setScaleType(ImageView.ScaleType.CENTER);
		 * imageView.setImageResource(R.drawable.nyk_logo);
		 * ActionBar.LayoutParams layoutParams = new ActionBar.LayoutParams(
		 * ActionBar.LayoutParams.WRAP_CONTENT,
		 * ActionBar.LayoutParams.WRAP_CONTENT, Gravity.RIGHT |
		 * Gravity.CENTER_VERTICAL); layoutParams.rightMargin = 5;
		 * imageView.setLayoutParams(layoutParams); ab.setCustomView(imageView);
		 */
		setContentView(R.layout.activity_admin_main);

		mTitle = mDrawerTitle = getTitle();

		mTitle = mDrawerTitle = getTitle();
//		aController = new Controller();
/*			registerReceiver(mHandleMessageReceiver, new IntentFilter(
				Config.DISPLAY_REGISTRATION_MESSAGE_ACTION));
*/
		navMenuIcons = getResources().obtainTypedArray(R.array.admin_nav_training_icons);
		// load slide menu items
		navMenuTitles = getResources().getStringArray(R.array.admin_nav_drawer_items);

		// nav drawer icons from resources

		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerList = (ListView) findViewById(R.id.list_slidermenu);

		navDrawerItems = new ArrayList<NavDrawerItem>();

		// adding nav drawer items to array
		// Home
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[0], navMenuIcons.getResourceId(0, -1)));
		// Find People
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[1], navMenuIcons.getResourceId(1, -1)));
		// Photos
		navDrawerItems.add(new NavDrawerItem(navMenuTitles[2], navMenuIcons.getResourceId(2, -1)));

		navDrawerItems.add(new NavDrawerItem(navMenuTitles[3], navMenuIcons.getResourceId(3, -1)));
		
		navMenuIcons.recycle();

		mDrawerList.setOnItemClickListener(new SlideMenuClickListener());

		// setting the nav drawer list adapter
		adapter = new NavDrawerListAdapter(getApplicationContext(),
				navDrawerItems);
		mDrawerList.setAdapter(adapter);

		// enabling action bar app icon and behaving it as toggle button
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setHomeButtonEnabled(true);
		
		mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
				R.drawable.ic_drawer, // nav menu toggle icon
				R.string.app_name, // nav drawer open - description for
									// accessibility
				R.string.app_name // nav drawer close - description for
									// accessibility
		) {
			public void onDrawerClosed(View view) {
				getActionBar().setTitle(mTitle);
				// calling onPrepareOptionsMenu() to show action bar icons
				invalidateOptionsMenu();
			}

			public void onDrawerOpened(View drawerView) {
				getActionBar().setTitle(mDrawerTitle);
				// calling onPrepareOptionsMenu() to hide action bar icons
				invalidateOptionsMenu();
			}
		};
		mDrawerLayout.setDrawerListener(mDrawerToggle);

		if (savedInstanceState == null) {
			// on first time display view for first nav item
			displayView(0);
		}
	}

	/**
	 * Slide menu item click listener
	 * */
	private class SlideMenuClickListener implements
			ListView.OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			// display view for selected nav drawer item
			displayView(position);

		}
	}

	/*
	 * @Override public boolean onCreateOptionsMenu(Menu menu) {
	 * getMenuInflater().inflate(R.menu.home, menu); return true; }
	 */@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// toggle nav drawer on selecting action bar app icon/title
		if (mDrawerToggle.onOptionsItemSelected(item)) {
			return true;
		}
		/*
		 * // Handle action bar actions click switch (item.getItemId()) { case
		 * R.id.action_logout:
		 * 
		 * 
		 * return true; case R.id.action_login:
		 * 
		 * return true; default: return super.onOptionsItemSelected(item); }
		 */return true;

	}

	/**
	 * Diplaying fragment view for selected nav drawer list item
	 * */
	@SuppressLint("NewApi")
	public void displayView(int position) {
		// update the main content by replacing fragments

		Fragment fragment = null;
			switch (position) {
			case 0:
				fragment = new AdminRegisterTabFragment(0);
				break;
			case 1:
				fragment = new AdminRegisterTabFragment(1);
				break;
			case 2:
				fragment = new AdminRegisterTabFragment(2);
				break;
			case 3:
				fragment = new AdminExit();
				break;

			default:
				break;
			}
		

		if (fragment != null) {
			try {
				//set to handle back Button
				FragmentManager fragmentManager = getSupportFragmentManager();
				
				while (fragmentManager.getBackStackEntryCount() > 1) {
		                fragmentManager.popBackStackImmediate();
		        }
				  
				fragmentManager.beginTransaction()
						.replace(R.id.frame_container, fragment).addToBackStack(null).commitAllowingStateLoss();

				// update selected item and title, then close the drawer
				mDrawerList.setItemChecked(position, true);
				mDrawerList.setSelection(position);
				if (position <2)
				{
				setTitle("Admin Action");
				}else if (position >1)
				{
					setTitle(navMenuTitles[position]);
						
				}
				
				//		setTitle(navMenuTitles[0]);
				
				mDrawerLayout.closeDrawer(mDrawerList);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				Log.e("Home Fragment : ", e.toString());

			}
		} else {
			// error in creating fragment
			Log.e("MainActivity", "Error in creating fragment");
		}
		
	}

	@Override
	public void setTitle(CharSequence title) {
		mTitle = title;
		getActionBar().setTitle(mTitle);
	}

	/**
	 * When using the ActionBarDrawerToggle, you must call it during
	 * onPostCreate() and onConfigurationChanged()...
	 */

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		// Sync the toggle state after onRestoreInstanceState has occurred.
		mDrawerToggle.syncState();
	}

	private int backFlag = 0;

	@Override
	public void onBackPressed() {
		//super.onBackPressed();	
		//this.finish();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		// Pass any configuration change to the drawer toggls
		mDrawerToggle.onConfigurationChanged(newConfig);
	}

}