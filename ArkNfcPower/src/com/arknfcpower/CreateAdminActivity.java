package com.arknfcpower;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.nfc.FormatException;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.arknfcpower.db.DatabaseSupport;
import com.arknfcpower.model.UserNfcData;

public class CreateAdminActivity extends ActionBarActivity {

	Button yes; 
	Button no;
	
	NfcAdapter adapter;
	PendingIntent pendingIntent;
	IntentFilter writeTagFilters[];
	boolean writeMode;
	Tag mytag;
	Context ctx;
	
	LinearLayout notRegisterLayOut;
	LinearLayout registerLayOut;
	AlertDialog alertDialog ;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
	    ActionBar ab = getActionBar(); 
        ColorDrawable colorDrawable = new ColorDrawable(Color.parseColor("#009bff"));     
        ab.setBackgroundDrawable(colorDrawable);
        ab.setTitle(" Create Admin Card");
		/**
		 * work on 4.4.2
		 */
		//ab.setDisplayUseLogoEnabled(false);
		
		ab.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM | ActionBar.DISPLAY_SHOW_HOME | ActionBar.DISPLAY_SHOW_TITLE);
		View homeIcon = findViewById(
	            Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB ? 
	                    android.R.id.home : android.support.v7.appcompat.R.id.home);
	    ((View) homeIcon.getParent()).setVisibility(View.GONE);
	    ((View) homeIcon).setVisibility(View.GONE);
	    
	    /**
		 * below code for hide the icon.
		 */
		ab.setIcon(new ColorDrawable(getResources().getColor(android.R.color.transparent)));
 

		setContentView(R.layout.activity_create_admin);
		ctx=this;
	    yes = (Button) findViewById(R.id.Yes);
	    yes.setBackgroundColor(Color.WHITE);
	    no = (Button) findViewById(R.id.No);
	    no.setBackgroundColor(Color.WHITE);

	    
	    
		DatabaseSupport db = new DatabaseSupport(getApplicationContext());
    	List<UserNfcData> nfcDataList = new ArrayList<UserNfcData>();
    	nfcDataList = db.getUserNFCRow();
    	if(nfcDataList != null && nfcDataList.size() > 0)
    	{
    		     		
    		
			Intent i = new Intent(this, AuthActivity.class);
			i.putExtra("userId", "userid");
			i.putExtra("nameAndRole", "");
			i.putExtra("strAuthMessagePrv", "");
			i.putExtra("strAuthMessage", "You are not authorized to view this screen. Please login with a different card to access this screen.");
			startActivity(i);
			finish();
    		
    	}
	    	    
	    yes.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				try {
					
					if(mytag==null){
						Toast.makeText(ctx, ctx.getString(R.string.error_detected), Toast.LENGTH_LONG ).show();
					}else{
						
						if("".equals(CardInfo.getUserId(getApplicationContext())))
						{
							String message  = "ShipAdmin";
							String msg = "Admin";
							write(message,mytag);
							DatabaseSupport db = new DatabaseSupport(ctx);
							String tenantId = "1";
	          			    String shipId = "1";
	          			    
	          			    Date date  = new Date();
	          			   DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	          			   
	          			   /**
							 * Above code comment due to card id use as primary key which is duplicate.
							 * So change pk format and cardid use in iuserservicetermid 
							 */
							String pk = CardInfo.getId(getApplicationContext())+"_"+date.getTime();
							
	          			    db.addUserNFCRow(new UserNfcData(pk, "User","ShipAdmin",
	          			    		String.valueOf(CardInfo.getId(ctx)), "0", "0",dateFormat.format(date),
	          					dateFormat.format(date), tenantId, shipId));
	          			    
							//Toast.makeText(ctx, ctx.getString(R.string.ok_writing) + msg + ctx.getString(R.string.ok_writing1), Toast.LENGTH_LONG ).show();
							
							String dialogTitle = "Admin Card Registered";
							String confirmMsg = "Your admin card registration has been done. Please use this card to login to the admin section, synchronize this device with The Ark system and then initialize the cards for all the crew members on board.";
							showAlertDialogBox(dialogTitle, confirmMsg, true);
							
						}else{
							
							String msg = "";
							String dialogTitle = "Registration Error";
							msg = "This card is already registered";
							msg = msg + " and cannot be reused for another person. ";
							msg = msg + " Please use a fresh card to register.";
							
							
							showAlertDialogBox(dialogTitle, msg, false);
							
						}
				
					}
				} catch (IOException e) {
					Toast.makeText(ctx, ctx.getString(R.string.error_writing), Toast.LENGTH_LONG ).show();
					e.printStackTrace();
				} catch (FormatException e) {
					Toast.makeText(ctx, ctx.getString(R.string.error_writing) , Toast.LENGTH_LONG ).show();
					e.printStackTrace();
				}
			}
		});
	     no.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(CreateAdminActivity.this , WelcomeAdmin.class);
				startActivity(i);
				finish();
			}
		});
	     
	    adapter = NfcAdapter.getDefaultAdapter(this);
		pendingIntent = PendingIntent.getActivity(this, 0, new Intent(this, getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
		IntentFilter tagDetected = new IntentFilter(NfcAdapter.ACTION_TAG_DISCOVERED);
		tagDetected.addCategory(Intent.CATEGORY_DEFAULT);
		writeTagFilters = new IntentFilter[] { tagDetected };

	
	}
	
	private void showAlertDialogBox(final String dialogTitle, final String msg, final boolean registered) {
		// TODO Auto-generated method stub
		
		alertDialog = new AlertDialog.Builder(this)
	    .setTitle(dialogTitle)
	    .setMessage(msg)
	    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
	        public void onClick(DialogInterface dialog, int which) { 
	            // continue with delete
	        	alertDialog.dismiss();
	        	if(registered){
	        		Intent i = new Intent(CreateAdminActivity.this , WelcomeAdmin.class);
					startActivity(i);
					finish();
	        	}
	        }
	     }).show();
	}
	
	private void write(String text, Tag tag) throws IOException, FormatException {

		NdefRecord[] records = { createRecord(text) };
		NdefMessage  message = new NdefMessage(records);
		// Get an instance of Ndef for the tag.
		Ndef ndef = Ndef.get(tag);
		// Enable I/O
		ndef.connect();
		// Write the message
		ndef.writeNdefMessage(message);		
		//Make read only
		if(ndef.canMakeReadOnly()){
			//Log.i("TheArkNFCCARD", "writing NFC :" + ndef.canMakeReadOnly());
			ndef.makeReadOnly();
		}
		
		// Close the connection
		ndef.close();
	}



	private NdefRecord createRecord(String text) throws UnsupportedEncodingException {
	//	String lang       = "en";
		byte[] textBytes  = text.getBytes();
/*		byte[] langBytes  = lang.getBytes("US-ASCII");
		int    langLength = langBytes.length;
		int    textLength = textBytes.length;
		byte[] payload    = new byte[1 + langLength + textLength];

		// set status byte (see NDEF spec for actual bits)
		payload[0] = (byte) langLength;

		// copy langbytes and textbytes into payload
		System.arraycopy(langBytes, 0, payload, 1,              langLength);
		System.arraycopy(textBytes, 0, payload, 1 + langLength, textLength);

*/		NdefRecord recordNFC = new NdefRecord(NdefRecord.TNF_WELL_KNOWN,  NdefRecord.RTD_TEXT,  new byte[0], textBytes);

		return recordNFC;
	}

	private String getMessage(NdefMessage[] msgs) {
		// TODO Auto-generated method stub
		String tags = "";
		if (msgs == null || msgs.length == 0) {
			return "";
		}

		NdefRecord[] records = msgs[0].getRecords();
		for (final NdefRecord record : records) {
			tags = new String(record.getPayload());
		}
		return tags.toString();
	}
	
	@Override
	protected void onNewIntent(Intent intent){
		Log.i("TheArkNFCCARD", "On New Intent");
		CardInfo.setUserId(getApplicationContext(),"");
		if(NfcAdapter.ACTION_TAG_DISCOVERED.equals(intent.getAction())){
			mytag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);    
			 byte[] id = mytag.getId();
             long ids = getDec(id);
             String i = String.valueOf(ids);
             CardInfo.setId(ctx, i);
             NdefMessage[] msgs;
            Parcelable[] rawMsgs = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
 			if (rawMsgs != null) {
             msgs = new NdefMessage[rawMsgs.length];
				for (int x = 0; x < rawMsgs.length; x++) {

					msgs[x] = (NdefMessage) rawMsgs[x];
				}
				String m = getMessage(msgs);
				Log.i("TheArkNFCCARD", "Message" + m);
				
				if(m != null && m.startsWith("en")){
			    	//m = m.substring(3, m.length()-1);
					m = m.substring(3, m.length());
			    }
				
				
				
				String strRoleNameArr[] = null;
				Integer cardTypeOnCard = null;
				String userIdOnCard = "";
				if(m != null && !"".equals(m)){
					strRoleNameArr = m.split("#");
					userIdOnCard = strRoleNameArr[0];
					CardInfo.setUserId(getApplicationContext(),
							strRoleNameArr[0]);
				}
            // Toast.makeText(ctx, "Card Id : " + i, Toast.LENGTH_LONG).show();
             
 			}
		}
	}

	/*@Override
	protected void onNewIntent(Intent intent){
		if(NfcAdapter.ACTION_TAG_DISCOVERED.equals(intent.getAction())){
			mytag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);   
			 byte[] id = mytag.getId();
             long ids = getDec(id);
             String i = String.valueOf(ids);
             CardInfo.setId(ctx, i);
            // Toast.makeText(ctx, "Card Id : " + i, Toast.LENGTH_LONG).show();
   
			
			//Toast.makeText(this, this.getString(R.string.ok_detection) + mytag.toString(), Toast.LENGTH_LONG ).show();
		}
	}*/
	

    private long getDec(byte[] bytes) {
        long result = 0;
        long factor = 1;
        for (int i = 0; i < bytes.length; ++i) {
            long value = bytes[i] & 0xffl;
            result += value * factor;
            factor *= 256l;
        }
        return result;
    }

	
	@Override
	public void onPause(){
		super.onPause();
		WriteModeOff();
	}

	@Override
	public void onResume(){
		super.onResume();
		WriteModeOn();
	}

	private void WriteModeOn(){
		writeMode = true;
		adapter.enableForegroundDispatch(this, pendingIntent, writeTagFilters, null);
	}

	private void WriteModeOff(){
		writeMode = false;
		adapter.disableForegroundDispatch(this);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		//getMenuInflater().inflate(R.menu.create_admin, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		/*if (id == R.id.action_settings) {
			return true;
		}*/
		return super.onOptionsItemSelected(item);
	}
}
