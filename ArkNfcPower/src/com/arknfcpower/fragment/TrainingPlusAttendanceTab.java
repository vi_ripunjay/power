package com.arknfcpower.fragment;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.TimePickerDialog;
import android.app.TimePickerDialog.OnTimeSetListener;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.telephony.TelephonyManager;
import android.text.format.Time;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.arknfc.adapter.TrainingSpinnerAdapter;
import com.arknfcpower.AttendanceActivity;
import com.arknfcpower.CardInfo;
import com.arknfcpower.LatterAttendanceActivity;
import com.arknfcpower.R;
import com.arknfcpower.db.DatabaseSupport;
import com.arknfcpower.model.TabletRoleFunctions;
import com.arknfcpower.model.TrainingData;
import com.arknfcpower.model.TrainingDataItem;
import com.arknfcpower.model.TrainingTransaction;
import com.arknfcpower.model.UserMaster;

public class TrainingPlusAttendanceTab extends Fragment {

	Spinner parentCategory;
	Spinner subParentCategory;
	Spinner childCategory;
	Spinner subChildCategory;

	EditText userDescription;
	EditText pickDate;
	EditText picktime;

	TextView pendingAttendanceText;

	private String strTrainingDataId = "";
	private String firstParentId = "";
	private String secondLastParentId = "";
	Button now;
	Button latter;

	LinearLayout trainingFormId;

	private DatePickerDialog datePickerDialog;
	private TimePickerDialog timePickerDialog;
	private SimpleDateFormat dateFormatter;

	ArrayList<TrainingData> parentList = null;
	ArrayList<TrainingData> parentCatList = null;
	ArrayList<TrainingData> parentCatSubList = null;
	ArrayList<TrainingData> parentCatSubChildList = null;
	Context context;

	ArrayList<TrainingDataItem> trainingDataItemList;
	ArrayList<TrainingDataItem> trainingParentSubCatList;
	ArrayList<TrainingDataItem> trainingSubCatList;
	ArrayList<TrainingDataItem> trainingChildCatList;

	@SuppressLint("SimpleDateFormat")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.fragment_training_attendance,
				container, false);

		trainingFormId = (LinearLayout) rootView
				.findViewById(R.id.trainingFormID);
		parentCategory = (Spinner) rootView.findViewById(R.id.spinner1);
		subParentCategory = (Spinner) rootView.findViewById(R.id.spinner2);
		childCategory = (Spinner) rootView.findViewById(R.id.spinner3);
		subChildCategory = (Spinner) rootView.findViewById(R.id.spinner4);
		userDescription = (EditText) rootView.findViewById(R.id.editText1);
		now = (Button) rootView.findViewById(R.id.now);
		latter = (Button) rootView.findViewById(R.id.latter);
		pickDate = (EditText) rootView.findViewById(R.id.date);
		picktime = (EditText) rootView.findViewById(R.id.time);
		dateFormatter = new SimpleDateFormat("dd-MM-yyyy");
		pendingAttendanceText = (TextView) rootView
				.findViewById(R.id.pendingAttendanceText);

		context = getActivity();

		Date dt = new Date();

		Time today = new Time(Time.getCurrentTimezone());
		today.setToNow();

		int hours = dt.getHours();
		int minutes = dt.getMinutes();

		String strHours = "0";
		String strMinuts = "0";

		String fullDate = dateFormatter.format(dt);

		if (String.valueOf(hours).length() == 1) {
			strHours = "0" + hours;
		} else {
			strHours = "" + hours;
		}

		if (String.valueOf(minutes).length() == 1) {
			strMinuts = "0" + minutes;
		} else {
			strMinuts = "" + minutes;
		}

		String curTime = strHours + ":" + strMinuts;

		pickDate.setText(fullDate);
		picktime.setText(curTime);

		DatabaseSupport db = new DatabaseSupport(getActivity());

		UserMaster um = new UserMaster();
		um = db.getUserMasterSingleRow(CardInfo.getUserId(getActivity()))
				.get(0);

		List<TabletRoleFunctions> trFunList = db
				.getTabletRoleFunctionsRowByRoleId(um.getiRoleId());
		String strRoleType = "Crew";
		if (trFunList != null && trFunList.size() > 0) {
			strRoleType = trFunList.get(0).getStrFunctionCode();
		}

		if (strRoleType != null && strRoleType.equalsIgnoreCase("Validator")) {
			pendingAttendanceText.setVisibility(View.GONE);
			trainingFormId.setVisibility(View.VISIBLE);

		} else if (strRoleType != null && strRoleType.equalsIgnoreCase("Crew")) {
			pendingAttendanceText.setText(getResources().getString(
					R.string.notAuthText));
			pendingAttendanceText.setVisibility(View.VISIBLE);
			trainingFormId.setVisibility(View.GONE);

		}

		parentList = new ArrayList<TrainingData>();
		parentList = db.getTrainingDataForCategory("null");

		List<TrainingData> bbsList = new ArrayList<TrainingData>();
		bbsList = db.getTrainingDataForCategoryForBBS("SMS", "BBS");
		if (bbsList != null && bbsList.size() > 0) {
			parentList.addAll(bbsList);
		}

		db.close();

		List<String> trainingParentCatList = new ArrayList<String>();
		trainingParentCatList.add("Select");
		trainingDataItemList = new ArrayList<TrainingDataItem>();
		trainingDataItemList.add(new TrainingDataItem("Select", "Select",
				CardInfo.getTenantId(getActivity())));
		for (TrainingData td : parentList) {

			if (td.getStrTitle() != null
					&& "SMS".equalsIgnoreCase(td.getStrTitle())) {
				trainingDataItemList.add(new TrainingDataItem("BBS", td
						.getTraningDataId(), td.getiTenantId()));
			} else {
				trainingDataItemList.add(new TrainingDataItem(td.getStrTitle(),
						td.getTraningDataId(), td.getiTenantId()));
			}

		}

		TrainingSpinnerAdapter spin = new TrainingSpinnerAdapter(
				trainingDataItemList, getActivity());
		parentCategory.setAdapter(spin);

		parentCategory.setOnItemSelectedListener(new OnItemSelectedListener() {

			public void onItemSelected(AdapterView<?> parent, View arg1,
					int pos, long arg3) {

				DatabaseSupport db = new DatabaseSupport(getActivity());

				String strParentId = "";

				strParentId = trainingDataItemList.get(pos).getTrainingDataId();
				strTrainingDataId = strParentId; // this value assign for save
													// in transaction table
				firstParentId = strParentId;

				parentCatList = new ArrayList<TrainingData>();
				parentCatList = db.getTrainingDataForCategory(strParentId);

				TrainingData selectedTraingCat = new TrainingData();
				List<TrainingData> actTdList = db
						.getTrainingDataById(strParentId);
				if (actTdList != null && actTdList.size() > 0) {
					selectedTraingCat = actTdList.get(0);
				} else {
					selectedTraingCat = null;
				}

				trainingParentSubCatList = new ArrayList<TrainingDataItem>();
				trainingParentSubCatList.add(new TrainingDataItem("Select",
						"Select", CardInfo.getTenantId(getActivity())));
				for (TrainingData td : parentCatList) {

					if (!(td.getStrTitle() != null && "Non BMS"
							.equalsIgnoreCase(td.getStrTitle()))) {
						trainingParentSubCatList.add(new TrainingDataItem(td
								.getStrTitle(), td.getTraningDataId(), td
								.getiTenantId()));
					}
				}

				if (parentCategory.getSelectedItem().toString()
						.equalsIgnoreCase("Select")) {
					subParentCategory.setVisibility(View.GONE);
					childCategory.setVisibility(View.GONE);
					subChildCategory.setVisibility(View.GONE);

				} else if (!parentCategory.getSelectedItem().toString()
						.equalsIgnoreCase("Select")) {

					TrainingSpinnerAdapter spin = new TrainingSpinnerAdapter(
							trainingParentSubCatList, getActivity());
					subParentCategory.setAdapter(spin);

				}

				if (parentCatList != null && parentCatList.size() > 0) {
					subParentCategory.setVisibility(View.VISIBLE);
					secondLastParentId = strParentId;
				} else {
					subParentCategory.setVisibility(View.GONE);
					/**
					 * status 99 means display free text.
					 */
					if (selectedTraingCat != null
							&& Integer.parseInt(selectedTraingCat
									.getFlgStatus()) == 99) {
						userDescription.setVisibility(View.VISIBLE);
					} else {
						userDescription.setVisibility(View.GONE);
					}
				}

			}

			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}
		});

		subParentCategory
				.setOnItemSelectedListener(new OnItemSelectedListener() {

					public void onItemSelected(AdapterView<?> arg0, View arg1,
							int pos, long arg3) {

						DatabaseSupport db = new DatabaseSupport(getActivity());

						String strParentId = "";

						strParentId = trainingParentSubCatList.get(pos)
								.getTrainingDataId();
						strTrainingDataId = strParentId;

						/*
						 * if("sms".equalsIgnoreCase(trainingParentSubCatList.get
						 * (pos).getText())) {
						 * 
						 * userDescription.setVisibility(View.VISIBLE);
						 * 
						 * }else { userDescription.setVisibility(View.GONE);
						 * 
						 * }
						 */

						TrainingData selectedTraingCat = new TrainingData();
						List<TrainingData> actTdList = db
								.getTrainingDataById(strParentId);
						if (actTdList != null && actTdList.size() > 0) {
							selectedTraingCat = actTdList.get(0);
						} else {
							selectedTraingCat = null;
						}

						parentCatSubList = new ArrayList<TrainingData>();

						/**
						 * Below code hard code for SMS which not display NON
						 * BMS in new apk
						 */
						if (!"sms".equalsIgnoreCase(trainingParentSubCatList
								.get(pos).getText())) {

							parentCatSubList = db
									.getTrainingDataForCategory(strParentId);

							trainingSubCatList = new ArrayList<TrainingDataItem>();
							trainingSubCatList.add(new TrainingDataItem(
									"Select", "Select", CardInfo
											.getTenantId(getActivity())));
							for (TrainingData td : parentCatSubList) {

								trainingSubCatList.add(new TrainingDataItem(td
										.getStrTitle(), td.getTraningDataId(),
										td.getiTenantId()));
							}

						}

						if (subParentCategory.getSelectedItem().toString()
								.equalsIgnoreCase("Select")) {
							childCategory.setVisibility(View.GONE);
							subChildCategory.setVisibility(View.GONE);

						} else if (!subParentCategory.getSelectedItem()
								.toString().equalsIgnoreCase("Select")) {

							TrainingSpinnerAdapter spin = new TrainingSpinnerAdapter(
									trainingSubCatList, getActivity());
							childCategory.setAdapter(spin);

						}
						if (parentCatSubList != null
								&& parentCatSubList.size() > 0) {
							childCategory.setVisibility(View.VISIBLE);
							secondLastParentId = strParentId;

						} else {
							childCategory.setVisibility(View.GONE);

							/**
							 * status 99 means display free text.
							 */
							if (selectedTraingCat != null
									&& Integer.parseInt(selectedTraingCat
											.getFlgStatus()) == 99) {
								userDescription.setVisibility(View.VISIBLE);
							} else {
								userDescription.setVisibility(View.GONE);
							}
						}

					}

					public void onNothingSelected(AdapterView<?> arg0) {
						// TODO Auto-generated method stub

					}
				});

		childCategory.setOnItemSelectedListener(new OnItemSelectedListener() {

			public void onItemSelected(AdapterView<?> arg0, View arg1, int pos,
					long arg3) {

				/*
				 * Toast.makeText(getActivity(), "OnItemSelectedListener : " +
				 * parentCategory.getSelectedItem(), Toast.LENGTH_SHORT).show();
				 */DatabaseSupport db = new DatabaseSupport(getActivity());

				String strParentId = "";

				strParentId = trainingSubCatList.get(pos).getTrainingDataId();
				strTrainingDataId = strParentId;

				TrainingData selectedTraingCat = new TrainingData();
				List<TrainingData> actTdList = db
						.getTrainingDataById(strParentId);
				if (actTdList != null && actTdList.size() > 0) {
					selectedTraingCat = actTdList.get(0);
				} else {
					selectedTraingCat = null;
				}

				parentCatSubChildList = new ArrayList<TrainingData>();
				parentCatSubChildList = db
						.getTrainingDataForCategory(strParentId);

				trainingChildCatList = new ArrayList<TrainingDataItem>();
				trainingChildCatList.add(new TrainingDataItem("Select",
						"Select", CardInfo.getTenantId(getActivity())));
				for (TrainingData td : parentCatSubChildList) {

					trainingChildCatList.add(new TrainingDataItem(td
							.getStrTitle(), td.getTraningDataId(), td
							.getiTenantId()));
				}

				if (childCategory.getSelectedItem().toString()
						.equalsIgnoreCase("Select")) {
					subChildCategory.setVisibility(View.GONE);

				} else if (!childCategory.getSelectedItem().toString()
						.equalsIgnoreCase("Select")) {

					TrainingSpinnerAdapter spin = new TrainingSpinnerAdapter(
							trainingChildCatList, getActivity());
					subChildCategory.setAdapter(spin);

				}

				if (parentCatSubChildList != null
						&& parentCatSubChildList.size() > 0) {
					subChildCategory.setVisibility(View.VISIBLE);
					secondLastParentId = strParentId;

				} else {
					subChildCategory.setVisibility(View.GONE);

					/**
					 * status 99 means display free text.
					 */
					if (selectedTraingCat != null
							&& Integer.parseInt(selectedTraingCat
									.getFlgStatus()) == 99) {
						userDescription.setVisibility(View.VISIBLE);
					} else {
						userDescription.setVisibility(View.GONE);
					}
				}

			}

			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}
		});

		subChildCategory
				.setOnItemSelectedListener(new OnItemSelectedListener() {

					public void onItemSelected(AdapterView<?> arg0, View arg1,
							int pos, long arg3) {

						String strParentId = "";

						strParentId = trainingChildCatList.get(pos)
								.getTrainingDataId();
						strTrainingDataId = strParentId;

						DatabaseSupport db = new DatabaseSupport(getActivity());

						TrainingData selectedTraingCat = new TrainingData();
						List<TrainingData> actTdList = db
								.getTrainingDataById(strParentId);
						if (actTdList != null && actTdList.size() > 0) {
							selectedTraingCat = actTdList.get(0);
						} else {
							selectedTraingCat = null;
						}

						/**
						 * status 99 means display free text.
						 */
						if (selectedTraingCat != null
								&& Integer.parseInt(selectedTraingCat
										.getFlgStatus()) == 99) {
							userDescription.setVisibility(View.VISIBLE);
						} else {
							userDescription.setVisibility(View.GONE);
						}

					}

					public void onNothingSelected(AdapterView<?> arg0) {
						// TODO Auto-generated method stub

					}
				});

		pickDate.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				setDateField();
			}
		});

		picktime.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				Calendar mcurrentTime = Calendar.getInstance();
				int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
				int minute = mcurrentTime.get(Calendar.MINUTE);

				String editTime = (picktime.getText() != null
						&& !"".equals(picktime.getText().toString()) ? picktime
						.getText().toString() : "");
				if (!"".equals(editTime)) {
					String hrString[] = editTime.split(":");
					if (hrString != null && hrString.length > 0) {
						hour = Integer.parseInt(hrString[0]);
						minute = Integer.parseInt(hrString[1]);
					}
				}

				timePickerDialog = new TimePickerDialog(context,
						new OnTimeSetListener() {

							public void onTimeSet(TimePicker view,
									int hourOfDay, int minute) {
								// TODO Auto-generated method stub

								String strMinuts = "00";
								String strHours = "00";
								if (String.valueOf(minute).length() == 1) {
									strMinuts = "0" + minute;
								} else {
									strMinuts = "" + minute;
								}

								if (String.valueOf(hourOfDay).length() == 1) {
									strHours = "0" + hourOfDay;
								} else {
									strHours = "" + hourOfDay;
								}
								picktime.setText(strHours + ":" + strMinuts);
							}
						}, hour, minute, false);
				timePickerDialog.setTitle("Select Time");
				timePickerDialog.show();

			}
		});

		now.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if ((!"".equals(strTrainingDataId) && !strTrainingDataId
						.equalsIgnoreCase("Select"))
						&& (!pickDate.getText().toString().equalsIgnoreCase(""))
						&& (!picktime.getText().toString().equalsIgnoreCase(""))) {

					Date curDate = new Date();
					DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

					String datePickerFormat[] = pickDate.getText().toString()
							.split("-");
					String actualTraningDate = datePickerFormat[2] + "-"
							+ datePickerFormat[1] + "-" + datePickerFormat[0];

					TelephonyManager tm = (TelephonyManager) getActivity()
							.getSystemService(Context.TELEPHONY_SERVICE);
					String IMEINumbeR = tm.getDeviceId() + "_"
							+ curDate.getTime();
					DatabaseSupport db = new DatabaseSupport(getActivity());
					List<TrainingTransaction> transactionList = db
							.getTrainingTrainsactionByDataIdDateTime(
									strTrainingDataId, actualTraningDate,
									picktime.getText().toString());
					if (transactionList != null && transactionList.size() > 0) {

						TrainingTransaction trainingTransaction = new TrainingTransaction();

						trainingTransaction = transactionList.get(0);

						Intent i = new Intent(getActivity(),
								AttendanceActivity.class);
						i.putExtra("trainingDataId",
								trainingTransaction.getTraningDataId());
						i.putExtra("trainingTransactionId",
								trainingTransaction.getTraningTransactionId());
						i.putExtra("trainingDate", actualTraningDate);
						i.putExtra("trainingTime", picktime.getText()
								.toString());
						startActivity(i);
						getActivity().finish();

					} else {

						db.addTrainingTransactionRow(new TrainingTransaction(
								IMEINumbeR, strTrainingDataId,
								actualTraningDate, picktime.getText()
										.toString(), userDescription.getText()
										.toString(), "0", CardInfo
										.getShipId(getActivity()), CardInfo
										.getTenantId(getActivity()), df
										.format(curDate), df.format(curDate),
								"0", "1", secondLastParentId));
						Intent i = new Intent(getActivity(),
								AttendanceActivity.class);
						i.putExtra("trainingDataId", strTrainingDataId);
						i.putExtra("trainingTransactionId", IMEINumbeR);
						i.putExtra("trainingDate", actualTraningDate);
						i.putExtra("trainingTime", picktime.getText()
								.toString());
						startActivity(i);
						getActivity().finish();
					}

				} else {
					Toast.makeText(context, "Fill the required Fields",
							Toast.LENGTH_SHORT).show();
				}
			}
		});

		latter.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub

				if ((!"".equals(strTrainingDataId) && !strTrainingDataId
						.equalsIgnoreCase("Select"))
						&& (!pickDate.getText().toString().equalsIgnoreCase(""))
						&& (!picktime.getText().toString().equalsIgnoreCase(""))) {

					Date curDate = new Date();
					DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

					String datePickerFormat[] = pickDate.getText().toString()
							.split("-");
					String actualTraningDate = datePickerFormat[2] + "-"
							+ datePickerFormat[1] + "-" + datePickerFormat[0];

					TelephonyManager tm = (TelephonyManager) getActivity()
							.getSystemService(Context.TELEPHONY_SERVICE);
					String IMEINumbeR = tm.getDeviceId() + "_"
							+ curDate.getTime();

					DatabaseSupport db = new DatabaseSupport(getActivity()
							.getApplicationContext());
					List<TrainingTransaction> transactionList = db
							.getTrainingTrainsactionByDataIdDateTime(
									strTrainingDataId, actualTraningDate,
									picktime.getText().toString());
					if (transactionList != null && transactionList.size() > 0) {
						TrainingTransaction trainingTransaction = new TrainingTransaction();

						trainingTransaction = transactionList.get(0);

						Intent i = new Intent(getActivity(),
								LatterAttendanceActivity.class);
						i.putExtra("trainingDataId",
								trainingTransaction.getTraningDataId());
						i.putExtra("trainingTransactionId",
								trainingTransaction.getTraningTransactionId());
						i.putExtra("trainingDate", actualTraningDate);
						i.putExtra("trainingTime", picktime.getText()
								.toString());
						startActivity(i);
						getActivity().finish();

					} else

					{

						db.addTrainingTransactionRow(new TrainingTransaction(
								IMEINumbeR, strTrainingDataId,
								actualTraningDate, picktime.getText()
										.toString(), userDescription.getText()
										.toString(), "0", CardInfo
										.getShipId(getActivity()), CardInfo
										.getTenantId(getActivity()), df
										.format(curDate), df.format(curDate),
								"0", "1", secondLastParentId));

						Intent i = new Intent(getActivity(),
								LatterAttendanceActivity.class);
						i.putExtra("trainingDataId", strTrainingDataId);
						i.putExtra("trainingTransactionId", IMEINumbeR);
						i.putExtra("trainingDate", actualTraningDate);
						i.putExtra("trainingTime", picktime.getText()
								.toString());

						startActivity(i);
						getActivity().finish();

					}

				} else {
					Toast.makeText(context, "Fill the required Fields",
							Toast.LENGTH_SHORT).show();
				}
			}
		});

		return rootView;
	}

	private void setDateField() {
		System.out.println("hello  i am here");
		Calendar newCalendar = Calendar.getInstance();
		datePickerDialog = new DatePickerDialog(getActivity(),
				new OnDateSetListener() {

					public void onDateSet(DatePicker view, int year,
							int monthOfYear, int dayOfMonth) {
						Calendar newDate = Calendar.getInstance();
						newDate.set(year, monthOfYear, dayOfMonth);
						pickDate.setText(dateFormatter.format(newDate.getTime()));
					}

				}, newCalendar.get(Calendar.YEAR), newCalendar
						.get(Calendar.MONTH), newCalendar
						.get(Calendar.DAY_OF_MONTH));
		datePickerDialog.show();
	}

}
