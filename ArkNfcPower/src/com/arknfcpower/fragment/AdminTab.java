package com.arknfcpower.fragment;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.arknfcpower.R;
import com.arknfcpower.db.DatabaseSupport;

public class AdminTab extends Fragment{

	Context mContext;
    TextView appVersion;
    Button exportData;
    
    
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.admin_tab, container, false);
		mContext = getActivity();
		
		exportData = (Button) rootView.findViewById(R.id.exportData);
		
		appVersion = (TextView) rootView.findViewById(R.id.appVerId);
		
		PackageInfo pInfo = null;
		String version="";
		try {
			pInfo = mContext.getPackageManager().getPackageInfo(mContext.getPackageName(), 0);
		} catch (NameNotFoundException e) {			
			e.printStackTrace();
		}
		if(pInfo != null && pInfo.versionName != null){
			version = pInfo.versionName;
		}else{
			version ="1.5.8";
		}
		
		appVersion.setText(version);
		
		exportData.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				exportDB();
			}
		});
		
		return rootView;
	}
	
	/**
	 * Ripunjay.S
	 * 
	 * @param fileOrDirectory
	 */
	private void exportDB() {
		try {
			File sd = Environment.getExternalStorageDirectory();
			File data = Environment.getDataDirectory();
			FileChannel source = null;
			FileChannel destination = null;
			String currentDBPath = "/data/" + mContext.getPackageName()
					+ "/databases/" + DatabaseSupport.DATABASE_NAME;
			
			StringBuffer sb = new StringBuffer(
					Environment.getExternalStorageDirectory() + "/TheArk/");
			File file = new File(sb.toString());
			if (!file.exists()) {
				file.mkdirs();
			}
			String backupDBPath = DatabaseSupport.DATABASE_NAME;
			File currentDB = new File(data, currentDBPath);
			File backupDB = new File(file, backupDBPath);
			try {
				source = new FileInputStream(currentDB).getChannel();
				destination = new FileOutputStream(backupDB).getChannel();
				destination.transferFrom(source, 0, source.size());
				source.close();
				destination.close();
				Toast.makeText(mContext, "DB Exported!", Toast.LENGTH_LONG)
						.show();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
}
