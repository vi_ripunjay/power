package com.arknfcpower.fragment;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;

import com.arknfc.adapter.PendingAttendanceAdapter;
import com.arknfcpower.CardInfo;
import com.arknfcpower.PendingAttendance;
import com.arknfcpower.R;
import com.arknfcpower.WelcomeTrainingPlus;
import com.arknfcpower.db.DatabaseSupport;
import com.arknfcpower.model.TraingAttendance;

public class TrainingPlusSignInAttendance extends Fragment {

	ListView lv;
	ImageView imageView;
    TextView textView;
    TextView dateTime;
	//	Button done;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.fragment_sign_in_training, container, false);
		
		
	//    getActivity().setTitle("Training Attendance");

		
		DatabaseSupport db = new DatabaseSupport(getActivity());
		ArrayList<TraingAttendance>  attendanceList = db.getTrainingAttendanceByUserIdAndStatus(CardInfo.getUserId(getActivity()), "1");
		TraingAttendance ta = new TraingAttendance();
		ta.setStrDescription(getResources().getString(R.string.signinText));
		attendanceList.add(0, ta);
		
		
		   lv = (ListView) rootView.findViewById(R.id.pendingList);
	//	   done = (Button) rootView.findViewById(R.id.done);
	       PendingAttendanceAdapter adapter = new PendingAttendanceAdapter(attendanceList,getActivity());
		   lv.setAdapter(adapter);  
		   db.close();
		   
/*		   done.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(getActivity() ,WelcomeTrainingPlus.class );
				startActivity(i);
				getActivity().finish();
			}
		});
		
		
*/		   lv.setOnItemClickListener(new OnItemClickListener() {
		    	 
	            @Override
	            public void onItemClick(AdapterView<?> parent, View view,
	               int position, long id) {
	              
	             // ListView Clicked item index
	             int itemPosition     = position;
	             
	        	 imageView=((ImageView)view.findViewById(R.id.select_padding_icon));
	        	 textView =((TextView)view.findViewById(R.id.trainingCatDesc));

	        	 
	     	     dateTime = ((TextView)view.findViewById(R.id.trDateTime));


	        	 
	        	 if (position > 0)
	        	 {

	             DatabaseSupport db = new DatabaseSupport(getActivity());  
	             
	             // ListView Clicked item value
	             TraingAttendance  itemValue    =  (TraingAttendance) lv.getItemAtPosition(position);
	             
	 			Date curDate = new Date();
	 			DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
	             
	             if(db.getTrainingAttendanceByTransactionAndUserId(itemValue.getTraningTransactionId(),itemValue.getiUserId() , "1").size() > 0)
	 	        {
	            	 
	            	 itemValue.setFlgStatus("0");
	            	 itemValue.setFlgIsDirty("1");
	            	 itemValue.setDtUpdated(df.format(curDate));
	            	 db.updateTrainingAttendanceRow(itemValue);
						imageView.setVisibility(View.VISIBLE);
						imageView.setImageResource(R.drawable.select_icon);
						imageView.invalidate();
						
						textView.setTextColor(getResources().getColor(R.color.white));
						dateTime.setTextColor(getResources().getColor(R.color.white));
 		                view.setBackgroundColor(Color.parseColor("#06dc1a"));
		 	        
	 	        }else
	 	        {
	            	 itemValue.setFlgStatus("1");
	            	 itemValue.setFlgIsDirty("1");
	            	 itemValue.setDtUpdated(df.format(curDate));
	            	 db.updateTrainingAttendanceRow(itemValue);
	 	        	 imageView.setVisibility(View.GONE);
	 	        	textView.setTextColor(getResources().getColor(R.color.black));
					dateTime.setTextColor(getResources().getColor(R.color.appcolor));
	 	        	view.setBackgroundColor(Color.parseColor("#ffffff"));
	 	            //Update logic Apply here
	 	        }
	      
	            }
	        	 
	            }
	           });
		

		
		
		
		return rootView;
	}

}
