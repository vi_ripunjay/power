package com.arknfcpower.fragment;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.arknfcpower.R;
import com.arknfcpower.db.DatabaseSupport;
import com.arknfcpower.model.UserMaster;
public class StartSync extends Fragment{
	
	//private AdminSyncTab adminSyncTab;
	public static Context mContext;
	public TextView setDateTimeText;
	public TextView startTextView;
	private AdminSyncTab adminSyncTab;
	//private Button usbTest;
	
	   @Override
	   public View onCreateView(LayoutInflater inflater,
	      ViewGroup container, Bundle savedInstanceState) {
	      // Inflate the layout for this fragment
		   View rootView = inflater.inflate(R.layout.startwizard, container, false);
		   mContext = getActivity().getApplicationContext();
		   
		   setDateTimeText = (TextView)rootView.findViewById(R.id.setDateTimeText);
		   startTextView = (TextView)rootView.findViewById(R.id.startTextView);
		   startTextView.setVisibility(View.VISIBLE);
		   setDateTimeText.setVisibility(View.GONE);
		   
			/*List<UserMaster> umList = new ArrayList<UserMaster>();
			DatabaseSupport db = new DatabaseSupport(getActivity());
			umList = db.getUserMasterRow();
			if(!adminSyncTab.setTime && (umList == null || umList.size() == 0)){
				setDateTimeText.setVisibility(View.GONE);
				startTextView.setVisibility(View.VISIBLE);
			}
			else{
				setDateTimeText.setVisibility(View.GONE);
				startTextView.setVisibility(View.VISIBLE);
			}*/
			
/*		try {
			
			
			StorageManager storage = (StorageManager)mContext.getApplicationContext().getSystemService(getActivity().STORAGE_SERVICE);
			 Method method = storage.getClass().getDeclaredMethod("enableUsbMassStorage");
			 method.setAccessible(true); 
			 Object r = method.invoke(storage);
			 
			 System.out.println("helllllloooooooooo");
			 
		} catch (NoSuchMethodException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		   
		   */
		   
		   //adminSyncTab.isConnected(mContext);
		   
		  // usbTest = (Button) rootView.findViewById(R.id.usbTest);
		   
		/*   usbTest.setOnClickListener(new OnClickListener() {
				
				@SuppressLint("InlinedApi")
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
				if(canWriteToFlash())
				{
		        
		        
		         try {
		        	 
		        	 PackageManager pm = mContext.getPackageManager();
		        	 List<PackageInfo> list = pm.getInstalledPackages(0);

		        	 for(PackageInfo pi : list) {
		        	     ApplicationInfo ai = pm.getApplicationInfo(pi.packageName, 0);

		        	     System.out.println(">>>>>>packages is<<<<<<<<" + ai.publicSourceDir);

		        	     if ((ai.flags & ApplicationInfo.FLAG_SYSTEM) != 0) {
		        	         System.out.println(">>>>>>packages is system package"+pi.packageName);          
		        	     }
		        	 }
		        	 
					//Settings.Secure.putInt(getActivity().getContentResolver(),Settings.Secure.USB_MASS_STORAGE_ENABLED,1);
		        	 
		        	Settings.Secure.putInt(getActivity().getContentResolver(),Settings.Global.USB_MASS_STORAGE_ENABLED,1);
									   
		        	 
		        	 
		        	StorageManager storage = (StorageManager)mContext.getApplicationContext().getSystemService(getActivity().STORAGE_SERVICE);
		        	 Method method = storage.getClass().getDeclaredMethod("enableUsbMassStorage");
		        	 method.setAccessible(true); 
		        	 Object r = method.invoke(storage);
		        	 
		        	 
				} catch (Exception e) {
					// TODO Auto-generated catch block
					L.fv(e.getMessage());
					e.printStackTrace();
				}
		         
		         Toast.makeText(mContext, "mounted :" + canWriteToFlash(), Toast.LENGTH_LONG).show();	

				}else 
				{
					 try {
						//Settings.Secure.putInt(getActivity().getContentResolver(),Settings.Secure.USB_MASS_STORAGE_ENABLED,0);
						 
						 Settings.Secure.putInt(getActivity().getContentResolver(),Settings.Global.USB_MASS_STORAGE_ENABLED,0);
						 
						 StorageManager storage = (StorageManager) mContext.getApplicationContext().getSystemService(getActivity().STORAGE_SERVICE);
						 Method method = storage.getClass().getDeclaredMethod("disableUsbMassStorage");
						 method.setAccessible(true);
						 Object r = method.invoke(storage);
						 
						 Toast.makeText(getActivity().getApplicationContext(), "Unmounted : " + canWriteToFlash(), Toast.LENGTH_LONG).show();
					} catch (Exception e) {
						// TODO Auto-generated catch block
						L.fv(e.getMessage());
						e.printStackTrace();
					}	
				}
				}
			});*/
		   
	      return rootView;
	   }
	   

		private boolean canWriteToFlash() {
			String state = Environment.getExternalStorageState();
			System.out.println("state : " + state);
			if (Environment.MEDIA_MOUNTED.equals(state)) {
				return true;
			} else if (Environment.MEDIA_SHARED.equals(state)) {
				// Read only isn't good enough

				return false;
			} else {
				return false;
			}
		}

		private boolean getMassStorageOn() {
			String state = Environment.getExternalStorageState();
			System.out.println("state : " + state);
			if (Environment.MEDIA_SHARED.equals(state) || Environment.MEDIA_MOUNTED.equals(state)) {
				return true;
			} else {
				return false;
			}
		}

		@Override
		public void onResume() {
			// TODO Auto-generated method stub
			super.onResume();
			AdminSyncTab.isConnected(mContext);
		}

		@Override
		public void onPause() {
			// TODO Auto-generated method stub
			super.onPause();
			try {
				if (AdminSyncTab.myBroadcast != null) {
					mContext.unregisterReceiver(AdminSyncTab.myBroadcast);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}
