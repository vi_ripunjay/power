package com.arknfcpower;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Fragment;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.arknfc.adapter.ScoreAdapter;
import com.arknfcpower.db.DatabaseSupport;
import com.arknfcpower.model.PowerPlusCategory;
import com.arknfcpower.model.PowerPlusPeriodicity;
import com.arknfcpower.model.PowerPlusRoleRankCategory;
import com.arknfcpower.model.PowerPlusTask;
import com.arknfcpower.model.PowerPlusTransaction;
import com.arknfcpower.model.UserMaster;
import com.arknfcpower.model.UserServiceTermRoleData;
import com.arknfcpower.reader.Pair;
import com.arknfcpower.reader.PowerPlusUtils;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
@SuppressLint("NewApi")
public class PowerUp extends Fragment {
	ListView lv;
	ImageView imageView;
	TextView listItemText;
	TextView score;
	

	public PowerUp() {
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.fragment_power_up, container,
				false);

		lv = (ListView) rootView.findViewById(R.id.powerup);

		DatabaseSupport db = new DatabaseSupport(getActivity());
		
		List<PowerPlusCategory> ppcList = new ArrayList<PowerPlusCategory>();
		PowerPlusCategory ppCat = new PowerPlusCategory();
		//ppcList = db.getPowerPlusCategoryRowByTitle("Power Up");
		ppcList = db.getPowerPlusCategoryRowByCatCode("powerup");
		if(ppcList != null && ppcList.size() > 0){
			ppCat = ppcList.get(0);
		}
		
		Date curDate = new Date();
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		ArrayList<PowerPlusTask> pptList = new ArrayList<PowerPlusTask>();
		List<UserServiceTermRoleData> serviceTermRoleData = new ArrayList<UserServiceTermRoleData>();
		serviceTermRoleData = db.getUserServiceTermRoleRowBiId(CardInfo.getUserId(getActivity()),format.format(curDate));
		UserServiceTermRoleData userSrvTrmRlData = null;
		if(serviceTermRoleData != null && serviceTermRoleData.size() > 0){
			userSrvTrmRlData = new UserServiceTermRoleData();
			userSrvTrmRlData = serviceTermRoleData.get(0);
		}
		
		List<PowerPlusRoleRankCategory> powerPlusRoleRankCategoryList = new ArrayList<PowerPlusRoleRankCategory>();
		powerPlusRoleRankCategoryList = db.getPowerPlusRoleRankCategoryRowByRoleId((userSrvTrmRlData != null ? userSrvTrmRlData.getiRoleId() : "0"));
		StringBuffer pprrcId = new StringBuffer("('5'");
		if(powerPlusRoleRankCategoryList != null && powerPlusRoleRankCategoryList.size() > 0){
			
			for(PowerPlusRoleRankCategory pprrc : powerPlusRoleRankCategoryList) {
				
					pprrcId.append(",'"+pprrc.getiPowerPlusRoleRankCategoryId()+"'");
				
			}
		}
		pprrcId.append(")");
		
		ArrayList<PowerPlusTask> list = (ArrayList<PowerPlusTask>) db
				.getPowerPlusTaskRowByCategoryRole((ppCat.getiPowerPlusCategoryId() != null ? ppCat.getiPowerPlusCategoryId() : "0001"),pprrcId.toString());
	
//==================================================================================================
	
		
		/**
		 * List update on condition basis.
		 */
		for(PowerPlusTask ppt : list) {
			
			List<PowerPlusPeriodicity> pppLst = db.getPowerPlusPeriodicityRowById(ppt.getiPowerPlusPeriodicityId());
			if(pppLst != null && pppLst.size() > 0){
				PowerPlusPeriodicity pwrplsPrd = new PowerPlusPeriodicity();
				pwrplsPrd = pppLst.get(0);
				
				int iTimes = Integer.parseInt(pwrplsPrd.getiTimes());
				String pattern = pwrplsPrd.getStrPattern();				
				
				if(iTimes  >0 && pattern != null){
					if("month".equalsIgnoreCase(pattern)){	
						
						if(db.getPowerPlusTransactionCountForValidate(CardInfo.getUserId(getActivity()), format.format(PowerPlusUtils.getDateRangeOfMonth().getBegining()), format.format(PowerPlusUtils.getDateRangeOfMonth().getEnd()), ppt.getiPowerPlusTaskId()) < (pwrplsPrd.getiTimes() != null ? Integer.parseInt(pwrplsPrd.getiTimes()) : 0)){
							pptList.add(ppt);
						}						
					}
					else if("year".equalsIgnoreCase(pattern)){
						if(db.getPowerPlusTransactionCountForValidate(CardInfo.getUserId(getActivity()), format.format(PowerPlusUtils.getDateRangeOfYear().getBegining()), format.format(PowerPlusUtils.getDateRangeOfYear().getEnd()), ppt.getiPowerPlusTaskId()) < (pwrplsPrd.getiTimes() != null ? Integer.parseInt(pwrplsPrd.getiTimes()) : 0)){
							pptList.add(ppt);
						}
					}
					else if("week".equalsIgnoreCase(pattern)){
						if(db.getPowerPlusTransactionCountForValidate(CardInfo.getUserId(getActivity()),format.format(PowerPlusUtils.getDateRangeOfweek().getBegining()), format.format(PowerPlusUtils.getDateRangeOfweek().getEnd()), ppt.getiPowerPlusTaskId()) < (pwrplsPrd.getiTimes() != null ? Integer.parseInt(pwrplsPrd.getiTimes()) : 0)){
							pptList.add(ppt);
						}
					}
					else if("day".equalsIgnoreCase(pattern)){
						if(db.getPowerPlusTransactionCountForValidate(CardInfo.getUserId(getActivity()), format.format(curDate), format.format(curDate), ppt.getiPowerPlusTaskId()) < (pwrplsPrd.getiTimes() != null ? Integer.parseInt(pwrplsPrd.getiTimes()) : 0)){
							pptList.add(ppt);
						}
					}
					else if("contract".equalsIgnoreCase(pattern)){
						
						if(db.getPowerPlusTransactionCountForValidate(CardInfo.getUserId(getActivity()), userSrvTrmRlData.getDtTermFrom(), userSrvTrmRlData.getDtTermTo(), ppt.getiPowerPlusTaskId()) < (pwrplsPrd.getiTimes() != null ? Integer.parseInt(pwrplsPrd.getiTimes()) : 0)){
							pptList.add(ppt);
						}
						
					}
					else {
							
						pptList.add(ppt);						
					}
					
				}
				else {
					
					pptList.add(ppt);						
				}
				
				
			}
		}
//==============================================================================================
		
		
		PowerPlusTask pptForInfo = new PowerPlusTask();
		pptForInfo.setStrTitle(getResources().getString(R.string.positivetext));
		pptList.add(0, pptForInfo);

		System.out.println("List :" + pptList);
		ScoreAdapter adapter = new ScoreAdapter(pptList, getActivity());

		// handle listview and assign adapter

		lv.setAdapter(adapter);
		db.close();
		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {

				// ListView Clicked item index
				@SuppressWarnings("unused")
				int itemPosition = position;
				DateFormat format = new SimpleDateFormat("yyyy-MM-dd");

				imageView=((ImageView)view.findViewById(R.id.select_info));
				listItemText=((TextView)view.findViewById(R.id.list_item_string));
				score=((TextView)view.findViewById(R.id.textView1));
				
				if (position > 0) {
					DatabaseSupport db = new DatabaseSupport(getActivity());

					// ListView Clicked item value
					PowerPlusTask itemValue = (PowerPlusTask) lv
							.getItemAtPosition(position);
					if (db.getScoreInEnterMode(
							CardInfo.getUserId(getActivity()),
							itemValue.getiPowerPlusTaskId(), "Entered") == 0) {
						Date currentDate = new Date();
						ArrayList<UserMaster> data = db
								.getUserMasterSingleRow(CardInfo
										.getUserId(getActivity()));
						UserServiceTermRoleData userServiceTermRoleData = new UserServiceTermRoleData();
						UserMaster userMaster = new UserMaster();
						if (data != null && data.size() > 0) {
							userMaster = data.get(0);
							Date curDate = new Date();
			 				List<UserServiceTermRoleData> serviceTermRoleData = new ArrayList<UserServiceTermRoleData>();
			 				serviceTermRoleData = db.getUserServiceTermRoleRowBiId(userMaster.getiUserId(),format.format(curDate));
			 				if(serviceTermRoleData != null && serviceTermRoleData.size() > 0){
			 					
			 					userServiceTermRoleData = serviceTermRoleData.get(0);
			 				}
						}

						String powerPlusTransactionId = CardInfo
								.getId(getActivity())
								+ "_"
								+ new Date().getTime();
						db.addPowerPlusTransactionRow(new PowerPlusTransaction(
								powerPlusTransactionId, format.format(currentDate),
								format.format(currentDate), "0", "1", "1",
								"Entered", "1", itemValue.getPowerPoints(), "",
								"", "", "", "",  CardInfo.getUserId(getActivity()),  CardInfo.getUserId(getActivity()),
								itemValue.getStrTitle(), itemValue
										.getiPowerPlusTaskId(), CardInfo.getShipId(getActivity()), CardInfo.getTenantId(getActivity()),
										userMaster.getiUserId(), "P",itemValue.getiPowerPlusCategoryId(),"",userMaster.getiRoleId(),userServiceTermRoleData.getIuserServiceTermId(),"0",userMaster.getStrFirstName()));
						// Show Alert
						imageView.setVisibility(View.VISIBLE);
						imageView.setImageResource(R.drawable.select_icon);
						imageView.invalidate();
						
						listItemText.setTextColor(getResources().getColor(R.color.white));
					    score.setTextColor(getResources().getColor(R.color.white));

						
						view.setBackgroundColor(Color.parseColor("#06dc1a"));

/*						Toast.makeText(
								getActivity(),
								"Position :" + itemPosition + "  ListItem : "
										+ itemValue.getStrTitle(),
								Toast.LENGTH_LONG).show();
*/					} else {
						PowerPlusTransaction ppt = db
								.getPowerPlusTransactionById(
										CardInfo.getUserId(getActivity()),
										itemValue.getiPowerPlusTaskId(),
										"Entered");
						ppt.setFlgStatus("Rejected");
						db.UpdatePowerPlusTransactionRow(ppt);
/*						Toast.makeText(
								getActivity(),
								" Reverted " + itemPosition + "  ListItem : "
										+ itemValue.getStrTitle(),
								Toast.LENGTH_LONG).show();
*/
						listItemText.setTextColor(getResources().getColor(R.color.black));
					    score.setTextColor(getResources().getColor(R.color.black));

						imageView.setVisibility(View.GONE);
						view.setBackgroundColor(Color.parseColor("#ffffff"));

					}
					db.close();
				}
			}

		});
		return rootView;
	}


}
