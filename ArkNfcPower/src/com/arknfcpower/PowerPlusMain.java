package com.arknfcpower;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.arknfc.adapter.NavDrawerListAdapter;
import com.arknfcpower.db.DatabaseSupport;
import com.arknfcpower.model.NavDrawerItem;
import com.arknfcpower.model.PowerPlusCategory;
import com.arknfcpower.model.TabletRoleFunctions;
import com.arknfcpower.model.UserMaster;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
@SuppressLint("NewApi")
public class PowerPlusMain extends Activity {
	private DrawerLayout mDrawerLayout;
	private ListView mDrawerList;
	private ActionBarDrawerToggle mDrawerToggle;

	// nav drawer title
	private CharSequence mDrawerTitle;

	// used to store app title
	private CharSequence mTitle;

	ProgressBar progressbar;
	EditText userName;
	EditText userPass;
	TextView errortext = null;

	// slide menu items
	private String[] navMenuTitles;
	private TypedArray navMenuIcons;
	private ArrayList<NavDrawerItem> navDrawerItems;
	private NavDrawerListAdapter adapter;

	private int pos;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		ActionBar ab = getActionBar();
		ColorDrawable colorDrawable = new ColorDrawable(
				Color.parseColor("#009bff"));
		ab.setBackgroundDrawable(colorDrawable);
		/**
		 * work on 4.4.2
		 */
		//ab.setDisplayUseLogoEnabled(false);
		
		ab.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM | ActionBar.DISPLAY_SHOW_HOME | ActionBar.DISPLAY_SHOW_TITLE);
		View homeIcon = findViewById(
	            Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB ? 
	                    android.R.id.home : android.support.v7.appcompat.R.id.home);
	    ((View) homeIcon.getParent()).setVisibility(View.GONE);
	    ((View) homeIcon).setVisibility(View.GONE);
	    
	    /**
		 * below code for hide the icon.
		 */
		ab.setIcon(new ColorDrawable(getResources().getColor(android.R.color.transparent)));
		/*
		 * ab.setDisplayOptions(ab.getDisplayOptions() |
		 * ActionBar.DISPLAY_SHOW_CUSTOM); ImageView imageView = new
		 * ImageView(ab.getThemedContext());
		 * imageView.setScaleType(ImageView.ScaleType.CENTER);
		 * imageView.setImageResource(R.drawable.nyk_logo);
		 * ActionBar.LayoutParams layoutParams = new ActionBar.LayoutParams(
		 * ActionBar.LayoutParams.WRAP_CONTENT,
		 * ActionBar.LayoutParams.WRAP_CONTENT, Gravity.RIGHT |
		 * Gravity.CENTER_VERTICAL); layoutParams.rightMargin = 5;
		 * imageView.setLayoutParams(layoutParams); ab.setCustomView(imageView);
		 */
		setContentView(R.layout.activity_home);

		mTitle = mDrawerTitle = getTitle();

		// load slide menu items

		navMenuIcons = getResources()
				.obtainTypedArray(R.array.nav_drawer_icons);
		// navMenuTitles =
		// getResources().getStringArray(R.array.nav_drawer_items);
		DatabaseSupport db = new DatabaseSupport(this);
		ArrayList<PowerPlusCategory> data = db.getPowerPlusCategoryRow();
		System.out.println("List size :" + data.size());
		navMenuTitles = new String[data.size() + 1];
		navMenuTitles[0] = "Home";

		UserMaster um = new UserMaster();
		um = db.getUserMasterSingleRow(
				CardInfo.getUserId(getApplicationContext())).get(0);
		/*String strRoleType = db.getRoleSingleRow(um.getiRoleId())
				.getStrRoleType();
		*/
		List<TabletRoleFunctions> trFunList = db.getTabletRoleFunctionsRowByRoleId(um.getiRoleId());
		String strRoleType ="Crew";
		if(trFunList != null && trFunList.size() > 0){
			strRoleType = trFunList.get(0).getStrFunctionCode();
		}

		int ind =0;
		for (int i = 0; i < data.size(); i++) {
			String category = data.get(i).getStrTitle();
			String categoryCode = data.get(i).getStrCategoryCode();

			if (strRoleType != null
					&& strRoleType.equalsIgnoreCase("Validator")) {

				System.out.println("Category : " + category);
				navMenuTitles[i + 1] = category;
			} else {
				if (!categoryCode.equalsIgnoreCase("validatescore")&& !categoryCode.equalsIgnoreCase("leadershippower")) {
					navMenuTitles[ind + 1] = category;
					ind++;

				}
			}

		}

		// nav drawer icons from resources

		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mDrawerList = (ListView) findViewById(R.id.list_slidermenu);
		navDrawerItems = new ArrayList<NavDrawerItem>();

		// adding nav drawer items to array
		// Home
		/*
		 * UserMaster um = new UserMaster(); um =
		 * db.getUserMasterSingleRow(CardInfo
		 * .getUserId(getApplicationContext())).get(0); String strRoleType =
		 * db.getRoleSingleRow(um.getiRoleId()).getStrRoleType();
		 */if (strRoleType != null && strRoleType.equalsIgnoreCase("Validator")) {

			navDrawerItems.add(new NavDrawerItem(navMenuTitles[0], navMenuIcons
					.getResourceId(0, -1)));
			navDrawerItems.add(new NavDrawerItem(navMenuTitles[1], navMenuIcons
					.getResourceId(1, -1)));
			navDrawerItems.add(new NavDrawerItem(navMenuTitles[2], navMenuIcons
					.getResourceId(2, -1)));
			navDrawerItems.add(new NavDrawerItem(navMenuTitles[3], navMenuIcons
					.getResourceId(3, -1)));
			navDrawerItems.add(new NavDrawerItem(navMenuTitles[4], navMenuIcons
					.getResourceId(4, -1)));
			navDrawerItems.add(new NavDrawerItem(navMenuTitles[5], navMenuIcons
					.getResourceId(5, -1)));
			navDrawerItems.add(new NavDrawerItem(navMenuTitles[6], navMenuIcons
					.getResourceId(6, -1)));
			navDrawerItems.add(new NavDrawerItem(navMenuTitles[7], navMenuIcons
					.getResourceId(7, -1)));

		} else if (strRoleType != null && strRoleType.equalsIgnoreCase("Crew")) {

			navDrawerItems.add(new NavDrawerItem(navMenuTitles[0], navMenuIcons
					.getResourceId(0, -1)));
			navDrawerItems.add(new NavDrawerItem(navMenuTitles[1], navMenuIcons
					.getResourceId(1, -1)));
			navDrawerItems.add(new NavDrawerItem(navMenuTitles[2], navMenuIcons
					.getResourceId(3, -1)));
			navDrawerItems.add(new NavDrawerItem(navMenuTitles[3], navMenuIcons
					.getResourceId(4, -1)));
			navDrawerItems.add(new NavDrawerItem(navMenuTitles[4], navMenuIcons
					.getResourceId(6, -1)));
			navDrawerItems.add(new NavDrawerItem(navMenuTitles[5], navMenuIcons
					.getResourceId(7, -1)));

		}

		navMenuIcons.recycle();

		mDrawerList.setOnItemClickListener(new SlideMenuClickListener());

		// setting the nav drawer list adapter
		adapter = new NavDrawerListAdapter(getApplicationContext(),
				navDrawerItems);
		mDrawerList.setAdapter(adapter);

		// enabling action bar app icon and behaving it as toggle button
		getActionBar().setDisplayHomeAsUpEnabled(true);
		getActionBar().setHomeButtonEnabled(true);

		mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
				R.drawable.ic_drawer, // nav menu toggle icon
				R.string.app_name, // nav drawer open - description for
									// accessibility
				R.string.app_name // nav drawer close - description for
									// accessibility
		) {
			public void onDrawerClosed(View view) {
				getActionBar().setTitle(mTitle);
				// calling onPrepareOptionsMenu() to show action bar icons
				invalidateOptionsMenu();
			}

			public void onDrawerOpened(View drawerView) {
				getActionBar().setTitle(mDrawerTitle);
				// calling onPrepareOptionsMenu() to hide action bar icons
				invalidateOptionsMenu();
			}
		};
		mDrawerLayout.setDrawerListener(mDrawerToggle);

		if (savedInstanceState == null) {
			// on first time display view for first nav item
			displayView(0);
		}
	}

	/**
	 * Slide menu item click listener
	 * */
	private class SlideMenuClickListener implements
			ListView.OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			// display view for selected nav drawer item
			displayView(position);

		}
	}

	/*
	 * @Override public boolean onCreateOptionsMenu(Menu menu) {
	 * getMenuInflater().inflate(R.menu.home, menu); return true; }
	 */@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// toggle nav drawer on selecting action bar app icon/title
		if (mDrawerToggle.onOptionsItemSelected(item)) {
			return true;
		}
		/*
		 * // Handle action bar actions click switch (item.getItemId()) { case
		 * R.id.action_logout:
		 * 
		 * 
		 * return true; case R.id.action_login:
		 * 
		 * return true; default: return super.onOptionsItemSelected(item); }
		 */return true;

	}

	/**
	 * Diplaying fragment view for selected nav drawer list item
	 * */
	@SuppressLint("NewApi")
	public void displayView(int position) {
		// update the main content by replacing fragments

		Fragment fragment = null;
		UserMaster um = new UserMaster();
		DatabaseSupport db = new DatabaseSupport(getApplicationContext());
		um = db.getUserMasterSingleRow(
				CardInfo.getUserId(getApplicationContext())).get(0);
		/*String strRoleType = db.getRoleSingleRow(um.getiRoleId())
				.getStrRoleType();*/
		List<TabletRoleFunctions> trFunList = db.getTabletRoleFunctionsRowByRoleId(um.getiRoleId());
		String strRoleType ="Crew";
		if(trFunList != null && trFunList.size() > 0){
			strRoleType = trFunList.get(0).getStrFunctionCode();
		}

		if (strRoleType != null && strRoleType.equalsIgnoreCase("Validator")) {
			switch (position) {
			case 0:
				fragment = new Main();
				break;
			case 1:
				fragment = new PowerUp();
				break;
			case 2:
				fragment = new LeadershipPowerUp();
				break;
			case 3:
				fragment = new TurboPowerUp();
				break;
			case 4:
				fragment = new PowerDown();
				break;
			case 5:
				fragment = new Validator();
				break;
			case 6:
				fragment = new Help();
				break;
			case 7:
				fragment = new Exit();
				break;

			default:
				break;
			}
		} else if (strRoleType != null && strRoleType.equalsIgnoreCase("Crew")) {
			switch (position) {
			case 0:
				fragment = new Main();
				break;
			case 1:
				fragment = new PowerUp();
				break;
			case 2:
				fragment = new TurboPowerUp();
				break;
			case 3:
				fragment = new PowerDown();
				break;
			case 4:
				fragment = new Help();
				break;
			case 5:
				fragment = new Exit();
				break;
			default:
				break;
			}

		}
		if (fragment != null) {

			FragmentManager fragmentManager = getFragmentManager();
			fragmentManager.beginTransaction()
					.replace(R.id.frame_container, fragment)
					.addToBackStack(null).commit();

			// update selected item and title, then close the drawer
			mDrawerList.setItemChecked(position, true);
			pos = position;
			mDrawerList.setSelection(position);
			setTitle(navMenuTitles[position]);
			mDrawerLayout.closeDrawer(mDrawerList);
		} else {
			// error in creating fragment
			Log.e("MainActivity", "Error in creating fragment");
		}
	}

	@Override
	public void setTitle(CharSequence title) {
		mTitle = title;
		getActionBar().setTitle(mTitle);
	}

	/**
	 * When using the ActionBarDrawerToggle, you must call it during
	 * onPostCreate() and onConfigurationChanged()...
	 */

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		// Sync the toggle state after onRestoreInstanceState has occurred.
		mDrawerToggle.syncState();
	}

	private int backFlag = 0;

	/*
	 * @Override public void onBackPressed() { // TODO Auto-generated method
	 * stub if (getFragmentManager().getBackStackEntryCount() == 1) { Intent i =
	 * new Intent(PowerPlusMain.this, PowerPlusMain.class); startActivity(i);
	 * this.finish(); } else {
	 * 
	 * displayView(0); if(backFlag > 0){ Intent i = new
	 * Intent(PowerPlusMain.this, PowerPlusMain.class); startActivity(i);
	 * this.finish(); } backFlag++; // getFragmentManager().popBackStack(); }
	 * 
	 * 
	 * }
	 */
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		super.onBackPressed();
		/*
		 * setTitle(navMenuTitles[pos]);
		 * 
		 * Intent i = new Intent(PowerPlusMain.this , PowerUp.class);
		 * 
		 * startActivity(i);
		 */
		this.finish();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		// Pass any configuration change to the drawer toggls
		mDrawerToggle.onConfigurationChanged(newConfig);
	}

}