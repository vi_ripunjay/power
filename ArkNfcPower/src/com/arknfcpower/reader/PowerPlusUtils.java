package com.arknfcpower.reader;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import android.content.Context;

public class PowerPlusUtils {
	
	public Context context;
	
	public PowerPlusUtils() {
		super();
	}
	
	
	public PowerPlusUtils(Context context) {
		super();
		this.context = context;
	}



	/**
	 * @author Ripunjay shukla
	 * below code for get date range according to pattern
	 * @return
	 */
	public static Pair getDateRangeOfMonth() {
	    Date begining, end;

	    {
	        Calendar calendar = getCalendarForNow();
	        calendar.set(Calendar.DAY_OF_MONTH,
	                calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
	        setTimeToBeginningOfDay(calendar);
	        begining = calendar.getTime();
	    }

	    {
	        Calendar calendar = getCalendarForNow();
	        calendar.set(Calendar.DAY_OF_MONTH,
	                calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
	        setTimeToEndofDay(calendar);
	        end = calendar.getTime();
	    }

	    return new Pair(begining, end);
	}
	
	/**
	 * @author Ripunjay shukla
	 * below code for get date range according to pattern
	 * @return
	 */
	public static Pair getDateRangeOfYear() {
	    Date begining, end;

	    {
	        Calendar calendar = getCalendarForNow();
	        calendar.set(Calendar.DAY_OF_YEAR,
	                calendar.getActualMinimum(Calendar.DAY_OF_YEAR));
	        setTimeToBeginningOfDay(calendar);
	        begining = calendar.getTime();
	    }

	    {
	        Calendar calendar = getCalendarForNow();
	        calendar.set(Calendar.DAY_OF_YEAR,
	                calendar.getActualMaximum(Calendar.DAY_OF_YEAR));
	        setTimeToEndofDay(calendar);
	        end = calendar.getTime();
	    }

	    return new Pair(begining, end);
	}
	
	/**
	 * @author Ripunjay shukla
	 * below code for get date range according to pattern
	 * @return
	 */
	public static Pair getDateRangeOfweek() {
	    Date begining, end;

	    {
	        Calendar calendar = getCalendarForNow();
	        calendar.set(Calendar.DAY_OF_WEEK,
	                calendar.getActualMinimum(Calendar.DAY_OF_WEEK));
	        setTimeToBeginningOfDay(calendar);
	        begining = calendar.getTime();
	    }

	    {
	        Calendar calendar = getCalendarForNow();
	        calendar.set(Calendar.DAY_OF_WEEK,
	                calendar.getActualMaximum(Calendar.DAY_OF_WEEK));
	        setTimeToEndofDay(calendar);
	        end = calendar.getTime();
	    }

	    return new Pair(begining, end);
	}

	private static Calendar getCalendarForNow() {
	    Calendar calendar = GregorianCalendar.getInstance();
	    calendar.setTime(new Date());
	    return calendar;
	}

	private static void setTimeToBeginningOfDay(Calendar calendar) {
	    calendar.set(Calendar.HOUR_OF_DAY, 0);
	    calendar.set(Calendar.MINUTE, 0);
	    calendar.set(Calendar.SECOND, 0);
	    calendar.set(Calendar.MILLISECOND, 0);
	}

	private static void setTimeToEndofDay(Calendar calendar) {
	    calendar.set(Calendar.HOUR_OF_DAY, 23);
	    calendar.set(Calendar.MINUTE, 59);
	    calendar.set(Calendar.SECOND, 59);
	    calendar.set(Calendar.MILLISECOND, 999);
	}

}
