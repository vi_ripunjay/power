package com.arknfcpower;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
 

public class TrainingExit extends Fragment {
	ListView lv;

	public TrainingExit() {
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.exit, container,
				false);

        getActivity().finish();		
		
		return rootView;
	}
	
	

}
