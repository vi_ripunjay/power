package com.arknfcpower;

import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.Settings;
import android.util.Log;

import com.arknfcpower.db.DatabaseSupport;
import com.arknfcpower.model.UserMaster;
import com.arknfcpower.model.UserNfcData;
import com.arknfcpower.model.UserServiceTermRoleData;

/**
 * An {@link Activity} which handles a broadcast of a new tag that the device just discovered.
 */
public class WelcomeTrainingPlus extends Activity {

    private static final DateFormat TIME_FORMAT = SimpleDateFormat.getDateTimeInstance();
//    private LinearLayout mTagContent;

    private NfcAdapter mAdapter;
    private PendingIntent mPendingIntent;
    private NdefMessage mNdefPushMessage;
    long ids;
    private AlertDialog mDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
	    ActionBar ab = getActionBar(); 
        ColorDrawable colorDrawable = new ColorDrawable(Color.parseColor("#009bff"));     
        ab.setBackgroundDrawable(colorDrawable);
        ab.setDisplayShowHomeEnabled(false);
        ab.hide();
        
   /*     ab.setDisplayOptions(ab.getDisplayOptions()
                | ActionBar.DISPLAY_SHOW_CUSTOM);
        ImageView imageView = new ImageView(ab.getThemedContext());
        imageView.setScaleType(ImageView.ScaleType.CENTER);
        imageView.setImageResource(R.drawable.nyk_logo);
        ActionBar.LayoutParams layoutParams = new ActionBar.LayoutParams(
                ActionBar.LayoutParams.WRAP_CONTENT,
                ActionBar.LayoutParams.WRAP_CONTENT, Gravity.RIGHT
                        | Gravity.CENTER_VERTICAL);
        layoutParams.rightMargin = 5;
        imageView.setLayoutParams(layoutParams);
        ab.setCustomView(imageView);

*/
        setContentView(R.layout.activity_main);
  //      mTagContent = (LinearLayout) findViewById(R.id.list);
        
        if(CardInfo.getTag(getApplicationContext()).equalsIgnoreCase("first"))
    	{
        	CardInfo.setTag(getApplicationContext(), "second");
    	}
    	
        
        
        resolveIntent(getIntent());

        mDialog = new AlertDialog.Builder(this).setNeutralButton("Ok", null).create();

        mAdapter = NfcAdapter.getDefaultAdapter(this);
        if (mAdapter == null) {
            showMessage(R.string.error, R.string.no_nfc);
            finish();
            return;
        }

        mPendingIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
        mNdefPushMessage = new NdefMessage(new NdefRecord[] { newTextRecord(
                "Message from NFC Reader :-)", Locale.ENGLISH, true) });
    }

    private void showMessage(int title, int message) {
        mDialog.setTitle(title);
        mDialog.setMessage(getText(message));
        mDialog.show();
    }

    private NdefRecord newTextRecord(String text, Locale locale, boolean encodeInUtf8) {
        byte[] langBytes = locale.getLanguage().getBytes(Charset.forName("US-ASCII"));

        Charset utfEncoding = encodeInUtf8 ? Charset.forName("UTF-8") : Charset.forName("UTF-16");
        byte[] textBytes = text.getBytes(utfEncoding);

        int utfBit = encodeInUtf8 ? 0 : (1 << 7);
        char status = (char) (utfBit + langBytes.length);

        byte[] data = new byte[1 + langBytes.length + textBytes.length];
        data[0] = (byte) status;
        System.arraycopy(langBytes, 0, data, 1, langBytes.length);
        System.arraycopy(textBytes, 0, data, 1 + langBytes.length, textBytes.length);

        return new NdefRecord(NdefRecord.TNF_WELL_KNOWN, NdefRecord.RTD_TEXT, new byte[0], data);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (mAdapter != null) {
            if (!mAdapter.isEnabled()) {
                showWirelessSettingsDialog();
            }
            mAdapter.enableForegroundDispatch(this, mPendingIntent, null, null);
            mAdapter.enableForegroundNdefPush(this, mNdefPushMessage);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mAdapter != null) {
            mAdapter.disableForegroundDispatch(this);
            mAdapter.disableForegroundNdefPush(this);
        }
    }

    private void showWirelessSettingsDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.nfc_disabled);
        builder.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                Intent intent = new Intent(Settings.ACTION_WIRELESS_SETTINGS);
                startActivity(intent);
            }
        });
        builder.setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialogInterface, int i) {
                finish();
            }
        });
        builder.create().show();
        return;
    }
    
    private void makeEntryInUsrNFC(String userId, String rank, String cardType) {
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		Date curDate = new Date();
		DatabaseSupport db = new DatabaseSupport(this);
		
		List<UserServiceTermRoleData> serviceTermRoleData = new ArrayList<UserServiceTermRoleData>();
		serviceTermRoleData = db.getUserServiceTermRoleRowBiId(
				userId,
				df.format(curDate));
		if (serviceTermRoleData != null
				&& serviceTermRoleData.size() > 0) {
		
			String roleName = db
					.getRoleName(serviceTermRoleData.get(0)
							.getiRoleId());
			
			if(userId.equalsIgnoreCase(serviceTermRoleData.get(0).getIuserId()) && rank.equalsIgnoreCase(roleName)){
					Integer cardTypeOnCard = null;
					Integer cardTypeOnDB = serviceTermRoleData.get(0).getField2();
					try {
						cardTypeOnCard = Integer
								.parseInt(rank);
					} catch (Exception e) {
						e.printStackTrace();
					}
					
					if(cardTypeOnCard == null ||  cardTypeOnDB== null || cardTypeOnCard.intValue() == cardTypeOnDB.intValue()){
						
						if(CardInfo.getShipId(getApplicationContext()) == null || CardInfo.getShipId(getApplicationContext()).equalsIgnoreCase("1")){
							CardInfo.setShipId(getApplicationContext(), serviceTermRoleData.get(0).getiShipId());
							CardInfo.setTenantId(getApplicationContext(), serviceTermRoleData.get(0).getiTenantId());
						}
							
						/*db.addUserNFCRow(new UserNfcData(CardInfo
								.getId(getApplicationContext()), "User", userId, "S1", "1",
								"0", df.format(curDate), df.format(curDate), CardInfo
										.getTenantId(getApplicationContext()), CardInfo
										.getShipId(getApplicationContext())));*/
						/**
						 * Above code comment due to card id use as primary key which is duplicate.
						 * So change pk format and cardid use in iuserservicetermid 
						 */
						String pk = CardInfo.getId(getApplicationContext())+"_"+curDate.getTime();
						db.addUserNFCRow(new UserNfcData(pk, "User", userId, CardInfo
								.getId(getApplicationContext()), "1",
								"0", df.format(curDate), df.format(curDate), CardInfo
										.getTenantId(getApplicationContext()), CardInfo
										.getShipId(getApplicationContext())));
					}
			}
		}
	}
    
    private void resolveIntent(Intent intent) {
        String action = intent.getAction();
        if (NfcAdapter.ACTION_TAG_DISCOVERED.equals(action)
                || NfcAdapter.ACTION_TECH_DISCOVERED.equals(action)
                || NfcAdapter.ACTION_NDEF_DISCOVERED.equals(action)) {
            Parcelable[] rawMsgs = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
            NdefMessage[] msgs;
            if (rawMsgs != null) {
            	// IF TAG IS NOT NULL | 
                Parcelable tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
                Tag tag1 = (Tag) tag;
                byte[] id = tag1.getId();
                ids = getDec(id);
                CardInfo.setId(getApplicationContext(), String.valueOf(ids));
       //         Toast.makeText(getApplicationContext(), "ID :" + ids, Toast.LENGTH_LONG).show();
                msgs = new NdefMessage[rawMsgs.length];
                for (int i = 0; i < rawMsgs.length; i++) {

                	msgs[i] = (NdefMessage) rawMsgs[i];
                }
               String m =  getMessage(msgs);
               
               
				if(m != null && m.startsWith("en")){
			    	//m = m.substring(3, m.length()-1);
					m = m.substring(3, m.length());
			    }
				
				
				
				String strRoleNameArr[] = null;
				Integer cardTypeOnCard = null;
				String userIdOnCard = "";
				if(m != null && !"".equals(m)){
					strRoleNameArr = m.split("#");
					userIdOnCard = strRoleNameArr[0];
				}
				
              
       //        Toast.makeText(getApplicationContext(), "CardInfo :" +CardInfo.getAdminId(getApplicationContext())  , Toast.LENGTH_LONG).show();
               System.out.println("Card Value : " + m);
               Log.i("Card Value :", m);
               
               if(!m.equalsIgnoreCase("ShipAdmin"))
               {
            	   
            	 DatabaseSupport db = new DatabaseSupport(this);
            	 ArrayList<UserNfcData> data  = db.getUserNFCSingleRowByUserId(userIdOnCard);
            	 
            	 if (data != null && data.size() > 0) {
 					if (strRoleNameArr!= null && strRoleNameArr.length >= 3) {
 						makeEntryInUsrNFC(strRoleNameArr[0],strRoleNameArr[1],strRoleNameArr[2]);
 					}
 					data = db.getUserNFCSingleRow(String
 							.valueOf(ids));
 				}
            	// System.out.println("Data : " + data);
            	// System.out.println("Data Size : " + data.size());
 
            	 if (userIdOnCard != null && !"".equals(userIdOnCard))
            	 {
            		 
        			Date curDate = new Date();
					DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
					List<UserServiceTermRoleData> serviceTermRoleData = new ArrayList<UserServiceTermRoleData>();
					serviceTermRoleData = db.getUserServiceTermRoleRowBiId(userIdOnCard,format.format(curDate));
					if (serviceTermRoleData != null
							&& serviceTermRoleData.size() > 0) {						
						
						
						String roleName = db
								.getRoleName(serviceTermRoleData.get(0)
										.getiRoleId());
						
						String userIdInDB = serviceTermRoleData.get(0)
								.getIuserId();
						//String userIdOnCard = strRoleNameArr[0];

						if (roleName != null && strRoleNameArr != null
								&& strRoleNameArr.length >= 3) {
							

							if (userIdInDB.equals(userIdOnCard)
									&& roleName.trim().equalsIgnoreCase(
											strRoleNameArr[1].trim())) {	
            		 
            		 
			            		 //	Toast.makeText(getApplicationContext(), "Msg2 :" + m, Toast.LENGTH_LONG).show();   
			            		 String uid = "";
			            		 String name = "";
			            		 //for(int i = 0; i < data.size(); i++)
			            		 //{
			            			 uid = userIdOnCard;
			            			 CardInfo.setUserId(getApplicationContext(), uid);
			            		     CardInfo.setUserForTurbo(getApplicationContext(), uid);
			            		     
			 						CardInfo.setTenantId(getApplicationContext(),(serviceTermRoleData.get(0).getiTenantId() != null ? serviceTermRoleData.get(0).getiTenantId() : "1"));
									CardInfo.setShipId(getApplicationContext(), (serviceTermRoleData.get(0).getiShipId() != null ? serviceTermRoleData.get(0).getiShipId() : "1"));
			            			 //System.out.println("Uid :" + uid);
			            		 //}
			            		 ArrayList<UserMaster> data2  = db.getUserMasterSingleRow(uid);
			            		
			            		 for(int i = 0; i < data2.size(); i++)
			            		 {
			            			 name = data2.get(i).getStrFirstName();
			            			 //System.out.println("name :" + name);
			
			            		 }
			            		 
			            		 Intent i = new Intent(this,  TrainingPlus.class); 
			            		 i.putExtra("name", name);
			            		 startActivity(i);
			            		 finish();
							 }
							 else{
								 /*ArrayList<UserMaster> data2 = db
											.getUserMasterSingleRow(data.get(0).getiUserId());
									String strName="";
									
									for (int i = 0; i < data2.size(); i++) {
										strName = data2.get(i).getStrFirstName()+" "+data2.get(i).getStrLastName();
										//System.out.println("name :" + name);

									}
									Toast.makeText(getApplicationContext(),
											strName+" your rank has been changed, login denied, need to login with fresh card.",
											4000).show();*/
									
									Intent i = new Intent(this, AuthActivity.class);
									i.putExtra("userId", userIdOnCard);
									i.putExtra("nameAndRole", strRoleNameArr[3]
											+ " / " + strRoleNameArr[1]);
									i.putExtra("strAuthMessagePrv", "This card is registered for ");
									i.putExtra("strAuthMessage", ". Please register with  a fresh card if you have been promoted.");
									startActivity(i);
									finish();
							 }
						 }
						 else{
							 /*ArrayList<UserMaster> data2 = db
										.getUserMasterSingleRow(data.get(0).getiUserId());
								String strName="";
								for (int i = 0; i < data2.size(); i++) {
									strName = data2.get(i).getStrFirstName()+" "+data2.get(i).getStrLastName();
									//System.out.println("name :" + name);

								}
								Toast.makeText(getApplicationContext(),
										strName+"  your rank has been changed, login denied, need to login with fresh card.",
										4000).show();*/
								
								Intent i = new Intent(this, AuthActivity.class);
								i.putExtra("userId", userIdOnCard);
								i.putExtra("nameAndRole", strRoleNameArr[3]
										+ " / " + strRoleNameArr[1]);
								i.putExtra("strAuthMessagePrv", "This card is registered for ");
								i.putExtra("strAuthMessage", ". Please register with  a fresh card if you have been promoted.");
								startActivity(i);
								finish();
							 }
					}
            		 else 
            		 {
            			 /*ArrayList<UserMaster> data2 = db
								.getUserMasterSingleRow(data.get(0).getiUserId());
						String strName="";
						for (int i = 0; i < data2.size(); i++) {
							strName = data2.get(i).getStrFirstName()+" "+data2.get(i).getStrLastName();
							//System.out.println("name :" + name);

						}
						Toast.makeText(getApplicationContext(),
								strName+" service term has expired, login denied",
								2000).show();*/
            			 
            			String authMessagePrv = "There is no valid service term for ";
						String authMessage = " on this device. In case this crew is on board please update The Ark system by editing his service term or adding him as a new crew. Synchronize this device after such an update to login with this card.";

            			 
						Intent i = new Intent(this, AuthActivity.class);
						i.putExtra("userId", userIdOnCard);
						i.putExtra("nameAndRole", strRoleNameArr[3]+ " / " + strRoleNameArr[1]);
						i.putExtra("strAuthMessagePrv", authMessagePrv);
						i.putExtra("strAuthMessage", authMessage);
						startActivity(i);
						finish();
				}
            	 }
            	 else{
 					/*Toast.makeText(getApplicationContext(), "You are not authorize for this screen.",
 							Toast.LENGTH_LONG).show();*/
 					
 					Intent i = new Intent(this, AuthActivity.class);
 					i.putExtra("userId", "");
 					i.putExtra("nameAndRole", "");
 					i.putExtra("strAuthMessagePrv", "");
 					i.putExtra("strAuthMessage", "This card needs to be initialized through the card registration process before being used.");
 					startActivity(i);
 					finish();
 				}
            	
               }     
               else{
					/*Toast.makeText(getApplicationContext(), "You are not authorize for this screen.",
							Toast.LENGTH_LONG).show();*/
					
					Intent i = new Intent(this, AuthActivity.class);
					i.putExtra("userId", "");
					i.putExtra("nameAndRole", "");
					i.putExtra("strAuthMessagePrv", "");
					i.putExtra("strAuthMessage", "You are not authorized to view this screen. Please login with a different card to access this screen.");
					startActivity(i);
					finish();
				}
               
                
            }
            else if(rawMsgs == null){
				/*Toast.makeText(getApplicationContext(), "You are not authorize for this screen.",
						Toast.LENGTH_LONG).show();*/
            	
				Intent i = new Intent(this, AuthActivity.class);
				i.putExtra("userId", "");
				i.putExtra("nameAndRole", "");
				i.putExtra("strAuthMessagePrv", "");
				i.putExtra("strAuthMessage", "This card needs to be initialized through the card registration process before being used.");
				startActivity(i);
				finish();
			}
        }
    }

    private String getMessage(NdefMessage[] msgs) {
		// TODO Auto-generated method stub
    	String tags = "";
    	if (msgs == null || msgs.length == 0) {
            return "";
        }
    	
    	NdefRecord[] records = msgs[0].getRecords();
    	for (final NdefRecord record : records) {
    		       tags = new String(record.getPayload());
   		   }
          return tags.toString();
	}


    private long getDec(byte[] bytes) {
        long result = 0;
        long factor = 1;
        for (int i = 0; i < bytes.length; ++i) {
            long value = bytes[i] & 0xffl;
            result += value * factor;
            factor *= 256l;
        }
        return result;
    }


 /*   void buildTagViews(NdefMessage[] msgs) {
        if (msgs == null || msgs.length == 0) {
            return;
        }
        LayoutInflater inflater = LayoutInflater.from(this);
        LinearLayout content = mTagContent;

        // Parse the first message in the list
        // Build views for all of the sub records
        Date now = new Date();
        List<ParsedNdefRecord> records = NdefMessageParser.parse(msgs[0]);
        final int size = records.size();
        for (int i = 0; i < size; i++) {
            TextView timeView = new TextView(this);
            timeView.setText(TIME_FORMAT.format(now));
            content.addView(timeView, 0);
            ParsedNdefRecord record = records.get(i);
            content.addView(record.getView(this, inflater, content, i), 1 + i);
            content.addView(inflater.inflate(R.layout.tag_divider, content, false), 2 + i);
        }
    }

 

*/
    @Override
    public void onBackPressed() {
    	// TODO Auto-generated method stub
    	super.onBackPressed();
    }
    
    
    @Override
    public void onNewIntent(Intent intent1) {
        setIntent(intent1);
        resolveIntent(intent1);
    }
}
