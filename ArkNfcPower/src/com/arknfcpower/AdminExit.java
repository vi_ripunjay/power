package com.arknfcpower;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

public class AdminExit extends Fragment {
	ListView lv;

	public AdminExit() {
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.exit, container,
				false);

        getActivity().finish();		
		
		return rootView;
	}
	
	

}
