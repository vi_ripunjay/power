package com.arknfcpower;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;

public class CardInfo {

	public static final String setId = "00";
	public static final String setUserRole = "asd";
	public static final String setAdminId = "Admin";
	public static final String setUserId = "Select User";
	public static final String setPosition = "0";
	public static final String setTitle = "Select User";

	public static final String setUserForTurbo = "Self";

	public static final String setTag = "first";

	public static final String setShipId = "shipid";
	public static final String setTenantId = "tenantid";

	public static String getTag(Context context) {
		return ((SharedPreferences) PreferenceManager
				.getDefaultSharedPreferences(context))
				.getString(setTag, setTag);
	}

	public static void setTag(Context applicationContext, String stringconvert) {
		// TODO Auto-generated method stub
		SharedPreferences msharedPreferences = (SharedPreferences) PreferenceManager
				.getDefaultSharedPreferences(applicationContext);
		Editor edit = msharedPreferences.edit();
		edit.putString(setTag, stringconvert);
		edit.commit();

	}

	public static String getId(Context context) {
		return ((SharedPreferences) PreferenceManager
				.getDefaultSharedPreferences(context)).getString(setId, null);
	}

	public static void setId(Context applicationContext, String stringconvert) {
		// TODO Auto-generated method stub
		SharedPreferences msharedPreferences = (SharedPreferences) PreferenceManager
				.getDefaultSharedPreferences(applicationContext);
		Editor edit = msharedPreferences.edit();
		edit.putString(setId, stringconvert);
		edit.commit();

	}

	public static String getShipId(Context context) {
		return ((SharedPreferences) PreferenceManager
				.getDefaultSharedPreferences(context)).getString(setShipId,
				null);
	}

	public static void setShipId(Context applicationContext,
			String stringconvert) {
		// TODO Auto-generated method stub
		SharedPreferences msharedPreferences = (SharedPreferences) PreferenceManager
				.getDefaultSharedPreferences(applicationContext);
		Editor edit = msharedPreferences.edit();
		edit.putString(setShipId, stringconvert);
		edit.commit();

	}

	public static String getTenantId(Context context) {
		return ((SharedPreferences) PreferenceManager
				.getDefaultSharedPreferences(context)).getString(setTenantId,
				null);
	}

	public static void setTenantId(Context applicationContext,
			String stringconvert) {
		// TODO Auto-generated method stub
		SharedPreferences msharedPreferences = (SharedPreferences) PreferenceManager
				.getDefaultSharedPreferences(applicationContext);
		Editor edit = msharedPreferences.edit();
		edit.putString(setTenantId, stringconvert);
		edit.commit();

	}

	public static String getUserRole(Context context) {
		return ((SharedPreferences) PreferenceManager
				.getDefaultSharedPreferences(context)).getString(setUserRole,
				null);
	}

	public static void setUserRole(Context applicationContext,
			String stringconvert) {
		// TODO Auto-generated method stub
		SharedPreferences msharedPreferences = (SharedPreferences) PreferenceManager
				.getDefaultSharedPreferences(applicationContext);
		Editor edit = msharedPreferences.edit();
		edit.putString(setUserRole, stringconvert);
		edit.commit();

	}

	public static String getAdminId(Context context) {
		return ((SharedPreferences) PreferenceManager
				.getDefaultSharedPreferences(context)).getString(setAdminId,
				setAdminId);
	}

	public static void setAdminId(Context applicationContext,
			String stringconvert) {
		// TODO Auto-generated method stub
		SharedPreferences msharedPreferences = (SharedPreferences) PreferenceManager
				.getDefaultSharedPreferences(applicationContext);
		Editor edit = msharedPreferences.edit();
		edit.putString(setAdminId, stringconvert);
		edit.commit();

	}

	public static String getUserId(Context context) {
		return ((SharedPreferences) PreferenceManager
				.getDefaultSharedPreferences(context)).getString(setUserId,
				setUserId);
	}

	public static void setUserId(Context applicationContext,
			String stringconvert) {
		// TODO Auto-generated method stub
		SharedPreferences msharedPreferences = (SharedPreferences) PreferenceManager
				.getDefaultSharedPreferences(applicationContext);
		Editor edit = msharedPreferences.edit();
		edit.putString(setUserId, stringconvert);
		edit.commit();

	}

	public static String getPosition(Context context) {
		return ((SharedPreferences) PreferenceManager
				.getDefaultSharedPreferences(context)).getString(setPosition,
				setPosition);
	}

	public static void setPosition(Context applicationContext,
			String stringconvert) {
		// TODO Auto-generated method stub
		SharedPreferences msharedPreferences = (SharedPreferences) PreferenceManager
				.getDefaultSharedPreferences(applicationContext);
		Editor edit = msharedPreferences.edit();
		edit.putString(setPosition, stringconvert);
		edit.commit();

	}

	public static String getTitle(Context context) {
		return ((SharedPreferences) PreferenceManager
				.getDefaultSharedPreferences(context)).getString(setTitle,
				setTitle);
	}

	public static void setTitle(Context applicationContext, String stringconvert) {
		// TODO Auto-generated method stub
		SharedPreferences msharedPreferences = (SharedPreferences) PreferenceManager
				.getDefaultSharedPreferences(applicationContext);
		Editor edit = msharedPreferences.edit();
		edit.putString(setTitle, stringconvert);
		edit.commit();

	}

	public static String getUserForTurbo(Context context) {
		return ((SharedPreferences) PreferenceManager
				.getDefaultSharedPreferences(context)).getString(
				setUserForTurbo, setUserForTurbo);
	}

	public static void setUserForTurbo(Context applicationContext,
			String stringconvert) {
		// TODO Auto-generated method stub
		SharedPreferences msharedPreferences = (SharedPreferences) PreferenceManager
				.getDefaultSharedPreferences(applicationContext);
		Editor edit = msharedPreferences.edit();
		edit.putString(setUserForTurbo, stringconvert);
		edit.commit();

	}

}
