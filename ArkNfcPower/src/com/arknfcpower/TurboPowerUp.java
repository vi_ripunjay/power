package com.arknfcpower;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Fragment;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.arknfc.adapter.SpinnerAdapter;
import com.arknfc.adapter.TurboScoreAdapter;
import com.arknfcpower.db.DatabaseSupport;
import com.arknfcpower.model.PowerPlusCategory;
import com.arknfcpower.model.PowerPlusRoleRankCategory;
import com.arknfcpower.model.PowerPlusTask;
import com.arknfcpower.model.PowerPlusTransaction;
import com.arknfcpower.model.UserMaster;
import com.arknfcpower.model.UserSelectItem;
import com.arknfcpower.model.UserServiceTermRoleData;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
@SuppressLint("NewApi")
public class TurboPowerUp extends Fragment {

	ListView lv;
	Spinner userDropDown;
	ImageView imageView;

	TextView listItemText;
	TextView textView1;
	PowerPlusCategory ppCat;
	public static Map<String, String> turboPowerMap;

	public TurboPowerUp() {
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.fragment_turbo_power_up,
				container, false);
		lv = (ListView) rootView.findViewById(R.id.turbo_power_up);
		userDropDown = (Spinner) rootView.findViewById(R.id.spinner1);

		turboPowerMap = new HashMap<String, String>();
		CardInfo.setPosition(getActivity(), String.valueOf(0));

		/**
		 * getting user list for turbo
		 */
		DatabaseSupport db1 = new DatabaseSupport(getActivity());
		ArrayList<UserMaster> list1 = db1.getUserMasterRow();
		ArrayList<UserSelectItem> userlist = new ArrayList<UserSelectItem>();
		// userlist.add(new UserSelectItem("Self", "Self","Self"));
		int enter = 0;
		Date curDate = new Date();
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		for (int i = 0; i < list1.size(); i++) {

			if (!(list1.get(i).getiUserId().equalsIgnoreCase(CardInfo
					.getUserId(getActivity())))) {

				List<UserServiceTermRoleData> serviceTermRoleData = new ArrayList<UserServiceTermRoleData>();
				serviceTermRoleData = db1.getUserServiceTermRoleRowBiId(list1
						.get(i).getiUserId(), format.format(curDate));
				if (serviceTermRoleData != null
						&& serviceTermRoleData.size() > 0) {

					String roleid = list1.get(i).getiRoleId();
					String tenantId = list1.get(i).getiTenantId();
					String name = list1.get(i).getStrFirstName() + " "
							+ list1.get(i).getStrLastName() + " / "
							+ db1.getRoleName(roleid);
					userlist.add(new UserSelectItem(name, list1.get(i)
							.getiUserId(), tenantId));

					if (enter == 0) {
						CardInfo.setUserForTurbo(getActivity(), list1.get(i)
								.getiUserId());
					}
					enter++;
				}
			}

		}
		L.fv("Initial Selected user for turbo is  : "
				+ CardInfo.getUserForTurbo(getActivity()));
		db1.close();

		DatabaseSupport db = new DatabaseSupport(getActivity());

		List<PowerPlusCategory> ppcList = new ArrayList<PowerPlusCategory>();
		ppCat = new PowerPlusCategory();
		// ppcList = db.getPowerPlusCategoryRowByTitle("Turbo Power");
		ppcList = db.getPowerPlusCategoryRowByCatCode("turbopower");
		if (ppcList != null && ppcList.size() > 0) {
			ppCat = ppcList.get(0);
		}

		/*
		 * ArrayList<PowerPlusTask> list = (ArrayList<PowerPlusTask>)
		 * db.getPowerPlusTaskRow("0003");
		 * 
		 * DatabaseSupport db = new DatabaseSupport(getActivity());
		 * 
		 * ArrayList<PowerPlusTask> list = (ArrayList<PowerPlusTask>) db
		 * .getPowerPlusTaskRow(ppCat.getiPowerPlusCategoryId() != null ?
		 * ppCat.getiPowerPlusCategoryId() : "0003");
		 */

		List<PowerPlusRoleRankCategory> powerPlusRoleRankCategoryList = new ArrayList<PowerPlusRoleRankCategory>();
		ArrayList<UserMaster> umList = db.getUserMasterSingleRow(CardInfo
				.getUserForTurbo(getActivity()));
		powerPlusRoleRankCategoryList = db
				.getPowerPlusRoleRankCategoryRowByRoleId(umList != null
						&& umList.size() > 0 ? umList.get(0).getiRoleId() : "0");
		StringBuffer pprrcId = new StringBuffer("('5'");
		if (powerPlusRoleRankCategoryList != null
				&& powerPlusRoleRankCategoryList.size() > 0) {

			for (PowerPlusRoleRankCategory pprrc : powerPlusRoleRankCategoryList) {

				pprrcId.append(",'" + pprrc.getiPowerPlusRoleRankCategoryId()
						+ "'");

			}
		}
		pprrcId.append(")");

		ArrayList<PowerPlusTask> list = (ArrayList<PowerPlusTask>) db
				.getPowerPlusTaskRowByCategoryRole(
						(ppCat.getiPowerPlusCategoryId() != null ? ppCat
								.getiPowerPlusCategoryId() : "0003"), pprrcId
								.toString());

		/**
		 * Below method call for check turbo power task provided by login user
		 * for same date if provided then task should not be display. i.e.
		 * single task assign only once by login user to selected user.
		 */
		ArrayList<PowerPlusTask> ppTaskList = new ArrayList<PowerPlusTask>();
		if (list != null && list.size() > 0) {
			for (PowerPlusTask ppt : list) {

				List<PowerPlusTransaction> taskProvided = db
						.getPowerPlusTransactionByTaskIdLoginUserStatusAndTurboUserForToday(
								ppt.getiPowerPlusTaskId(),
								CardInfo.getUserId(getActivity()),
								CardInfo.getUserForTurbo(getActivity()),
								"Validated", format.format(curDate));
				if (taskProvided == null || taskProvided.size() == 0) {
					ppTaskList.add(ppt);
				}

			}
		}

		PowerPlusTask pptForInfo = new PowerPlusTask();
		pptForInfo.setStrTitle(getResources().getString(R.string.positivetext));

		/*
		 * list.add(0, pptForInfo); System.out.println("List :" +list);
		 * TurboScoreAdapter adapter = new
		 * TurboScoreAdapter(list,getActivity());
		 */
		ppTaskList.add(0, pptForInfo);
		System.out.println("List :" + ppTaskList);
		TurboScoreAdapter adapter = new TurboScoreAdapter(ppTaskList,
				getActivity());

		// handle listview and assign adapter

		lv.setAdapter(adapter);
		db.close();
		lv.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {

				// ListView Clicked item index
				int itemPosition = position;

				imageView = ((ImageView) view.findViewById(R.id.select_info));

				listItemText = ((TextView) view
						.findViewById(R.id.list_item_string));
				textView1 = ((TextView) view.findViewById(R.id.textView1));

				if (position > 0) {
					DatabaseSupport db = new DatabaseSupport(getActivity());

					// ListView Clicked item value
					PowerPlusTask itemValue = (PowerPlusTask) lv
							.getItemAtPosition(position);
					Date currentDate = new Date();
					DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
					ArrayList<UserMaster> data = db
							.getUserMasterSingleRow(CardInfo
									.getUserForTurbo(getActivity()));
					UserMaster userMaster = new UserMaster();
					UserServiceTermRoleData userServiceTermRoleData = new UserServiceTermRoleData();
					if (data != null && data.size() > 0) {
						userMaster = data.get(0);

						Date curDate = new Date();
						DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
						List<UserServiceTermRoleData> serviceTermRoleData = new ArrayList<UserServiceTermRoleData>();
						serviceTermRoleData = db.getUserServiceTermRoleRowBiId(
								userMaster.getiUserId(), format.format(curDate));
						if (serviceTermRoleData != null
								&& serviceTermRoleData.size() > 0) {

							userServiceTermRoleData = serviceTermRoleData
									.get(0);
						}
					}

					/*L.fv("Selected user for turbo is  : "
							+ userMaster.getStrFirstName() + " and id is "
							+ userMaster.getiUserId());
					L.fv("Check Turbo map is  : " + turboPowerMap);*/

					if (turboPowerMap != null
							&& turboPowerMap.get(itemValue
									.getiPowerPlusTaskId()) != null) {
						String strTransid = turboPowerMap.get(itemValue
								.getiPowerPlusTaskId());
						//L.fv("Turbo map is transaction id : " + strTransid);
						if (db.getScoreInEnterModeForValidateWithTrans(
								strTransid,
								CardInfo.getUserForTurbo(getActivity()),
								itemValue.getiPowerPlusTaskId(), "Validated") > 0) {

							//L.fv("Turbo power update as rejected  : "+ strTransid);

							PowerPlusTransaction ppt = new PowerPlusTransaction();
							ppt = db.getPowerPlusTransactionByTransId(
									strTransid,
									CardInfo.getUserForTurbo(getActivity()),
									itemValue.getiPowerPlusTaskId(),
									"Validated");
							ppt.setFlgIsDirty("1");
							ppt.setFlgStatus("Rejected");
							/**
							 * Set approver Id.
							 */
							ppt.setStrApproverId(CardInfo
									.getUserId(getActivity()));

							// L.fv("Approver id ppTran: "+ppt.getStrApproverId());

							db.UpdatePowerPlusTransactionRow(ppt);

							listItemText.setTextColor(getResources().getColor(
									R.color.black));
							textView1.setTextColor(getResources().getColor(
									R.color.black));
							textView1.setVisibility(View.VISIBLE);

							imageView.setVisibility(View.GONE);
							view.setBackgroundColor(Color.parseColor("#ffffff"));

						} else if (db.getScoreInEnterModeForValidateWithTrans(
								strTransid,
								CardInfo.getUserForTurbo(getActivity()),
								itemValue.getiPowerPlusTaskId(), "Rejected") > 0) {

							//L.fv("Turbo power validated again  : " + strTransid);

							PowerPlusTransaction ppt = new PowerPlusTransaction();
							ppt = db.getPowerPlusTransactionByTransId(
									strTransid,
									CardInfo.getUserForTurbo(getActivity()),
									itemValue.getiPowerPlusTaskId(), "Rejected");
							ppt.setFlgIsDirty("1");
							ppt.setFlgStatus("Validated");
							/**
							 * Set approver Id.
							 */
							ppt.setStrApproverId(CardInfo
									.getUserId(getActivity()));

							// L.fv("Approver id ppTran: "+ppt.getStrApproverId());

							db.UpdatePowerPlusTransactionRow(ppt);

							imageView.setVisibility(View.VISIBLE);
							imageView.setImageResource(R.drawable.select_icon);
							imageView.invalidate();
							textView1.setVisibility(View.VISIBLE);

							listItemText.setTextColor(getResources().getColor(
									R.color.white));
							textView1.setTextColor(getResources().getColor(
									R.color.white));

							view.setBackgroundColor(Color.parseColor("#06dc1a"));

						}
					} else {
						L.fv("Turbo enter for insert values  : ");
						String powerPlusTransactionId = CardInfo
								.getId(getActivity())
								+ "_"
								+ new Date().getTime();
						if (turboPowerMap == null) {
							turboPowerMap = new HashMap<String, String>();
						}
						//L.fv("Turbo power insert b4 map  : " + turboPowerMap);
						turboPowerMap.put(itemValue.getiPowerPlusTaskId(),
								powerPlusTransactionId);

						//L.fv("Turbo power after map  : " + turboPowerMap);

						db.addPowerPlusTransactionRow(new PowerPlusTransaction(
								powerPlusTransactionId,
								df.format(currentDate),
								df.format(currentDate),
								"0",
								"1",
								"1",
								"Validated",
								"1",
								itemValue.getPowerPoints(),
								"",
								"",
								CardInfo.getUserId(getActivity()),
								"",
								"",
								CardInfo.getUserId(getActivity()),
								CardInfo.getUserId(getActivity()),
								itemValue.getStrTitle(),
								itemValue.getiPowerPlusTaskId(),
								CardInfo.getShipId(getActivity()),
								CardInfo.getTenantId(getActivity()),
								userMaster.getiUserId(),
								"P",
								itemValue.getiPowerPlusCategoryId(),
								"",
								userMaster.getiRoleId(),
								userServiceTermRoleData.getIuserServiceTermId(),
								"0", userMaster.getStrFirstName()));

						imageView.setVisibility(View.VISIBLE);
						imageView.setImageResource(R.drawable.select_icon);
						imageView.invalidate();
						textView1.setVisibility(View.VISIBLE);

						listItemText.setTextColor(getResources().getColor(
								R.color.white));
						textView1.setTextColor(getResources().getColor(
								R.color.white));

						view.setBackgroundColor(Color.parseColor("#06dc1a"));

					}

					db.close();
				}

			}
		});

		/**
		 * UserList get from Above and drop dow build
		 */
		SpinnerAdapter spin = new SpinnerAdapter(userlist, getActivity());

		userDropDown.setPopupBackgroundResource(R.drawable.spinner);
		userDropDown.setAdapter(spin);
		userDropDown.setSelection(
				Integer.parseInt(CardInfo.getPosition(getActivity())), true);

		userDropDown.setOnItemSelectedListener(new OnItemSelectedListener() {

			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int position, long arg3) {

				/*
				 * Toast.makeText(getActivity(), "OnItemSelectedListener : " +
				 * userDropDown.getSelectedItem(), Toast.LENGTH_SHORT).show();
				 */
				turboPowerMap = new HashMap<String, String>();
				String message = "";
				String userId = "";
				UserSelectItem mydata;
				if (!(userDropDown.getSelectedItem() == null)) {
					mydata = (UserSelectItem) userDropDown.getSelectedItem();
					userId = mydata.getUserId();
					System.out.println("Value : " + userId);
				}
				/**
				 * setting user id in context for turbo
				 */
				CardInfo.setUserForTurbo(getActivity(), userId);
				String text = userDropDown.getSelectedItem().toString();

				if (text.equals("Self")) {
					//L.fv("Enter into self block  : " + turboPowerMap);
					DatabaseSupport db = new DatabaseSupport(getActivity());

					List<PowerPlusRoleRankCategory> powerPlusRoleRankCategoryList = new ArrayList<PowerPlusRoleRankCategory>();

					ArrayList<UserMaster> umList = db
							.getUserMasterSingleRow(CardInfo
									.getUserId(getActivity()));
					powerPlusRoleRankCategoryList = db
							.getPowerPlusRoleRankCategoryRowByRoleId(umList != null
									&& umList.size() > 0 ? umList.get(0)
									.getiRoleId() : "0");
					StringBuffer pprrcId = new StringBuffer("('5'");
					if (powerPlusRoleRankCategoryList != null
							&& powerPlusRoleRankCategoryList.size() > 0) {

						for (PowerPlusRoleRankCategory pprrc : powerPlusRoleRankCategoryList) {

							pprrcId.append(",'"
									+ pprrc.getiPowerPlusRoleRankCategoryId()
									+ "'");

						}
					}
					pprrcId.append(")");

					ArrayList<PowerPlusTask> list = (ArrayList<PowerPlusTask>) db.getPowerPlusTaskRowByCategoryRole(
							(ppCat.getiPowerPlusCategoryId() != null ? ppCat
									.getiPowerPlusCategoryId() : "0003"),
							pprrcId.toString());

					/**
					 * Below method call for check turbo power task provided by
					 * login user for same date if provided then task should not
					 * be display. i.e. single task assign only once by login
					 * user to selected user.
					 */
					ArrayList<PowerPlusTask> ppTaskList = new ArrayList<PowerPlusTask>();
					Date curDate = new Date();
					DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
					if (list != null && list.size() > 0) {
						for (PowerPlusTask ppt : list) {

							List<PowerPlusTransaction> taskProvided = db
									.getPowerPlusTransactionByTaskIdLoginUserStatusAndTurboUserForToday(
											ppt.getiPowerPlusTaskId(),
											CardInfo.getUserId(getActivity()),
											userId,
											"Validated", format.format(curDate));
							if (taskProvided == null
									|| taskProvided.size() == 0) {
								ppTaskList.add(ppt);
							}

						}
					}

					PowerPlusTask pptForInfo = new PowerPlusTask();
					pptForInfo.setStrTitle(getResources().getString(
							R.string.positivetext));

					/*
					 * list.add(0, pptForInfo); TurboScoreAdapter adapter = new
					 * TurboScoreAdapter(list,getActivity());
					 */

					ppTaskList.add(0, pptForInfo);
					TurboScoreAdapter adapter = new TurboScoreAdapter(
							ppTaskList, getActivity());

					// handle listview and assign adapter
					lv.setAdapter(adapter);
					db.close();
					CardInfo.setUserForTurbo(getActivity(),
							CardInfo.getUserId(getActivity()));
					/*
					 * Toast.makeText(getActivity(), "Please Select User",
					 * Toast.LENGTH_LONG).show();
					 */} else {
					//L.fv("Enter into else of self block  : " + turboPowerMap);
					DatabaseSupport db = new DatabaseSupport(getActivity());
					List<PowerPlusRoleRankCategory> powerPlusRoleRankCategoryList = new ArrayList<PowerPlusRoleRankCategory>();

					ArrayList<UserMaster> umList = db
							.getUserMasterSingleRow(userId);
					powerPlusRoleRankCategoryList = db
							.getPowerPlusRoleRankCategoryRowByRoleId(umList != null
									&& umList.size() > 0 ? umList.get(0)
									.getiRoleId() : "0");
					StringBuffer pprrcId = new StringBuffer("('5'");
					if (powerPlusRoleRankCategoryList != null
							&& powerPlusRoleRankCategoryList.size() > 0) {

						for (PowerPlusRoleRankCategory pprrc : powerPlusRoleRankCategoryList) {

							pprrcId.append(",'"
									+ pprrc.getiPowerPlusRoleRankCategoryId()
									+ "'");

						}
					}
					pprrcId.append(")");

					ArrayList<PowerPlusTask> list = (ArrayList<PowerPlusTask>) db.getPowerPlusTaskRowByCategoryRole(
							(ppCat.getiPowerPlusCategoryId() != null ? ppCat
									.getiPowerPlusCategoryId() : "0003"),
							pprrcId.toString());

					/**
					 * Below method call for check turbo power task provided by
					 * login user for same date if provided then task should not
					 * be display. i.e. single task assign only once by login
					 * user to selected user.
					 */
					ArrayList<PowerPlusTask> ppTaskList = new ArrayList<PowerPlusTask>();
					Date curDate = new Date();
					DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
					if (list != null && list.size() > 0) {
						for (PowerPlusTask ppt : list) {

							List<PowerPlusTransaction> taskProvided = db
									.getPowerPlusTransactionByTaskIdLoginUserStatusAndTurboUserForToday(
											ppt.getiPowerPlusTaskId(),
											CardInfo.getUserId(getActivity()),
											userId,
											"Validated", format.format(curDate));
							if (taskProvided == null
									|| taskProvided.size() == 0) {
								ppTaskList.add(ppt);
							}

						}
					}

					PowerPlusTask pptForInfo = new PowerPlusTask();
					pptForInfo.setStrTitle(getResources().getString(
							R.string.positivetext));

					/*
					 * list.add(0, pptForInfo); TurboScoreAdapter adapter = new
					 * TurboScoreAdapter(list,getActivity());
					 */

					ppTaskList.add(0, pptForInfo);
					TurboScoreAdapter adapter = new TurboScoreAdapter(
							ppTaskList, getActivity());

					// handle listview and assign adapter
					lv.setAdapter(adapter);
					db.close();
					// Make String here
					CardInfo.setUserForTurbo(getActivity(), userId);
					L.fv("user id for turbo power  : " + userId);
					System.out.println("Message : " + userId);

				}

				CardInfo.setPosition(getActivity(), String.valueOf(position));
				System.out.println("Message : " + userId);

			}

			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub

			}
		});

		return rootView;
	}
}
