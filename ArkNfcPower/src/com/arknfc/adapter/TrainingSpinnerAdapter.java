package com.arknfc.adapter;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.arknfcpower.R;
import com.arknfcpower.db.DatabaseSupport;
import com.arknfcpower.model.TrainingDataItem;
import com.arknfcpower.model.UserMaster;
import com.arknfcpower.model.UserSelectItem;

@SuppressLint("InflateParams")
public class TrainingSpinnerAdapter extends BaseAdapter implements ListAdapter {
	private ArrayList<TrainingDataItem> list = new ArrayList<TrainingDataItem>();
	private Context context;

	public TrainingSpinnerAdapter(ArrayList<TrainingDataItem> list2, Context context) {
		this.list = list2;
		this.context = context;
	}

	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public Object getItem(int pos) {
		return list.get(pos);
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		View view = convertView;
		if (view == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = inflater.inflate(R.layout.training_spinner_list, null);
		}

		// Handle TextView and display string from your list
		TextView spinnerUsername = (TextView) view
				.findViewById(R.id.trainingName);
 
		
		spinnerUsername.setText(list.get(position).getText());
		
		return view;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}
}