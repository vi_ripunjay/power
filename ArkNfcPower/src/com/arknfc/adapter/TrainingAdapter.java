package com.arknfc.adapter;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.arknfcpower.R;
import com.arknfcpower.db.DatabaseSupport;
import com.arknfcpower.model.UserMaster;

@SuppressLint("InflateParams")
public class TrainingAdapter extends BaseAdapter implements ListAdapter {
	private ArrayList<UserMaster> list = new ArrayList<UserMaster>();
	private Context context;
	String trainingTransactionId = "";

	public TrainingAdapter(ArrayList<UserMaster> list2, Context context,
			String trainingTransactionId) {
		this.list = list2;
		this.context = context;
		this.trainingTransactionId = trainingTransactionId;
	}

	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public Object getItem(int pos) {
		return list.get(pos);
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		View view = convertView;
		if (view == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = inflater.inflate(R.layout.training_list, null);
		}
		DatabaseSupport db = new DatabaseSupport(context);
		// Handle TextView and display string from your list
		LinearLayout sethight = (LinearLayout) view
				.findViewById(R.id.linearlayoutText);
		TextView name = (TextView) view.findViewById(R.id.strFirstNameLName);
		TextView roleName = (TextView) view.findViewById(R.id.roleName);
		ImageView select_icon = (ImageView) view.findViewById(R.id.select_icon);
		ImageView help = (ImageView) view.findViewById(R.id.help_letter_icon);
		LinearLayout selectLout = (LinearLayout) view.findViewById(R.id.selectLout);

		// TextView rank = (TextView)view.findViewById(R.id.strRanks);

		if (position == 0) {
			help.setVisibility(View.VISIBLE);
			roleName.setVisibility(View.GONE);
			select_icon.setVisibility(View.GONE);

			name.setText(list.get(position).getStrFirstName());

			SpannableString spanString = new SpannableString(list.get(position)
					.getStrFirstName());		
			spanString.setSpan(new StyleSpan(Typeface.ITALIC), 0,
					spanString.length(), 0);
			name.setText(spanString);

			name.setTextColor(context.getResources().getColor(R.color.black));
			view.setBackgroundColor(Color.parseColor("#ffffff"));

		}
		if (position > 0) {

			select_icon.setVisibility(View.GONE);
			help.setVisibility(View.GONE);
			roleName.setVisibility(View.VISIBLE);

			sethight.setMinimumHeight(70);

			// name.setMinHeight(60);
			// roleName.setMinHeight(60);

			// name.setMaxHeight(500);
			// roleName.setMaxHeight(500);

			String roleNameS = db.getRoleSingleRow(
					list.get(position).getiRoleId()).getStrRole();

			name.setText(list.get(position).getStrFirstName() + " "
					+ list.get(position).getStrLastName() + " ");

			roleName.setText(roleNameS);
			
			if (db.getTrainingAttendanceByTransactionAndUserId(
					trainingTransactionId, list.get(position).getiUserId(), "1")
					.size() > 0) {
				
				
			 	selectLout.setVisibility(View.VISIBLE);
			 	select_icon.setVisibility(View.VISIBLE);
			 	select_icon.setImageResource(R.drawable.select_icon);
			 	select_icon.invalidate();
				
				name.setTextColor(context.getResources().getColor(R.color.white));
				roleName.setTextColor(context.getResources().getColor(R.color.appcolor));

		        view.setBackgroundColor(Color.parseColor("#06dc1a"));		          
				
			} else {
				selectLout.setVisibility(View.VISIBLE);
			 	select_icon.setVisibility(View.VISIBLE);
			 	select_icon.setImageResource(R.drawable.select_icon);
			 	select_icon.invalidate();
				
				name.setTextColor(context.getResources().getColor(R.color.black));
				roleName.setTextColor(context.getResources().getColor(R.color.appcolor));

				view.setBackgroundColor(Color.parseColor("#ffffff"));
			}

		}
		return view;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}
}