package com.arknfc.adapter;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.arknfcpower.CardInfo;
import com.arknfcpower.R;
import com.arknfcpower.model.NavDrawerItem;

public class NavDrawerListAdapter extends BaseAdapter {
	
	private Context context;
	private ArrayList<NavDrawerItem> navDrawerItems;
	
	public NavDrawerListAdapter(Context context, ArrayList<NavDrawerItem> navDrawerItems){
		this.context = context;
		this.navDrawerItems = navDrawerItems;
	}

	@Override
	public int getCount() {
		return navDrawerItems.size();
	}

	@Override
	public Object getItem(int position) {		
		return navDrawerItems.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@SuppressLint("InflateParams")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater)
                    context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.drawer_list_item, null);
        }
		
		ImageView imgIcon = (ImageView) convertView.findViewById(R.id.icon);
		TextView txtTitle = (TextView) convertView.findViewById(R.id.title);
		
		imgIcon.setImageResource(navDrawerItems.get(position).getIcon()); 
        txtTitle.setText(navDrawerItems.get(position).getTitle());
        
        // displaying count
        // check whether it set visible or not
        
        System.out.println("crew   == "+CardInfo.getUserRole(context));
        System.out.println("Title : "+navDrawerItems.get(position).getTitle());
        
        
       /* if(CardInfo.getUserRole(context).equalsIgnoreCase("crew"))
        {
        	System.out.println("enter in validator score fffffffffffffff");
        	 if(navDrawerItems.get(position).getTitle().equalsIgnoreCase("Validate Score")){
        		 
        		 System.out.println("enter in validator score ddddddddddddddddddddddd");
        		 convertView.setFocusable(true);	
        	     convertView.setClickable(true);
        	       
        	 }
        	 else{
        		 System.out.println("enter in validator score eeeeeeeeeeeeeeeeeeeee");
        		 convertView.setFocusable(false);	
             	convertView.setClickable(false);
        	 }
        	
     }*/
       return convertView;
	}

}
