package com.arknfc.adapter;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.arknfcpower.CardInfo;
import com.arknfcpower.R;
import com.arknfcpower.db.DatabaseSupport;
import com.arknfcpower.model.PowerPlusCategory;
import com.arknfcpower.model.PowerPlusTransaction;
import com.arknfcpower.model.UserMaster;

@SuppressLint("InflateParams")
public class ValidatorAdapter extends BaseAdapter implements ListAdapter { 
private ArrayList<PowerPlusTransaction> list = new ArrayList<PowerPlusTransaction>(); 
private Context context; 



public ValidatorAdapter(ArrayList<PowerPlusTransaction> list2, Context context) { 
    this.list = list2; 
    this.context = context; 
} 

@Override
public int getCount() { 
    return list.size(); 
} 

@Override
public Object getItem(int pos) { 
    return list.get(pos); 
} 

@Override
public View getView(final int position, View convertView, ViewGroup parent) {
    View view = convertView;
    if (view == null) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE); 
        view = inflater.inflate(R.layout.validator_list, null);
    } 
    
    TextView listItemText = (TextView)view.findViewById(R.id.description);
    TextView listItemText2 = (TextView)view.findViewById(R.id.namerank);
    ImageView helpicon = (ImageView) view.findViewById(R.id.helpinfo1);
	ImageView listImageView = (ImageView) view.findViewById(R.id.selection);
	LinearLayout linear = (LinearLayout) view.findViewById(R.id.linearl1);
	TextView validator_score = (TextView)view.findViewById(R.id.validator_score);
    //    TextView listItemText3 = (TextView)view.findViewById(R.id.status);
    
    DatabaseSupport db = new DatabaseSupport(context);
    PowerPlusTransaction userScore = new PowerPlusTransaction();
    userScore = list.get(position);
    //Handle TextView and display string from your list
    if(position == 0)
    {
    	listItemText2.setVisibility(View.GONE);
 //   	listItemText3.setVisibility(View.GONE);
    	listImageView.setVisibility(View.GONE);
    	linear.setVisibility(View.GONE);
    	helpicon.setVisibility(View.VISIBLE);
    	
		listItemText.setTextColor(context.getResources().getColor(R.color.black));

    	
    	SpannableString spanString  = new SpannableString(userScore.getStrTitle());
    //	spanString.setSpan(new UnderlineSpan(), 0, spanString.length(), 0);
    //    spanString.setSpan(new StyleSpan(Typeface.BOLD), 0, spanString.length(), 0); 
        spanString.setSpan(new StyleSpan(Typeface.ITALIC), 0, spanString.length(), 0);
        listItemText.setText(spanString);

    //	listItemText.setText(userScore.getStrTitle()); 
    	
    	view.setBackgroundColor(Color.parseColor("#ffffff"));
//		view.setBackgroundColor(Color.parseColor("#D3D3D3"));
	
    }else if(position > 0)
    {
    	linear.setVisibility(View.VISIBLE);
    	listItemText2.setVisibility(View.VISIBLE);
    	helpicon.setVisibility(View.GONE);
    	

    	//   	listItemText3.setVisibility(View.VISIBLE);
    //	view.setBackgroundColor(Color.parseColor("#ffffff"));
    		
    UserMaster um = new UserMaster();
    List<UserMaster> umList = db.getUserMasterSingleRow(userScore.getiUserId());
    if(umList != null && umList.size() > 0){
    	 um = umList.get(0);
    }
   
	String strTitleText = userScore.getStrTitle() != null ? userScore.getStrTitle() : "";			
	
	List<Integer> indextStartList = new ArrayList<Integer>();
	List<Integer> indextEndList = new ArrayList<Integer>();
	
	int indexStart = strTitleText.indexOf("#*");
	int startInd = indexStart;
	int mul = 1;
	while (indexStart >= 0) {
		int tot = 0;
		indextStartList.add(startInd);
		indexStart = strTitleText.indexOf("#*", indexStart + 1);
		tot = mul * 4;
		startInd = indexStart - tot;

		mul++;
	}

	int indexEnd = strTitleText.indexOf("*#");
	int endInd = indexEnd;
	mul = 1;
	while (indexEnd >= 0) {
		int tot = 0;
		indextEndList.add(endInd-2);
		indexEnd = strTitleText.indexOf("*#", indexEnd + 1);

		tot = mul * 4;
		endInd = indexEnd - tot;

		mul++;
	}
	
	if(strTitleText != null && strTitleText.length() > 0){	
		
		strTitleText = strTitleText.replace("#*", "");	
		strTitleText = strTitleText.replace("*#", "");	
	}
	
	SpannableString spanString  = new SpannableString(strTitleText);
	
	if(indextStartList != null && indextStartList.size() > 0){
		for(int i = 0; i<indextStartList.size(); i++){
			spanString.setSpan(new StyleSpan(Typeface.BOLD), indextStartList.get(i), indextEndList.get(i), 0);
		}
	}
			
	listItemText.setText(spanString);
     
   // listItemText.setText(userScore.getStrTitle()); 
    
    List<PowerPlusCategory> ppcList = new ArrayList<PowerPlusCategory>();
	PowerPlusCategory ppCat = new PowerPlusCategory();
	//ppcList = db.getPowerPlusCategoryRowByTitle("Leadership Power");
	ppcList = db.getPowerPlusCategoryRowByCatCode("leadershippower");
	if(ppcList != null && ppcList.size() > 0){
		ppCat = ppcList.get(0);
	}
    
     if(userScore.getCetogoryId().equalsIgnoreCase(ppCat.getiPowerPlusCategoryId() != null ? ppCat.getiPowerPlusCategoryId() : "0002")){
    	 listItemText2.setText("Leadership Power Plus Task.");
     }
     else {
    	
    	 listItemText2.setText(um.getStrFirstName() + " "+ um.getStrLastName() + " / " + db.getRoleSingleRow(um.getiRoleId()).getStrRole());
     }
    

   /* List<PowerPlusCategory> ppcDList = new ArrayList<PowerPlusCategory>();
 	PowerPlusCategory ppDCat = new PowerPlusCategory();
 	ppcDList = db.getPowerPlusCategoryRowByCatCode("powerdown");
 	if(ppcDList != null && ppcDList.size() > 0){
 		ppDCat = ppcList.get(0);
 	}  
 	
    if(userScore.getCetogoryId().equalsIgnoreCase(ppDCat.getiPowerPlusCategoryId() != null ? ppDCat.getiPowerPlusCategoryId() : "0004")){
    	validator_score.setText("-"+userScore.getPowerPoints());
    }
    else {
    	validator_score.setText(userScore.getPowerPoints());
    }*/
     
     validator_score.setText(userScore.getPowerPoints());
     
     
     if (db.getScoreInEnterModeForValidateWithTrans(userScore.getiPowerPlusTransactionId(), userScore.getiUserId(),
    		 userScore.getiPowerPlusTaskId(), "Validated") > 0) { 
		
    	 	listImageView.setVisibility(View.VISIBLE);
    	 	listImageView.setImageResource(R.drawable.select_icon);
    	 	listImageView.invalidate();
			
    	 	listItemText.setTextColor(context.getResources().getColor(R.color.white));
			listItemText2.setTextColor(context.getResources().getColor(R.color.white));
			validator_score.setTextColor(context.getResources().getColor(R.color.white));

			view.setBackgroundColor(Color.parseColor("#06dc1a"));
     }
     else if (db.getScoreInEnterModeForValidateWithTrans(userScore.getiPowerPlusTransactionId(), userScore.getiUserId(),
    		 userScore.getiPowerPlusTaskId(), "Entered") > 0) {
    	 
    	 	listImageView.setVisibility(View.GONE);
    	 	
    		listItemText.setTextColor(context.getResources().getColor(R.color.black));
			listItemText2.setTextColor(context.getResources().getColor(R.color.black));
			validator_score.setTextColor(context.getResources().getColor(R.color.black));

			view.setBackgroundColor(Color.parseColor("#ffffff")); // white
    	 
     }
     else if (db.getScoreInEnterModeForValidateWithTrans(userScore.getiPowerPlusTransactionId(), userScore.getiUserId(),
    		 userScore.getiPowerPlusTaskId(), "Rejected") > 0) {
    	 
    	 	listImageView.setVisibility(View.VISIBLE);
    	 	listImageView.setImageResource(R.drawable.reject_icon);
    	 	listImageView.invalidate();
    	 
			listItemText.setTextColor(context.getResources().getColor(R.color.white));
			listItemText2.setTextColor(context.getResources().getColor(R.color.white));
			validator_score.setTextColor(context.getResources().getColor(R.color.white));

			
			view.setBackgroundColor(Color.parseColor("#ff0000")); // red
    	 
     }
     
     
     
     
//    listItemText3.setText(userScore.getFlgScoreStatus()); 
    }
    
    return view; 
}

@Override
public long getItemId(int position) {
	// TODO Auto-generated method stub
	return 0;
} 
}