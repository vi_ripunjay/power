package com.arknfc.adapter;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.arknfcpower.R;
import com.arknfcpower.db.DatabaseSupport;
import com.arknfcpower.model.UserMaster;
import com.arknfcpower.model.UserSelectItem;

@SuppressLint("InflateParams")
public class SpinnerAdapter extends BaseAdapter implements ListAdapter {
	private ArrayList<UserSelectItem> list = new ArrayList<UserSelectItem>();
	private Context context;

	public SpinnerAdapter(ArrayList<UserSelectItem> list2, Context context) {
		this.list = list2;
		this.context = context;
	}

	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public Object getItem(int pos) {
		return list.get(pos);
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		View view = convertView;
		if (view == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = inflater.inflate(R.layout.spinner_item, null);
		}

		// Handle TextView and display string from your list
		TextView spinnerUsername = (TextView) view
				.findViewById(R.id.spinnerUsername);
		TextView spinnerUserRank = (TextView) view.findViewById(R.id.spinnerUserRank);
 
		DatabaseSupport db = new DatabaseSupport(context);
		UserMaster um =new UserMaster();
		String item = list.get(position).getUserId();
		String role = "";
		String name = "";
		if(!item.equalsIgnoreCase("Self")){
			um = db.getUserMasterSingleRow(list.get(position).getUserId()).get(0);
			role = db.getRoleSingleRow(um.getiRoleId()).getStrRole();
			name = um.getStrFirstName() + " "+ um.getStrLastName() + " /";
		}
		else {
			name ="Self";
		}
		
		
		spinnerUsername.setText(name);
		spinnerUserRank.setText(role);
		
		return view;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}
}