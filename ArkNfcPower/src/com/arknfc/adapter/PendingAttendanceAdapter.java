package com.arknfc.adapter;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.arknfcpower.R;
import com.arknfcpower.db.DatabaseSupport;
import com.arknfcpower.model.TraingAttendance;
import com.arknfcpower.model.TrainingData;
import com.arknfcpower.model.TrainingTransaction;

@SuppressLint("InflateParams")
public class PendingAttendanceAdapter extends BaseAdapter implements
		ListAdapter {
	private ArrayList<TraingAttendance> list = new ArrayList<TraingAttendance>();
	private Context context;

	public PendingAttendanceAdapter(ArrayList<TraingAttendance> list2,
			Context context) {
		this.list = list2;
		this.context = context;
	}

	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public Object getItem(int pos) {
		return list.get(pos);
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		View view = convertView;
		if (view == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = inflater.inflate(R.layout.pending_attendance_list, null);
		}
		DatabaseSupport db = new DatabaseSupport(context);
		// Handle TextView and display string from your list
		TextView catDesc = (TextView) view.findViewById(R.id.trainingCatDesc);
		TextView dateTime = (TextView) view.findViewById(R.id.trDateTime);
		ImageView imageView = (ImageView) view
				.findViewById(R.id.select_padding_icon);
		ImageView help_icon = (ImageView) view.findViewById(R.id.help);

		ArrayList<TrainingData> dataList = db.getTrainingDataById(list.get(
				position).getTraningDataId());

		if (position == 0) {

			help_icon.setVisibility(View.VISIBLE);

			SpannableString spanString = new SpannableString(list.get(position)
					.getStrDescription());
			// spanString.setSpan(new UnderlineSpan(), 0, spanString.length(),
			// 0);
			// spanString.setSpan(new StyleSpan(Typeface.BOLD), 0,
			// spanString.length(), 0);
			spanString.setSpan(new StyleSpan(Typeface.ITALIC), 0,
					spanString.length(), 0);
			catDesc.setText(spanString);

			// listItemText.setText(list.get(position).getStrTitle());
			catDesc.setTextColor(context.getResources().getColor(R.color.black));

			imageView.setVisibility(View.GONE);
			dateTime.setVisibility(View.GONE);
			view.setBackgroundColor(Color.parseColor("#ffffff"));

		} else if (position > 0) {
			help_icon.setVisibility(View.GONE);
			imageView.setVisibility(View.GONE);

			if (dataList != null && dataList.size() > 0) {

				TrainingTransaction ttras = new TrainingTransaction();
				List<TrainingTransaction> ttrList = new ArrayList<TrainingTransaction>();
				ttrList = db.getTrainingTrainsactionByDataId(list.get(position)
						.getTraningTransactionId());
				if (ttrList != null && ttrList.size() > 0) {
					ttras = ttrList.get(0);
				}

				StringBuffer des = new StringBuffer();
				if (ttras.getStrDescription() != null
						&& !ttras.getStrDescription().equalsIgnoreCase("null")
						&& !"".equalsIgnoreCase(ttras.getStrDescription())) {
					des.append(ttras.getStrDescription());
					des.insert(0, dataList.get(0).getStrTitle() + " \n > ");
				} else {
					des.append(dataList.get(0).getStrTitle());
				}

				String perentId = dataList.get(0).getStrParentId();

				while (perentId != null && !perentId.equalsIgnoreCase("null")) {
					TrainingData td = new TrainingData();
					td = db.getTrainingDataById(perentId).get(0);
					perentId = td.getStrParentId();
					des.insert(0, td.getStrTitle() + " \n > ");

				}

				catDesc.setText(des.toString());

				dateTime.setText(list.get(position).getTraningDate() + " / "
						+ list.get(position).getTrainingTime());
				
				if (db.getTrainingAttendanceByTransactionAndUserId(
						ttras.getTraningTransactionId(), list.get(position).getiUserId(), "0")
						.size() > 0) {
					view.setBackgroundColor(Color.parseColor("#06dc1a"));
					imageView.setVisibility(View.VISIBLE);
				} else {
					view.setBackgroundColor(Color.parseColor("#ffffff"));
					imageView.setVisibility(View.GONE);
				}

			}

			
			//view.setBackgroundColor(Color.parseColor("#ffffff"));

		}

		return view;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}
}