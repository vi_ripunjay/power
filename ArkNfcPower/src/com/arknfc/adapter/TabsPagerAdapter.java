package com.arknfc.adapter;

import android.os.Parcelable;
import android.support.v4.app.Fragment;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.arknfcpower.fragment.TrainingPlusAttendanceTab;
import com.arknfcpower.fragment.TrainingPlusSignInAttendance;

public class TabsPagerAdapter extends FragmentPagerAdapter {

	public TabsPagerAdapter(FragmentManager fm) {
		super(fm);
	}

	@Override
	public Fragment getItem(int index) {
     Fragment fragment = null;
		switch (index) {
		case 0:
			
		    fragment =  new TrainingPlusAttendanceTab();
			
		    return fragment;
			//break;
		case 1:
			
		    fragment = new TrainingPlusSignInAttendance();
			return fragment;
	       // break;
	        
		}

		return null;
	}

	@Override
	public void restoreState(Parcelable state, ClassLoader loader) {

	}
	@Override
	public int getCount() {
		// get item count - equal to number of tabs
		return 2;
	}

}
