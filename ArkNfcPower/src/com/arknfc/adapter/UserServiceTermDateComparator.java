package com.arknfc.adapter;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;

import android.annotation.SuppressLint;

import com.arknfcpower.L;
import com.arknfcpower.model.UserServiceTermRoleData;

public class UserServiceTermDateComparator implements
		Comparator<UserServiceTermRoleData>, Serializable {

	private static final long serialVersionUID = 1L;

	public UserServiceTermDateComparator() {

	}

	@SuppressLint("SimpleDateFormat")
	public int compare(UserServiceTermRoleData c1, UserServiceTermRoleData c2) {
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		if (c1.getDtTermTo() != null && c2.getDtTermTo() != null) {
			L.fv("comparator c1 : " + c1.getDtTermTo() + " and c2 : "
					+ c2.getDtTermTo());
			try {
				Date c1Date = format.parse(c1.getDtTermTo());
				Date c2Date = format.parse(c2.getDtTermTo());
				
				L.fv("c1date and c2d date "+c1Date+" : "+c2Date);

				return c2Date.compareTo(c1Date);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			return 0;
		} else if (c1.getDtTermTo() == null && c2.getDtTermTo() != null) {
			return -1;
		} else if (c1.getDtTermTo() != null && c2.getDtTermTo() == null) {
			return 1;
		} else {
			return 0;
		}
	}

}