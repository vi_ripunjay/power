package com.arknfc.adapter;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.arknfcpower.CardInfo;
import com.arknfcpower.R;
import com.arknfcpower.db.DatabaseHandler;
import com.arknfcpower.db.DatabaseSupport;
import com.arknfcpower.model.PowerPlusPeriodicity;
import com.arknfcpower.model.PowerPlusTask;
import com.arknfcpower.model.Score;

@SuppressLint("InflateParams")
public class ScoreAdapter extends BaseAdapter implements ListAdapter {
	private ArrayList<PowerPlusTask> list = new ArrayList<PowerPlusTask>();
	private Context context;

	public ScoreAdapter(ArrayList<PowerPlusTask> list2, Context context) {
		this.list = list2;
		this.context = context;
	}

	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public Object getItem(int pos) {
		return list.get(pos);
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		View view = convertView;
		if (view == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = inflater.inflate(R.layout.score_list, null);
		}

		// Handle TextView and display string from your list
		TextView listItemText = (TextView) view
				.findViewById(R.id.list_item_string);
		TextView score = (TextView) view.findViewById(R.id.textView1);
		LinearLayout linear = (LinearLayout) view.findViewById(R.id.linear1);
		ImageView helpInfo = (ImageView) view.findViewById(R.id.help_info);
		ImageView selectInfo = (ImageView) view.findViewById(R.id.select_info);

		if (position == 0) {

			SpannableString spanString = new SpannableString(list.get(position)
					.getStrTitle());
			// spanString.setSpan(new UnderlineSpan(), 0, spanString.length(),
			// 0);
			// spanString.setSpan(new StyleSpan(Typeface.BOLD), 0,
			// spanString.length(), 0);
			spanString.setSpan(new StyleSpan(Typeface.ITALIC), 0,
					spanString.length(), 0);
			listItemText.setText(spanString);

			// listItemText.setText(list.get(position).getStrTitle());
			listItemText.setTextColor(context.getResources().getColor(
					R.color.black));
			score.setTextColor(context.getResources().getColor(R.color.black));
			score.setVisibility(View.GONE);
			linear.setVisibility(View.GONE);
			selectInfo.setVisibility(View.GONE);
			helpInfo.setVisibility(View.VISIBLE);
			// view.setBackgroundColor(Color.parseColor("#D3D3D3"));
			view.setBackgroundColor(Color.parseColor("#ffffff"));

		} else if (position > 0) {

			linear.setVisibility(View.VISIBLE);
			score.setVisibility(View.VISIBLE);
			selectInfo.setVisibility(View.VISIBLE);

			String strTitleText = list.get(position).getStrTitle() != null ? list
					.get(position).getStrTitle() : "";

			List<Integer> indextStartList = new ArrayList<Integer>();
			List<Integer> indextEndList = new ArrayList<Integer>();

			int indexStart = strTitleText.indexOf("#*");
			int startInd = indexStart;
			int mul = 1;
			while (indexStart >= 0) {
				int tot = 0;
				indextStartList.add(startInd);
				indexStart = strTitleText.indexOf("#*", indexStart + 1);
				tot = mul * 4;
				startInd = indexStart - tot;

				mul++;
			}

			int indexEnd = strTitleText.indexOf("*#");
			int endInd = indexEnd;
			mul = 1;
			while (indexEnd >= 0) {
				int tot = 0;
				indextEndList.add(endInd-2);
				indexEnd = strTitleText.indexOf("*#", indexEnd + 1);

				tot = mul * 4;
				endInd = indexEnd - tot;

				mul++;
			}

			if (strTitleText != null && strTitleText.length() > 0) {

				strTitleText = strTitleText.replace("#*", "");
				strTitleText = strTitleText.replace("*#", "");
			}

			SpannableString spanString = new SpannableString(strTitleText);

			if (indextStartList != null && indextStartList.size() > 0) {
				for (int i = 0; i < indextStartList.size(); i++) {
					spanString.setSpan(new StyleSpan(Typeface.BOLD),
							indextStartList.get(i), indextEndList.get(i), 0);
				}
			}

			listItemText.setText(spanString);
			// listItemText.setText(list.get(position).getStrTitle());
			score.setVisibility(View.VISIBLE);
			helpInfo.setVisibility(View.GONE);
			score.setText(list.get(position).getPowerPoints());

			DatabaseSupport db = new DatabaseSupport(context);
			if (db.getScoreInEnterMode(CardInfo.getUserId(context),
					list.get(position).getiPowerPlusTaskId(), "Entered") > 0) {
				selectInfo.setVisibility(View.VISIBLE);

				listItemText.setTextColor(context.getResources().getColor(
						R.color.white));
				score.setTextColor(context.getResources().getColor(
						R.color.white));

				view.setBackgroundColor(Color.parseColor("#06dc1a"));
			} else {

				listItemText.setTextColor(context.getResources().getColor(
						R.color.black));
				score.setTextColor(context.getResources().getColor(
						R.color.black));
				selectInfo.setVisibility(View.GONE);
				view.setBackgroundColor(Color.parseColor("#ffffff"));
			}
		}
		return view;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}
}