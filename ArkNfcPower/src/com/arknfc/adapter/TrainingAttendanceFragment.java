package com.arknfc.adapter;

import java.util.List;

import android.app.ActionBar;
import android.app.Activity;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arknfcpower.CardInfo;
import com.arknfcpower.R;
import com.arknfcpower.db.DatabaseSupport;
import com.arknfcpower.model.TabletRoleFunctions;
import com.arknfcpower.model.UserMaster;


public class TrainingAttendanceFragment extends Fragment {
	ViewPager Tab;
    TabsPagerAdapter TabAdapter;
	ActionBar actionBar;
	private FragmentActivity myContext;
    int tabvalue=0;

    //Mandatory Constructor
    public TrainingAttendanceFragment() {
    }

    public TrainingAttendanceFragment(int i) {
    	tabvalue = i;
    }

    @Override
    public void onAttach(Activity activity) {
        myContext=(FragmentActivity) activity;
        super.onAttach(activity);
    }
    
   

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.activity_training_plus_attendance,container, false);
        
        
        UserMaster um = new UserMaster();
        DatabaseSupport db = new DatabaseSupport(getActivity());
            um = db.getUserMasterSingleRow(CardInfo.getUserId(getActivity())).get(0);
            //String strRoleType = db.getRoleSingleRow(um.getiRoleId()).getStrRoleType();
    		List<TabletRoleFunctions> trFunList = db.getTabletRoleFunctionsRowByRoleId(um.getiRoleId());
    		String strRoleType ="Crew";
    		if(trFunList != null && trFunList.size() > 0){
    			strRoleType = trFunList.get(0).getStrFunctionCode();
    		}


            if(strRoleType != null && strRoleType.equalsIgnoreCase("Crew"))
            {
                    tabvalue = 1;
            }
        db.close();
   //     TabAdapter = new TabsPagerAdapter(getActivity().getSupportFragmentManager());
     
        TabAdapter = new TabsPagerAdapter(getChildFragmentManager());
        
        Tab = (ViewPager) rootView.findViewById(R.id.pager);
        
        Tab.setOnPageChangeListener(
                new ViewPager.SimpleOnPageChangeListener() {
                    @Override
                    public void onPageSelected(int position) {
                       
                    	actionBar = getActivity().getActionBar();
                    	actionBar.setSelectedNavigationItem(position);                    }
                });
        
        Tab.setAdapter(TabAdapter);
        
        
        actionBar = getActivity().getActionBar();
        //Enable Tabs on Action Bar
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        ActionBar.TabListener tabListener = new ActionBar.TabListener(){

			@Override
			public void onTabReselected(android.app.ActionBar.Tab tab,
					FragmentTransaction ft) {
				// TODO Auto-generated method stub
				
			}

			@Override
			 public void onTabSelected(ActionBar.Tab tab, FragmentTransaction ft) {
	          
	            Tab.setCurrentItem(tab.getPosition());
	        }

			@Override
			public void onTabUnselected(android.app.ActionBar.Tab tab,
					FragmentTransaction ft) {
				// TODO Auto-generated method stub
				
			}};
			
			actionBar.removeAllTabs();
			if(actionBar.getTabCount() == 0)
		    {
		     

				
		    //Add New Tab
				
			actionBar.addTab(actionBar.newTab().setText("Create Traning").setTabListener(tabListener));
			actionBar.addTab(actionBar.newTab().setText("Sign In").setTabListener(tabListener));
			actionBar.setSelectedNavigationItem(tabvalue);

		    }
        return rootView;
    }
}


