package com.arknfc.adapter;

import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.arknfcpower.fragment.AdminRegisterTab;
import com.arknfcpower.fragment.AdminSyncTab;
import com.arknfcpower.fragment.AdminTab;
import com.arknfcpower.fragment.TrainingPlusAttendanceTab;
import com.arknfcpower.fragment.TrainingPlusSignInAttendance;

public class AdminTabPagerAdapter extends FragmentPagerAdapter {

	public AdminTabPagerAdapter(FragmentManager fm) {
		super(fm);
	}

	@Override
	public Fragment getItem(int index) {
		Fragment fragment = null;
		switch (index) {
		case 0:

			fragment = new AdminRegisterTab();

			return fragment;
			// break;
		case 1:

			fragment = new AdminSyncTab();
			return fragment;
			// break;

		case 2:

			fragment = new AdminTab();
			return fragment;

		}

		return null;
	}

	@Override
	public void restoreState(Parcelable state, ClassLoader loader) {

	}

	@Override
	public int getCount() {
		// get item count - equal to number of tabs
		return 3;
	}

}
