package com.arknfc.adapter;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.arknfcpower.R;
import com.arknfcpower.db.DatabaseSupport;
import com.arknfcpower.model.UserMaster;
import com.arknfcpower.model.UserSelectItem;

@SuppressLint("InflateParams")
public class UserListAdapter extends BaseAdapter implements ListAdapter {
	private ArrayList<UserSelectItem> list = new ArrayList<UserSelectItem>();
	private Context context;

	public UserListAdapter(ArrayList<UserSelectItem> list2, Context context) {
		this.list = list2;
		this.context = context;
	}

	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public Object getItem(int pos) {
		return list.get(pos);
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		View view = convertView;
		if (view == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			view = inflater.inflate(R.layout.user_list_item, null);
		}

		// Handle TextView and display string from your list
		TextView spinnerUsername = (TextView) view
				.findViewById(R.id.spinnerUsername);
		TextView spinnerUserRank = (TextView) view.findViewById(R.id.spinnerUserRank);
		ImageView helpIcon = (ImageView) view.findViewById(R.id.helpIcon);
		ImageView select_icon = (ImageView) view.findViewById(R.id.select_icon);
		
 
		DatabaseSupport db = new DatabaseSupport(context);
		UserMaster um =new UserMaster();
		String item = list.get(position).getUserId();
		String role = "";
		String name = "";
	
	
		if (position == 0)
		{
			helpIcon.setVisibility(View.VISIBLE);
			select_icon.setVisibility(View.GONE);
			SpannableString spanString  = new SpannableString(list.get(position).getText());
		    //	spanString.setSpan(new UnderlineSpan(), 0, spanString.length(), 0);
		    //    spanString.setSpan(new StyleSpan(Typeface.BOLD), 0, spanString.length(), 0); 
		        spanString.setSpan(new StyleSpan(Typeface.ITALIC), 0, spanString.length(), 0);
		        spinnerUsername.setText(spanString);
		//	spinnerUsername.setText(name);
			spinnerUserRank.setText(name);
			spinnerUsername.setTextColor(context.getResources().getColor(R.color.black));
		    spinnerUserRank.setTextColor(context.getResources().getColor(R.color.black));
		    
	    	view.setBackgroundColor(Color.parseColor("#ffffff"));
		}if (position > 0)
		{
			helpIcon.setVisibility(View.GONE);

			um = db.getUserMasterSingleRow(list.get(position).getUserId()).get(0);
			role = db.getRoleSingleRow(um.getiRoleId()).getStrRole();
			name = um.getStrFirstName() + " "+ um.getStrLastName() + " ";
			
			
			if (db.getUserNFCSingleRowByUserId(list.get(position).getUserId()).size()>0)
			{

				select_icon.setVisibility(View.VISIBLE);

				spinnerUsername.setTextColor(context.getResources().getColor(R.color.white));
			    spinnerUserRank.setTextColor(context.getResources().getColor(R.color.white));
				
			    spinnerUsername.setText(name);
				spinnerUserRank.setText(role);
				view.setBackgroundColor(Color.parseColor("#06dc1a"));
		    }else if(!(db.getUserNFCSingleRowByUserId(list.get(position).getUserId()).size()>0))
		    {

				select_icon.setVisibility(View.GONE);
				spinnerUsername.setText(name);
				spinnerUserRank.setText(role);

				spinnerUsername.setTextColor(context.getResources().getColor(R.color.black));
			    spinnerUserRank.setTextColor(context.getResources().getColor(R.color.appcolor));
				
		    	view.setBackgroundColor(Color.parseColor("#ffffff"));
			    	
		    }

			
		}
		
		return view;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}
}