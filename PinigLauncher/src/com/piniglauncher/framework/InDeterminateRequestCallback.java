package com.piniglauncher.framework;

public interface InDeterminateRequestCallback extends RequestCallback
{
	void result(boolean result);
	
	void result(boolean result, Object data);
}
