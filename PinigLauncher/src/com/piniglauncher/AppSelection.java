package com.piniglauncher;

import java.util.ArrayList;

import java.util.Collections;

import java.util.List;

import com.piniglauncher.adapter.Listadapter;
import com.piniglauncher.db.DatabaseHandler;
import com.piniglauncher.model.AppDetail;
import com.piniglauncher.model.AppList;


import android.app.Activity;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;

public class AppSelection extends Activity implements OnItemClickListener{

	ListView apps;
	PackageManager packageManager;
	ArrayList<String> checkedValue;
	Button bt1,cancel;
	
	PackageInfo pi = new PackageInfo();
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
        bt1 = (Button) findViewById(R.id.button1);
        cancel = (Button) findViewById(R.id.cancel);
        
        apps = (ListView) findViewById(R.id.listView1);
        apps.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        packageManager = getPackageManager();
        checkedValue = new ArrayList<String>();
        
        Intent main=new Intent(Intent.ACTION_MAIN, null);
        main.addCategory(Intent.CATEGORY_LAUNCHER);
        List<ResolveInfo> packages =packageManager.queryIntentActivities(main, 0);
        
         List<ResolveInfo> packageList1 = new ArrayList<ResolveInfo>() ;

		 for (Object packageInfo : packages) {
			 ResolveInfo info = (ResolveInfo) packageInfo;
			     packageList1.add(info) ;
		 
		 }
		 
				DatabaseHandler db = new DatabaseHandler(this);
				ArrayList<AppDetail> app = db.getAllSelectedApp();
				System.out.println("Data Base app :" + app);
				  Collections.sort(packageList1,
		                     new ResolveInfo.DisplayNameComparator(packageManager)); 
				final Listadapter Adapter = new Listadapter(this,packageList1, packageManager, app );
				apps.setAdapter(Adapter);
				apps.setOnItemClickListener(this);
				
				bt1.setOnClickListener(new View.OnClickListener() {
					
					@Override
					public void onClick(View v) {
						

						 
						   DatabaseHandler db = new DatabaseHandler(AppSelection.this);
						   db.deleteAll();
								 // Inserting Contacts  
						    Log.d("Insert: ", "Inserting ..");  
						    ArrayList<AppDetail> alllist = new ArrayList<AppDetail>();
						    
						    alllist = Adapter.selectedAppList ;
						    System.out.println("Selected App List :" + alllist);
						    System.out.println("all list :" + alllist.toString());
						    for(int i=0;i<alllist.size();i++){
						    AppDetail ad = alllist.get(i);
						  
						      db.addAppRow(new AppList(ad.getAppName(),ad.getPackageName())); 
				            }
						    SharePref.setId(getApplicationContext(), "Selected");
						    Intent i = new Intent(AppSelection.this , Home.class);
						    startActivity(i);
					}
				});
				
				
				cancel.setOnClickListener(new OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						finish();
					}
				});
        
    }
/*

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        
		MenuItem item = menu.findItem(R.id.menu_item_share);
	     mShareAction = (ShareActionProvider) item.getActionProvider();
	     // Create the share Intent
	     
	     String playStoreLink = "https://play.google.com/store/apps/details?id=" +
	         getPackageName();
	     
	     String yourShareText = "Install this app " + playStoreLink;
	     Intent shareIntent = ShareCompat.IntentBuilder.from(this)
	         .setType("text/plain").setText(yourShareText).getIntent();
	     // Set the share Intent
	     mShareAction.setShareIntent(shareIntent);
        return true;
    }

*/   
		
    
	@Override
	public void onItemClick(AdapterView<?> arg0, View v, int position, long arg3) {
		// TODO Auto-generated method stub
		/*CheckBox cb = (CheckBox) v.findViewById(R.id.checkBox1);
		TextView tv = (TextView) v.findViewById(R.id.textView1);
		pi = (PackageInfo) arg0.getItemAtPosition(position);
		cb.performClick();
		if (cb.isChecked()) {
			checkedValue.add(tv.getText().toString());
		} else if (!cb.isChecked()) {
			checkedValue.remove(tv.getText().toString());
		}*/

		 
		
	}
    
}
