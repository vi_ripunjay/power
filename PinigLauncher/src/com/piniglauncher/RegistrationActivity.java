package com.piniglauncher;

import java.lang.ref.WeakReference;


import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.android.gcm.GCMRegistrar;
import com.piniglauncher.db.DatabaseHandler;
import com.piniglauncher.framework.InDeterminateRequestCallback;
import com.piniglauncher.model.DeviceData;

import android.support.v7.app.ActionBarActivity;
import android.telephony.TelephonyManager;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class RegistrationActivity extends ActionBarActivity {

	
	// UI elements
	EditText txtName; 
	EditText txtEmail;
	Intent intent = null;
	static RegistrationHandler HANDLER = null;

	static Controller aController;
	
	static // Asyntask
	AsyncTask<Void, Void, Void> mRegisterTask;

	// Register button
	Button btnRegister;
	static ProgressDialog barProgressDialog = null;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_registration);
		HANDLER = new RegistrationHandler(this);

		intent = getIntent();
		//Get Global Controller Class object (see application tag in AndroidManifest.xml)
		aController = new Controller();
		registerReceiver(mHandleMessageReceiver, new IntentFilter(
				Config.DISPLAY_REGISTRATION_MESSAGE_ACTION));
        
		txtName = (EditText) findViewById(R.id.txtName);
		txtEmail = (EditText) findViewById(R.id.txtEmail);
		btnRegister = (Button) findViewById(R.id.btnRegister);
		barProgressDialog = new ProgressDialog(RegistrationActivity.this);
		
		
		// Click event on Register button
		btnRegister.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {  

			HANDLER.sendEmptyMessage(Do.REGISTRATION);
			}
		});


	}

	public static class Do {
		static final int REGISTRATION = 1;
		static final int REGISTRATION_GCM = 2;
		static final int SUICIDE = -1;
	}


	
	public static class RegistrationHandler extends Handler {
		private final WeakReference<RegistrationActivity> parent;

		RegistrationHandler(RegistrationActivity parent) {
			this.parent = new WeakReference<RegistrationActivity>(parent);
		}

		@SuppressWarnings("static-access")
		@Override
		public void handleMessage(Message msg) {
			final RegistrationActivity target = parent.get();
			try {
				switch (msg.what) {
				case Do.REGISTRATION: {
					final String name = target.txtName.getText()
							.toString();
					final String email = target.txtEmail.getText()
							.toString();

					final Context context = target;
					
            			     barProgressDialog.setMessage("Please Wait....");
					         barProgressDialog.setProgressStyle(barProgressDialog.STYLE_SPINNER);
					         barProgressDialog.setProgress(0);
					         barProgressDialog.setMax(20);
					         barProgressDialog.show();
					         
					         List<NameValuePair> nameValuePairs  =   new ArrayList<NameValuePair>();
					         nameValuePairs.add( new BasicNameValuePair("name", name) );
					         nameValuePairs.add( new BasicNameValuePair("email", email) );
					         nameValuePairs.add( new BasicNameValuePair("IMEI", getIMEI(target)) );

					         System.out.println("phone "+ email);
					         RequestListener.onReceive(UrlConnection.Request.REGISTER,
							 new InDeterminateRequestCallback() {
								@Override
								public void result(final boolean result) {
									target.runOnUiThread(new Runnable() {
										@Override
										public void run() {
											//
											if (result) {

												
											    DatabaseHandler db = new DatabaseHandler(context);
								//				db.addDeviceData(new DeviceData(target.txtName.getText().toString(),target.txtEmail.getText().toString(),regId, imei));
												
												db.close();
										
										/*		UrlConnection.setPhone(target, target.tex.getText().toString());
												Intent intent = new Intent();
											    intent.setClass(target, CodeAuthentication.class);
										     	target.startActivity(intent);
											    target.overridePendingTransition(R.anim.right_in, R.anim.left_out);
											    barProgressDialog.dismiss();
											    target.finish();
								*/
											} else {
												barProgressDialog.dismiss();
											}
										}
									});
								}

								@Override
								public void result(final boolean result,
										final Object data) {
									target.runOnUiThread(new Runnable() {
										@Override
										public void run() {
											barProgressDialog.dismiss();
											
											if (result) {
												JSONObject json;
												try {
													json = new JSONObject(data.toString());
													
													JSONArray jsonArray = json.getJSONArray("result");
												    
													System.out.println("length :"+jsonArray.length()); 

													JSONObject c = jsonArray.getJSONObject(0);
									                        
													String name = c.getString("name");
									                String email = c.getString("email");
									                String imei = c.getString("imei");
									                String regid = c.getString("regid");
									                
									                System.out.println(name);
									                System.out.println(email);
									                System.out.println(imei);
									                System.out.println(regid);
									                
									                DatabaseHandler db = new DatabaseHandler(context);
									                db.addDeviceData(new DeviceData(name,email,imei,regid));
									                
									            	UrlConnection.setImei(target, imei);
									                 System.out
															.println("Succesfully Saved");
												
												} catch (JSONException e) {
													// TODO Auto-generated catch block
													e.printStackTrace();
												}
												
												
											}
									if (!result) {
												int status = (Integer) data;
												System.out
												.println("Status :" +status);
										 if (status == HttpURLConnection.HTTP_NOT_FOUND)       //404
										{
											
										}
								// Did not find device, then did not find user
										else if (status == HttpURLConnection.HTTP_FORBIDDEN)     //403
										{
											
											System.out.println("Device not found");
											HANDLER.sendEmptyMessage(Do.REGISTRATION_GCM);
											
										}	
							
										else if (status == HttpURLConnection.HTTP_BAD_REQUEST)   //400
								
										{
											
										}// Device found but username not correct
										else if (status == HttpURLConnection.HTTP_UNAUTHORIZED)   //401
										{
											
										}
										else if (status == HttpURLConnection.HTTP_CONFLICT)     //409
										{
											System.out
													.println("Code Not Found :");
										}								/*				FileToPrint.fe(target.cx,
												GoOn.Event.EXCEPTION,
												new Exception("Request.DEVICE_REGISTER failed."));     */
		

											}
										}
									});
								}
							}, nameValuePairs);
					break;
				}
				case Do.REGISTRATION_GCM: {
/*					final String phone = target.phoneNumber.getText()
							.toString();
*/					         barProgressDialog.setMessage("Please Wait....");
					         barProgressDialog.setProgressStyle(barProgressDialog.STYLE_SPINNER);
					         barProgressDialog.setProgress(0);
					         barProgressDialog.setMax(20);
					         barProgressDialog.show();
					    //     check(target);
					         

					        final String imei  = getIMEI(target);
									
					     	// Make sure the device has the proper dependencies.
					 		GCMRegistrar.checkDevice(target);

					 		// Make sure the manifest permissions was properly set 
					 		GCMRegistrar.checkManifest(target);

					 		// Get GCM registration id
					 		final String regId = GCMRegistrar.getRegistrationId(target);

					 		System.out.println("RegisterID +" + regId);
					 		if (regId.equals("")) {
								
								 Log.i("GCM K", "--- Regid = ''"+regId);
								// Register with GCM			
								GCMRegistrar.register(target, Config.GOOGLE_SENDER_ID);
								HANDLER.sendEmptyMessage(Do.REGISTRATION_GCM);
								
								
							} else 
							{
								// Device is already registered on GCM Server
								if (GCMRegistrar.isRegisteredOnServer(target)) {
									
									// Skips registration.				
									Toast.makeText(target, 
											"Already registered with GCM Server", 
											Toast.LENGTH_LONG).show();
									Log.i("GCM K", "Already registered with GCM Server");
									
									//GCMRegistrar.unregister(context);
									
								} else
								{
	
					         // Try to register again, but not in the UI thread.
									// It's also necessary to cancel the thread onDestroy(),
									// hence the use of AsyncTask instead of a raw thread.
									final Context context = target;
									
																
					         List<NameValuePair> nameValuePairs  =   new ArrayList<NameValuePair>();
					         nameValuePairs.add( new BasicNameValuePair("name", target.txtName.getText().toString()) );
					         nameValuePairs.add( new BasicNameValuePair("email",target.txtEmail.getText().toString()) );
					         nameValuePairs.add( new BasicNameValuePair("IMEI", imei) );
					         nameValuePairs.add( new BasicNameValuePair("regid", regId) );
					         nameValuePairs.add( new BasicNameValuePair("status", "Installed") );

					         //
//				             System.out.println("phone "+ phone);
					         RequestListener.onReceive(UrlConnection.Request.REGISTERATION,
							 new InDeterminateRequestCallback() {
								@Override
								public void result(final boolean result) {
									target.runOnUiThread(new Runnable() {
										@Override
										public void run() {
											//
											if (result) {
								
											    GCMRegistrar.setRegisteredOnServer(context, true);
										
											    DatabaseHandler db = new DatabaseHandler(context);
												db.addDeviceData(new DeviceData(target.txtName.getText().toString(),
														target.txtEmail.getText().toString(),regId,
														imei));
												
												db.close();
											    
											    
												UrlConnection.setImei(target, imei);
	/*											Intent intent = new Intent();
											    intent.setClass(target, CodeAuthentication.class);
										     	target.startActivity(intent);
											    target.overridePendingTransition(R.anim.right_in, R.anim.left_out);
											    barProgressDialog.dismiss();
											    target.finish();
							*/	
											    barProgressDialog.dismiss();
												
											} else {
												barProgressDialog.dismiss();
											}
										}
									});
								}

								@Override
								public void result(final boolean result,
										final Object data) {
									target.runOnUiThread(new Runnable() {
										@Override
										public void run() {
											barProgressDialog.dismiss();
											int status = (Integer) data;
											if (!result) {
												
												System.out
												.println("Status :" +status);
										 if (status == HttpURLConnection.HTTP_NOT_FOUND)       //404
										{
											
										}
											// Did not find device, then did not find user
										else if (status == HttpURLConnection.HTTP_FORBIDDEN)     //403
										{
											
											System.out.println("Device not found");
								
				//						    DBAdapter.init(target);
										  //Get Global Controller Class object (see application tag in AndroidManifest.xml)
										//	check(target);		
											
										}	
							
										else if (status == HttpURLConnection.HTTP_BAD_REQUEST)   //400
								
										{
											
										}// Device found but username not correct
										else if (status == HttpURLConnection.HTTP_UNAUTHORIZED)   //401
										{
											
										}
										else if (status == HttpURLConnection.HTTP_CONFLICT)     //409
										{
											System.out
													.println("Code Not Found :");
										}								/*				FileToPrint.fe(target.cx,
												GoOn.Event.EXCEPTION,
												new Exception("Request.DEVICE_REGISTER failed."));     */
		

											}
										}
									});
								}
							}, nameValuePairs);
								
								}
							}
				    
				
					         
					break;
				}
				case -1: {
					target.finish();
					break;
				}
				}
			} catch (Exception e) {
		//		FileToPrint.fe(target.cx, GoOn.Event.EXCEPTION, e);
				//
				Log.e("Exception :", e.getMessage());
				target.setResult(Activity.RESULT_CANCELED, null);
				target.finish();
			}
		}
	}	
	
	
	public static String getIMEI(Context context)
	{
		String id ="";
		TelephonyManager tManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
		 		  id = tManager.getDeviceId(); 

		return id;
	}
	
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.registration, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	// Create a broadcast receiver to get message and show on screen 
	private final BroadcastReceiver mHandleMessageReceiver = new BroadcastReceiver() {
		
		@Override
		public void onReceive(Context context, Intent intent) {
			
			String newMessage = intent.getExtras().getString(Config.EXTRA_MESSAGE);
			
			// Waking up mobile if it is sleeping
			aController.acquireWakeLock(context);
			
			// Display message on the screen
//			lblMessage.append(newMessage + "\n");			
			
			Toast.makeText(context, 
					"Got Message: " + newMessage, 
					Toast.LENGTH_LONG).show();
			
			// Releasing wake lock
			aController.releaseWakeLock();
		}
	};
	
	@Override
    protected void onDestroy() {
        // Cancel AsyncTask
        if (mRegisterTask != null) {
            mRegisterTask.cancel(true);
        }
        try {
            // Unregister Broadcast Receiver
            unregisterReceiver(mHandleMessageReceiver);
             
            //Clear internal resources.
            GCMRegistrar.onDestroy(this);
             
        } catch (Exception e) {
            Log.e("UnRegister Receiver Error", "> " + e.getMessage());
        }
        super.onDestroy();
    }

	
}
