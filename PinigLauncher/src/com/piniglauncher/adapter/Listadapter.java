package com.piniglauncher.adapter;


import java.util.ArrayList;





import java.util.List;

import com.piniglauncher.R;
import com.piniglauncher.model.AppDetail;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

public class Listadapter extends BaseAdapter{

	List<ResolveInfo> packageList;
	Activity context;
	PackageManager packageManager;
	boolean[] itemChecked;
	public ArrayList<AppDetail> selectedAppList = new ArrayList<AppDetail>();
	
	public Listadapter(Activity context, List<ResolveInfo> packageList,
			PackageManager packageManager ,ArrayList<AppDetail> selectedAppList) {
		super();
		this.context = context;
		this.packageList = packageList;
		this.packageManager = packageManager;
		this.selectedAppList = selectedAppList;
		itemChecked = new boolean[packageList.size()];
	}

	private class ViewHolder {
		TextView apkName;
		CheckBox ck1;
	}

	public int getCount() {
		return packageList.size();
	}

	public Object getItem(int position) {
		return packageList.get(position);
	}

	public long getItemId(int position) {
		return 0;
	}

	@SuppressLint("InflateParams")
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		final ViewHolder holder;
		
		LayoutInflater inflater = context.getLayoutInflater();

		if (convertView == null) {
			convertView = inflater.inflate(R.layout.list_item, null);
			holder = new ViewHolder();

			holder.apkName = (TextView) convertView
					.findViewById(R.id.textView1);
			holder.ck1 = (CheckBox) convertView
					.findViewById(R.id.checkBox1);

			convertView.setTag(holder);
			// holder.ck1.setTag(packageList.get(position));

		} else {
			
			holder = (ViewHolder) convertView.getTag();
		}
		// ViewHolder holder = (ViewHolder) convertView.getTag();
		ResolveInfo packageInfo =  (ResolveInfo) getItem(position);

		Drawable appIcon = packageInfo.activityInfo.loadIcon(packageManager);
		final String appName = (String) packageInfo.activityInfo.loadLabel(packageManager);
		final String packageName = packageInfo.activityInfo.packageName;
		appIcon.setBounds(0, 0, 40, 40);
		holder.apkName.setCompoundDrawables(appIcon, null, null, null);
		holder.apkName.setCompoundDrawablePadding(15);
		holder.apkName.setText(appName);
	//	holder.ck1.setChecked(false);

	    final AppDetail ad = new AppDetail();
	    
	    if(listPackage(packageName).equalsIgnoreCase(packageName))
	     {
			holder.ck1.setChecked(true);
		 } else if(!listPackage(packageName).equalsIgnoreCase(packageName))
		 {
			holder.ck1.setChecked(false);
		 }
/*		DatabaseHandler db = new DatabaseHandler(context);
        if(db.searchPackageName(packageName).equalsIgnoreCase(packageName))
        {
        	System.out.println("Checked");
			holder.ck1.setChecked(true);
			if(selectedAppList.size()==0)
			{
		    	System.out.println("list is : "+selectedAppList.size());
		        ad.setAppName(appName);
				ad.setPackageName(packageName);
				ad.setSelected(true);
				selectedAppList.add(ad);
				holder.ck1.setChecked(true);
	    	} else if(selectedAppList.size()!= 0)
	    	{
	    		if (listPackage(packageName).equalsIgnoreCase(packageName))
	    		{
	    			System.out.println("Already Added in list");
	    		} else if (!listPackage(packageName).equalsIgnoreCase(packageName))
	    		{
	    			System.out.println("Not Added Add" + selectedAppList.toString());
	    			System.out.println("list is : "+selectedAppList.size());
			        ad.setAppName(appName);
					ad.setPackageName(packageName);
					ad.setSelected(true);
					selectedAppList.add(ad);
					holder.ck1.setChecked(true);
	    			
	    		} 
	    	}
			
			
        } else if(!db.searchPackageName(packageName).equalsIgnoreCase(packageName))
        {
        	System.out.println("Unchecked");
			holder.ck1.setChecked(false);
			
			if (listPackage(packageName).equalsIgnoreCase(packageName))
    		{
		    	System.out.println("checked");
				holder.ck1.setChecked(true);
				} else if (!listPackage(packageName).equalsIgnoreCase(packageName))
    		{
			    	System.out.println("Unchecked");
					holder.ck1.setChecked(false);
    			
    		}
        }
        */
    
/*		DatabaseHandler db = new DatabaseHandler(context);
        if(db.searchPackageName(packageName)>0)
        {
        	System.out.println("Checked");
	        ad.setAppName(appName);
			ad.setPackageName(packageName);
			ad.setSelected(true);
			selectedAppList.add(ad);
			holder.ck1.setChecked(true);
        }else  if(db.searchPackageName(packageName) <= 0)
        {
        	System.out.println("Unchecked");
			holder.ck1.setChecked(false);
        }

	    DatabaseHandler db = new DatabaseHandler(context);    
	   if (packageName.equalsIgnoreCase(db.searchPackageName(packageName)))  //EQUAL  
	   {
    
           if(selectedAppList.size() == 0)
           {
            
            System.out.println("i m in checked");
   	        ad.setAppName(appName);
   			ad.setPackageName(packageName);
   			ad.setSelected(true);
   			selectedAppList.add(ad);
   			holder.ck1.setChecked(true);
            
         } else if (selectedAppList.size() > 0)
         {	
			
	
            if (listPackage(packageName).equalsIgnoreCase(db.searchPackageName(packageName)))
            { 
            	System.out.println("package is found ");
            	
            } else if (!listPackage(packageName).equalsIgnoreCase(db.searchPackageName(packageName)))
            {
                System.out.println("package is not found ");
       	        ad.setAppName(appName);
       			ad.setPackageName(packageName);
       			ad.setSelected(true);
       			selectedAppList.add(ad);
       			holder.ck1.setChecked(true);
    	    }
				
				
	    	    if(selectedAppList.get(i).getPackageName().equalsIgnoreCase(packageName)) 
	    	    {
	    	    	System.out.println("package is found");
	    	    }
	    	    if(!selectedAppList.get(i).getPackageName().equalsIgnoreCase(packageName)) 
	    	    {
	                System.out.println("package is not found ");
	       	        ad.setAppName(appName);
	       			ad.setPackageName(packageName);
	       			ad.setSelected(true);
	       			selectedAppList.add(ad);
	       			holder.ck1.setChecked(true);
	    	    	
	    	    } 
         }
		   
	   }else if (!packageName.equalsIgnoreCase(db.searchPackageName(packageName))) //NOT EQUAL
	   {   
		   
			if (itemChecked[position]){
				System.out.println("Checked");
				holder.ck1.setChecked(true);}
			else
				{System.out.println("Unchecked");
				holder.ck1.setChecked(false);}
		   
	   }
	    
		DatabaseHandler db = new DatabaseHandler(context);
		List<AppList> selected = db.getAllSelectedApp();
		System.out.println("size :" + selected.size());
		for (int i = 0; i < selected.size(); i++) {
			try {
				
					if(selected.get(i).get_projectname().equalsIgnoreCase(packageName))
					{	
						System.out.println("Checked");
						holder.ck1.setChecked(true);
						break;
				    }else if(!selected.get(i).get_projectname().equalsIgnoreCase(packageName))
					{
							System.out.println("Unchecked");
							holder.ck1.setChecked(false);
							break;
					}
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		 }
		
		
		
		if (itemChecked[position]){
			System.out.println("Checked");
			holder.ck1.setChecked(true);}
		else
			{System.out.println("Unchecked");
			holder.ck1.setChecked(false);}

		
	*/	
		
		holder.ck1.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if (holder.ck1.isChecked()){
			
					System.out.println("hello");
			        System.out.println(appName);
			        System.out.println(packageName);
			        System.out.println(true);
			        ad.setAppName(appName);
					ad.setPackageName(packageName);
					ad.setSelected(true);
					selectedAppList.add(ad);
					System.out.println("selected :" + selectedAppList);
					itemChecked[position] = true;}
				else if (!holder.ck1.isChecked())
					{
					System.out.println("hi");
			        System.out.println("selected :" + selectedAppList);
					for (int i = 0; i < selectedAppList.size(); i++) {
				    	    if(selectedAppList.get(i).getPackageName().equalsIgnoreCase(packageName)) 
				    	    {  selectedAppList.remove(i);}
				        }
					itemChecked[position] = false;}
			}
		});

		return convertView;

	}
	
	public String listPackage(String pkg)
	{   String p = "";
		for (int i = 0; i < selectedAppList.size(); i++) {
    	    if(selectedAppList.get(i).getPackageName().equalsIgnoreCase(pkg)) 
    	    { 
    		p= selectedAppList.get(i).getPackageName();
    	    }
        }
		System.out.println("selected package :" +p);
		return p;
	}
	
	

}
