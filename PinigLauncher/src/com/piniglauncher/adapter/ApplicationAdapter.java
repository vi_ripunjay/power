package com.piniglauncher.adapter;

import java.util.ArrayList;



import java.util.List;

import com.piniglauncher.R;
import com.piniglauncher.model.AppDetail;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;

import android.widget.TextView;


@SuppressLint("InflateParams")
public class ApplicationAdapter extends ArrayAdapter<ApplicationInfo> {
	private List<ApplicationInfo> appsList = null;
	private Context context;
	private PackageManager packageManager;
	
	 public ArrayList<AppDetail> AppList = new ArrayList<AppDetail>();
	
	

	public ApplicationAdapter(Context context, int textViewResourceId,
			List<ApplicationInfo> appsList) {
		super(context, textViewResourceId, appsList);
		this.context = context;
		this.appsList = appsList;
		packageManager = context.getPackageManager();
	}

	private class ViewHolder {
		TextView appName;
		TextView packageName;
		ImageView iconview;
		CheckBox checkBox;
    }
	
	@Override
	public int getCount() {
		return ((null != appsList) ? appsList.size() : 0);
	}

	@Override
	public ApplicationInfo getItem(int position) {
		return ((null != appsList) ? appsList.get(position) : null);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertview, ViewGroup parent) {
		View view = convertview;
		Log.v("ConvertView", String.valueOf(position));
		if (null == convertview) {
			LayoutInflater layoutInflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertview = layoutInflater.inflate(R.layout.snippet_list_row, null);
			final ViewHolder holder = (ViewHolder) view.getTag();
            holder.appName = (TextView) view.findViewById(R.id.app_name);
			holder.packageName = (TextView) view.findViewById(R.id.app_paackage);
			holder.iconview = (ImageView) view.findViewById(R.id.app_icon);
			holder.checkBox = (CheckBox) view.findViewById( R.id.checkBox1 ); 
			
			holder.checkBox
            .setOnCheckedChangeListener(
                new CompoundButton.OnCheckedChangeListener() {

                @Override
                public void onCheckedChanged(CompoundButton buttonView,
                        boolean isChecked) {
                    AppDetail element = (AppDetail) holder.checkBox
                            .getTag();
                    element.setSelected(buttonView.isChecked());
                }
            });
        view.setTag(holder);
        holder.checkBox.setTag(appsList.get(position));

		}
		else
		{
			view = convertview;
		    ((ViewHolder) view.getTag()).checkBox.setTag(appsList.get(position));
		}
		ViewHolder holder = (ViewHolder) view.getTag();
		holder.appName.setText(appsList.get(position).loadLabel(packageManager));
		holder.packageName.setText(appsList.get(position).packageName);
		holder.iconview.setImageDrawable(appsList.get(position).loadIcon(packageManager));
/*		ApplicationInfo applicationInfo = appsList.get(position);
		if (null != applicationInfo) {
			
			holder = new ViewHolder();
			
			holder.appName = (TextView) convertview.findViewById(R.id.app_name);
			holder.packageName = (TextView) convertview.findViewById(R.id.app_paackage);
			holder.iconview = (ImageView) convertview.findViewById(R.id.app_icon);
			holder.checkBox = (CheckBox) convertview.findViewById( R.id.checkBox1 ); 
		    convertview.setTag(holder);
		    
			holder.appName.setText(applicationInfo.loadLabel(packageManager));
			holder.packageName.setText(applicationInfo.packageName);
			holder.iconview.setImageDrawable(applicationInfo.loadIcon(packageManager));
			
		    AppDetail ab = new AppDetail();  
		    ab.setAppName(applicationInfo.loadLabel(packageManager).toString());
		    ab.setPackageName(applicationInfo.packageName);
		    
			holder.checkBox.setOnClickListener( new View.OnClickListener() {  
			     public void onClick(View v) {  
			      CheckBox cb = (CheckBox) v ;
			      AppDetail appdetails = new AppDetail();
				  ApplicationInfo applicationInfo = appsList.get(position);
			      Toast.makeText(context,
			       "Clicked on Checkbox: " + applicationInfo.loadLabel(packageManager) +
			       " is " + cb.isChecked(), 
			       Toast.LENGTH_LONG).show();
			      appdetails.setAppName(applicationInfo.loadLabel(packageManager).toString());
			      appdetails.setPackageName(applicationInfo.packageName);
			      appdetails.setSelected(cb.isChecked());
			   		if (cb.isChecked())
			   		{
			   		    AppList.add(appdetails);
					}
			   		else
			   		{
			      for (int i = 0; i < AppList.size(); i++) {
			    	    if(AppList.get(i).getPackageName().equalsIgnoreCase(applicationInfo.packageName)) 
			    	    {  AppList.remove(i);}
			        }
			   		}  // if end
			     }  
			    });
		}
*/		return convertview;
	}
	
}


	  