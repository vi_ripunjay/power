package com.piniglauncher;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;


public class SharePref {
	
	public SharePref()
	{}
	
	public static final String setApp= "All";
	public static final String setwall= "";
	public static final String setTag= "first";
	public static final String setpass= "123456";
	public static final String setFragment= "01";

	   public static String getId(Context context)
		{
			return ((SharedPreferences)PreferenceManager.getDefaultSharedPreferences(context)).getString(setApp, setApp);
		}
		
		public static void setId(Context applicationContext,
				String stringconvert) {
			// TODO Auto-generated method stub
			SharedPreferences msharedPreferences = (SharedPreferences)PreferenceManager.getDefaultSharedPreferences(applicationContext);
			Editor edit = msharedPreferences.edit();
			edit.putString(setApp,stringconvert);
			edit.commit();
			
		}

		   public static String getFragment(Context context)
			{
				return ((SharedPreferences)PreferenceManager.getDefaultSharedPreferences(context)).getString(setFragment, setFragment);
			}
			
			public static void setFragment(Context applicationContext,
					String stringconvert) {
				// TODO Auto-generated method stub
				SharedPreferences msharedPreferences = (SharedPreferences)PreferenceManager.getDefaultSharedPreferences(applicationContext);
				Editor edit = msharedPreferences.edit();
				edit.putString(setFragment,stringconvert);
				edit.commit();
				
			}


		
		   public static String getTag(Context context)
			{
				return ((SharedPreferences)PreferenceManager.getDefaultSharedPreferences(context)).getString(setTag, setTag);
			}
			
			public static void setTag(Context applicationContext,
					String stringconvert) {
				// TODO Auto-generated method stub
				SharedPreferences msharedPreferences = (SharedPreferences)PreferenceManager.getDefaultSharedPreferences(applicationContext);
				Editor edit = msharedPreferences.edit();
				edit.putString(setTag,stringconvert);
				edit.commit();
				
			}

		
		   public static String getWall(Context context)
			{
				return ((SharedPreferences)PreferenceManager.getDefaultSharedPreferences(context)).getString(setwall, setwall);
			}
			
			public static void setWall(Context applicationContext,
					String stringconvert) {
				// TODO Auto-generated method stub
				SharedPreferences msharedPreferences = (SharedPreferences)PreferenceManager.getDefaultSharedPreferences(applicationContext);
				Editor edit = msharedPreferences.edit();
				edit.putString(setwall,stringconvert);
				edit.commit();
				
			}

			
			   public static String getAdminPassword(Context context)
				{
					return ((SharedPreferences)PreferenceManager.getDefaultSharedPreferences(context)).getString(setpass, null);
				}
				
				public static void setAdminPassword(Context applicationContext,
						String stringconvert) {
					// TODO Auto-generated method stub
					SharedPreferences msharedPreferences = (SharedPreferences)PreferenceManager.getDefaultSharedPreferences(applicationContext);
					Editor edit = msharedPreferences.edit();
					edit.putString(setpass,stringconvert);
					edit.commit();
					
				}

	
}
