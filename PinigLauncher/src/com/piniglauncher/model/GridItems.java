package com.piniglauncher.model;



import android.content.pm.ResolveInfo;

public class GridItems {
	 
	 public int id;
	 public ResolveInfo list;
	 
	 public GridItems(int id, ResolveInfo resolveInfo) {
	  this.id = id;
	  this.list = resolveInfo;
	 }
	}