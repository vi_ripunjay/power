package com.piniglauncher.model;

public class AppList {  
	 int _id;
	    String _projectName; 
	    public String get_projectname() {
			return _projectName;
		}
		public void set_projectname(String _projectname) {
			this._projectName = _projectname;
		}

		String _appname;  
	     public AppList(){   }  
	    public AppList(int id,String projectname, String appname){  
	        this._id = id;
	        this._projectName = projectname;
	        this._appname = appname;  
	    }  
	   
	    public AppList(String projectname, String appname){
	    	this._projectName = projectname;
	        this._appname = appname;  
	    }  
	    public int getID(){  
	        return this._id;  
	    }  
	  
	    public void setID(int id){  
	        this._id = id;  
	    }  
	  
	    public String getAppName(){  
	        return this._appname;  
	    }  
	  
	    public void setAppName(String name){  
	        this._appname = name;  
	    }  
	}  