package com.piniglauncher.model;

public class AppDetail {

	  int id;
	  
	 public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}

	String appName = null;
	 public String packageName = null;
	 
	 boolean selected = false;

	 public AppDetail(String appName, String packageName, boolean selected) {
		  super();
		  this.appName = appName;
		  this.packageName = packageName;
		  this.selected = selected;
		 }
		  
	 
	public AppDetail() {
		// TODO Auto-generated constructor stub
	}


	public String getAppName() {
		return appName;
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}

	public String getPackageName() {
		return packageName;
	}

	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

	public boolean isSelected() {
		return selected;
	}

	public void setSelected(boolean selected) {
		this.selected = selected;
	}
}
