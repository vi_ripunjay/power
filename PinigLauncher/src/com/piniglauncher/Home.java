package com.piniglauncher;

import java.util.ArrayList;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.ActionBar;
import android.content.res.Configuration;
import android.content.res.Resources.NotFoundException;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.piniglauncher.adapter.NavDrawerListAdapter;
import com.piniglauncher.fragment.AdminFragment;
import com.piniglauncher.fragment.CallPinig;
import com.piniglauncher.fragment.Help;
import com.piniglauncher.fragment.MainFragment;
import com.piniglauncher.fragment.PowerPlusFragment;
import com.piniglauncher.fragment.TrainingPlusFragment;
import com.piniglauncher.model.NavDrawerItem;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
@SuppressLint("NewApi")
public class Home extends ActionBarActivity {

	private DrawerLayout mDrawerLayout;
	private ListView mDrawerList;
	private ActionBarDrawerToggle mDrawerToggle;

	// nav drawer title
	private CharSequence mDrawerTitle;
	// used to store app title
	private CharSequence mTitle;

	// slide menu items
	private String[] navMenuTitles;

	private TypedArray navMenuIcons;

	private ArrayList<NavDrawerItem> navDrawerItems;
	private NavDrawerListAdapter adapter;

	// static Controller aController;

	/*
	 * static // Asyntask AsyncTask<Void, Void, Void> mRegisterTask;
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		try {

			ActionBar ab = getActionBar();
			ColorDrawable colorDrawable = new ColorDrawable(
					Color.parseColor("#009bff"));
			ab.setBackgroundDrawable(colorDrawable);
			// ab.setDisplayShowHomeEnabled(false);

			ab.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM
					| ActionBar.DISPLAY_SHOW_HOME
					| ActionBar.DISPLAY_SHOW_TITLE);
			View homeIcon = findViewById(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB ? android.R.id.home
					: android.support.v7.appcompat.R.id.home);
			((View) homeIcon.getParent()).setVisibility(View.GONE);
			((View) homeIcon).setVisibility(View.GONE);
			setContentView(R.layout.activity_homee);
			
			/**
			 * below code for hide the icon.
			 */
			ab.setIcon(new ColorDrawable(getResources().getColor(android.R.color.transparent)));
			/*
			 * getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON
			 * );
			 */
			mTitle = mDrawerTitle = getTitle();
			// aController = new Controller();
			/*
			 * registerReceiver(mHandleMessageReceiver, new IntentFilter(
			 * Config.DISPLAY_REGISTRATION_MESSAGE_ACTION));
			 */
			navMenuIcons = getResources().obtainTypedArray(
					R.array.nav_drawer_icons);
			// load slide menu items
			navMenuTitles = getResources().getStringArray(
					R.array.nav_drawer_items);

			// nav drawer icons from resources

			mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
			mDrawerList = (ListView) findViewById(R.id.list_slidermenu);

			navDrawerItems = new ArrayList<NavDrawerItem>();

			// adding nav drawer items to array
			// Home
			navDrawerItems.add(new NavDrawerItem(navMenuTitles[0], navMenuIcons
					.getResourceId(0, -1)));
			// Find People
			navDrawerItems.add(new NavDrawerItem(navMenuTitles[1], navMenuIcons
					.getResourceId(1, -1)));
			// Photos
			navDrawerItems.add(new NavDrawerItem(navMenuTitles[2], navMenuIcons
					.getResourceId(2, -1)));
			// Communities, Will add a counter here
			navDrawerItems.add(new NavDrawerItem(navMenuTitles[3], navMenuIcons
					.getResourceId(3, -1)));
			// Pages
			navDrawerItems.add(new NavDrawerItem(navMenuTitles[4], navMenuIcons
					.getResourceId(4, -1)));

			navDrawerItems.add(new NavDrawerItem(navMenuTitles[5], navMenuIcons
					.getResourceId(5, -1)));

			/*
			 * navDrawerItems.add(new NavDrawerItem(navMenuTitles[6],
			 * navMenuIcons.getResourceId(6, -1)));
			 */
			// navDrawerItems.add(new NavDrawerItem(navMenuTitles[7]/*,
			// navMenuIcons.getResourceId(4, -1))*/));

			// navDrawerItems.add(new NavDrawerItem(navMenuTitles[5],
			// navMenuIcons.getResourceId(5, -1), true, "50+"));

			/*
			 * navDrawerItems.add(new NavDrawerItem(navMenuTitles[5],
			 * navMenuIcons.getResourceId(5, -1)));
			 * 
			 * navDrawerItems.add(new NavDrawerItem(navMenuTitles[6],
			 * navMenuIcons.getResourceId(6, -1)));
			 * 
			 * navDrawerItems.add(new NavDrawerItem(navMenuTitles[7],
			 * navMenuIcons.getResourceId(7, -1)));
			 * 
			 * navDrawerItems.add(new NavDrawerItem(navMenuTitles[8],
			 * navMenuIcons.getResourceId(8, -1)));
			 */

			// Recycle the typed array
			// navMenuIcons.recycle();

			mDrawerList.setOnItemClickListener(new SlideMenuClickListener());

			// setting the nav drawer list adapter
			adapter = new NavDrawerListAdapter(getApplicationContext(),
					navDrawerItems);
			mDrawerList.setAdapter(adapter);

			// enabling action bar app icon and behaving it as toggle button
			getActionBar().setDisplayHomeAsUpEnabled(true);
			getActionBar().setHomeButtonEnabled(true);

			mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
					R.drawable.ic_drawer, // nav menu toggle icon
					R.string.app_name, // nav drawer open - description for
										// accessibility
					R.string.app_name // nav drawer close - description for
										// accessibility
			) {
				public void onDrawerClosed(View view) {
					getActionBar().setTitle(mTitle);
					// calling onPrepareOptionsMenu() to show action bar icons
					invalidateOptionsMenu();
				}

				public void onDrawerOpened(View drawerView) {
					getActionBar().setTitle(mDrawerTitle);
					// calling onPrepareOptionsMenu() to hide action bar icons
					invalidateOptionsMenu();
				}
			};
			mDrawerLayout.setDrawerListener(mDrawerToggle);

			if (savedInstanceState == null) {
				// on first time display view for first nav item
				displayView(0);
			}
		} catch (NotFoundException e) {
			// TODO Auto-generated catch block
			L.fe(getApplicationContext(), "Home ", e);
		}

	}

	/**
	 * Slide menu item click listener
	 * */
	private class SlideMenuClickListener implements
			ListView.OnItemClickListener {
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			// display view for selected nav drawer item
			displayView(position);
		}
	}

	@Override
	public void setTitle(CharSequence title) {
		mTitle = title;
		getActionBar().setTitle(mTitle);
	}

	@SuppressLint("NewApi")
	public void displayView(int position) {
		// update the main content by replacing fragments
		Fragment fragment = null;
		switch (position) {
		case 0:
			fragment = new MainFragment();
			break;
		case 1:
			fragment = new PowerPlusFragment();
			break;
		case 2:
			fragment = new TrainingPlusFragment();
			break;
		case 3:
			fragment = new AdminFragment();
			break;
		case 4:
			fragment = new CallPinig();
			break;
		case 5:
			fragment = new Help();
			break;
		/*
		 * case 6: fragment = new MainFragment(); break;
		 */
		default:
			break;
		}

		if (fragment != null) {
			try {
				// set to handle back Button
				SharePref.setFragment(getApplicationContext(), "1");
				FragmentManager fragmentManager = getSupportFragmentManager();

				while (fragmentManager.getBackStackEntryCount() > 1) {
					fragmentManager.popBackStackImmediate();
				}

				fragmentManager.beginTransaction()
						.replace(R.id.frame_container, fragment)
						.addToBackStack(null).commitAllowingStateLoss();

				// update selected item and title, then close the drawer
				mDrawerList.setItemChecked(position, true);
				mDrawerList.setSelection(position);
				// setTitle(navMenuTitles[position]);
				setTitle(navMenuTitles[0]);

				mDrawerLayout.closeDrawer(mDrawerList);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				L.fe(getApplicationContext(), "Home Fragment : ", e);

			}
		} else {
			// error in creating fragment
			Log.e("MainActivity", "Error in creating fragment");
		}
	}

	/*
	 * @Override public boolean onCreateOptionsMenu(Menu menu) { // Inflate the
	 * menu; this adds items to the action bar if it is present.
	 * getMenuInflater().inflate(R.menu.home, menu); return true; }
	 */

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		if (mDrawerToggle.onOptionsItemSelected(item)) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		Log.i("On Resume : ", "I am In OnResume");

		/*
		 * try { displayView(0); } catch (Exception e) { // TODO Auto-generated
		 * catch block L.fe(getApplicationContext(), "Home On Resume : ", e);
		 * 
		 * }
		 */

	}

	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		if (mDrawerLayout.isDrawerOpen(mDrawerList)) {
			mDrawerLayout.closeDrawers();
		}
		if (SharePref.getFragment(getApplicationContext()).equalsIgnoreCase(
				"01")) {

		} else {
			displayView(0);
		}

	}

	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		super.onPostCreate(savedInstanceState);
		// Sync the toggle state after onRestoreInstanceState has occurred.
		mDrawerToggle.syncState();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		// Pass any configuration change to the drawer toggls
		mDrawerToggle.onConfigurationChanged(newConfig);
	}

	/*
	 * // Create a broadcast receiver to get message and show on screen private
	 * final BroadcastReceiver mHandleMessageReceiver = new BroadcastReceiver()
	 * {
	 * 
	 * @Override public void onReceive(Context context, Intent intent) {
	 * 
	 * String newMessage = intent.getExtras().getString(Config.EXTRA_MESSAGE);
	 * 
	 * // Waking up mobile if it is sleeping //
	 * aController.acquireWakeLock(context); // Display message on the screen //
	 * lblMessage.append(newMessage + "\n");
	 * 
	 * Toast.makeText(context, "Got Message: " + newMessage,
	 * Toast.LENGTH_LONG).show();
	 * 
	 * // Releasing wake lock aController.releaseWakeLock(); } };
	 */
	protected void onStop() {
		super.onStop();
		Log.i("On Stop : ", "I am In OnStop");
		L.fv("I am In OnStop ");
	};

	@Override
	protected void onDestroy() {

		super.onDestroy();
		Log.i("On Destroy : ", "I am In Destroy");
		L.fv("I am In OnDestroy ");

		/*
		 * // Cancel AsyncTask if (mRegisterTask != null) {
		 * mRegisterTask.cancel(true); } try { // Unregister Broadcast Receiver
		 * unregisterReceiver(mHandleMessageReceiver);
		 * 
		 * //Clear internal resources.
		 * 
		 * GCMRegistrar.onDestroy(this);
		 * 
		 * } catch (Exception e) { Log.e("UnRegister Receiver Error", "> " +
		 * e.getMessage()); L.fe(getApplicationContext(), "Home On Destroy :",
		 * e.getMessage());
		 * 
		 * }
		 */}

}
