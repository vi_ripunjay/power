package com.piniglauncher.reciver;


import com.piniglauncher.MyService;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class OnBootupStart extends BroadcastReceiver {
	@Override
	public void onReceive(Context context, Intent intent) {

		if(intent.getAction().equalsIgnoreCase(Intent.ACTION_BOOT_COMPLETED))
		   {
			 System.out.println("-----------------------------------------------------------------------------");
		     //here we start the service             
		     Intent serviceIntent = new Intent(context, MyService.class);
		     context.startService(serviceIntent);
		   }
		

	}
}
