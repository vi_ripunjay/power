package com.piniglauncher.fragment;

import com.piniglauncher.R;


import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;



@TargetApi(Build.VERSION_CODES.HONEYCOMB)
@SuppressLint("NewApi")
public class AdminFragment extends Fragment {
	
	public AdminFragment(){}
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
 
        View rootView = inflater.inflate(R.layout.admin_fragment, container, false);
        
        Intent intent = new Intent("applicationAdmin.intent.action.Launch");
        startActivity(intent);
     //    launchApp("com.arknfcpower");
        return rootView;
    }
	
	
	protected void launchApp(String packageName) {
        Intent mIntent = getActivity().getPackageManager().getLaunchIntentForPackage(
                packageName);
        if (mIntent != null) {
            try {
                startActivity(mIntent);
            } catch (ActivityNotFoundException err) {
                Toast t = Toast.makeText(getActivity(),
                        "App Not Found", Toast.LENGTH_SHORT);
                t.show();
            }
        }
 }

}
