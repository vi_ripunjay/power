package com.piniglauncher.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.Toast;

import com.piniglauncher.L;
import com.piniglauncher.R;
import com.piniglauncher.adapter.GridViewAdapter;
import com.piniglauncher.model.GridItems;
 
@SuppressLint("ValidFragment")
public class GridFragment extends Fragment {
 
 private GridView mGridView;
 private GridViewAdapter mGridAdapter;
 GridItems[] gridItems = {};
 private Activity activity;
 
 public GridFragment(GridItems[] gridItems, Activity activity) {
  this.gridItems = gridItems;
  this.activity = activity;
 }
 
 @Override
 public View onCreateView(LayoutInflater inflater, ViewGroup container,
   Bundle savedInstanceState) {
  View view;
  view = inflater.inflate(R.layout.grid, container, false);
  mGridView = (GridView) view.findViewById(R.id.gridView);
  return view;
 }
 
 @Override
 public void onActivityCreated(Bundle savedInstanceState) {
  super.onActivityCreated(savedInstanceState);
 
  if (activity != null) {
 
   mGridAdapter = new GridViewAdapter(activity, gridItems);
   if (mGridView != null) {
    mGridView.setAdapter(mGridAdapter);
   }
 
    mGridView.setOnItemClickListener(new OnItemClickListener() {
    @Override
    public void onItemClick(@SuppressWarnings("rawtypes") AdapterView parent, View view,
      int position, long id) {
      System.out.println("hey i m here ");
      onGridItemClick((GridView) parent, view, position, id);
    }
   });
  }
 }
 
 public void onGridItemClick(GridView g, View v, int position, long id) {
	 
	ActivityInfo activity=gridItems[position].list.activityInfo;
  
  ComponentName name=new ComponentName(activity.applicationInfo.packageName,
                                       activity.name);
  Intent i=new Intent(Intent.ACTION_MAIN);
  
  i.addCategory(Intent.CATEGORY_LAUNCHER);
  i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
  i.setComponent(name);
  startActivity(i); 
  
  

//launchApp(activity.applicationInfo.packageName);
 }
 
 
	protected void launchApp(String packageName) {
        Intent mIntent = getActivity().getPackageManager().getLaunchIntentForPackage(
                packageName);
        if (mIntent != null) {
            try {
                startActivity(mIntent);
            } catch (ActivityNotFoundException err) {
                Toast t = Toast.makeText(getActivity(),
                        "App Not Found", Toast.LENGTH_SHORT);
                t.show();
                L.fe(getActivity(),"GridFragment : launch App", err);
            }
        }
    }

}