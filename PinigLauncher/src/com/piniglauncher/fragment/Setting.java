package com.piniglauncher.fragment;




import android.annotation.SuppressLint;


import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.Toast;

import com.piniglauncher.AppSelection;
import com.piniglauncher.L;
import com.piniglauncher.R;
import com.piniglauncher.SharePref;

import android.view.View.OnClickListener;
@TargetApi(Build.VERSION_CODES.HONEYCOMB)
@SuppressLint("NewApi")
public class Setting extends Fragment {
	
	private Switch mySwitch;
	private Button wall;
	private Button chooseApp;
	private Button adminPass;
	
	
	private static final int SELECT_PICTURE = 1;
	private String selectedImagePath;
	Uri selectedImageUri;
	MainFragment main;
	
	
	Dialog dialog_pass;
	CheckBox cbShowPass;
	
	EditText pass_current_et, pass_new_et, pass_confirm_et;
	String pass_current, pass_new, pass_confirm;
	String getpass;
	String pass_op;
	String loadedpassword;
	
	
	public Setting(){}
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
 
        View rootView = inflater.inflate(R.layout.security_fragment, container, false);
        
        mySwitch = (Switch) rootView.findViewById(R.id.Switch);
        wall = (Button) rootView.findViewById(R.id.wall);
        chooseApp = (Button) rootView.findViewById(R.id.chooseapp);
        adminPass = (Button) rootView.findViewById(R.id.adminpass);
         
      //set the switch to ON 
        if(SharePref.getId(getActivity()).equalsIgnoreCase("All"))
        {
        mySwitch.setChecked(false);
        }if(!SharePref.getId(getActivity()).equalsIgnoreCase("All"))
        {
            mySwitch.setChecked(true);
        	
        }
        //attach a listener to check for changes in state
        mySwitch.setOnCheckedChangeListener(new OnCheckedChangeListener() {
       
         @Override
         public void onCheckedChanged(CompoundButton buttonView,
           boolean isChecked) {
       
          if(isChecked){
        	showInputDialog();
           Toast.makeText(getActivity(), "ON", Toast.LENGTH_LONG).show();
           }
          else{
        	  showInputDialog();
              Toast.makeText(getActivity(), "Off", Toast.LENGTH_LONG).show();
          }
       
         }
        });
        
        wall.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent();
				intent.setType("image/*");
	            intent.setAction(Intent.ACTION_GET_CONTENT);
	            startActivityForResult(Intent.createChooser(intent,"Select Picture"), SELECT_PICTURE);
			}
		});
        
        chooseApp.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				showInputDialogForApp();
			}
		});
        
        adminPass.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				changePasswordDialog();
			}
		});
        
        return rootView;
    }
	
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_PICTURE) {
                try {
					selectedImageUri = data.getData();
					selectedImagePath = getPath(selectedImageUri);
					SharePref.setWall(getActivity(), selectedImagePath);
					Toast.makeText(getActivity(), "wallpaper Changed", Toast.LENGTH_LONG).show();
					System.out.println("Image Path : " + selectedImagePath);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					L.fe(getActivity(), "Setting : OnActivityResult :", e);
					e.printStackTrace();
				}
                
            }
        }
    }
	
    public String getPath(Uri uri) {
    	
        String[] projection = { MediaStore.Images.Media.DATA };
        Cursor cursor = getActivity().getContentResolver().query(uri,projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }
	
    
    public void changePasswordDialog()
    {
    	
    	
		dialog_pass = new Dialog(getActivity());
		dialog_pass.setContentView(R.layout.change_password);
		dialog_pass.setTitle("Set password");
		dialog_pass.setCancelable(true);
	    	  	
		pass_current_et = (EditText) dialog_pass.findViewById(R.id.EditText_PasswordCur);
		pass_current_et.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
		pass_new_et = (EditText) dialog_pass.findViewById(R.id.EditText_PasswordNew);
		pass_new_et.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
		pass_confirm_et = (EditText) dialog_pass.findViewById(R.id.EditText_PasswordConfirm);
		pass_confirm_et.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
	
		cbShowPass = (CheckBox)dialog_pass.findViewById(R.id.cb_password);
				
   		cbShowPass.setOnCheckedChangeListener(new OnCheckedChangeListener() {
    					
    	   	@Override 
    	   	public void onCheckedChanged(CompoundButton buttonView,	boolean isChecked) { 
    		        		
    			if (buttonView.isChecked()) { 
    				pass_current_et.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
    				pass_new_et.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
    				pass_confirm_et.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
    	   		}
    	   		else
    	   		{
    	   			pass_current_et.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
    	   			pass_new_et.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
    	   			pass_confirm_et.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
    	   		}
    						
				}
    		});
		    			
	    Button btn_SetPassword = (Button) dialog_pass.findViewById(R.id.btn_savePass);
	    btn_SetPassword.setOnClickListener(new View.OnClickListener() {
		
		@Override
		public void onClick(View v) {
			pass_current = pass_current_et.getText().toString();
			pass_new = pass_new_et.getText().toString();
			pass_confirm = pass_confirm_et.getText().toString();
		//	DatabaseHandler db = new  DatabaseHandler(getActivity());
				if(pass_current.equals(""))
				{
					pass_current_et.setError("Field can't be blank");
				}else if(pass_current.equals(""))
				{
					pass_current_et.setError("Field can't be blank");
				}else if(pass_current.equals(""))
				{
					pass_current_et.setError("Field can't be blank");
				}
		//	if(pass_current.equals(db.getAdminPassword()))
				
				if(pass_current.equals(SharePref.getAdminPassword(getActivity())))
					
			{
                   
				if((pass_new.length() > 4)&&( pass_new.length() > 4) )
				{
					
				   if(pass_new.equals(pass_confirm))
                   {
    
					   
				/*	     User us = new User();
					     us.setUser("admin");
		               	 us.setPass(pass_confirm);
		               	 db.UpdateAdminPassword(us);
              */ 
					   SharePref.setAdminPassword(getActivity(), pass_confirm);
					   
		               	 Toast.makeText(getActivity(), "Successfully Updated", Toast.LENGTH_LONG).show();
                	   dialog_pass.dismiss();
                	   
                	   
                   }
                   else if (!pass_new.equals(pass_confirm))
                   {

                	    pass_new_et.setError("Password do not match");
    					pass_confirm_et.setError("Password do not match");

                   }
				   
				}
				else
				{
					messageDialog();
				}
				
			}
				
		//	else if(!pass_current.equals(db.getAdminPassword()))
				
				else if(!pass_current.equals(SharePref.getAdminPassword(getActivity())))
							
			{
				pass_current_et.setError("Incorrect Password");
				
			}
		}
	    });
	    	  
	    Button btn_CancelPassword = (Button) dialog_pass.findViewById(R.id.btn_cancelPass);
	    btn_CancelPassword.setOnClickListener(new View.OnClickListener() {
			@Override
				public void onClick(View v) {
					dialog_pass.dismiss();
	        }
	    	});
		dialog_pass.show();

    	
    	
    }
    
    
    @SuppressWarnings("deprecation")
	public void messageDialog()
    {
    	AlertDialog alertDialog = new AlertDialog.Builder(
                    getActivity()).create();

// Setting Dialog Title
alertDialog.setTitle("Password Correction ");

// Setting Dialog Message
alertDialog.setMessage("Password Should be Greater then 4 digit ");

// Setting Icon to Dialog
//alertDialog.setIcon(R.drawable.tick);

// Setting OK Button
alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
        public void onClick(DialogInterface dialog, int which) {
        // Write your code here to execute after dialog closed
        	dialog.dismiss();
        }
});

// Showing Alert Message
alertDialog.show();
    }
    
    
	@SuppressLint("InflateParams")
	protected void showInputDialogForApp() {

		// get prompts.xml view
		LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
		View promptView = layoutInflater.inflate(R.layout.activity_admin_authorization, null);
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
		alertDialogBuilder.setView(promptView);

		final EditText editText = (EditText) promptView.findViewById(R.id.edittext);
		// setup a dialog window
		alertDialogBuilder.setCancelable(false)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
				//		 DatabaseHandler db = new DatabaseHandler(getActivity());
						 String password  = editText.getText().toString();
						 
			//			 if (db.getUser("admin").equalsIgnoreCase(password))
							 if (SharePref.getAdminPassword(getActivity()).equalsIgnoreCase(password))

							{
								Intent i = new Intent(getActivity(), AppSelection.class);
								startActivity(i);
								dialog.dismiss();
							}
							else
							{
								Toast.makeText(getActivity(), "Wrong Password", Toast.LENGTH_LONG).show();
								dialog.dismiss();
									
						    }
                         //  db.close();
					}
				})
				.setNegativeButton("Cancel",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
								dialog.cancel();
							}
						});

		// create an alert dialog
		AlertDialog alert = alertDialogBuilder.create();
		alert.show();
	}
	
	
	@SuppressLint("InflateParams")
	protected void showInputDialog() {

		// get prompts.xml view
		LayoutInflater layoutInflater = LayoutInflater.from(getActivity());
		View promptView = layoutInflater.inflate(R.layout.activity_admin_authorization, null);
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
		alertDialogBuilder.setView(promptView);

		final EditText editText = (EditText) promptView.findViewById(R.id.edittext);
		// setup a dialog window
		alertDialogBuilder.setCancelable(false)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {
					//	 DatabaseHandler db = new DatabaseHandler(getActivity());
						 String password  = editText.getText().toString();
						 int flag = 0;
					//	 if (db.getUser("admin").equalsIgnoreCase(password))
							 if (SharePref.getAdminPassword(getActivity()).equalsIgnoreCase(password))

						 {

						        if(SharePref.getId(getActivity()).equalsIgnoreCase("Selected"))
						        {
		                            SharePref.setId(getActivity(), "All");	
                                    flag = 1;
		                            mySwitch.setChecked(false);
		                            Toast.makeText(getActivity(), "Security Mode Off", Toast.LENGTH_LONG).show();
		                           

						        } if(SharePref.getId(getActivity()).equalsIgnoreCase("All")&&flag ==0)
						        {
		                            SharePref.setId(getActivity(), "Selected");	
						        	mySwitch.setChecked(true);
		                            Toast.makeText(getActivity(), "Security Mode On", Toast.LENGTH_LONG).show();
						        	
						        }

                			}
							else // if (!db.getUser("admin").equalsIgnoreCase(password))
								 if (!SharePref.getAdminPassword(getActivity()).equalsIgnoreCase(password))

							{
						        if(SharePref.getId(getActivity()).equalsIgnoreCase("Selected"))
						        {
                                    flag = 1;
						        	mySwitch.setChecked(true);
		                            Toast.makeText(getActivity(), "Security Mode Off", Toast.LENGTH_LONG).show();

		                            
						        }if(!SharePref.getId(getActivity()).equalsIgnoreCase("All")&&flag == 0)
						        {
						        	mySwitch.setChecked(false);
		                            Toast.makeText(getActivity(), "Security Mode On", Toast.LENGTH_LONG).show();
						        	
						        }

						    }
           //                          db.close();
					}
				})
				.setNegativeButton("Cancel",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int id) {
							      //set the switch to ON 
						        if(SharePref.getId(getActivity()).equalsIgnoreCase("All"))
						        {
						        mySwitch.setChecked(false);
						        dialog.dismiss();
						        }if(SharePref.getId(getActivity()).equalsIgnoreCase("Selected"))
						        {
						            mySwitch.setChecked(true);
						            dialog.dismiss();
						        	
						        }
							
							}
						});

		// create an alert dialog
		AlertDialog alert = alertDialogBuilder.create();
		alert.show();
	}
	
	

}
