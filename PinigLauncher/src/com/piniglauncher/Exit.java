package com.piniglauncher;

import android.app.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
 

public class Exit extends Fragment {
	ListView lv;

	public Exit() {
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View rootView = inflater.inflate(R.layout.exit, container,
				false);
		
		getActivity().setTitle("Home");

    //    getActivity().finish();		
		
		return rootView;
	}
	
	

}
