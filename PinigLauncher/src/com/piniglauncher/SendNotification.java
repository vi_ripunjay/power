package com.piniglauncher;

import java.lang.ref.WeakReference;


import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.piniglauncher.adapter.CustomAdapter;
import com.piniglauncher.db.DatabaseHandler;
import com.piniglauncher.framework.InDeterminateRequestCallback;
import com.piniglauncher.model.DeviceData;
import com.piniglauncher.model.UserData;

import android.support.v7.app.ActionBarActivity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.view.View.OnClickListener;
public class SendNotification extends ActionBarActivity {
	EditText edit;
	Button send;
	static ProgressDialog barProgressDialog = null;
	static MessageHandler HANDLER = null;

	 static String imei ;     //send to
	 static String sendfrom ;   //send from 
	 static String regid;   //send from 
	 
		// label to display gcm messages
		TextView lblMessage;
		Controller aController;
		
		// Asyntask
		AsyncTask<Void, Void, Void> mRegisterTask;
		
		String name;
		String message;
	    String UserDeviceIMEI;
		
		/**************  Intialize Variables *************/
	    public  ArrayList<UserData> CustomListViewValuesArr = new ArrayList<UserData>();
	    TextView output = null;
	    CustomAdapter adapter;
	    SendNotification activity = null;

	 
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_send_notification);
        
		DatabaseHandler db = new DatabaseHandler(this);
		aController = (Controller) getApplicationContext();
		
	edit = (EditText) findViewById(R.id.message);
	send = (Button) findViewById(R.id.Send);
	HANDLER = new MessageHandler(this);
	lblMessage  = (TextView) findViewById(R.id.lblMessage);
	
	barProgressDialog = new ProgressDialog(SendNotification.this);

    if(lblMessage.getText().equals("")){
		
		// Register custom Broadcast receiver to show messages on activity
		registerReceiver(mHandleMessageReceiver, new IntentFilter(
				Config.DISPLAY_MESSAGE_ACTION));
	}
	
    List<UserData> data = db.getAllUserData();      
	System.out.println("Data : " + data); 
	
    for (UserData dt : data) {
        
        lblMessage.append(dt.getName()+" : "+dt.getMessage()+"\n");
    }
    
    Intent i = getIntent();
	name = i.getStringExtra("name");    // send to name
	System.out.println("name" + name );
	imei = i.getStringExtra("imeito");     //send to
	System.out.println("imei" + imei );
	sendfrom = i.getStringExtra("sendfrom");   //send from 
	System.out.println("sendfrom" + sendfrom );
	regid = i.getStringExtra("regid");   //send from 
	System.out.println("regid" + regid );
	
	//set the title of action bar
	setTitle(name.toString());
	
	send.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			
			HANDLER.sendEmptyMessage(Do.SEND_MESSAGE);
			
		}
	});
	
	}

	
	// Create a broadcast receiver to get message and show on screen 
	private final BroadcastReceiver mHandleMessageReceiver = new BroadcastReceiver() {
		
		@Override
		public void onReceive(Context context, Intent intent) {
			
			String newMessage = intent.getExtras().getString(Config.EXTRA_MESSAGE);
			String newName = intent.getExtras().getString("name");
			String newIMEI = intent.getExtras().getString("imei");
			
			Log.i("GCMBroadcast","Broadcast called."+newIMEI);
			
			// Waking up mobile if it is sleeping
			aController.acquireWakeLock(getApplicationContext());
			
			String msg = lblMessage.getText().toString();
			msg = newName+" : "+newMessage+"\n"+msg;
			// Display message on the screen
			lblMessage.setText(msg);
			//lblMessage.append("\n"+newName+" : "+newMessage);			
		
			Toast.makeText(getApplicationContext(), 
					"Got Message: " + newMessage, 
					Toast.LENGTH_LONG).show();
			
			/************************************/
			 //CustomListViewValuesArr.clear();
			 int rowCount = DBAdapter.validateNewMessageUserData(newIMEI);
			 Log.i("GCMBroadcast", "rowCount:"+rowCount);
             if(rowCount <= 1 ){
		        	final UserData schedSpinner = new UserData();
		            
		            /******* Firstly take data in model object ********/
		        	schedSpinner.setName(newName);
		        	schedSpinner.setIMEI(newIMEI);
		             
		              
		          /******** Take Model Object in ArrayList **********/
		          CustomListViewValuesArr.add(schedSpinner);
		          adapter.notifyDataSetChanged();
		          
		        }
		        
		        //CustomListViewValuesArr.addAll(SpinnerUserData);
		        
		        
			
			/************************************/
			
			// Releasing wake lock
			aController.releaseWakeLock();
		}
	};
	
	
	public static class Do {
		static final int SEND_MESSAGE = 1;
		static final int SUICIDE = -1;
	}

	
	public static class MessageHandler extends Handler {
		private final WeakReference<SendNotification> parent;

		MessageHandler(SendNotification parent) {
			this.parent = new WeakReference<SendNotification>(parent);
		}

		@SuppressWarnings("static-access")
		@Override
		public void handleMessage(Message msg) {
			final SendNotification target = parent.get();
			try {
				switch (msg.what) {
				case Do.SEND_MESSAGE: {
					final String message = target.edit.getText()
							.toString();

					final Context context = target;
					
            			     barProgressDialog.setMessage("Please Wait....");
					         barProgressDialog.setProgressStyle(barProgressDialog.STYLE_SPINNER);
					         barProgressDialog.setProgress(0);
					         barProgressDialog.setMax(20);
					         barProgressDialog.show();
					         
					         List<NameValuePair> nameValuePairs  =   new ArrayList<NameValuePair>();
					         nameValuePairs.add( new BasicNameValuePair("message", message) );
					         nameValuePairs.add( new BasicNameValuePair("imeito", imei) );
					         nameValuePairs.add( new BasicNameValuePair("sendfrom",sendfrom));
					         nameValuePairs.add( new BasicNameValuePair("RegId",regid));

					         RequestListener.onReceive(UrlConnection.Request.SEND_MESSAGE,
							 new InDeterminateRequestCallback() {
								@Override
								public void result(final boolean result) {
									target.runOnUiThread(new Runnable() {
										@Override
										public void run() {
											//
											if (result) {

												
											    DatabaseHandler db = new DatabaseHandler(context);
								//				db.addDeviceData(new DeviceData(target.txtName.getText().toString(),target.txtEmail.getText().toString(),regId, imei));
												
												db.close();

												barProgressDialog.dismiss();
										/*		UrlConnection.setPhone(target, target.tex.getText().toString());
												Intent intent = new Intent();
											    intent.setClass(target, CodeAuthentication.class);
										     	target.startActivity(intent);
											    target.overridePendingTransition(R.anim.right_in, R.anim.left_out);
											    barProgressDialog.dismiss();
											    target.finish();
								*/
											} else {
												barProgressDialog.dismiss();
											}
										}
									});
								}

								@Override
								public void result(final boolean result,
										final Object data) {
									target.runOnUiThread(new Runnable() {
										@Override
										public void run() {
											barProgressDialog.dismiss();
											
											if (result) {
												JSONObject json;
												try {
													json = new JSONObject(data.toString());
													
													JSONArray jsonArray = json.getJSONArray("result");
												    
													System.out.println("length :"+jsonArray.length()); 

													JSONObject c = jsonArray.getJSONObject(0);
									                        
													String name = c.getString("name");
									                String email = c.getString("email");
									                String imei = c.getString("imei");
									                String regid = c.getString("regid");
									                
									                System.out.println(name);
									                System.out.println(email);
									                System.out.println(imei);
									                System.out.println(regid);
									                
									                DatabaseHandler db = new DatabaseHandler(context);
									                db.addDeviceData(new DeviceData(name,email,imei,regid));
									                
									            	UrlConnection.setImei(target, imei);
									            	
									            	
									                 System.out
															.println("Succesfully Saved");
												
												} catch (JSONException e) {
													// TODO Auto-generated catch block
													e.printStackTrace();
												}
												
												
											}
									if (!result) {
												int status = (Integer) data;
												System.out
												.println("Status :" +status);
										 if (status == HttpURLConnection.HTTP_NOT_FOUND)       //404
										{
											
										}
											// Did not find device, then did not find user
										else if (status == HttpURLConnection.HTTP_FORBIDDEN)     //403
										{
											
											
										}	
							
										else if (status == HttpURLConnection.HTTP_BAD_REQUEST)   //400
								
										{
											
										}// Device found but username not correct
										else if (status == HttpURLConnection.HTTP_UNAUTHORIZED)   //401
										{
											
										}
										else if (status == HttpURLConnection.HTTP_CONFLICT)     //409
										{
											System.out
													.println("Code Not Found :");
										}								/*				FileToPrint.fe(target.cx,
												GoOn.Event.EXCEPTION,
												new Exception("Request.DEVICE_REGISTER failed."));     */
		

											}
										}
									});
								}
							}, nameValuePairs);
					break;
				}
				case -1: {
					target.finish();
					break;
				}
				}
			} catch (Exception e) {
		//		FileToPrint.fe(target.cx, GoOn.Event.EXCEPTION, e);
				//
				Log.e("Exception :", e.getMessage());
				target.setResult(Activity.RESULT_CANCELED, null);
				target.finish();
			}
		}
	}	

	
	@Override
	protected void onDestroy() {
		try {
			// Unregister Broadcast Receiver
			unregisterReceiver(mHandleMessageReceiver);
			
			
		} catch (Exception e) {
			Log.e("UnRegister Receiver Error", "> " + e.getMessage());
		}
		super.onDestroy();
	}
	
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.send_notification, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
